/*!
 * \file    main.cpp
 * \author  Remi BERTHO
 * \date    14/03/16
 * \version 1.0.0
 */

/*
 * main.cpp
 *
 * Copyright 2017 Remi BERTHO <remi.bertho@dalan.fr>
 *
 * This file is part of GDV.
 *
 * GDV is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * GDV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

#include "log.hpp"
#include <clocale>
#include <gtkmm.h>
#include <iostream>
#include "internationalization.hpp"
#include <glib/gstdio.h>

#include "config.h"
#include "DataBase/database.hpp"
#include "gdv_application.hpp"

using namespace core;
using namespace std;
using namespace Glib;
using namespace Gtk;


//
// Fin test
//

/*!
 *  Print an error message on error terminate
 *  \param log_domain the log domain of the message
 *  \param log_level the log level of the message (including the fatal and recursion flags)
 *  \param message the message to process
 *  \param user_data user data
 */
void terminateFunction([[maybe_unused]] const gchar* log_domain,
		[[maybe_unused]] GLogLevelFlags				 log_level,
		const gchar*								 message,
		[[maybe_unused]] gpointer					 user_data)
{
	cerr << message << endl;

	MessageDialog error(message, false, MESSAGE_ERROR, BUTTONS_OK, true);
	error.run();
}

/*!
 * \fn int main(int argc, char *argv[])
 *  Begin csuper.
 * \param[in] argc the number of argument.
 * \param[in] argv the array of argument.
 * \return 0 if everything is OK
 */
int main(int argc, char* argv[])
{
// Setup directory
#if (defined G_OS_WIN32) && (defined INSTALL)
	string directory = path_get_dirname(argv[0]);
	if (path_is_absolute(directory))
		g_chdir(directory.c_str());
#endif	  // G_OS_WIN32

	// Set locals
	setlocale(LC_ALL, ".UTF8");
	bindtextdomain(GDV_PROJECT_NAME, GDV_LOCALE_DIR);
	bind_textdomain_codeset(GDV_PROJECT_NAME, "UTF-8");
	textdomain(GDV_PROJECT_NAME);

	// Set name
	set_prgname(GDV_PROJECT_NAME);
	set_application_name(_("GDV"));

	// Logs
	initLog();
	GDV_DEBUG("GDV launch");

	// Set terminate function
	g_log_set_handler("glibmm", (GLogLevelFlags)(G_LOG_LEVEL_MASK | G_LOG_FLAG_FATAL | G_LOG_FLAG_RECURSION), terminateFunction, nullptr);
	g_log_set_handler("gtkmm", (GLogLevelFlags)(G_LOG_LEVEL_MASK | G_LOG_FLAG_FATAL | G_LOG_FLAG_RECURSION), terminateFunction, nullptr);

	// Create the application
	RefPtr<GdvApplication> app(new GdvApplication(argc, argv));
	// Add the icons path in resource
	IconTheme::get_default()->add_resource_path("/fr/dalan/gdv/data/icons");
	// Load the Glade file
	RefPtr<Builder> builder = Builder::create();
	try
	{
		builder->add_from_resource("/fr/dalan/gdv/UI/main_window.ui");
		builder->add_from_resource("/fr/dalan/gdv/UI/fuel_price_window.ui");
		builder->add_from_resource("/fr/dalan/gdv/UI/message_window.ui");
		builder->add_from_resource("/fr/dalan/gdv/UI/about_window.ui");
		builder->add_from_resource("/fr/dalan/gdv/UI/Views/car.ui");
		builder->add_from_resource("/fr/dalan/gdv/UI/Views/car_inspection.ui");
		builder->add_from_resource("/fr/dalan/gdv/UI/Views/car_insurance.ui");
		builder->add_from_resource("/fr/dalan/gdv/UI/Views/car_service.ui");
		builder->add_from_resource("/fr/dalan/gdv/UI/Views/car_tyre.ui");
		builder->add_from_resource("/fr/dalan/gdv/UI/Views/car_wiper.ui");
		builder->add_from_resource("/fr/dalan/gdv/UI/Views/fulltank.ui");
	}
	catch (Glib::Exception& e)
	{
		GDV_ERROR(e.what());
		return EXIT_FAILURE;
	}
	app->init(builder);

	int return_value = app->launch();
	GDV_DEBUG(ustring::compose("End with value %1", return_value));
	return return_value;
}
