/*!
 * \file
 */

/*
 * place_window.cpp
 *
 * Copyright 2016-2019 Remi BERTHO <remi.bertho@dalan.fr>
 *
 * This file is part of GDV.
 *
 * GDV is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * GDV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

#include "log.hpp"

#include "place_window.hpp"

#include "gdv_application.hpp"


using namespace Gtk;
using namespace Glib;
using namespace core;

///////////////////////////////////////////////////////////////////////////////////
/////////////////////////// Constructor ///////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////
PlaceWindow::PlaceWindow(BaseObjectType* cobject) : Window(cobject)
{
}

///////////////////////////////////////////////////////////////////////////////////
/////////////////////////// Function //////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////
void PlaceWindow::connectButtons()
{
	if (apply_button_ != nullptr)
	{
		apply_button_->signal_clicked().connect(mem_fun(*this, &PlaceWindow::onApplyClicked));
	}
	if (cancel_button_ != nullptr)
	{
		cancel_button_->signal_clicked().connect(mem_fun(*this, &PlaceWindow::onCancelClicked));
	}
}

bool PlaceWindow::internalLaunch(Window* parent_window)
{
	if (parent_window != nullptr)
		set_transient_for(*parent_window);
	show_all();

	apply_ = true;
	wait();

	return apply_;
}

///////////////////////////////////////////////////////////////////////////////////
/////////////////////////// Slots /////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////
void PlaceWindow::onApplyClicked()
{
	hide();
	go();
}

void PlaceWindow::onCancelClicked()
{
	apply_ = false;
	hide();
	go();
}
