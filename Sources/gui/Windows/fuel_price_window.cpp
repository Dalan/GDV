/*!
 * \file    fuel_price_window.cpp
 * \author  Remi BERTHO
 * \date    11/10/18
 * \version 1.0.0
 */

/*
 * fuel_price_window.cpp
 *
 * Copyright 2016-2018 Remi BERTHO <remi.bertho@dalan.fr>
 *
 * This file is part of GDV.
 *
 * GDV is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * GDV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

#include "log.hpp"

#include "internationalization.hpp"

#include "fuel_price_window.hpp"
#include "version.hpp"
#include "gdv_application.hpp"

using namespace Gtk;
using namespace Gdk;
using namespace Glib;
using namespace std;
using namespace core;
using namespace Gtk::PLplot;
using namespace database;




FuelPriceWindow::Row::Row(FuelPriceWindow& window)
{
	set_margin_start(10);
	set_margin_end(10);
	set_margin_bottom(5);
	set_margin_top(5);

	set_spacing(10);

	remove_button_.set_image_from_icon_name("list-remove-symbolic", Gtk::ICON_SIZE_MENU);
	remove_button_.set_relief(Gtk::RELIEF_NONE);
	remove_button_.signal_clicked().connect(sigc::bind<-1, Row&>(mem_fun(window, &FuelPriceWindow::onRemoveClicked), *this));
	pack_end(remove_button_, false, false);

	for (size_t int_engine = 0; int_engine < ENGINE_TYPE_NB; int_engine++)
	{
		EngineType engine = static_cast<EngineType>(int_engine);
		engine_type_.append(engineToString(engine));
	}
	engine_type_.signal_changed().connect(sigc::bind<-1, Row&>(mem_fun(window, &FuelPriceWindow::onFilterChanged), *this));
	pack_start(engine_type_);

	additive_.append(_("No additive"));
	additive_.append(_("With additive"));
	additive_.signal_changed().connect(sigc::bind<-1, Row&>(mem_fun(window, &FuelPriceWindow::onFilterChanged), *this));
	pack_start(additive_);

	cheap_.append(_("Expensive"));
	cheap_.append(_("Cheap"));
	cheap_.signal_changed().connect(sigc::bind<-1, Row&>(mem_fun(window, &FuelPriceWindow::onFilterChanged), *this));
	pack_start(cheap_);
}

PlotData2D* FuelPriceWindow::Row::getPlotData(DataBase& db)
{
	static const Date			   epoch(1, Date::JANUARY, 1970);
	static const array<ustring, 9> color_list{
			"#196B91", "#78202F", "#004926", "#D4D414", "#14A5D4", "#14D42F", "#A41299", "#E1911D", "#61474E"};

	ustring engine_str = engine_type_.get_active_text();
	if (engine_str == "")
		return nullptr;
	EngineType engine = stringToEngine(engine_str);

	ustring		   additive_str = additive_.get_active_text();
	optional<bool> additive;
	if (additive_str == _("No additive"))
		additive = false;
	else if (additive_str == _("With additive"))
		additive = true;

	ustring		   cheap_str = cheap_.get_active_text();
	optional<bool> cheap;
	if (cheap_str == _("Expensive"))
		cheap = false;
	else if (cheap_str == _("Cheap"))
		cheap = true;


	vector<double> x_value;
	vector<double> y_value;
	RGBA		   color;
	PlotData2D*	   plot_data = nullptr;
	try
	{
		std::map<Date, double> fuel_prices = db.fuelPrice<car::Car>(engine, additive, cheap);

		x_value.reserve(fuel_prices.size());
		y_value.reserve(fuel_prices.size());

		if (fuel_prices.size() < 2)
		{
			return plot_data;
		}

		for (auto& price : fuel_prices)
		{
			x_value.push_back(epoch.days_between(price.first));
			y_value.push_back(price.second);
		}
	}
	catch (core::Exception& e)
	{
		GDV_APP_SEND_ERROR(_("Cannot get fuel price"), e.what());
	}


	plot_data = manage(new PlotData2D(x_value, y_value));
	plot_data->set_name(engine_str + " " + additive_str + " " + cheap_str);
	switch (engine)
	{
	case DIESEL:
		plot_data->set_symbol("◻");
		break;
	case GAS:
		plot_data->set_symbol("⚪");
		break;
	case GPL:
		plot_data->set_symbol("⬦");
		break;
	case ELECTRICITY:
		plot_data->set_symbol("⚡");
		break;
	}
	color.set(color_list[getBoolOptionAsInt(additive) + getBoolOptionAsInt(cheap) * 3]);


	plot_data->set_color(color);
	plot_data->set_symbol_color(color);
	plot_data->set_line_width(2);

	return plot_data;
}




///////////////////////////////////////////////////////////////////////////////////
/////////////////////////// Constructor ///////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////
FuelPriceWindow::FuelPriceWindow(BaseObjectType* cobject, [[maybe_unused]] const RefPtr<Builder>& refGlade)
		: GdvWidget(), ApplicationWindow(cobject)
{
	// Get widgets
	refGlade->get_widget("fuel_price_close_button", close_button_);
	refGlade->get_widget("fuel_price_box", box_);
	refGlade->get_widget("fuel_price_selector_add_button", add_filter_button_);
	refGlade->get_widget("fuel_price_selector_list", filter_list_);

	// Set plot
	box_->pack_end(canvas_);
	canvas_.set_hexpand(true);
	canvas_.set_vexpand(true);

	// Event
	close_button_->signal_clicked().connect(mem_fun(*this, &FuelPriceWindow::onCloseClicked));
	add_filter_button_->signal_clicked().connect(mem_fun(*this, &FuelPriceWindow::onAddClicked));

	// Add a row
	Row* row = manage(new Row(*this));
	filter_list_->append(*row);
}


///////////////////////////////////////////////////////////////////////////////////
/////////////////////////// Function //////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////
void FuelPriceWindow::removeData(FuelPriceWindow::Row& row)
{
	auto current_plot_data = plot_datas_.find(&row);
	if (current_plot_data != plot_datas_.end())
	{
		plot_->remove_data(*current_plot_data->second);
		plot_datas_.erase(&row);
	}
}

size_t FuelPriceWindow::getBoolOptionAsInt(optional<bool> opt)
{
	if (opt)
	{
		if (*opt)
		{
			return 2;
		}
		return 1;
	}
	return 0;
}

void FuelPriceWindow::launch(Gtk::Window& parent_window)
{
	set_transient_for(parent_window);

	plot_ = manage(new Plot2D(_("Date"), _("Price (€)"), _("Fuel price")));

	try
	{
		plot_->set_axis_time_format_x("%d/%m/%y");
		plot_->config_time_x(1, Glib::DateTime::create_local(1970, 1, 1, 12, 0, 0));

		canvas_.add_plot(*plot_);

		show_all();
	}
	catch (core::Exception& e)
	{
		delete plot_;
		GDV_APP_SEND_ERROR(_("Cannot get fuel price"), e.what());
	}

	for (Gtk::Widget* widget : filter_list_->get_children())
	{
		auto list_row = static_cast<Gtk::ListBoxRow*>(widget);
		Row* tmp_row  = static_cast<Row*>(list_row->get_child());

		onFilterChanged(*tmp_row);
	}
}

///////////////////////////////////////////////////////////////////////////////////
/////////////////////////// Slots /////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////
void FuelPriceWindow::onCloseClicked()
{
	hide();
	canvas_.remove_plot(*plot_);
	plot_ = nullptr;
	plot_datas_.clear();
}

void FuelPriceWindow::onRemoveClicked(Row& row)
{
	removeData(row);
	for (Gtk::Widget* widget : filter_list_->get_children())
	{
		auto list_row = static_cast<Gtk::ListBoxRow*>(widget);
		Row* tmp_row  = static_cast<Row*>(list_row->get_child());

		if (&row == tmp_row)
		{
			filter_list_->remove(*list_row);
			break;
		}
	}
}

void FuelPriceWindow::onAddClicked()
{
	Row* row = manage(new Row(*this));
	filter_list_->append(*row);
	row->show_all();
}

void FuelPriceWindow::onFilterChanged(Row& row)
{
	removeData(row);

	auto plot_data = row.getPlotData(*app()->database());
	if (plot_data == nullptr)
		return;

	plot_->add_data(*plot_data);
	plot_datas_[&row] = plot_data;
}
