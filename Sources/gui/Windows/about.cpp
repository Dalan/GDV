/*!
 * \file    about.cpp
 * \author  Remi BERTHO
 * \date    22/11/17
 * \version 1.0.0
 */

/*
 * about.cpp
 *
 * Copyright 2016-2017 Remi BERTHO <remi.bertho@dalan.fr>
 *
 * This file is part of GDV.
 *
 * GDV is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * GDV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

#include "log.hpp"

#include "about.hpp"
#include "version.hpp"

using namespace Gtk;
using namespace Glib;
using namespace core;

About::About(BaseObjectType* cobject, [[maybe_unused]] const RefPtr<Builder>& refGlade) : GdvWidget(), AboutDialog(cobject)
{
	Version version = Version::GetSoftwareCurrent();
#ifdef NDEBUG
	set_version("Version " + version.toUstring() + version.time().format(" build the %x at %X"));
#else
	set_version("Version " + version.toString() + " " + version.git());
#endif
}



void About::launch(Window& parent_window)
{
	set_transient_for(parent_window);
	show();
	run();
	hide();
}
