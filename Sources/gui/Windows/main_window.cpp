/*!
 * \file    main_window.cpp
 * \author  Remi BERTHO
 * \date    22/11/17
 * \version 1.0.0
 */

/*
 * main_window.cpp
 *
 * Copyright 2016-2017 Remi BERTHO <remi.bertho@dalan.fr>
 *
 * This file is part of GDV.
 *
 * GDV is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * GDV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */


#include "log.hpp"

#include "main_window.hpp"

#include "internationalization.hpp"
#include "gdv_application.hpp"
#include "exception.hpp"

#include "Widgets/object_selector.hpp"

#include "Views/car_view.hpp"
#include "Views/service_view.hpp"
#include "Views/inspection_view.hpp"
#include "Views/insurance_view.hpp"
#include "Views/fulltank_view.hpp"
#include "Views/tyre_view.hpp"
#include "Views/wiper_view.hpp"

#include "question_window.hpp"
#include "about.hpp"
#include "fuel_price_window.hpp"

using namespace Gtk;
using namespace Glib;
using namespace std;
using namespace core;
using namespace database;
using namespace car;
using namespace vehicle;

///////////////////////////////////////////////////////////////////////////////////
/////////////////////////// Constructor ///////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////
MainWindow::MainWindow(BaseObjectType* cobject, const RefPtr<Builder>& refGlade) : GdvWidget(), ApplicationWindow(cobject)
{
	// Get widgets
	refGlade->get_widget("main_windows_stack", views_stack_);
	refGlade->get_widget("main_windows_stack_bar", views_sidebar_);

	refGlade->get_widget("main_infobar", infobar_);
	refGlade->get_widget("main_infobar_title", infobar_title_label_);
	refGlade->get_widget("main_infobar_msg", infobar_msg_label_);
	refGlade->get_widget("main_infobar_log_msg", infobar_log_msg_label_);
	refGlade->get_widget("main_infobar_icon", infobar_image_);
	refGlade->get_widget("main_infobar_show_info_button", infobar_info_button_);

	refGlade->get_widget("main_headerbar", header_bar_);
	refGlade->get_widget("save_button", save_button_);
	refGlade->get_widget("main_window_menu_popover", main_menu_popover_);
	refGlade->get_widget("undo_button", undo_button_);
	refGlade->get_widget("redo_button", redo_button_);
	refGlade->get_widget("fuel_price_button", fuel_price_button_);
	refGlade->get_widget("check_for_update_button", check_update_button_);
	refGlade->get_widget("about_button", about_button_);

	refGlade->get_widget_derived("car_scrolledwindow", car_view_, *this);
	refGlade->get_widget_derived("service_grid", car_service_view_, *this);
	refGlade->get_widget_derived("inspection_grid", car_inspection_view_, *this);
	refGlade->get_widget_derived("insurance_grid", car_insurance_view_, *this);
	refGlade->get_widget_derived("fulltank_grid", fulltank_view_, *this);
	refGlade->get_widget_derived("tyre_grid", car_tyre_view_, *this);
	refGlade->get_widget_derived("wiper_grid", car_wiper_view_, *this);


	// Set selector
	vehicle_selector_ = manage(new ObjectSelector<car::Car, true>());
	header_bar_->pack_start(*vehicle_selector_);


	// Disable delete
	save_button_->set_sensitive(false);
	undo_button_->set_sensitive(false);
	redo_button_->set_sensitive(false);


	// Event
	signal_delete_event().connect(mem_fun(*this, &MainWindow::onDeleteEvent));
	app()->signalVehicleChanged().connect(mem_fun(*this, &MainWindow::onApplicationCarChanged));
	vehicle_selector_->signalRemove().connect(mem_fun(*this, &MainWindow::onCarRemoveButton));
	vehicle_selector_->signalAdd().connect(mem_fun(*this, &MainWindow::onCarNewButton));
	vehicle_selector_->signalSelected().connect(mem_fun(*this, &MainWindow::onCarSelected));
	save_button_->signal_clicked().connect(mem_fun(*this, &MainWindow::onSaveButton));
	undo_button_->signal_clicked().connect(mem_fun(*this, &MainWindow::onUndoButton));
	redo_button_->signal_clicked().connect(mem_fun(*this, &MainWindow::onRedoButton));
	app()->database()->signalChangeRequest().connect(mem_fun(*this, &MainWindow::onDatabaseChangeRequest));
	app()->database()->signalFunctionDone().connect(mem_fun(*this, &MainWindow::onDatabaseFunctionDone));
	views_stack_->property_visible_child().signal_changed().connect(mem_fun(*this, &MainWindow::onViewVisibleChanged));
	infobar_->signal_response().connect(mem_fun(*this, &MainWindow::onInfobarResponse));
	infobar_info_button_->signal_clicked().connect(mem_fun(*this, &MainWindow::onInfobarInformation));


#if GTK_CHECK_VERSION(3, 22, 29)
	infobar_->set_property("revealed", false);
#else
	infobar_->hide();
#endif
}

///////////////////////////////////////////////////////////////////////////////////
/////////////////////////// Functions /////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////

void MainWindow::init()
{
	check_update_button_->signal_clicked().connect(
			[&]()
			{
				hideMainMenu();
				app()->checkForUpdate();
			});
	about_button_->signal_clicked().connect(
			[&]()
			{
				hideMainMenu();
				app()->about()->launch(*this);
			});
	fuel_price_button_->signal_clicked().connect(
			[&]()
			{
				hideMainMenu();
				app()->fuelPriceWindow()->launch(*this);
			});
}

void MainWindow::askSaveIfNeeded()
{
	if (app()->database()->needSave())
	{
		QuestionWindow::Answer answer = QuestionWindow::run(this,
				"Save before changing car ?",
				_("There is some unsaved changes on the current car.\nDo you want to save them before your action ?"),
				QuestionWindow::ButtonDescription(_("Discard changes"),
						QuestionWindow::ButtonAction::DESTRUCTIVE,
						QuestionWindow::ButtonPosition(QuestionWindow::LEFT, 0)),
				QuestionWindow::ButtonDescription(
						_("Save"), QuestionWindow::ButtonAction::SUGGESTED, QuestionWindow::ButtonPosition(QuestionWindow::RIGHT, 0)));

		if ((answer.isButtonAnswer) && (answer.position.side_ == QuestionWindow::RIGHT))
			app()->database()->save();
		else
			app()->database()->deleteChanges();
	}
}

void MainWindow::setSubtitle(VehiclePtr vehicle, const bool need_save)
{
	if (vehicle != nullptr)
	{
		if (need_save)
			header_bar_->set_subtitle("*" + vehicle->name());
		else
			header_bar_->set_subtitle(vehicle->name());
	}
	else
	{
		header_bar_->set_subtitle("");
	}
}

void MainWindow::enableViews(const VehicleType type, const bool enable)
{
	if (enable)
	{
		switch (type)
		{
		case VehicleType::CAR:
			enableView(car_view_);
			enableView(car_service_view_);
			enableView(car_insurance_view_);
			enableView(car_inspection_view_);
			enableView(car_tyre_view_);
			enableView(car_wiper_view_);
			enableView(fulltank_view_);

			views_stack_->set_visible_child(car_service_view_->name());
			views_stack_->set_visible_child(car_view_->name());

			views_stack_->show_all();
			break;
		}
	}
	else
	{
		views_stack_->hide();

		// Remove views
		auto views = views_stack_->get_children();
		for (auto view : views)
		{
			views_stack_->remove(*view);
			dynamic_cast<View*>(view)->enable(false);
		}
	}
}

void MainWindow::enableView(View* view)
{
	view->enable(true);
	auto title = view->title();
	title.replace(0, 1, title.substr(0, 1).uppercase());
	views_stack_->add(*dynamic_cast<Widget*>(view), view->name(), title);
	// views_stack_->child_property_icon_name(*dynamic_cast<Widget*>(view)).set_value("gtk-missing-image");
}

void MainWindow::hideMainMenu()
{
#if GTK_CHECK_VERSION(3, 22, 0)
	main_menu_popover_->popdown();
#else
	main_menu_popover_->hide();
#endif
}

void MainWindow::sendMessage(MessageType type, const ustring& title, const ustring& msg, const ustring& icon_name, const ustring& log_msg)
{
	infobar_->set_message_type(type);
	infobar_title_label_->set_text(title);
	infobar_msg_label_->set_text(msg);
	infobar_log_msg_label_->set_text(log_msg);
	infobar_log_msg_label_->hide();
	infobar_image_->set_from_icon_name(icon_name, ICON_SIZE_DIALOG);
#if GTK_CHECK_VERSION(3, 22, 29)
	infobar_->set_property("revealed", true);
#else
	infobar_->show_all();
#endif
}


///////////////////////////////////////////////////////////////////////////////////
/////////////////////////// Slots /////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////
bool MainWindow::onDeleteEvent([[maybe_unused]] GdkEventAny* any_event)
{
	app()->onQuit();

	return true;
}

void MainWindow::onCarSelected(const gint64 car_id)
{
	askSaveIfNeeded();

	app()->setCar(car_id);
}

void MainWindow::onApplicationCarChanged(VehiclePtr vehicle)
{
	setSubtitle(vehicle, false);
	if (vehicle != nullptr)
	{
		// Get vehicle type
		VehicleType current_type = VehicleType::CAR;
		if (dynamic_pointer_cast<Car>(vehicle) != nullptr)
			current_type = VehicleType::CAR;

		if (views_stack_->get_children().size() == 0)
		{
			enableViews(current_type);
		}
		else
		{
			if (last_type_ != current_type)
			{
				enableViews(last_type_, false);
				enableViews(current_type);
			}
		}

		last_type_ = current_type;
	}
	else
	{
		// Disable views
		enableViews(last_type_, false);
	}
	save_button_->set_sensitive(false);
	undo_button_->set_sensitive(false);
	redo_button_->set_sensitive(false);
}

void MainWindow::onCarRemoveButton(gint64 id)
{
	try
	{
		app()->database()->removeCar(id);
		if (app()->car() != nullptr)
		{
			if (app()->car()->id() == id)
				app()->setCar(-1);
		}
	}
	catch (core::Exception& e)
	{
		GDV_APP_SEND_ERROR(_("The car cannot be found."), e.what());
	}
}

void MainWindow::onCarNewButton()
{
	askSaveIfNeeded();

	Glib::ustring new_car_name = _("New car");
	try
	{
		app()->database()->deleteCurrentTracking();
		CarPtr car = app()->database()->newCar(new_car_name);
		app()->setVehicle(car);
	}
	catch (core::Exception& e)
	{
		GDV_APP_SEND_ERROR(ustring::compose(_("There is already a car named %1."), new_car_name), e.what());
	}
	vehicle_selector_->objectAdded(app()->car()->objectId());
}


void MainWindow::onSaveButton()
{
	try
	{
		app()->database()->save();
	}
	catch (core::Exception& e)
	{
		GDV_APP_SEND_ERROR(_("Error when saving."), e.what());
	}
}

void MainWindow::onUndoButton()
{
	try
	{
		app()->database()->undo(app()->vehicle());
	}
	catch (core::Exception& e)
	{
		GDV_APP_SEND_ERROR(_("Error when undo."), e.what());
	}
}

void MainWindow::onRedoButton()
{
	try
	{
		app()->database()->redo(app()->vehicle());
	}
	catch (core::Exception& e)
	{
		GDV_APP_SEND_ERROR(_("Error when redo."), e.what());
	}
}

void MainWindow::onDatabaseChangeRequest(const bool need_save)
{
	save_button_->set_sensitive(need_save);
	setSubtitle(app()->vehicle(), need_save);

	undo_button_->set_sensitive(true);
	redo_button_->set_sensitive(false);
}

void MainWindow::onDatabaseFunctionDone(DataBase::FunctionDone fn)
{
	switch (fn)
	{
	case DataBase::FunctionDone::SAVE:
		save_button_->set_sensitive(false);
		undo_button_->set_sensitive(false);
		redo_button_->set_sensitive(false);
		setSubtitle(app()->vehicle(), false);
		vehicle_selector_->objectsChanged();
		break;
	case DataBase::FunctionDone::REDO:
	case DataBase::FunctionDone::UNDO:
		save_button_->set_sensitive(app()->database()->needSave());
		undo_button_->set_sensitive(app()->database()->canUndo());
		redo_button_->set_sensitive(app()->database()->canRedo());
		setSubtitle(app()->vehicle(), app()->database()->needSave());
		break;
	case DataBase::FunctionDone::DELETE_CHANGES:
		break;
	}
}

void MainWindow::onViewVisibleChanged()
{
	auto view_visible_name = views_stack_->get_visible_child_name();
	if (view_visible_name.empty())
	{
		GDV_DEBUG("No current main window view");
	}
	else
	{
		GDV_DEBUG(ustring::compose("Current main window view is %1", view_visible_name));
	}
	emitSignalViewChanged(view_visible_name);
}

void MainWindow::onInfobarResponse(int response_id)
{
	switch (response_id)
	{
	case RESPONSE_CLOSE:
#if GTK_CHECK_VERSION(3, 22, 29)
		infobar_->set_property("revealed", false);
#else
		infobar_->hide();
#endif
		break;
	default:
		break;
	}
}

void MainWindow::onInfobarInformation()
{
	infobar_log_msg_label_->show();
}
