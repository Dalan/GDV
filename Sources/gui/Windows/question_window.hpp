/*!
 * \file    question_window.h
 * \author  Remi BERTHO
 * \date    27/05/18
 * \version 1.0.0
 */

/*
 * question_window.h
 *
 * Copyright 2016-2018 Remi BERTHO <remi.bertho@dalan.fr>
 *
 * This file is part of GDV.
 *
 * GDV is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * GDV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

#ifndef QUESTION_WINDOW_H_INCLUDED
#define QUESTION_WINDOW_H_INCLUDED

#include <gtkmm.h>

#include "Utilities/waiter.hpp"
#include "Utilities/gdv_widget.hpp"

/*! \class Question_Window
 *   \brief This class represent the question_window dialog
 */
class QuestionWindow : public GdvWidget, public Gtk::Window, public Waiter
{
	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Attributes ////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
public:
	enum ButtonSide
	{
		LEFT,
		RIGHT
	};

	enum ButtonAction
	{
		NORMAL,
		SUGGESTED,
		DESTRUCTIVE
	};

	struct ButtonPosition
	{
		ButtonSide side_;
		guint	   index_;

		ButtonPosition(ButtonSide side, guint index) : side_(side), index_(index)
		{
		}
	};

	struct ButtonDescription
	{
		Glib::ustring  name_;
		ButtonAction   action_;
		ButtonPosition position_;

		ButtonDescription(Glib::ustring name, ButtonAction action, ButtonPosition position)
				: name_(name), action_(action), position_(position)
		{
		}
	};

	struct Answer
	{
		bool		   isButtonAnswer;
		ButtonPosition position;

		Answer() : isButtonAnswer(false), position(LEFT, 0)
		{
		}
	};

	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Attributes ////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
private:
	Gtk::HeaderBar header_bar_;
	Gtk::Box	   box_;
	Gtk::Image	   image_;
	Gtk::Label	   msg_label_;

	Answer answer_;

	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Constructor ///////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
private:
	/*!
	 *  \brief Constructor
	 */
	QuestionWindow();



	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Function //////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
private:
	template <class... Args> void configureButton(const ButtonDescription& button_description, Args... args)
	{
		Glib::RefPtr<Gtk::StyleContext> style;
		Gtk::Button*					button = Gtk::manage(new Gtk::Button);

		switch (button_description.position_.side_)
		{
		case LEFT:
			header_bar_.pack_start(*button);
			if (button_description.position_.index_ == 0)
				button->add_accelerator("clicked", get_accel_group(), GDK_KEY_L, Gdk::CONTROL_MASK, Gtk::ACCEL_VISIBLE);
			break;
		case RIGHT:
			if (button_description.position_.index_ == 0)
				button->add_accelerator("clicked", get_accel_group(), GDK_KEY_R, Gdk::CONTROL_MASK, Gtk::ACCEL_VISIBLE);
			header_bar_.pack_end(*button);
			break;
		}

		button->set_can_focus(true);
		button->set_label(button_description.name_);
		button->signal_clicked().connect(
				sigc::bind<ButtonPosition>(mem_fun(*this, &QuestionWindow::onButtonClicked), button_description.position_));

		switch (button_description.action_)
		{
		case SUGGESTED:
			style = button->get_style_context();
			style->add_class("suggested-action");
			set_focus(*button);
			break;
		case DESTRUCTIVE:
			style = button->get_style_context();
			style->add_class("destructive-action");
			break;
		default:
			break;
		}

		if constexpr (sizeof...(args) > 0)
			configureButton(args...);
	}

	template <class... Args> Answer launch(Gtk::Window* parent_window, const Glib::ustring& title, const Glib::ustring& msg, Args... args)
	{
		if (parent_window != nullptr)
			set_transient_for(*parent_window);
		header_bar_.set_title(title);
		msg_label_.set_text(msg);

		configureButton(args...);
		answer_.isButtonAnswer = false;
		show_all();

		wait();
		return answer_;
	}

public:
	/*!
	 *  \brief Show the question_window dialog
	 */
	template <class... Args>
	static Answer run(Gtk::Window* parent_window, const Glib::ustring& title, const Glib::ustring& msg, Args... args)
	{
		QuestionWindow* window = new QuestionWindow();
		return window->launch(parent_window, title, msg, args...);
	}


	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Slots /////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
protected:
	/*!
	 *  \brief On delete event signal
	 *  \param any_event	The event which triggered this signal
	 *  \return true to stop other handlers from being invoked for the event. false to propagate the event further
	 */
	bool onDeleteEvent(GdkEventAny* any_event);

	/**
	 * @brief Function called when the apply button is clicked
	 */
	void onButtonClicked(ButtonPosition position);
};

#endif	  // QUESTION_WINDOW_H_INCLUDED
