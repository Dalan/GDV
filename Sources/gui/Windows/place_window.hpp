/*!
 * \file
 */

/*
 * place_window.h
 *
 * Copyright 2016-2019 Remi BERTHO <remi.bertho@dalan.fr>
 *
 * This file is part of GDV.
 *
 * GDV is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * GDV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

#ifndef PLACE_WINDOW_H_INCLUDED
#define PLACE_WINDOW_H_INCLUDED

#include <gtkmm.h>
#include "Utilities/waiter.hpp"
#include "Place/place.hpp"

/*! \class Message_Window
 *   \brief This class represent the place_window dialog
 */
class PlaceWindow : public Gtk::Window, public Waiter
{
	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Attributes ////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
private:
	volatile bool apply_ = true;

protected:
	Gtk::HeaderBar* header_bar_	   = nullptr;
	Gtk::Button*	apply_button_  = nullptr;
	Gtk::Button*	cancel_button_ = nullptr;

	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Constructor ///////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
protected:
	/*!
	 *  \brief Constructor with builder
	 *  \param cobject the C object
	 */
	explicit PlaceWindow(BaseObjectType* cobject);



	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Function //////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
protected:
	void connectButtons();

	/*!
	 *  \brief Show the place_window dialog
	 *  \return true if apply, false otherwise
	 */
	virtual bool internalLaunch(Gtk::Window* parent_window);

public:
	virtual void launch(Gtk::Window* parent_window, bool cancellable, std::shared_ptr<place::Place> place) = 0;


	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Slots /////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
private:
	void onApplyClicked();
	void onCancelClicked();
};

#endif	  // PLACE_WINDOW_H_INCLUDED
