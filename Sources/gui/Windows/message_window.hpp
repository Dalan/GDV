/*!
 * \file    message_window.h
 * \author  Remi BERTHO
 * \date    27/05/18
 * \version 1.0.0
 */

/*
 * message_window.h
 *
 * Copyright 2016-2018 Remi BERTHO <remi.bertho@dalan.fr>
 *
 * This file is part of GDV.
 *
 * GDV is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * GDV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

#ifndef MESSAGE_WINDOW_H_INCLUDED
#define MESSAGE_WINDOW_H_INCLUDED

#include <gtkmm.h>

#include "Utilities/waiter.hpp"
#include "Utilities/gdv_widget.hpp"

/*! \class Message_Window
 *   \brief This class represent the message_window dialog
 */
class MessageWindow : public GdvWidget, public Gtk::Window, public Waiter
{
	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Attributes ////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
private:
	Gtk::HeaderBar* header_bar_;
	Gtk::Image*		icon_image_;
	Gtk::Label*		msg_label_;
	Gtk::Label*		log_msg_label_;
	Gtk::Button*	apply_button_;
	Gtk::Expander*	log_expander_;

	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Constructor ///////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
public:
	/*!
	 *  \brief Constructor with builder
	 *  \param cobject the C object
	 *  \param refGlade the builder
	 */
	MessageWindow(BaseObjectType* cobject, const Glib::RefPtr<Gtk::Builder>& refGlade);



	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Function //////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
public:
	/*!
	 *  \brief Show the message_window dialog
	 */
	void launch(Gtk::Window*	 parent_window,
			const Glib::ustring& title,
			const Glib::ustring& msg,
			const Glib::ustring& icon_name,
			const Glib::ustring& log_msg);


	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Slots /////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
protected:
	/**
	 * @brief Function called when the apply button is clicked
	 */
	void onApplyClicked();
};

#endif	  // MESSAGE_WINDOW_H_INCLUDED
