/*!
 * \file    main_window.h
 * \author  Remi BERTHO
 * \date    22/11/17
 * \version 1.0.0
 */

/*
 * main_window.h
 *
 * Copyright 2016-2017 Remi BERTHO <remi.bertho@dalan.fr>
 *
 * This file is part of GDV.
 *
 * GDV is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * GDV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */


#ifndef MAIN_WINDOW_H_INCLUDED
#define MAIN_WINDOW_H_INCLUDED

#include <gtkmm.h>

#include "Utilities/gdv_widget.hpp"
#include "DataBase/database.hpp"
#include "Views/view.hpp"
#include "Car/car.hpp"

class CarView;
class ServiceView;
class InspectionView;
class InsuranceView;
class FulltankView;
class TyreView;
class WiperView;
template <typename Objectlass, bool top_object> class ObjectSelector;

/*! \class MainWindow
 *   \brief This class represent the main window
 */
class MainWindow : public GdvWidget, public Gtk::ApplicationWindow
{
	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Enumeration ///////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
private:
	enum VehicleType
	{
		CAR
	};

	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Signals ///////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
public:
	typedef sigc::signal<void, const Glib::ustring> type_signal_view_changed;



	inline type_signal_view_changed signalViewChanged()
	{
		return signal_view_changed_;
	}


protected:
	inline void emitSignalViewChanged(const Glib::ustring& view_visible_name)
	{
		signalViewChanged().emit(view_visible_name);
	}

private:
	type_signal_view_changed signal_view_changed_;

	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Attributes ////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
private:
	Gtk::Stack*		   views_stack_;
	Gtk::StackSidebar* views_sidebar_;
	VehicleType		   last_type_;

	CarView*		car_view_;
	ServiceView*	car_service_view_;
	InspectionView* car_inspection_view_;
	InsuranceView*	car_insurance_view_;
	TyreView*		car_tyre_view_;
	WiperView*		car_wiper_view_;
	FulltankView*	fulltank_view_;

	Gtk::InfoBar* infobar_;
	Gtk::Label*	  infobar_title_label_;
	Gtk::Label*	  infobar_msg_label_;
	Gtk::Label*	  infobar_log_msg_label_;
	Gtk::Image*	  infobar_image_;
	Gtk::Button*  infobar_info_button_;

	ObjectSelector<car::Car, true>* vehicle_selector_;
	Gtk::HeaderBar*					header_bar_;
	Gtk::Button*					save_button_;
	Gtk::Popover*					main_menu_popover_;
	Gtk::Button*					undo_button_;
	Gtk::Button*					redo_button_;
	Gtk::Button*					fuel_price_button_;
	Gtk::Button*					check_update_button_;
	Gtk::Button*					about_button_;

	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Constructor ///////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
public:
	/*!
	 *  \brief Constructor with builder
	 *  \param cobject the C object
	 *  \param refGlade the builder
	 */
	MainWindow(BaseObjectType* cobject, const Glib::RefPtr<Gtk::Builder>& refGlade);

	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Functions /////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
protected:
	void askSaveIfNeeded();

	void setSubtitle(vehicle::VehiclePtr vehicle, const bool need_save);

	void enableViews(const VehicleType type, const bool enable = true);
	void enableView(View* view);
	void hideMainMenu();

public:
	void sendMessage(Gtk::MessageType type,
			const Glib::ustring&	  title,
			const Glib::ustring&	  msg,
			const Glib::ustring&	  icon_name,
			const Glib::ustring&	  log_msg);

	void init();


	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Slots /////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
protected:
	/*!
	 *  \brief On delete event signal
	 *  \param any_event	The event which triggered this signal
	 *  \return true to stop other handlers from being invoked for the event. false to propagate the event further
	 */
	bool onDeleteEvent(GdkEventAny* any_event);

	/**
	 * @brief Function called when a car is selected in the popover
	 * @param car_name the car
	 */
	void onCarSelected(const gint64 car_id);

	/**
	 * @brief Function called when the vehicle is changed in the application
	 * @param vehicle the vehicle
	 */
	void onApplicationCarChanged(vehicle::VehiclePtr vehicle);

	/**
	 * @brief Function called to remove a car
	 * @param id the vehicle id
	 */
	void onCarRemoveButton(gint64 id);

	/**
	 * @brief Function called to create a car
	 */
	void onCarNewButton();

	/**
	 * @brief Function called the user click save
	 */
	void onSaveButton();

	/**
	 * @brief Function called the user click undo
	 */
	void onUndoButton();

	/**
	 * @brief Function called the user click redo
	 */
	void onRedoButton();

	/**
	 * @brief Called when the database receive a change request
	 * @param need_save true if the database need to be saved, false otherwise
	 */
	void onDatabaseChangeRequest(const bool need_save);

	/**
	 * @brief Called when the database finish a function
	 * @param fn the Function
	 */
	void onDatabaseFunctionDone(database::DataBase::FunctionDone fn);

	/**
	 * @brief Function called when the page changed on the notebook
	 * @param child the view focused
	 */
	void onViewVisibleChanged();

	/**
	 * @brief Function called when the user click a button in thi infobar
	 * @param response_id the button id
	 */
	void onInfobarResponse(int response_id);

	/**
	 * @brief Function called when the user click the information button in infobar
	 */
	void onInfobarInformation();
};



#endif	  // MAIN_WINDOW_H_INCLUDED
