/*!
 * \file    question_window.cpp
 * \author  Remi BERTHO
 * \date    27/05/18
 * \version 1.0.0
 */

/*
 * question_window.cpp
 *
 * Copyright 2016-2018 Remi BERTHO <remi.bertho@dalan.fr>
 *
 * This file is part of GDV.
 *
 * GDV is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * GDV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

#include "log.hpp"

#include "question_window.hpp"

#include "gdv_application.hpp"


using namespace Gtk;
using namespace Glib;
using namespace core;

///////////////////////////////////////////////////////////////////////////////////
/////////////////////////// Constructor ///////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////
QuestionWindow::QuestionWindow() : GdvWidget(), Window(), answer_()
{
	set_titlebar(header_bar_);

	add(box_);
	box_.set_orientation(ORIENTATION_HORIZONTAL);
	box_.set_spacing(10);
	box_.set_margin_bottom(10);
	box_.set_margin_top(10);
	box_.set_margin_start(10);
	box_.set_margin_end(10);
	box_.pack_start(image_, false, true);
	image_.set_from_icon_name("gtk-dialog-question", ICON_SIZE_DIALOG);
	box_.pack_end(msg_label_, true, true);
	msg_label_.set_line_wrap(true);
	msg_label_.set_line_wrap_mode(Pango::WrapMode::WRAP_WORD);
	msg_label_.set_size_request(300, 200);

	set_modal(true);
	set_skip_pager_hint(true);
	set_skip_taskbar_hint(true);
	set_position(WindowPosition::WIN_POS_CENTER_ON_PARENT);

	signal_delete_event().connect(mem_fun(*this, &QuestionWindow::onDeleteEvent));
}

///////////////////////////////////////////////////////////////////////////////////
/////////////////////////// Slots /////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////
void QuestionWindow::onButtonClicked(ButtonPosition id)
{
	answer_.position	   = id;
	answer_.isButtonAnswer = true;

	hide();
	go();
}

bool QuestionWindow::onDeleteEvent([[maybe_unused]] GdkEventAny* any_event)
{
	return true;
}
