/*!
 * \file    fuel_price_window.h
 * \author  Remi BERTHO
 * \date    11/10/18
 * \version 1.0.0
 */

/*
 * fuel_price_window.h
 *
 * Copyright 2016-2018 Remi BERTHO <remi.bertho@dalan.fr>
 *
 * This file is part of GDV.
 *
 * GDV is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * GDV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

#ifndef FUEL_PRICE_WINDOW_H_INCLUDED
#define FUEL_PRICE_WINDOW_H_INCLUDED

#include <gtkmm.h>
#include <array>
#include "share.hpp"
#include "Utilities/gdv_widget.hpp"
#include "gtkmm-plplot.h"
#include "DataBase/database.hpp"

/*! \class FuelPriceWindow
 *   \brief This class represent the fuel_price_window dialog
 */
class FuelPriceWindow : public GdvWidget, public Gtk::ApplicationWindow
{
	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////////// Row ///////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
	class Row : public Gtk::Box, public GdvWidget
	{
		// Atrributes
	private:
		Gtk::Button		  remove_button_;
		Gtk::ComboBoxText engine_type_;
		Gtk::ComboBoxText additive_;
		Gtk::ComboBoxText cheap_;


		// Constructor
	public:
		explicit Row(FuelPriceWindow& window);

		// Getter
	public:
		Gtk::PLplot::PlotData2D* getPlotData(database::DataBase& db);
	};




	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Attributes ////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
private:
	Gtk::Button*  close_button_;
	Gtk::Box*	  box_;
	Gtk::Button*  add_filter_button_;
	Gtk::ListBox* filter_list_;

	Gtk::PLplot::Canvas						 canvas_;
	Gtk::PLplot::Plot2D*					 plot_ = nullptr;
	std::map<Row*, Gtk::PLplot::PlotData2D*> plot_datas_;

	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Constructor ///////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
public:
	FuelPriceWindow(BaseObjectType* cobject, const Glib::RefPtr<Gtk::Builder>& refGlade);



	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Function //////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
private:
	void		  removeData(Row& row);
	static size_t getBoolOptionAsInt(std::optional<bool> opt);

public:
	void launch(Gtk::Window& parent_window);


	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Slots /////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
protected:
	void onCloseClicked();
	void onRemoveClicked(Row& row);
	void onAddClicked();
	void onFilterChanged(Row& row);
};

#endif	  // FUEL_PRICE_WINDOW_H_INCLUDED
