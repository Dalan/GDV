/*!
 * \file    message_window.cpp
 * \author  Remi BERTHO
 * \date    27/05/18
 * \version 1.0.0
 */

/*
 * message_window.cpp
 *
 * Copyright 2016-2018 Remi BERTHO <remi.bertho@dalan.fr>
 *
 * This file is part of GDV.
 *
 * GDV is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * GDV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

#include "log.hpp"

#include "message_window.hpp"

#include "gdv_application.hpp"


using namespace Gtk;
using namespace Glib;
using namespace core;

///////////////////////////////////////////////////////////////////////////////////
/////////////////////////// Constructor ///////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////
MessageWindow::MessageWindow(BaseObjectType* cobject, const RefPtr<Builder>& refGlade) : GdvWidget(), Window(cobject)
{
	// Get widgets
	refGlade->get_widget("message_headerbar", header_bar_);
	refGlade->get_widget("message_icon_image", icon_image_);
	refGlade->get_widget("message_msg_label", msg_label_);
	refGlade->get_widget("message_log_msg_label", log_msg_label_);
	refGlade->get_widget("message_apply_button", apply_button_);
	refGlade->get_widget("message_window_log_expander", log_expander_);


	// Event
	apply_button_->signal_clicked().connect(mem_fun(*this, &MessageWindow::onApplyClicked));
}

///////////////////////////////////////////////////////////////////////////////////
/////////////////////////// Function //////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////
void MessageWindow::launch(
		Window* parent_window, const ustring& title, const ustring& msg, const ustring& icon_name, const ustring& log_msg)
{
	if (parent_window != nullptr)
		set_transient_for(*parent_window);
	header_bar_->set_title(title);
	msg_label_->set_text(msg);
	log_msg_label_->set_text(log_msg);
	log_expander_->set_expanded(false);
	icon_image_->set_from_icon_name(icon_name, ICON_SIZE_DIALOG);
	show_all();

	wait();
}

///////////////////////////////////////////////////////////////////////////////////
/////////////////////////// Slots /////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////
void MessageWindow::onApplyClicked()
{
	hide();
	go();
}
