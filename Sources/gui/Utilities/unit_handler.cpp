/*!
 * \file    unit_handler.cpp
 * \author  Remi BERTHO
 * \date    16/09/18
 * \version 1.0.0
 */

/*
 * unit_handler.cpp
 *
 * Copyright 2016-2018 Remi BERTHO <remi.bertho@dalan.fr>
 *
 * This file is part of GDV.
 *
 * GDV is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * GDV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

#include "log.hpp"

#include "unit_handler.hpp"

#include <utility>

using namespace Glib;

///////////////////////////////////////////////////////////////////////////////////
/////////////////////////// Constructor ///////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////
UnitHandler::UnitHandler(ustring unit, const bool allow_negative) : unit_(std::move(unit)), allow_negative_(allow_negative)
{
}

UnitHandler::~UnitHandler() = default;


///////////////////////////////////////////////////////////////////////////////////
/////////////////////////// Function //////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////
ustring::size_type UnitHandler::unitPosition(const ustring& str) const
{
	return str.rfind(unit());
}

ustring& UnitHandler::addUnit(ustring& str) const
{
	if (!allow_negative_ && (str[0] == '-'))
		return str.assign(NEGATIVE_UNKNOWN);

	if (unitPosition(str) != ustring::npos)
	{
		return str;
	}

	return str.append(" " + unit());
}

ustring& UnitHandler::removeUnit(ustring& str) const
{
	if (str == NEGATIVE_UNKNOWN)
		return str.assign("-1");

	auto pos = unitPosition(str);
	if (pos == ustring::npos)
	{
		return str;
	}

	return str.erase(pos, unit().size());
}
