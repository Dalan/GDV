/*!
 * \file    gdv_widget.h
 * \author  Remi BERTHO
 * \date    22/11/17
 * \version 1.0.0
 */

/*
 * gdv_widget.h
 *
 * Copyright 2016-2017 Remi BERTHO <remi.bertho@dalan.fr>
 *
 * This file is part of GDV.
 *
 * GDV is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * GDV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */


#ifndef GDV_WIDGET_H_INCLUDED
#define GDV_WIDGET_H_INCLUDED

///////////////////////////////////////////////////////////////////////////////////
/////////////////////////////// Defines ///////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////
#define GDV_APP_SEND_DEBUG(log_msg) app()->sendMessage(GdvApplication::DEBUG, __FILE__, __LINE__, __FUNCTION__, "", log_msg)
#define GDV_APP_SEND_INFO(msg, log_msg) app()->sendMessage(GdvApplication::INFO, __FILE__, __LINE__, __FUNCTION__, msg, log_msg)
#define GDV_APP_SEND_WARNING(msg, log_msg) app()->sendMessage(GdvApplication::WARNING, __FILE__, __LINE__, __FUNCTION__, msg, log_msg)
#define GDV_APP_SEND_ERROR(msg, log_msg) app()->sendMessage(GdvApplication::ERROR, __FILE__, __LINE__, __FUNCTION__, msg, log_msg)
#define GDV_APP_SEND_FATAL(msg, log_msg) app()->sendMessage(GdvApplication::FATAL, __FILE__, __LINE__, __FUNCTION__, msg, log_msg)

class GdvApplication;

/*! \class GdvWidget
 *   \brief This class is a CsuWidget
 */
class GdvWidget
{
private:
	static GdvApplication* app_; /*!< The GdvApplication */

protected:
	/*!
	 *  \brief return the application
	 *  \return the application
	 */
	inline GdvApplication* app() const
	{
		return app_;
	}

public:
	/*!
	 *  \brief Set the application
	 *  \param app the application
	 */
	static void setApp(GdvApplication* app);
};


#endif	  // GDV_WIDGET_H_INCLUDED
