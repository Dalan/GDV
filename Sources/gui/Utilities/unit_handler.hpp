/*!
 * \file    unit_handler.h
 * \author  Remi BERTHO
 * \date    16/09/18
 * \version 1.0.0
 */

/*
 * unit_handler.h
 *
 * Copyright 2016-2018 Remi BERTHO <remi.bertho@dalan.fr>
 *
 * This file is part of GDV.
 *
 * GDV is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * GDV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */


#ifndef UNIT_HANDLER_H_INCLUDED
#define UNIT_HANDLER_H_INCLUDED

#include <glibmm.h>


/*! \class UnitHandler
 *   \brief This class is used to handle units
 */
class UnitHandler
{
	static const constexpr char* NEGATIVE_UNKNOWN = "unknown";

	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Attributes ////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
private:
	Glib::ustring unit_;
	bool		  allow_negative_;


	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Constructor ///////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
protected:
	UnitHandler(Glib::ustring unit, const bool allow_negative = true);

	virtual ~UnitHandler();


	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Getter and setter /////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
public:
	inline Glib::ustring unit() const
	{
		return unit_;
	}


	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Function //////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
private:
	Glib::ustring::size_type unitPosition(const Glib::ustring& str) const;

public:
	Glib::ustring& addUnit(Glib::ustring& str) const;
	Glib::ustring& removeUnit(Glib::ustring& str) const;
};


#endif	  // UNIT_HANDLER_H_INCLUDED
