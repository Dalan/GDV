/*!
 * \file    gdv_application.cpp
 * \author  Remi BERTHO
 * \date    22/11/17
 * \version 1.0.0
 */

/*
 * gdv_application.cpp
 *
 * Copyright 2016-2017 Remi BERTHO <remi.bertho@dalan.fr>
 *
 * This file is part of GDV.
 *
 * GDV is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * GDV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

#include "log.hpp"

#include "gdv_application.hpp"

#include <iostream>
#include "internationalization.hpp"

#include "Utilities/gdv_widget.hpp"
#include "exception.hpp"
#include "Windows/main_window.hpp"
#include "Windows/about.hpp"
#include "Windows/message_window.hpp"
#include "Windows/question_window.hpp"
#include "Windows/fuel_price_window.hpp"

using namespace Gtk;
using namespace Glib;
using namespace Gio;
using namespace std;
using namespace core;
using namespace sigc;
using namespace car;
using namespace vehicle;
using namespace database;

///////////////////////////////////////////////////////////////////////////////////
/////////////////////////////// Defines ///////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////
#define GDV_SEND_DEBUG(log_msg) sendMessage(GdvApplication::DEBUG, __FILE__, __LINE__, __FUNCTION__, "", log_msg)
#define GDV_SEND_INFO(msg, log_msg) sendMessage(GdvApplication::INFO, __FILE__, __LINE__, __FUNCTION__, msg, log_msg)
#define GDV_SEND_WARNING(msg, log_msg) sendMessage(GdvApplication::WARNING, __FILE__, __LINE__, __FUNCTION__, msg, log_msg)
#define GDV_SEND_ERROR(msg, log_msg) sendMessage(GdvApplication::ERROR, __FILE__, __LINE__, __FUNCTION__, msg, log_msg)
#define GDV_SEND_FATAL(msg, log_msg) sendMessage(GdvApplication::FATAL, __FILE__, __LINE__, __FUNCTION__, msg, log_msg)

///////////////////////////////////////////////////////////////////////////////////
/////////////////////////// Constructor ///////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////
GdvApplication::GdvApplication(int& argc, char**& argv) : Gtk::Application(argc, argv, "fr.dalan.gdv")
{
	signal_startup().connect(mem_fun(*this, &GdvApplication::onStartup));

	// Initialize GDV
	try
	{
		database_ = DataBase::initialize();
	}
	catch (Glib::Exception& e)
	{
		GDV_SEND_FATAL(_("Fatal error during initialization."), e.what());
	}

	signal_check_for_update_.connect(mem_fun(*this, &GdvApplication::onCheckForUpdateDone));
	exception_list_.connectSignalAdded(mem_fun(*this, &GdvApplication::onExceptionListAdd));
}

GdvApplication::~GdvApplication()
{
	if (about_ != nullptr)
		delete about_;
	if (main_window_ != nullptr)
		delete main_window_;
	if (message_window_ != nullptr)
		delete message_window_;
}


///////////////////////////////////////////////////////////////////////////////////
/////////////////////////// Function //////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////
void GdvApplication::init(RefPtr<Builder>& builder)
{
	builder_ = builder;
	GdvWidget::setApp(this);

	builder_->get_widget_derived("about_dialog", about_);
	builder_->get_widget_derived("main_window", main_window_);
	builder_->get_widget_derived("message_window", message_window_);
	builder_->get_widget_derived("fuel_price_window", fuel_price_window_);

	mainWindow()->init();

	GDV_DEBUG("Init done");
}

int GdvApplication::launch()
{
	int res = this->run(*main_window_);
	return res;
}

void GdvApplication::onStartup()
{
	add_window(*fuelPriceWindow());

	checkForUpdate(true);

	// Emit the signal that a car change even if there is no car to initialize all widgets
	emitSignalVehicleChanged();
}

void GdvApplication::onQuit()
{
	if (database()->needSave())
	{
		QuestionWindow::Answer answer = QuestionWindow::run(mainWindow(),
				"Save before exit ?",
				_("There is some unsaved change on the current car.\nDo you want to save them before exiting?"),
				QuestionWindow::ButtonDescription(_("Discard changes"),
						QuestionWindow::ButtonAction::DESTRUCTIVE,
						QuestionWindow::ButtonPosition(QuestionWindow::LEFT, 0)),
				QuestionWindow::ButtonDescription(
						_("Save"), QuestionWindow::ButtonAction::SUGGESTED, QuestionWindow::ButtonPosition(QuestionWindow::RIGHT, 0)));

		if ((answer.isButtonAnswer) && (answer.position.side_ == QuestionWindow::RIGHT))
			database()->save();
	}

	database()->cleanOrphans();

	quit();
	about()->hide();
}

void GdvApplication::sendMessage(MessageType type, ustring file, guint line, ustring fn, const ustring& msg, const ustring& log_msg)
{
	LogLevel		 log_level;
	Gtk::MessageType gtk_type;
	ustring			 title;
	ustring			 icon_name;

	switch (type)
	{
	case INFO:
		log_level = LogLevel::INFO;
		gtk_type  = MESSAGE_INFO;
		title	  = _("Information");
		icon_name = "dialog-information-symbolic";
		break;
	case WARNING:
		log_level = LogLevel::WARNING;
		gtk_type  = MESSAGE_WARNING;
		title	  = _("Warning");
		icon_name = "dialog-warning-symbolic";
		break;
	case ERROR:
		log_level = LogLevel::ERROR;
		gtk_type  = MESSAGE_ERROR;
		title	  = _("Error");
		icon_name = "dialog-error-symbolic";
		break;
	case FATAL:
		log_level = LogLevel::FATAL;
		gtk_type  = MESSAGE_ERROR;
		title	  = _("Fatal error");
		icon_name = "dialog-error-symbolic";
		break;
	default:
		log_level = LogLevel::DEBUG;
		gtk_type  = MESSAGE_OTHER;
		title	  = _("Debug");
		icon_name = "dialog-information-symbolic";
	}

	if (type != DEBUG)
	{
		for (auto window : get_windows())
		{
			cout << window << " " << window->get_title() << endl;
		}

		Window* active_window = get_active_window();
		if ((active_window != nullptr) && (active_window == main_window_) && (type != FATAL))
		{
			main_window_->sendMessage(gtk_type, title, msg, icon_name, log_msg);
		}
		else
		{
			if (message_window_ != nullptr)
				message_window_->launch(active_window, title, msg, icon_name, log_msg);
			else
			{
				MessageDialog* error = new MessageDialog(msg, false, gtk_type, BUTTONS_OK, true);
				error->run();
				error->hide();
				delete error;
			}
		}
	}

	log(log_level, file, line, fn, log_msg);
}

void GdvApplication::checkForUpdate(const bool auto_check)
{
	Version::getSoftwareLastAsynchronously(
			[=, this](const Version& version)
			{
				if (auto_check /*&& (version <= pref()->version().lastCheckVersion())*/)
					return;

				last_version_ = version;
				signal_check_for_update_.emit();
			},
			[=, this](const core::Exception& e)
			{
				if (!auto_check)
					this->exceptionList().add(e, "Cannot get the last version from internet");
			});
}


///////////////////////////////////////////////////////////////////////////////////
/////////////////////////// Slots /////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////
void GdvApplication::onCheckForUpdateDone()
{
	ustring msg, log_msg;
	Version current_version = Version::GetSoftwareCurrent();
	if (last_version_ > current_version)
	{
		msg = ustring::compose(_("A update is available: you use the version %1 of GDV whereas the version %2 is available.\n"
								 "You can download the new version on this website: https://www.dalan.fr/"),
				current_version.toString(),
				last_version_.toString());
	}
	else
	{
		msg = ustring::compose(_("You use the version %1 of GDV which is the latest version."), current_version.toString());
	}

	log_msg = ustring::compose("Current version %1 and last version %2", current_version.toString(), last_version_.toString());
	GDV_SEND_INFO(msg, log_msg);

	/*pref()->version().setLastCheckVersion(last_version_);
	pref()->writeToFile();*/
}

void GdvApplication::onExceptionListAdd()
{
	while (!exceptionList().empty())
	{
		auto ex = exceptionList().get();
		GDV_SEND_ERROR(ex.second, ex.first->what());
	}
}


///////////////////////////////////////////////////////////////////////////////////
/////////////////////////// Setter and getter /////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////
void GdvApplication::setCar(gint64 car_id)
{
	if (car_id == -1)
	{
		vehicle_ = nullptr;
		emitSignalVehicleChanged();
	}
	else
	{
		try
		{
			database()->deleteCurrentTracking();
			vehicle_ = database()->getCar(car_id);
			emitSignalVehicleChanged();
		}
		catch (core::Exception& e)
		{
			GDV_SEND_ERROR(_("The car cannot be found."), e.what());
		}
	}
}

void GdvApplication::setVehicle(VehiclePtr vehicle)
{
	vehicle_ = vehicle;
	emitSignalVehicleChanged();
}
