/*!
 * \file    gdv_application.h
 * \author  Remi BERTHO
 * \date    22/11/17
 * \version 1.0.0
 */

/*
 * gdv_application.h
 *
 * Copyright 2016-2017 Remi BERTHO <remi.bertho@dalan.fr>
 *
 * This file is part of GDV.
 *
 * GDV is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * GDV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

#ifndef GDV_APPLICATION_H_INCLUDED
#define GDV_APPLICATION_H_INCLUDED

#include <gtkmm.h>

class MainWindow;
class About;
class MessageWindow;
class FuelPriceWindow;

#include "DataBase/database.hpp"
#include "exception_list.hpp"

/*! \class CsuApplication
 *   \brief This class represent the Application
 */
class GdvApplication : public Gtk::Application
{
	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Enumeration ///////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
public:
	enum MessageType
	{
		DEBUG,
		INFO,
		WARNING,
		ERROR,
		FATAL
	};

	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////////// Signals ///////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
	Glib::Dispatcher signal_check_for_update_; /*!< The signal when the version is retrieved from
												  the internet is done */

public:
	typedef sigc::signal<void, vehicle::VehiclePtr> type_signal_vehicle_changed;

	/*!
	 *  \brief Return the car changed signal
	 *  \return the signal
	 */
	inline type_signal_vehicle_changed signalVehicleChanged()
	{
		return signal_vehicle_changed_;
	}


private:
	/*!
	 *  \brief Emit the car changed signal
	 */
	inline void emitSignalVehicleChanged()
	{
		signalVehicleChanged().emit(vehicle_);
	}

	type_signal_vehicle_changed signal_vehicle_changed_; /*!< The signal when car changed*/

	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Attributes ////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
private:
	Glib::RefPtr<Gtk::Builder> builder_; /*!< The Builder */

	database::DataBasePtr database_ = nullptr; /*!< The database */
	vehicle::VehiclePtr	  vehicle_	= nullptr; /*!< The car */
	core::Version		  last_version_;	   /*!< The last version checked from the internet */
	core::ExceptionList	  exception_list_;	   /*!< The exception list */

	MainWindow*		 main_window_		= nullptr; /*!< The main window */
	About*			 about_				= nullptr; /*!< The about dialog */
	MessageWindow*	 message_window_	= nullptr;
	FuelPriceWindow* fuel_price_window_ = nullptr;


	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Constructor ///////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
public:
	/*!
	 *  \brief Constructor
	 *  \param argc The parameter received by your main() function.
	 *  \param argv The parameter received by your main() function.
	 */
	GdvApplication(int& argc, char**& argv);


	/*!
	 *  \brief Destructor
	 */
	~GdvApplication();



	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Slots /////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
private:
	/*!
	 *  \brief Function used on the startup signal. It create the menu
	 */
	void onStartup();

	/**
	 * @brief A function called when a new version is getted from the internet
	 */
	void onCheckForUpdateDone();

	/**
	 * @brief A function that check and print the exception from the exception list
	 */
	void onExceptionListAdd();



	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Function //////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
public:
	/*!
	 *  \brief Initialize the CsuApplication with the builder
	 *  \param builder the GTK::Builder
	 */
	void init(Glib::RefPtr<Gtk::Builder>& builder);

	/*!
	 *  \brief Launch the application
	 */
	int launch();

	/*!
	 *  \brief Quit Csuper
	 */
	void onQuit();


	/*!
	 *  \brief Check if there is an update
	 *  \param auto_check indicate if it's an auto check or a manual check
	 */
	void checkForUpdate(const bool auto_check = false);

	void sendMessage(
			MessageType type, Glib::ustring file, guint line, Glib::ustring fn, const Glib::ustring& msg, const Glib::ustring& log_msg);



	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Getter ////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
public:
	inline MainWindow* mainWindow()
	{
		return main_window_;
	}

	inline About* about()
	{
		return about_;
	}

	inline FuelPriceWindow* fuelPriceWindow()
	{
		return fuel_price_window_;
	}



	/*!
	 *  \brief return the Database
	 *  \return the Database
	 */
	inline database::DataBasePtr database()
	{
		return database_;
	}

	/*!
	 *  \brief return the vehicle
	 *  \return the vehicle
	 */
	inline vehicle::VehiclePtr vehicle()
	{
		return vehicle_;
	}
	/*!
	 *  \brief return the vehicle as Car
	 *  \return the car
	 */
	inline car::CarPtr car()
	{
		return std::dynamic_pointer_cast<car::Car>(vehicle());
	}

	inline core::ExceptionList& exceptionList()
	{
		return exception_list_;
	}



	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Setter ////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
public:
	/*!
	 *  \brief Set a new Car
	 *  \param car_id the new Car id
	 */
	void setCar(gint64 car_id);

	/*!
	 *  \brief Set a new vehicle
	 *  \param vehicle the new vehicle
	 */
	void setVehicle(vehicle::VehiclePtr vehicle);
};

#endif	  // GDV_APPLICATION_H_INCLUDED
