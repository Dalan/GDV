/*!
 * \file    unit_spinbutton.h
 * \author  Remi BERTHO
 * \date    16/09/18
 * \version 1.0.0
 */

/*
 * unit_spinbutton.h
 *
 * Copyright 2016-2018 Remi BERTHO <remi.bertho@dalan.fr>
 *
 * This file is part of GDV.
 *
 * GDV is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * GDV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

#ifndef UNIT_SPINBUTTON_H_INCLUDED
#define UNIT_SPINBUTTON_H_INCLUDED

#include <gtkmm.h>
#include "Utilities/unit_handler.hpp"

/*! \class UnitSpinButton
 *   \brief This class represent the unit_spinbutton dialog
 */
class UnitSpinButton : public UnitHandler, public Gtk::SpinButton
{
	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Constructor ///////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
public:
	/*!
	 *  \brief Constructor with builder
	 *  \param cobject the C object
	 *  \param refGlade the builder
	 */
	UnitSpinButton(BaseObjectType*			  cobject,
			const Glib::RefPtr<Gtk::Builder>& refGlade,
			const Glib::ustring&			  unit,
			const bool						  allow_negative = true);

	UnitSpinButton(const Glib::ustring& unit, const bool allow_negative = true);


	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Slots /////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
private:
	bool onOutput();
	int	 onInput(double* new_value);
};

#endif	  // UNIT_SPINBUTTON_H_INCLUDED
