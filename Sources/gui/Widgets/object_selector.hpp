/*!
 * \file
 */

/*
 * object_selector.h
 *
 * Copyright 2019-2020 Remi BERTHO <remi.bertho@dalan.fr>
 *
 * This file is part of GDV.
 *
 * GDV is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * GDV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

#ifndef OBJECT_SELECTOR_LIST_H_INCLUDED
#define OBJECT_SELECTOR_LIST_H_INCLUDED

#include <gtkmm.h>
#include <glibmm/i18n.h>

#include "DataBase/database.hpp"
#include "Utilities/gdv_widget.hpp"
#include "gdv_application.hpp"
#include "Windows/question_window.hpp"

template <typename Objectlass, bool top_object> class ObjectSelector : public Gtk::MenuButton, GdvWidget
{
	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////////// Row ///////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
	class Row : public Gtk::Box
	{
		// Atrributes
	private:
		Gtk::Button remove_button_;
		Gtk::Button modify_button_;
		Gtk::Label	name_label_;
		Gtk::Image	selected_image_;

		Glib::ustring name_;
		gint64		  id_;

		// Constructor
	public:
		Row(ObjectSelector& list, bool modify_button, const generic::ObjectId& objectId) : name_(objectId.first), id_(objectId.second)
		{
			set_margin_start(10);
			set_margin_end(10);
			set_margin_bottom(5);
			set_margin_top(5);
			set_spacing(10);

			if (id_ != -1)
			{
				remove_button_.set_image_from_icon_name("list-remove-symbolic", Gtk::ICON_SIZE_MENU);
				remove_button_.set_relief(Gtk::RELIEF_NONE);
				auto style = remove_button_.get_style_context();
				style->add_class("destructive-action");
				pack_end(remove_button_, false, false);
				remove_button_.signal_clicked().connect(
						sigc::bind<-1, const generic::ObjectId>(mem_fun(list, &ObjectSelector::onRemoveClicked), objectId));
			}

			if (modify_button && (id_ != -1))
			{
				modify_button_.set_image_from_icon_name("document-edit-symbolic", Gtk::ICON_SIZE_MENU);
				modify_button_.set_relief(Gtk::RELIEF_NONE);
				modify_button_.set_margin_start(20);
				pack_end(modify_button_, false, false);
				modify_button_.signal_clicked().connect(
						sigc::bind<-1, const generic::ObjectId>(mem_fun(list, &ObjectSelector::onModifyClicked), objectId));
			}
			else
			{
				remove_button_.set_margin_start(20);
			}


			selected_image_.set_from_icon_name("emblem-ok-symbolic", Gtk::ICON_SIZE_MENU);
			selected_image_.set_no_show_all();
			pack_end(selected_image_, false, false);


			name_label_.set_text(name_);
			name_label_.set_xalign(0);
			name_label_.set_margin_end(50);
			pack_start(name_label_, false, false);
		}

		// Getter
	public:
		inline auto id() const
		{
			return id_;
		}

		inline auto name() const
		{
			return name_;
		}

		// Setter
	public:
		void setName(const Glib::ustring& name)
		{
			if (name != name_)
			{
				name_ = name;
				name_label_.set_text(name_);
			}
		}

		// Function
		void select()
		{
			selected_image_.show();
		}

		void unselect()
		{
			selected_image_.hide();
		}
	};

	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Signals ///////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
public:
	typedef sigc::signal<void>		   type_signal_add;
	typedef sigc::signal<void, gint64> type_signal_element_remove;
	typedef sigc::signal<void, gint64> type_signal_element_selected;
	typedef sigc::signal<void, gint64> type_signal_element_modify;



	inline type_signal_add signalAdd()
	{
		return signal_add_;
	}
	inline type_signal_element_remove signalRemove()
	{
		return signal_remove_;
	}
	inline type_signal_element_selected signalSelected()
	{
		return signal_selected_;
	}
	inline type_signal_element_selected signalModify()
	{
		return signal_modify_;
	}


protected:
	inline void emitSignalAdd()
	{
		signalAdd().emit();
	}
	inline void emitSignalRemove(gint64 id)
	{
		signalRemove().emit(id);
	}
	inline void emitSignalSelected(gint64 id)
	{
		signalSelected().emit(id);
	}
	inline void emitSignalModify(gint64 id)
	{
		signalModify().emit(id);
	}

private:
	type_signal_add				 signal_add_;
	type_signal_element_remove	 signal_remove_;
	type_signal_element_selected signal_selected_;
	type_signal_element_modify	 signal_modify_;

	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Attributes ////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
private:
	Gtk::Label label_;
	Gtk::Box   button_box_;

	Gtk::Popover popover_;
	Gtk::Image	 arrow_;
	Gtk::Box	 popover_box_ = Gtk::Box(Gtk::ORIENTATION_VERTICAL);

	Gtk::ListBox list_;
	Gtk::Button	 add_button_;
	Gtk::Label	 no_element_label_;

	bool objects_changed_ = true;

	gint64 selected_id = -1;

	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Constructor ///////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
public:
	/*!
	 *  \brief Constructor
	 */
	ObjectSelector() : Gtk::MenuButton()
	{
		set_halign(Gtk::ALIGN_CENTER);

		button_box_.set_orientation(Gtk::ORIENTATION_HORIZONTAL);
		button_box_.pack_start(label_);
		button_box_.pack_end(arrow_);

		add(button_box_);
		set_popover(popover_);

		label_.set_alignment(0.5);
		label_.set_text(Glib::ustring::compose(_("Select %1"), Objectlass::userName(1, Determiner::A)));
		label_.show();

		popover_.set_position(Gtk::POS_BOTTOM);
		arrow_.set_from_icon_name("pan-down-symbolic", Gtk::ICON_SIZE_MENU);

		no_element_label_.set_label(Glib::ustring::compose(_("No %1"), Objectlass::userName()));
		no_element_label_.set_margin_top(10);
		no_element_label_.set_margin_bottom(10);
		no_element_label_.show();

		list_.set_selection_mode(Gtk::SELECTION_NONE);
		list_.set_placeholder(no_element_label_);
		list_.set_vexpand(true);
		list_.set_hexpand(true);

		add_button_.set_label(Glib::ustring::compose(_("Add %1"), Objectlass::userName(1, Determiner::A)));
		auto style = add_button_.get_style_context();
		style->add_class("suggested-action");
		add_button_.signal_clicked().connect(mem_fun(*this, &ObjectSelector::onAddClicked));

		popover_box_.set_margin_start(10);
		popover_box_.set_margin_end(10);
		popover_box_.set_margin_top(10);
		popover_box_.set_margin_bottom(10);
		popover_box_.set_spacing(10);
		popover_box_.add(list_);
		popover_box_.add(add_button_);

		createObjectList();

		popover_.add(popover_box_);
		popover_.set_relative_to(*this);
		popover_.show_all();

#if GTK_CHECK_VERSION(3, 22, 0)
		popover_.popdown();
#else
		popover_.hide();
#endif
	}

	~ObjectSelector()
	{
	}

	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Getter and setter /////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
public:
	gint64 getSelectedId() const
	{
		return selected_id;
	}

	void setSelectedId(const gint64& id)
	{
		for (Gtk::Widget* widget : list_.get_children())
		{
			auto list_row = static_cast<Gtk::ListBoxRow*>(widget);
			Row* row	  = static_cast<Row*>(list_row->get_child());
			if (row->id() == id)
			{
				row->select();
			}
			else
			{
				row->unselect();
			}
		}

		if constexpr (!top_object)
		{
			if (id != -1)
			{
				try
				{
					auto object = app()->database()->template getPlace<Objectlass>(id);
					label_.set_text(object->name());
				}
				catch (core::Exception& e)
				{
					GDV_APP_SEND_WARNING(
							Glib::ustring::compose(_("The current %1 don't exist in the database."), Objectlass::userName()), e.what());
					label_.set_text(Glib::ustring::compose(_("Don't exist: select %1"), Objectlass::userName(1, Determiner::A)));
				}
			}
			else
			{
				label_.set_text(Glib::ustring::compose(_("Unknown: select %1"), Objectlass::userName(1, Determiner::A)));
			}
		}

		selected_id = id;
	}

	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Function //////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
private:
	void addObject(const generic::ObjectId& objectId)
	{
		Row* row = manage(new Row(*this, !top_object, objectId));
		row->show_all();
		list_.append(*row);
	}

	void objectRemoved(gint64 id)
	{
		for (Gtk::Widget* widget : list_.get_children())
		{
			auto list_row = static_cast<Gtk::ListBoxRow*>(widget);
			Row* row	  = static_cast<Row*>(list_row->get_child());
			if (row->id() == id)
			{
				list_.remove(*widget);
				break;
			}
		}

		if (id == selected_id)
		{
			setSelectedId(-1);
		}
	}

	void createObjectList()
	{
		auto list = app()->database()->template objectsId<Objectlass>();

		list_.signal_row_activated().connect(mem_fun(*this, &ObjectSelector::onRowSelected));
		list_.set_sort_func(mem_fun(*this, &ObjectSelector::onSortRow));
		list_.set_header_func(mem_fun(*this, &ObjectSelector::onUpdateHeader));


		if constexpr (!top_object)
		{
			addObject(std::make_pair("Unknown", -1));
		}
		for (auto element : list)
		{
			addObject(element);
		}

		show_all();
	}

public:
	inline void objectsChanged()
	{
		auto list = app()->database()->template objectsId<Objectlass>();

		for (Gtk::Widget* widget : list_.get_children())
		{
			auto* list_row = static_cast<Gtk::ListBoxRow*>(widget);
			Row*  row	   = static_cast<Row*>(list_row->get_child());

			for (const auto& object_id : list)
			{
				if (object_id.second == row->id())
				{
					row->setName(object_id.first);
					list_row->changed();
					break;
				}
			}
		}
	}

	inline void objectChanged(const generic::ObjectId& objectId)
	{
		// Update list name
		for (Gtk::Widget* widget : list_.get_children())
		{
			auto list_row = static_cast<Gtk::ListBoxRow*>(widget);
			Row* row	  = static_cast<Row*>(list_row->get_child());

			if (objectId.second == row->id())
			{
				row->setName(objectId.first);
				list_row->changed();
			}
		}

		// Update current name
		if constexpr (!top_object)
		{
			if (objectId.second == getSelectedId())
			{
				label_.set_text(objectId.first);
			}
		}
	}

	void objectAdded(const generic::ObjectId& objectId)
	{
		addObject(objectId);

		setSelectedId(objectId.second);
	}



	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Slots /////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
private:
	int onSortRow(Gtk::ListBoxRow* list_row1, Gtk::ListBoxRow* list_row2)
	{
		Row* row1 = static_cast<Row*>(list_row1->get_child());
		Row* row2 = static_cast<Row*>(list_row2->get_child());
		return row1->name().compare(row2->name());
	}

	void onUpdateHeader(Gtk::ListBoxRow* row, Gtk::ListBoxRow* before)
	{
		if (before != nullptr)
		{
			if (row->get_header() == nullptr)
			{
				auto separator = Gtk::manage(new Gtk::Separator(Gtk::ORIENTATION_HORIZONTAL));
				row->set_header(*separator);
				separator->show();
			}
		}
	}

	void onRowSelected(Gtk::ListBoxRow* list_row)
	{
#if GTK_CHECK_VERSION(3, 22, 0)
		popover_.popdown();
#else
		popover_.hide();
#endif

		if (list_row == nullptr)
		{
			emitSignalSelected(-1);
			return;
		}

		Row* row = static_cast<Row*>(list_row->get_child());

		setSelectedId(row->id());

		emitSignalSelected(row->id());
	}

	inline void onRemoveClicked(const generic::ObjectId objectId)
	{
		QuestionWindow::Answer answer = QuestionWindow::run(app()->mainWindow(),
				Glib::ustring::compose(_("Delete %1?"), Objectlass::userName()),
				Glib::ustring::compose(_("Do you really want to delete %1?"), objectId.first),
				QuestionWindow::ButtonDescription(
						_("Cancel"), QuestionWindow::ButtonAction::SUGGESTED, QuestionWindow::ButtonPosition(QuestionWindow::LEFT, 0)),
				QuestionWindow::ButtonDescription(
						_("Delete"), QuestionWindow::ButtonAction::DESTRUCTIVE, QuestionWindow::ButtonPosition(QuestionWindow::RIGHT, 0)));

		if ((answer.isButtonAnswer) && (answer.position.side_ == QuestionWindow::RIGHT))
		{
			objectRemoved(objectId.second);
			emitSignalRemove(objectId.second);
#if GTK_CHECK_VERSION(3, 22, 0)
			popover_.popdown();
#else
			popover_.hide();
#endif
		}
	}


	inline void onModifyClicked(const generic::ObjectId objectId)
	{
		emitSignalModify(objectId.second);
#if GTK_CHECK_VERSION(3, 22, 0)
		popover_.popdown();
#else
		popover_.hide();
#endif
	}


	inline void onAddClicked()
	{
#if GTK_CHECK_VERSION(3, 22, 0)
		popover_.popdown();
#else
		popover_.hide();
#endif
		emitSignalAdd();
	}
};

#endif	  // OBJECT_SELECTOR_LIST_H_INCLUDED
