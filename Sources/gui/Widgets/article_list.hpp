/*!
 * \file
 */

/*
 * about.h
 *
 * Copyright 2019 Remi BERTHO <remi.bertho@dalan.fr>
 *
 * This file is part of GDV.
 *
 * GDV is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * GDV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

#ifndef ARTICLE_LIST_H_INCLUDED
#define ARTICLE_LIST_H_INCLUDED

#include <gtkmm.h>
#include <glibmm/i18n.h>

#include "Utilities/gdv_widget.hpp"
#include "Generic/object_array.hpp"
#include "Generic/element.hpp"
#include "gdv_application.hpp"
#include "unit_spinbutton.hpp"


template <class ElementType, class ArticleType> class ArticleList : public Gtk::MenuButton, GdvWidget
{
	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////////// Row ///////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
	class Row : public Gtk::Box
	{
		// Signal
	public:
		typedef sigc::signal<void, double, double> type_signal_price_changed;
		inline type_signal_price_changed		   signalPriceChanged()
		{
			return signal_price_changed_;
		}

	private:
		type_signal_price_changed signal_price_changed_;


		// Attributes
	private:
		Gtk::Button						   remove_button_;
		Gtk::Entry						   text_entry_;
		UnitSpinButton					   price_spinbutton_;
		Glib::RefPtr<Gtk::Adjustment>	   price_adjustement_;
		Gtk::Image						   selected_image_;
		Glib::RefPtr<Gtk::EntryCompletion> article_name_completion_;

		const bool			  is_article_;
		generic::OwnedObject& article_;

		// Constructor
	public:
		Row(ArticleList& list, generic::OwnedObject& article, Glib::RefPtr<Gtk::ListStore> article_name_list)
				: price_spinbutton_("€", false), is_article_(std::is_base_of_v<generic::Article, ArticleType>), article_(article)
		{
			set_margin_start(10);
			set_margin_end(10);
			set_margin_bottom(5);
			set_margin_top(5);
			set_spacing(10);

			set_hexpand(true);
			set_halign(Gtk::Align::ALIGN_CENTER);
			set_valign(Gtk::Align::ALIGN_CENTER);

			remove_button_.set_image_from_icon_name("list-remove-symbolic", Gtk::ICON_SIZE_MENU);
			remove_button_.set_relief(Gtk::RELIEF_NONE);
			auto style = remove_button_.get_style_context();
			style->add_class("destructive-action");
			pack_end(remove_button_, false, false);
			remove_button_.signal_clicked().connect(
					sigc::bind<-1, generic::OwnedObject&>(mem_fun(list, &ArticleList::onRemoveClicked), article));


			selected_image_.set_from_icon_name("emblem-ok-symbolic", Gtk::ICON_SIZE_MENU);
			selected_image_.set_no_show_all();
			pack_end(selected_image_, false, false);

			if (is_article_)
			{
				price_adjustement_ = Gtk::Adjustment::create(-1, -1, 10'000, 10, 50, 0);
				price_spinbutton_.set_adjustment(price_adjustement_);
				price_spinbutton_.set_digits(2);
				price_spinbutton_.set_hexpand(false);
				price_spinbutton_.set_halign(Gtk::ALIGN_FILL);
				price_spinbutton_.signal_changed().connect(mem_fun(*this, &Row::onPriceChanged));
				generic::Article& real_article = static_cast<generic::Article&>(article_);
				price_spinbutton_.set_value(real_article.price());
				pack_end(price_spinbutton_, false, false);
			}


			article_name_completion_ = Gtk::EntryCompletion::create();
			article_name_completion_->set_model(article_name_list);
			article_name_completion_->set_text_column(0);

			text_entry_.set_hexpand(true);
			text_entry_.set_halign(Gtk::ALIGN_FILL);
			text_entry_.set_width_chars(40);
			text_entry_.set_completion(article_name_completion_);
			text_entry_.signal_focus_out_event().connect(mem_fun(*this, &Row::onMyFocusOutEventEntry));
			pack_end(text_entry_, false, false);
		}

	public:
		inline void update()
		{
			text_entry_.set_text(article_.name());
			if (is_article_)
			{
				generic::Article& real_article = static_cast<generic::Article&>(article_);
				signalPriceChanged().emit(price_spinbutton_.get_value(), real_article.price());
				price_spinbutton_.set_value(real_article.price());
			}
		}

	private:
		inline auto text() const
		{
			return text_entry_.get_text();
		}

		bool onMyFocusOutEventEntry([[maybe_unused]] GdkEventFocus* gdk_event)
		{
			article_.setName(text());
			if (is_article_)
			{
				generic::Article& real_article = static_cast<generic::Article&>(article_);
				real_article.setPrice(price_spinbutton_.get_value());
			}
			return false;
		}

		void onPriceChanged()
		{
			if (is_article_)
			{
				generic::Article& real_article = static_cast<generic::Article&>(article_);
				signalPriceChanged().emit(real_article.price(), price_spinbutton_.get_value());
				real_article.setPrice(price_spinbutton_.get_value());
			}
		}
	};




	// Tree model columns, for the EntryCompletion's filter model:
	class ModelColumns : public Gtk::TreeModel::ColumnRecord
	{
	public:
		ModelColumns()
		{
			add(m_col_name);
		}

		Gtk::TreeModelColumn<unsigned int>	m_col_id;
		Gtk::TreeModelColumn<Glib::ustring> m_col_name;
	};


	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Enum //////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
public:
	enum Direction
	{
		UP,
		DOWN
	};

	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Attributes ////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
private:
	const bool is_article_;

	Gtk::Label	 label_;
	Gtk::Popover popover_;
	Gtk::Image	 arrow_;
	Gtk::Box	 button_box_;

	Gtk::Box	 popover_box_ = Gtk::Box(Gtk::ORIENTATION_VERTICAL);
	Gtk::ListBox list_;
	Gtk::Button	 add_button_;
	Gtk::Label	 no_element_label_;

	Gtk::Label price_label_;
	double	   total_price_ = 0;

	ElementType* element_ = nullptr;

	ModelColumns				 article_name_column_record_;
	Glib::RefPtr<Gtk::ListStore> article_name_list_;

	sigc::connection connection_array_added_;
	sigc::connection connection_array_removed_;

	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Constructor ///////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
public:
	ArticleList(Direction popover_direction) : is_article_(std::is_base_of_v<generic::Article, ArticleType>)
	{
		set_halign(Gtk::ALIGN_CENTER);

		button_box_.set_orientation(Gtk::ORIENTATION_HORIZONTAL);
		button_box_.pack_start(label_);
		button_box_.pack_end(arrow_);

		add(button_box_);
		set_popover(popover_);

		updateLabel();

		if (popover_direction == UP)
		{
			popover_.set_position(Gtk::POS_TOP);
			arrow_.set_from_icon_name("pan-up-symbolic", Gtk::ICON_SIZE_MENU);
		}
		else
		{
			popover_.set_position(Gtk::POS_BOTTOM);
			arrow_.set_from_icon_name("pan-down-symbolic", Gtk::ICON_SIZE_MENU);
		}

		no_element_label_.set_label(Glib::ustring::compose(_("No %1"), ArticleType::userName(0)));
		no_element_label_.set_margin_top(10);
		no_element_label_.set_margin_bottom(10);
		no_element_label_.show();

		list_.set_selection_mode(Gtk::SELECTION_NONE);
		list_.set_placeholder(no_element_label_);
		list_.set_vexpand(true);
		list_.set_hexpand(true);
		list_.set_header_func(mem_fun(*this, &ArticleList::onUpdateHeader));

		add_button_.set_label(Glib::ustring::compose(_("Add %1"), ArticleType::userName(1, Determiner::A)));
		add_button_.set_halign(Gtk::ALIGN_CENTER);
		auto style = add_button_.get_style_context();
		style->add_class("suggested-action");
		add_button_.signal_clicked().connect(mem_fun(*this, &ArticleList::onAddClicked));

		popover_box_.set_margin_start(10);
		popover_box_.set_margin_end(10);
		popover_box_.set_margin_top(10);
		popover_box_.set_margin_bottom(10);
		popover_box_.set_spacing(10);
		popover_box_.add(list_);
		if (is_article_)
			popover_box_.add(price_label_);
		popover_box_.add(add_button_);

		popover_.add(popover_box_);
		popover_.set_relative_to(*this);
		popover_.show_all();
		popover_.signal_show().connect(mem_fun(*this, &ArticleList::onPopoverShow));

#if GTK_CHECK_VERSION(3, 22, 0)
		popover_.popdown();
#else
		popover_.hide();
#endif

		article_name_list_ = Gtk::ListStore::create(article_name_column_record_);

		auto articles = app()->database()->template objectsName<ArticleType>();
		for (auto& article_name : articles)
		{
			auto iter									 = *(article_name_list_->append());
			iter[article_name_column_record_.m_col_name] = article_name;
		}
	}

	~ArticleList() = default;

	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Function //////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
private:
	generic::ObjectArray* articleArray()
	{
		if (element_ == nullptr)
			return nullptr;
		return &element_->template objects<ArticleType>();
	}

	void addRow(generic::OwnedObject& article)
	{
		Row* row = manage(new Row(*this, article, article_name_list_));
		row->show_all();
		list_.append(*row);
		row->signalPriceChanged().connect(
				[&](double old_price, double new_price)
				{
					total_price_ = total_price_ - old_price + new_price;
					updateTotalPrice();
				});
	}

	void updateLabel()
	{
		if ((articleArray() == nullptr) || (articleArray()->empty()))
		{
			label_.set_text(Glib::ustring::compose(_("No %1"), ArticleType::userName(0)));
		}
		else
		{
			label_.set_text(Glib::ustring::compose("%1 %2", articleArray()->size(), ArticleType::userName(articleArray()->size())));
		}
	}

	void updateTotalPrice()
	{
		if (is_article_ && (element_ != nullptr))
		{
			price_label_.set_text(Glib::ustring::compose(_("The total price %1 is %2€ and it remain %3€ to reach the total price."),
					ArticleType::userName(2, Determiner::OF),
					total_price_,
					element_->price() - total_price_));
		}
	}

public:
	void setElement(ElementType* element)
	{
		element_ = element;

		for (Gtk::Widget* widget : list_.get_children())
		{
			list_.remove(*widget);
		}

		if (element != nullptr)
		{
			element_	 = element;
			total_price_ = 0;

			for (auto object : *articleArray())
			{
				addRow(*object);
				auto article = static_cast<generic::Article*>(object);
				if (article->price() > 0)
					total_price_ += article->price();
			}

			connection_array_added_.disconnect();
			connection_array_added_ = articleArray()->signalAdded().connect(mem_fun(*this, &ArticleList::onArticleAdded));
			connection_array_removed_.disconnect();
			connection_array_removed_ = articleArray()->signalRemoved().connect(mem_fun(*this, &ArticleList::onArticleRemoved));
		}

		updateLabel();
	}



	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Slots /////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
private:
	void onArticleAdded(generic::OwnedObject& article)
	{
		addRow(article);
		updateLabel();
	}
	void onArticleRemoved([[maybe_unused]] database::ItemPtr article, guint index)
	{
		Gtk::ListBoxRow* row = list_.get_row_at_index(static_cast<int>(index));
		list_.remove(*row);
		total_price_ -= std::dynamic_pointer_cast<generic::Article>(article)->price();
		updateTotalPrice();
		updateLabel();
	}

	void onPopoverShow()
	{
		for (Widget* widget : list_.get_children())
		{
			auto list_row = static_cast<Gtk::ListBoxRow*>(widget);
			Row* row	  = static_cast<Row*>(list_row->get_child());
			row->update();
		}
		updateTotalPrice();
	}

	void onAddClicked()
	{
		try
		{
			element_->template addObject<ArticleType>();
		}
		catch (core::Exception& e)
		{
			GDV_APP_SEND_ERROR(_("Cannot add article."), e.what());
		}
	}
	void onRemoveClicked(generic::OwnedObject& object)
	{
		ArticleType& article = static_cast<ArticleType&>(object);
		try
		{
			element_->template removeObject<ArticleType>(article);
		}
		catch (core::Exception& e)
		{
			GDV_APP_SEND_ERROR(Glib::ustring::compose(_("Cannot remove article %1."), article.name()), e.what());
		}
	}

	void onUpdateHeader(Gtk::ListBoxRow* row, Gtk::ListBoxRow* before)
	{
		if (before != nullptr)
		{
			if (row->get_header() == nullptr)
			{
				auto separator = Gtk::manage(new Gtk::Separator(Gtk::ORIENTATION_HORIZONTAL));
				row->set_header(*separator);
				separator->show();
			}
		}
	}
};

#endif	  // TEXT_LIST_H_INCLUDED
