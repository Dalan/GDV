/*!
 * \file    calendar_button.cpp
 * \author  Remi BERTHO
 * \date    10/03/2018
 * \version 1.0.0
 */

/*
 * calendar_button.cpp
 *
 * Copyright 2016-2018 Remi BERTHO <remi.bertho@dalan.fr>
 *
 * This file is part of GDV.
 *
 * GDV is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * GDV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

#include "log.hpp"

#include "calendar_button.hpp"

#include "internationalization.hpp"
#include "share.hpp"
#include "gdv_application.hpp"


using namespace Gtk;
using namespace Glib;
using namespace core;

///////////////////////////////////////////////////////////////////////////////////
/////////////////////////// Constructor ///////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////

CalendarButton::CalendarButton(Direction popover_direction) : date_(2)
{
	set_orientation(ORIENTATION_HORIZONTAL);
	pack_start(entry_);
	pack_end(menu_button_);

	set_halign(Align::ALIGN_CENTER);
	set_valign(Align::ALIGN_CENTER);
	menu_button_.add(arrow_);
	menu_button_.set_popover(popover_);

	entry_.set_alignment(0.5);
	entry_.signal_activate().connect(mem_fun(*this, &CalendarButton::onEntryWrite));
	entry_.signal_focus_out_event().connect(mem_fun(*this, &CalendarButton::onMyFocusOutEntry));

	if (popover_direction == UP)
	{
		popover_.set_position(POS_TOP);
		arrow_.set_from_icon_name("pan-up-symbolic", ICON_SIZE_MENU);
	}
	else
	{
		popover_.set_position(POS_BOTTOM);
		arrow_.set_from_icon_name("pan-down-symbolic", ICON_SIZE_MENU);
	}

	popover_none_button_.set_label(_("None"));

	popover_box_.set_orientation(ORIENTATION_VERTICAL);
	popover_box_.set_spacing(5);
	popover_box_.set_margin_start(5);
	popover_box_.set_margin_end(5);
	popover_box_.set_margin_top(5);
	popover_box_.set_margin_bottom(5);
	popover_box_.add(popover_calendar_);
	popover_box_.add(popover_none_button_);

	popover_.add(popover_box_);
	popover_.set_relative_to(*this);
	popover_.show_all();


	popover_calendar_.signal_day_selected_double_click().connect(mem_fun(*this, &CalendarButton::onDateSelected));
	popover_none_button_.signal_clicked().connect(mem_fun(*this, &CalendarButton::onButtonDoneClicked));
}

CalendarButton::~CalendarButton()
{
}


///////////////////////////////////////////////////////////////////////////////////
/////////////////////////// Function //////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////
Date CalendarButton::getDate() const
{
	return date_;
}

void CalendarButton::setDate(const Date& date)
{
	if (date_ != date)
	{
		date_ = date;
		if (date != Date(1))
		{
			popover_calendar_.select_month(date.get_month() - 1, date.get_year());
			popover_calendar_.select_day(date.get_day());
			entry_.set_text(date.format_string("%x"));
		}
		else
		{
			Date today;
			today.set_time_current();
			popover_calendar_.select_month(today.get_month() - 1, today.get_year());
			popover_calendar_.select_day(today.get_day());
			entry_.set_text(_("None"));
		}
	}
}

///////////////////////////////////////////////////////////////////////////////////
/////////////////////////// Slots /////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////
void CalendarButton::onDateSelected()
{
	Date date_selected;
	popover_calendar_.get_date(date_selected);
	setDate(date_selected);

#if GTK_MINOR_VERSION >= 22
	popover_.popdown();
#else
	popover_.hide();
#endif

	emitSignalChanged();
}

void CalendarButton::onEntryWrite()
{
	Date new_date;
	new_date.set_parse(entry_.get_text());
	if (new_date.valid())
	{
		setDate(new_date);
		emitSignalChanged();
	}
	else
	{
		GDV_APP_SEND_WARNING(ustring::compose(_("Invalid date entered: '%1'"), entry_.get_text()), "Invalid date entered");
	}
}

void CalendarButton::onButtonDoneClicked()
{
	setDate(Date(1));

#if GTK_MINOR_VERSION >= 22
	popover_.popdown();
#else
	popover_.hide();
#endif

	emitSignalChanged();
}

bool CalendarButton::onMyFocusOutEntry([[maybe_unused]] GdkEventFocus* gdk_event)
{
	onEntryWrite();
	return false;
}
