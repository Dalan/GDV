/*!
 * \file    unit_spinbutton.cpp
 * \author  Remi BERTHO
 * \date    16/09/18
 * \version 1.0.0
 */

/*
 * unit_spinbutton.cpp
 *
 * Copyright 2016-2018 Remi BERTHO <remi.bertho@dalan.fr>
 *
 * This file is part of GDV.
 *
 * GDV is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * GDV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

#include "log.hpp"

#include "unit_spinbutton.hpp"
#include "share.hpp"

using namespace Gtk;
using namespace Glib;
using namespace core;

///////////////////////////////////////////////////////////////////////////////////
/////////////////////////// Constructor ///////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////
UnitSpinButton::UnitSpinButton(
		BaseObjectType* cobject, [[maybe_unused]] const RefPtr<Builder>& refGlade, const Glib::ustring& unit, const bool allow_negative)
		: UnitHandler(unit, allow_negative), SpinButton(cobject)
{
	signal_input().connect(sigc::mem_fun(*this, &UnitSpinButton::onInput));
	signal_output().connect(sigc::mem_fun(*this, &UnitSpinButton::onOutput));
}

UnitSpinButton::UnitSpinButton(const ustring& unit, const bool allow_negative) : UnitHandler(unit, allow_negative), SpinButton()
{
	signal_input().connect(sigc::mem_fun(*this, &UnitSpinButton::onInput));
	signal_output().connect(sigc::mem_fun(*this, &UnitSpinButton::onOutput));
}

///////////////////////////////////////////////////////////////////////////////////
/////////////////////////// Slots /////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////

bool UnitSpinButton::onOutput()
{
	auto	adjustment = get_adjustment();
	ustring text	   = doubleToString(adjustment->get_value(), static_cast<int>(get_digits()));
	set_text(addUnit(text));
	return true;
}

int UnitSpinButton::onInput(double* new_value)
{
	ustring text = get_text();
	*new_value	 = stringToDouble(removeUnit(text));
	return true;
}
