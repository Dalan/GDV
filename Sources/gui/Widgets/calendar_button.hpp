/*!
 * \file    calendar_button.h
 * \author  Remi BERTHO
 * \date    10/03/2018
 * \version 1.0.0
 */

/*
 * about.h
 *
 * Copyright 2016-2018 Remi BERTHO <remi.bertho@dalan.fr>
 *
 * This file is part of GDV.
 *
 * GDV is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * GDV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

#ifndef CALENDAR_BUTTON_H_INCLUDED
#define CALENDAR_BUTTON_H_INCLUDED

#include <gtkmm.h>

#include "Utilities/gdv_widget.hpp"

/*! \class CalendarButton
 *   \brief This class represent the about dialog
 */
class CalendarButton : public Gtk::Box, GdvWidget
{
	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Enum //////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
public:
	enum Direction
	{
		UP,
		DOWN
	};
	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Signals ///////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
public:
	typedef sigc::signal<void> type_signal_date_selected;

	/*!
	 *  \brief Return the changed signal
	 *  \return the signal
	 */
	inline type_signal_date_selected signalDateSelected()
	{
		return signal_date_selected_;
	}


protected:
	/*!
	 *  \brief Emit the changed signal
	 */
	inline void emitSignalChanged()
	{
		signalDateSelected().emit();
	}

private:
	type_signal_date_selected signal_date_selected_;

	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Attributes ////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
private:
	Gtk::Entry		entry_;
	Gtk::Image		arrow_;
	Gtk::MenuButton menu_button_;

	Gtk::Popover  popover_;
	Gtk::Box	  popover_box_;
	Gtk::Button	  popover_none_button_;
	Gtk::Calendar popover_calendar_;

	Glib::Date date_;

	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Constructor ///////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
public:
	/*!
	 *  \brief Constructor
	 */
	CalendarButton(Direction popover_direction);

	~CalendarButton();

	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Function //////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
public:
	Glib::Date getDate() const;

	void setDate(const Glib::Date& date);



	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Slots /////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
private:
	void onDateSelected();

	void onEntryWrite();
	bool onMyFocusOutEntry(GdkEventFocus* gdk_event);
	void onButtonDoneClicked();
};

#endif	  // CALENDAR_BUTTON_H_INCLUDED
