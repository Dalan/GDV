/*!
 * \file    element_list.h
 * \author  Remi BERTHO
 * \date    15/04/2018
 * \version 1.0.0
 */

/*
 * element_list.h
 *
 * Copyright 2016-2018 Remi BERTHO <remi.bertho@dalan.fr>
 *
 * This file is part of GDV.
 *
 * GDV is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * GDV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

#ifndef ELEMENT_LIST_LIST_H_INCLUDED
#define ELEMENT_LIST_LIST_H_INCLUDED

#include <gtkmm.h>
#include <glibmm/i18n.h>

#include "Utilities/gdv_widget.hpp"
#include "Generic/element_array.hpp"
#include "gdv_application.hpp"

/*! \class ElementList
 *   \brief This class represent a element list
 */
template <typename ElementClass, typename Position = std::nullptr_t> class ElementList : public Gtk::Box, GdvWidget
{
	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////////// Row ///////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
	class Row : public Gtk::Box
	{
		// Atrributes
	private:
		Gtk::Button remove_button_;
		Gtk::Label	label_;

		generic::Element& element_;

		// Constructor
	public:
		Row(ElementList& list, generic::Element& element) : element_(element)
		{
			set_margin_start(10);
			set_margin_end(10);
			set_margin_bottom(5);
			set_margin_top(5);

			set_spacing(10);

			remove_button_.set_image_from_icon_name("list-remove-symbolic", Gtk::ICON_SIZE_MENU);
			remove_button_.set_relief(Gtk::RELIEF_NONE);
			pack_end(remove_button_, false, false);
			remove_button_.signal_clicked().connect(
					sigc::bind<-1, generic::Element&>(mem_fun(list, &ElementList::onRemoveClicked), element));

			label_.set_text(element.nameId());
			pack_start(label_, false, false);
		}

		// Getter
	public:
		inline generic::Element& carItem() const
		{
			return element_;
		}

		inline void setLabel()
		{
			label_.set_text(element_.nameId());
		}
	};

	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Signals ///////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
public:
	typedef sigc::signal<void>					  type_signal_add;
	typedef sigc::signal<void, generic::Element&> type_signal_element_remove;
	typedef sigc::signal<void, generic::Element*> type_signal_element_selected;



	inline type_signal_add signalAdd()
	{
		return signal_add_;
	}
	inline type_signal_element_remove signalRemove()
	{
		return signal_remove_;
	}
	inline type_signal_element_selected signalSelected()
	{
		return signal_selected_;
	}


protected:
	inline void emitSignalAdd()
	{
		signalAdd().emit();
	}
	inline void emitSignalRemove(generic::Element& element)
	{
		signalRemove().emit(element);
	}
	inline void emitSignalSelected(generic::Element* element)
	{
		signalSelected().emit(element);
	}

private:
	type_signal_add				 signal_add_;
	type_signal_element_remove	 signal_remove_;
	type_signal_element_selected signal_selected_;

	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Attributes ////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
private:
	Position			   pos_;
	generic::ElementArray* array_ = nullptr;

	Gtk::ScrolledWindow scrolled_windows_;

	Gtk::ListBox* list_ = nullptr;
	Gtk::Label	  no_element_;

	Gtk::Box	header_box_;
	Gtk::Label	title_;
	Gtk::Button add_button_;

	sigc::connection connection_array_added_;
	sigc::connection connection_array_removed_;

	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Constructor ///////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
public:
	/*!
	 *  \brief Constructor
	 */
	ElementList() : Box(Gtk::ORIENTATION_VERTICAL, 10)
	{
		set_homogeneous(false);


		pack_start(header_box_, false, false);
		header_box_.set_valign(Gtk::ALIGN_START);
		header_box_.set_spacing(15);

		header_box_.pack_start(add_button_, false, false);
		add_button_.set_image_from_icon_name("list-add-symbolic", Gtk::ICON_SIZE_MENU);
		add_button_.signal_clicked().connect(mem_fun(*this, &ElementList::onAddClicked));

		header_box_.pack_end(title_, true, true);
		title_.set_text(Glib::ustring::compose(_("Select %1"), ElementClass::userName(1, Determiner::YOUR)));
		title_.set_halign(Gtk::ALIGN_START);

		no_element_.set_text(Glib::ustring::compose(_("No %1 in the car"), ElementClass::userName()));
		no_element_.show();

		pack_start(scrolled_windows_);
		scrolled_windows_.set_vexpand(true);
		scrolled_windows_.set_valign(Gtk::ALIGN_FILL);
		scrolled_windows_.set_shadow_type(Gtk::SHADOW_IN);
		scrolled_windows_.set_policy(Gtk::POLICY_NEVER, Gtk::POLICY_AUTOMATIC);
	}

	~ElementList()
	{
	}

	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Function //////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
private:
	void addElement(generic::Element& ci)
	{
		ci.signalChanged().connect(mem_fun(*this, &ElementList::onElementChanged));
		ci.signalNeedSort().connect(mem_fun(*this, &ElementList::onElementNeedSort));

		Row* row = manage(new Row(*this, ci));
		list_->append(*row);
	}
	void removeElement(guint index)
	{
		Gtk::ListBoxRow* row = list_->get_row_at_index(static_cast<int>(index));
		list_->remove(*row);
	}

public:
	void setArray(Position pos = nullptr)
	{
		array_ = &app()->car()->template elements<ElementClass>(pos);
		pos_   = pos;

		if (list_ != nullptr)
			delete list_;

		scrolled_windows_.remove();
		list_ = manage(new Gtk::ListBox());
		list_->set_placeholder(no_element_);
		list_->set_vexpand(true);
		scrolled_windows_.add(*list_);
		list_->signal_row_selected().connect(mem_fun(*this, &ElementList::onRowSelected));
		list_->set_sort_func(mem_fun(*this, &ElementList::onSortRow));
		list_->set_header_func(mem_fun(*this, &ElementList::onUpdateHeader));

		for (generic::Element& element : *array_)
		{
			addElement(element);
		}

		if (array_->size() > 0)
			list_->select_row(*list_->get_row_at_index(0));
		else
			emitSignalSelected(nullptr);

		connection_array_added_.disconnect();
		connection_array_added_ = array_->signalAdded().connect(mem_fun(*this, &ElementList::onElementAdded));
		connection_array_removed_.disconnect();
		connection_array_removed_ = array_->signalRemoved().connect(mem_fun(*this, &ElementList::onElementRemoved));

		show_all();
	}



	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Slots /////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
private:
	int onSortRow(Gtk::ListBoxRow* list_row1, Gtk::ListBoxRow* list_row2)
	{
		Row* row1 = static_cast<Row*>(list_row1->get_child());
		Row* row2 = static_cast<Row*>(list_row2->get_child());
		return -row1->carItem().compareDate(row2->carItem());
	}

	void onUpdateHeader(Gtk::ListBoxRow* row, Gtk::ListBoxRow* before)
	{
		if (before != nullptr)
		{
			if (row->get_header() == nullptr)
			{
				auto separator = Gtk::manage(new Gtk::Separator(Gtk::ORIENTATION_HORIZONTAL));
				row->set_header(*separator);
				separator->show();
			}
		}
	}

	void onElementAdded(generic::Element& ci)
	{
		addElement(ci);
		show_all();
		list_->select_row(*list_->get_row_at_index(0));
	}
	void onElementRemoved([[maybe_unused]] database::ItemPtr ci, guint index)
	{
		removeElement(index);
	}
	void onElementChanged(database::Item& dbi)
	{
		list_->foreach (
				[&](Widget& widget)
				{
					auto& list_row = static_cast<Gtk::ListBoxRow&>(widget);
					Row*  row	   = static_cast<Row*>(list_row.get_child());
					if (row->carItem().sameDatabaseId(dbi))
						row->setLabel();
				});
	}
	void onElementNeedSort([[maybe_unused]] generic::Element& ci)
	{
		for (Gtk::Widget* widget : list_->get_children())
		{
			auto list_row = static_cast<Gtk::ListBoxRow*>(widget);
			Row* row	  = static_cast<Row*>(list_row->get_child());
			if (row->carItem() == ci)
			{
				list_row->changed();
				break;
			}
		}
	}

	void onRowSelected(Gtk::ListBoxRow* list_row)
	{
		if (list_row == nullptr)
		{
			emitSignalSelected(nullptr);
			return;
		}

		Row* row = static_cast<Row*>(list_row->get_child());

		emitSignalSelected(&(row->carItem()));
	}
	inline void onRemoveClicked(generic::Element& ci)
	{
		ElementClass& element = static_cast<ElementClass&>(ci);
		try
		{
			app()->car()->template removeElement<ElementClass>(element);
		}
		catch (core::Exception& e)
		{
			GDV_APP_SEND_ERROR(Glib::ustring::compose(_("Cannot remove element %1."), element.nameId()), e.what());
		}
		emitSignalRemove(ci);
	}
	inline void onAddClicked()
	{
		try
		{
			app()->car()->template addElement<ElementClass>(pos_, true);
		}
		catch (core::Exception& e)
		{
			GDV_APP_SEND_ERROR(_("Cannot add element."), e.what());
		}
		emitSignalAdd();
	}
};

#endif	  // ELEMENT_LIST_LIST_H_INCLUDED
