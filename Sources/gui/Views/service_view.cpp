/*!
 * \file    service_view.cpp
 * \author  Remi BERTHO
 * \date    15/04/18
 * \version 1.0.0
 */

/*
 * service_view.cpp
 *
 * Copyright 2016-2018 Remi BERTHO <remi.bertho@dalan.fr>
 *
 * This file is part of GDV.
 *
 * GDV is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * GDV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */


#include "log.hpp"

#include "service_view.hpp"

#include "internationalization.hpp"
#include <iostream>
#include "gdv_application.hpp"
#include "exception.hpp"
#include "share.hpp"
#include "Windows/main_window.hpp"


using namespace Gtk;
using namespace Glib;
using namespace std;
using namespace core;
using namespace sigc;
using namespace database;
using namespace car;
using namespace vehicle;
using namespace generic;

///////////////////////////////////////////////////////////////////////////////////
///////////////////////// AgencyWindow ////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////

ServiceView::GarageWindow::GarageWindow(Window::BaseObjectType* cobject, const Glib::RefPtr<Builder>& refGlade) : PlaceWindow(cobject)
{
	refGlade->get_widget("garage_ok", apply_button_);
	refGlade->get_widget("garage_cancel", cancel_button_);

	refGlade->get_widget("garage_name_entry", name_entry_);
	refGlade->get_widget("garage_brand_entry", brand_entry_);
	refGlade->get_widget("garage_address_entry", address_entry_);
	refGlade->get_widget("garage_web_address_entry", web_address_entry_);

	connectButtons();
}

void ServiceView::GarageWindow::launch(Window* parent_window, bool cancellable, std::shared_ptr<place::Place> place)
{
	auto garage = dynamic_pointer_cast<place::Garage>(place);

	name_entry_->set_text(garage->name());
	brand_entry_->set_text(garage->brand());
	address_entry_->set_text(garage->address());
	web_address_entry_->set_text(garage->webAddress());

	if (cancellable)
	{
		cancel_button_->show();
	}
	else
	{
		cancel_button_->hide();
	}

	bool apply = internalLaunch(parent_window);

	if (apply == true)
	{
		garage->setName(name_entry_->get_text());
		garage->setBrand(brand_entry_->get_text());
		garage->setAddress(address_entry_->get_text());
		garage->setWebAddress(web_address_entry_->get_text());
	}
}


///////////////////////////////////////////////////////////////////////////////////
/////////////////////////// Constructor ///////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////
ServiceView::ServiceView(BaseObjectType* cobject, const RefPtr<Builder>& refGlade, MainWindow& window) : View(window), Grid(cobject)
{
	// Get widgets
	attach(list_, 0, 2, 1, 2);
	list_.set_size_request(250, -1);


	refGlade->get_widget_derived("garage_window", garage_window_);


	refGlade->get_widget_derived("service_total_price_entry", total_price_entry_, "€");
	refGlade->get_widget_derived("service_mean_price_entry", mean_price_entry_, "€");
	refGlade->get_widget_derived("service_mean_distance_entry", mean_distance_entry_, "km");
	refGlade->get_widget_derived("service_mean_duration_entry", mean_duration_entry_, _("day"));

	refGlade->get_widget("selected_service_grid", selected_grid_);

	refGlade->get_widget_derived("service_begin_km_spinbutton", begin_km_spinbutton_, "km");
	refGlade->get_widget_derived("service_price_spinbutton", price_spinbutton_, "€");
	refGlade->get_widget_derived("service_delay_year_spinbutton", delay_year_spinbutton_, "year");
	refGlade->get_widget_derived("service_delay_km_spinbutton", delay_km_spinbutton_, "km");

	refGlade->get_widget_derived("service_age_entry", age_entry_, _("day"));
	refGlade->get_widget_derived("service_day_before_entry", day_before_entry_, _("day"));
	refGlade->get_widget("service_end_date_entry", end_date_entry_);
	refGlade->get_widget_derived("service_km_entry", km_entry_, "km");
	refGlade->get_widget_derived("service_km_before_entry", km_before_entry_, "km");
	refGlade->get_widget_derived("service_end_km_entry", end_km_entry_, "km");
	refGlade->get_widget_derived("service_price_per_month_entry", price_per_month_entry_, "€");
	refGlade->get_widget_derived("service_price_per_100km_entry", price_per_100km_entry_, "€");

	refGlade->get_widget("service_end_km_label", end_km_label_);
	refGlade->get_widget("service_end_date_label", end_date_label_);
	refGlade->get_widget("service_age_label", age_duration_label_);


	garage_selector_ = manage(new ObjectSelector<place::Garage, false>());
	selected_grid_->attach(*garage_selector_, 1, 1, 1, 1);


	begin_date_button_ = manage(new CalendarButton(CalendarButton::DOWN));
	selected_grid_->attach(*begin_date_button_, 1, 0, 1, 1);

	operation_list_ = manage(new ArticleList<Service, Service::Operation>(ArticleList<Service, Service::Operation>::DOWN));
	selected_grid_->attach(*operation_list_, 1, 4, 1, 1);


	// Event
	connectionAdd(list_.signalSelected().connect(mem_fun(*this, &ServiceView::onServiceSelected)));

	connectObjectSelector(*garage_selector_, *garage_window_);
	connectSpinButton(*begin_km_spinbutton_);
	connectSpinButton(*price_spinbutton_);
	connectSpinButton(*delay_year_spinbutton_);
	connectSpinButton(*delay_km_spinbutton_);
	connectCalendarButton(*begin_date_button_);

	show_all();
}



///////////////////////////////////////////////////////////////////////////////////
/////////////////////////// Slots /////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////
void ServiceView::onApplicationVehicleChanged(VehiclePtr vehicle)
{
	if (vehicle != nullptr)
	{
		list_.set_sensitive(true);

		list_.setArray();
		connectElementArrayChanged(app()->car()->elements<Service>());
	}
	else
	{
		list_.set_sensitive(false);

		onServiceSelected(nullptr);
	}
}

void ServiceView::onGuiChange()
{
	Service* selected_service = static_cast<Service*>(selectedDbi());

	guint begin_km = static_cast<guint>(begin_km_spinbutton_->get_value_as_int());
	guint delay_km = static_cast<guint>(delay_km_spinbutton_->get_value_as_int());

	selected_service->setBeginDate(begin_date_button_->getDate(), Element::ALL);
	selected_service->setGarageId(garage_selector_->getSelectedId());
	selected_service->setBeginKm(begin_km, Element::ALL);
	selected_service->setPrice(price_spinbutton_->get_value());
	selected_service->setDelayYear(static_cast<guint>(delay_year_spinbutton_->get_value_as_int()), Element::END);
	selected_service->setDelayKm(delay_km, Element::END);
}

void ServiceView::onItemChanged(Item& dbi)
{
	const Service& service		= static_cast<Service&>(dbi);
	guint		   car_distance = app()->vehicle()->distance();

	onArrayChanged();

	begin_date_button_->setDate(service.beginDate());
	garage_selector_->setSelectedId(service.garageId());
	begin_km_spinbutton_->set_value(service.beginKm());
	price_spinbutton_->set_value(service.price());
	delay_year_spinbutton_->set_value(service.delayYear());
	delay_km_spinbutton_->set_value(service.delayKm());
	end_date_entry_->set_text(service.endDate().format_string("%x"));
	end_km_entry_->setText(uintToString(service.endKm()));
	price_per_month_entry_->setText(doubleToString(service.pricePerMonth(), 2));
	price_per_100km_entry_->setText(doubleToString(service.pricePer100Km(), 2));
	if (service.currentlyUsed())
	{
		end_km_label_->set_text(_("Maximum km before the next"));
		end_date_label_->set_text(_("Maximum date before the next"));
		age_duration_label_->set_text(_("Age in day"));
		day_before_entry_->setText(intToString(service.dayBefore()));
		km_before_entry_->setText(intToString(service.kmBefore(car_distance)));
		age_entry_->setText(uintToString(service.age()));
		km_entry_->setText(uintToString(service.km(car_distance)));
	}
	else
	{
		end_km_label_->set_text(_("End distance"));
		end_date_label_->set_text(_("End date"));
		age_duration_label_->set_text(_("Duration in day"));
		day_before_entry_->setText("-");
		km_before_entry_->setText("-");
		age_entry_->setText(uintToString(service.duration()));
		km_entry_->setText(doubleToString(service.distance(), 0));
	}
}

void ServiceView::onArrayChanged()
{
	ElementArray& services = app()->car()->elements<Service>();

	total_price_entry_->setText(doubleToString(services.totalPrice(), 2));
	mean_price_entry_->setText(doubleToString(services.meanPrice(), 2));
	mean_duration_entry_->setText(uintToString(services.meanDuration()));
	mean_distance_entry_->setText(doubleToString(services.meanDistance(), 0));
}

void ServiceView::onServiceSelected(Element* ci)
{
	setSelectedDbi(ci);

	if (ci == nullptr)
	{
		garage_selector_->set_sensitive(false);
		begin_km_spinbutton_->set_sensitive(false);
		price_spinbutton_->set_sensitive(false);
		delay_year_spinbutton_->set_sensitive(false);
		delay_km_spinbutton_->set_sensitive(false);
		begin_date_button_->set_sensitive(false);
		operation_list_->set_sensitive(false);

		operation_list_->setElement(nullptr);
	}
	else
	{
		// Block GUI signals to avoid getting them when GUI init
		connectionBlock();

		garage_selector_->set_sensitive(true);
		begin_km_spinbutton_->set_sensitive(true);
		price_spinbutton_->set_sensitive(true);
		delay_year_spinbutton_->set_sensitive(true);
		delay_km_spinbutton_->set_sensitive(true);
		begin_date_button_->set_sensitive(true);
		operation_list_->set_sensitive(true);

		operation_list_->setElement(static_cast<Service*>(ci));

		onItemChanged(*selectedDbi());

		connectionUnblock();

		connectCurrentDbiChanged();
	}
}
