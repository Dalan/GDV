/*!
 * \file    inspection_view.cpp
 * \author  Remi BERTHO
 * \date    15/04/18
 * \version 1.0.0
 */

/*
 * inspection_view.cpp
 *
 * Copyright 2016-2018 Remi BERTHO <remi.bertho@dalan.fr>
 *
 * This file is part of GDV.
 *
 * GDV is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * GDV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */


#include "log.hpp"

#include "inspection_view.hpp"

#include "internationalization.hpp"
#include <iostream>
#include "gdv_application.hpp"
#include "exception.hpp"
#include "share.hpp"
#include "Windows/main_window.hpp"


using namespace Gtk;
using namespace Glib;
using namespace std;
using namespace core;
using namespace sigc;
using namespace database;
using namespace car;
using namespace vehicle;
using namespace generic;
using namespace place;

///////////////////////////////////////////////////////////////////////////////////
///////////////////////// AgencyWindow ////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////

InspectionView::CenterWindow::CenterWindow(Window::BaseObjectType* cobject, const Glib::RefPtr<Builder>& refGlade) : PlaceWindow(cobject)
{
	refGlade->get_widget("inspection_center_ok", apply_button_);
	refGlade->get_widget("inspection_center_cancel", cancel_button_);

	refGlade->get_widget("inspection_center_name_entry", name_entry_);
	refGlade->get_widget("inspection_center_brand_entry", brand_entry_);
	refGlade->get_widget("inspection_center_address_entry", address_entry_);
	refGlade->get_widget("inspection_center_web_address_entry", web_address_entry_);

	connectButtons();
}

void InspectionView::CenterWindow::launch(Window* parent_window, bool cancellable, std::shared_ptr<place::Place> place)
{
	auto center = dynamic_pointer_cast<InspectionCenter>(place);

	name_entry_->set_text(center->name());
	brand_entry_->set_text(center->brand());
	address_entry_->set_text(center->address());
	web_address_entry_->set_text(center->webAddress());

	if (cancellable)
	{
		cancel_button_->show();
	}
	else
	{
		cancel_button_->hide();
	}

	bool apply = internalLaunch(parent_window);

	if (apply == true)
	{
		center->setName(name_entry_->get_text());
		center->setBrand(brand_entry_->get_text());
		center->setAddress(address_entry_->get_text());
		center->setWebAddress(web_address_entry_->get_text());
	}
}


///////////////////////////////////////////////////////////////////////////////////
/////////////////////////// Constructor ///////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////
InspectionView::InspectionView(BaseObjectType* cobject, const RefPtr<Builder>& refGlade, MainWindow& window) : View(window), Grid(cobject)
{
	// Get widgets
	attach(list_, 0, 2, 1, 2);
	list_.set_size_request(250, -1);


	refGlade->get_widget_derived("inspection_center_window", center_window_);


	refGlade->get_widget_derived("inspection_total_price_entry", total_price_entry_, "€");
	refGlade->get_widget_derived("inspection_mean_price_entry", mean_price_entry_, "€");
	refGlade->get_widget_derived("inspection_mean_distance_entry", mean_distance_entry_, "km");
	refGlade->get_widget_derived("inspection_mean_duration_entry", mean_duration_entry_, _("day"));

	refGlade->get_widget("selected_inspection_grid", selected_grid_);

	refGlade->get_widget_derived("inspection_begin_km_spinbutton", begin_km_spinbutton_, "km");
	refGlade->get_widget_derived("inspection_price_spinbutton", price_spinbutton_, "€");
	refGlade->get_widget_derived("inspection_delay_year_spinbutton", delay_year_spinbutton_, "year");
	refGlade->get_widget_derived("inspection_delay_km_spinbutton", delay_km_spinbutton_, "km");

	refGlade->get_widget_derived("inspection_age_entry", age_entry_, _("day"));
	refGlade->get_widget_derived("inspection_day_before_entry", day_before_entry_, _("day"));
	refGlade->get_widget("inspection_end_date_entry", end_date_entry_);
	refGlade->get_widget_derived("inspection_km_entry", km_entry_, "km");
	refGlade->get_widget_derived("inspection_km_before_entry", km_before_entry_, "km");
	refGlade->get_widget_derived("inspection_end_km_entry", end_km_entry_, "km");
	refGlade->get_widget_derived("inspection_price_per_month_entry", price_per_month_entry_, "€");
	refGlade->get_widget_derived("inspection_price_per_100km_entry", price_per_100km_entry_, "€");

	refGlade->get_widget("inspection_end_km_label", end_km_label_);
	refGlade->get_widget("inspection_end_date_label", end_date_label_);
	refGlade->get_widget("inspection_age_label", age_duration_label_);


	center_selector_ = manage(new ObjectSelector<place::InspectionCenter, false>());
	selected_grid_->attach(*center_selector_, 1, 1, 1, 1);


	begin_date_button_ = manage(new CalendarButton(CalendarButton::DOWN));
	selected_grid_->attach(*begin_date_button_, 1, 0, 1, 1);


	repeat_date_button_ = manage(new CalendarButton(CalendarButton::DOWN));
	selected_grid_->attach(*repeat_date_button_, 1, 4, 1, 1);


	// Event
	connectionAdd(list_.signalSelected().connect(mem_fun(*this, &InspectionView::onInspectionSelected)));

	connectObjectSelector(*center_selector_, *center_window_);
	connectSpinButton(*begin_km_spinbutton_);
	connectSpinButton(*price_spinbutton_);
	connectSpinButton(*delay_year_spinbutton_);
	connectSpinButton(*delay_km_spinbutton_);
	connectCalendarButton(*begin_date_button_);
	connectCalendarButton(*repeat_date_button_);

	show_all();
}



///////////////////////////////////////////////////////////////////////////////////
/////////////////////////// Slots /////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////
void InspectionView::onApplicationVehicleChanged(VehiclePtr vehicle)
{
	if (vehicle != nullptr)
	{
		list_.set_sensitive(true);

		list_.setArray();
		connectElementArrayChanged(app()->car()->elements<Inspection>());
	}
	else
	{
		list_.set_sensitive(false);

		onInspectionSelected(nullptr);
	}
}

void InspectionView::onGuiChange()
{
	Inspection* selected_inspection = static_cast<Inspection*>(selectedDbi());

	guint begin_km = static_cast<guint>(begin_km_spinbutton_->get_value_as_int());
	guint delay_km = static_cast<guint>(delay_km_spinbutton_->get_value_as_int());

	selected_inspection->setBeginDate(begin_date_button_->getDate(), Element::ALL);
	selected_inspection->setCenterId(center_selector_->getSelectedId());
	selected_inspection->setRepeatDate(repeat_date_button_->getDate());
	selected_inspection->setBeginKm(begin_km, Element::ALL);
	selected_inspection->setPrice(price_spinbutton_->get_value());
	selected_inspection->setDelayYear(static_cast<guint>(delay_year_spinbutton_->get_value_as_int()), Element::END);
	selected_inspection->setDelayKm(delay_km, Element::END);
}

void InspectionView::onItemChanged(Item& dbi)
{
	const Inspection& inspection   = static_cast<Inspection&>(dbi);
	guint			  car_distance = app()->vehicle()->distance();

	onArrayChanged();

	begin_date_button_->setDate(inspection.beginDate());
	repeat_date_button_->setDate(inspection.repeatDate());
	center_selector_->setSelectedId(inspection.centerId());
	begin_km_spinbutton_->set_value(inspection.beginKm());
	price_spinbutton_->set_value(inspection.price());
	delay_year_spinbutton_->set_value(inspection.delayYear());
	delay_km_spinbutton_->set_value(inspection.delayKm());
	end_date_entry_->set_text(inspection.endDate().format_string("%x"));
	end_km_entry_->setText(uintToString(inspection.endKm()));
	price_per_month_entry_->setText(doubleToString(inspection.pricePerMonth(), 2));
	price_per_100km_entry_->setText(doubleToString(inspection.pricePer100Km(), 2));
	if (inspection.currentlyUsed())
	{
		end_km_label_->set_text(_("Maximum km before the next"));
		end_date_label_->set_text(_("Maximum date before the next"));
		age_duration_label_->set_text(_("Age in day"));
		day_before_entry_->setText(intToString(inspection.dayBefore()));
		km_before_entry_->setText(intToString(inspection.kmBefore(car_distance)));
		age_entry_->setText(uintToString(inspection.age()));
		km_entry_->setText(uintToString(inspection.km(car_distance)));
	}
	else
	{
		end_km_label_->set_text(_("End distance"));
		end_date_label_->set_text(_("End date"));
		age_duration_label_->set_text(_("Duration in day"));
		day_before_entry_->setText("-");
		km_before_entry_->setText("-");
		age_entry_->setText(uintToString(inspection.duration()));
		km_entry_->setText(doubleToString(inspection.distance(), 0));
	}
}

void InspectionView::onArrayChanged()
{
	ElementArray& inspections = app()->car()->elements<Inspection>();

	total_price_entry_->setText(doubleToString(inspections.totalPrice(), 2));
	mean_price_entry_->setText(doubleToString(inspections.meanPrice(), 2));
	mean_duration_entry_->setText(uintToString(inspections.meanDuration()));
	mean_distance_entry_->setText(doubleToString(inspections.meanDistance(), 0));
}

void InspectionView::onInspectionSelected(Element* ci)
{
	setSelectedDbi(ci);

	if (ci == nullptr)
	{
		center_selector_->set_sensitive(false);
		begin_km_spinbutton_->set_sensitive(false);
		price_spinbutton_->set_sensitive(false);
		delay_year_spinbutton_->set_sensitive(false);
		delay_km_spinbutton_->set_sensitive(false);
		begin_date_button_->set_sensitive(false);
		repeat_date_button_->set_sensitive(false);
	}
	else
	{
		// Block GUI signals to avoid getting them when GUI init
		connectionBlock();

		center_selector_->set_sensitive(true);
		begin_km_spinbutton_->set_sensitive(true);
		price_spinbutton_->set_sensitive(true);
		delay_year_spinbutton_->set_sensitive(true);
		delay_km_spinbutton_->set_sensitive(true);
		begin_date_button_->set_sensitive(true);
		repeat_date_button_->set_sensitive(true);

		onItemChanged(*selectedDbi());

		connectionUnblock();

		connectCurrentDbiChanged();
	}
}
