/*!
 * \file    wiper_view.h
 * \author  Remi BERTHO
 * \date    15/04/18
 * \version 1.0.0
 */

/*
 * wiper_view.h
 *
 * Copyright 2016-2018 Remi BERTHO <remi.bertho@dalan.fr>
 *
 * This file is part of GDV.
 *
 * GDV is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * GDV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */


#ifndef WIPER_VIEW_H_INCLUDED
#define WIPER_VIEW_H_INCLUDED

#include <gtkmm.h>

#include "Car/car.hpp"
#include "Car/wiper.hpp"

#include "Widgets/element_list.hpp"
#include "view.hpp"
#include "Widgets/calendar_button.hpp"
#include "Widgets/unit_entry.hpp"
#include "Widgets/unit_spinbutton.hpp"

/*! \class WiperView
 *   \brief This class represent the wiper view
 */
class WiperView : public View, public Gtk::Grid
{
	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Attributes ////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
private:
	UnitEntry* total_price_entry_;
	UnitEntry* mean_price_entry_;
	UnitEntry* mean_distance_entry_;
	UnitEntry* mean_duration_entry_;

	std::array<Gtk::RadioButton*, car::Wiper::NB_POSITION> position_buttons_;

	Gtk::Grid* selected_grid_;

	CalendarButton* begin_date_button_;
	Gtk::Entry*		brand_entry_;
	Gtk::Entry*		model_entry_;
	UnitSpinButton* begin_km_spinbutton_;
	UnitSpinButton* price_spinbutton_;
	UnitSpinButton* delay_year_spinbutton_;
	UnitSpinButton* delay_km_spinbutton_;

	UnitEntry*	end_km_entry_;
	UnitEntry*	age_entry_;
	UnitEntry*	day_before_entry_;
	Gtk::Entry* end_date_entry_;
	UnitEntry*	km_entry_;
	UnitEntry*	km_before_entry_;
	UnitEntry*	price_per_month_entry_;
	UnitEntry*	price_per_100km_entry_;

	Gtk::Label* end_km_label_;
	Gtk::Label* end_date_label_;
	Gtk::Label* age_duration_label_;

	ElementList<car::Wiper, car::Wiper::Position> list_;

	car::Wiper::Position selected_position_ = car::Wiper::FRONT;

	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Constructor ///////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
public:
	/*!
	 *  \brief Constructor with builder
	 *  \param cobject the C object
	 *  \param refGlade the builder
	 */
	WiperView(BaseObjectType* cobject, const Glib::RefPtr<Gtk::Builder>& refGlade, MainWindow& window);

	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Functions /////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
	inline virtual Glib::ustring name() override
	{
		return car::Wiper::DATABASE_TABLE_NAME;
	}

	inline virtual Glib::ustring title() override
	{
		return car::Wiper::userName(2);
	}

	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Slots /////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
protected:
	/**
	 * @brief Function called when the vehicle is changed in the application
	 * @param vehicle the vehicle
	 */
	virtual void onApplicationVehicleChanged(vehicle::VehiclePtr vehicle) override;

	/**
	 * @brief Function called when the user change somesthing in the UI
	 */
	virtual void onGuiChange() override;

	/**
	 * @brief Function called when the wiper changed
	 * @param dbi the wiper as Item
	 */
	virtual void onItemChanged(database::Item& dbi) override;

	/**
	 * @brief Function called when the array of elements changed
	 */
	virtual void onArrayChanged() override;

	/**
	 * @brief Function called when the user select a wiper
	 * @param need the selected need
	 */
	void onWiperSelected(generic::Element* ci);

	/**
	 * @brief Function called when the user select a wiper
	 * @param need the selected need
	 */
	void onWiperPositionSelected(car::Wiper::Position position);
};



#endif	  // WIPER_VIEW_H_INCLUDED
