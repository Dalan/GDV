/*!
 * \file    wiper_view.cpp
 * \author  Remi BERTHO
 * \date    15/04/18
 * \version 1.0.0
 */

/*
 * wiper_view.cpp
 *
 * Copyright 2016-2018 Remi BERTHO <remi.bertho@dalan.fr>
 *
 * This file is part of GDV.
 *
 * GDV is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * GDV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */


#include "log.hpp"

#include "wiper_view.hpp"

#include "internationalization.hpp"
#include <iostream>
#include "gdv_application.hpp"
#include "exception.hpp"
#include "share.hpp"
#include "Windows/main_window.hpp"


using namespace Gtk;
using namespace Glib;
using namespace std;
using namespace core;
using namespace sigc;
using namespace database;
using namespace car;
using namespace vehicle;
using namespace generic;

///////////////////////////////////////////////////////////////////////////////////
/////////////////////////// Constructor ///////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////
WiperView::WiperView(BaseObjectType* cobject, const RefPtr<Builder>& refGlade, MainWindow& window) : View(window), Grid(cobject)
{
	// Get widgets
	attach(list_, 0, 3, 1, 2);
	list_.set_size_request(250, -1);


	refGlade->get_widget_derived("wiper_total_price_entry", total_price_entry_, "€");
	refGlade->get_widget_derived("wiper_mean_price_entry", mean_price_entry_, "€");
	refGlade->get_widget_derived("wiper_mean_distance_entry", mean_distance_entry_, "km");
	refGlade->get_widget_derived("wiper_mean_duration_entry", mean_duration_entry_, _("day"));

	refGlade->get_widget("wiper_position_front", position_buttons_[Wiper::FRONT]);
	refGlade->get_widget("wiper_position_bottom", position_buttons_[Wiper::BOTTOM]);

	refGlade->get_widget("selected_wiper_grid", selected_grid_);

	refGlade->get_widget("wiper_brand_entry", brand_entry_);
	refGlade->get_widget("wiper_model_entry", model_entry_);
	refGlade->get_widget_derived("wiper_begin_km_spinbutton", begin_km_spinbutton_, "km");
	refGlade->get_widget_derived("wiper_price_spinbutton", price_spinbutton_, "€");
	refGlade->get_widget_derived("wiper_delay_year_spinbutton", delay_year_spinbutton_, "year");
	refGlade->get_widget_derived("wiper_delay_km_spinbutton", delay_km_spinbutton_, "km");

	refGlade->get_widget_derived("wiper_end_km_entry", end_km_entry_, "km");
	refGlade->get_widget_derived("wiper_age_entry", age_entry_, _("day"));
	refGlade->get_widget_derived("wiper_day_before_entry", day_before_entry_, _("day"));
	refGlade->get_widget("wiper_end_date_entry", end_date_entry_);
	refGlade->get_widget_derived("wiper_km_entry", km_entry_, "km");
	refGlade->get_widget_derived("wiper_km_before_entry", km_before_entry_, "km");
	refGlade->get_widget_derived("wiper_price_per_month_entry", price_per_month_entry_, "€");
	refGlade->get_widget_derived("wiper_price_per_100km_entry", price_per_100km_entry_, "€");

	refGlade->get_widget("wiper_end_km_label", end_km_label_);
	refGlade->get_widget("wiper_end_date_label", end_date_label_);
	refGlade->get_widget("wiper_age_label", age_duration_label_);


	begin_date_button_ = manage(new CalendarButton(CalendarButton::DOWN));
	selected_grid_->attach(*begin_date_button_, 1, 0, 1, 1);

	// Event
	connectionAdd(list_.signalSelected().connect(mem_fun(*this, &WiperView::onWiperSelected)));

	connectEntry(*brand_entry_);
	connectEntry(*model_entry_);
	connectSpinButton(*begin_km_spinbutton_);
	connectSpinButton(*price_spinbutton_);
	connectSpinButton(*delay_year_spinbutton_);
	connectSpinButton(*delay_km_spinbutton_);
	connectCalendarButton(*begin_date_button_);

	connectionAdd(position_buttons_[Wiper::FRONT]->signal_toggled().connect(
			sigc::bind<Wiper::Position>(mem_fun(*this, &WiperView::onWiperPositionSelected), Wiper::FRONT)));
	connectionAdd(position_buttons_[Wiper::BOTTOM]->signal_toggled().connect(
			sigc::bind<Wiper::Position>(mem_fun(*this, &WiperView::onWiperPositionSelected), Wiper::BOTTOM)));


	// Default value
	position_buttons_[Wiper::FRONT]->set_active(true);


	show_all();
}



///////////////////////////////////////////////////////////////////////////////////
/////////////////////////// Slots /////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////
void WiperView::onApplicationVehicleChanged(VehiclePtr vehicle)
{
	if (vehicle != nullptr)
	{
		list_.set_sensitive(true);
		for (auto& postion_button : position_buttons_)
			postion_button->set_sensitive(true);

		list_.setArray(selected_position_);
		connectElementArrayChanged(app()->car()->elements<Wiper>(selected_position_));
	}
	else
	{
		list_.set_sensitive(false);
		for (auto& postion_button : position_buttons_)
			postion_button->set_sensitive(false);

		onWiperSelected(nullptr);
	}
}

void WiperView::onGuiChange()
{
	Wiper* selected_wiper = static_cast<Wiper*>(selectedDbi());

	if (selected_wiper == nullptr)
		return;

	guint begin_km = static_cast<guint>(begin_km_spinbutton_->get_value_as_int());
	guint delay_km = static_cast<guint>(delay_km_spinbutton_->get_value_as_int());

	selected_wiper->setBeginDate(begin_date_button_->getDate(), Element::ALL);
	selected_wiper->setBrand(brand_entry_->get_text());
	selected_wiper->setModel(model_entry_->get_text());
	selected_wiper->setBeginKm(begin_km, Element::ALL);
	selected_wiper->setPrice(price_spinbutton_->get_value());
	selected_wiper->setDelayYear(static_cast<guint>(delay_year_spinbutton_->get_value_as_int()), Element::END);
	selected_wiper->setDelayKm(delay_km, Element::END);
}

void WiperView::onItemChanged(Item& dbi)
{
	const Wiper& wiper		  = static_cast<Wiper&>(dbi);
	guint		 car_distance = app()->vehicle()->distance();

	onArrayChanged();

	begin_date_button_->setDate(wiper.beginDate());
	brand_entry_->set_text(wiper.brand());
	model_entry_->set_text(wiper.model());
	begin_km_spinbutton_->set_value(wiper.beginKm());
	price_spinbutton_->set_value(wiper.price());
	delay_year_spinbutton_->set_value(wiper.delayYear());
	delay_km_spinbutton_->set_value(wiper.delayKm());
	end_date_entry_->set_text(wiper.endDate().format_string("%x"));
	end_km_entry_->setText(intToString(static_cast<int>(wiper.endKm())));
	price_per_month_entry_->setText(doubleToString(wiper.pricePerMonth(), 2));
	price_per_100km_entry_->setText(doubleToString(wiper.pricePer100Km(), 2));
	if (wiper.currentlyUsed())
	{
		end_km_label_->set_text(_("Maximum km before the next"));
		end_date_label_->set_text(_("Maximum date before the next"));
		age_duration_label_->set_text(_("Age in day"));
		day_before_entry_->setText(intToString(wiper.dayBefore()));
		km_before_entry_->setText(intToString(wiper.kmBefore(car_distance)));
		age_entry_->setText(uintToString(wiper.age()));
		km_entry_->setText(uintToString(wiper.km(car_distance)));
	}
	else
	{
		end_km_label_->set_text(_("End distance"));
		end_date_label_->set_text(_("End date"));
		age_duration_label_->set_text(_("Duration in day"));
		day_before_entry_->setText("-");
		km_before_entry_->setText("-");
		age_entry_->setText(uintToString(wiper.duration()));
		km_entry_->setText(doubleToString(wiper.distance(), 0));
	}
}

void WiperView::onWiperSelected(Element* ci)
{
	setSelectedDbi(ci);

	if (ci == nullptr)
	{
		brand_entry_->set_sensitive(false);
		model_entry_->set_sensitive(false);
		begin_km_spinbutton_->set_sensitive(false);
		price_spinbutton_->set_sensitive(false);
		delay_year_spinbutton_->set_sensitive(false);
		delay_km_spinbutton_->set_sensitive(false);
		begin_date_button_->set_sensitive(false);
	}
	else
	{
		// Block GUI signals to avoid getting them when GUI init
		connectionBlock();

		brand_entry_->set_sensitive(true);
		model_entry_->set_sensitive(true);
		begin_km_spinbutton_->set_sensitive(true);
		price_spinbutton_->set_sensitive(true);
		delay_year_spinbutton_->set_sensitive(true);
		delay_km_spinbutton_->set_sensitive(true);
		begin_date_button_->set_sensitive(true);

		onItemChanged(*selectedDbi());

		connectionUnblock();

		connectCurrentDbiChanged();
	}
}

void WiperView::onWiperPositionSelected(Wiper::Position position)
{
	if (position_buttons_[position]->get_active())
	{
		selected_position_ = position;
		list_.setArray(selected_position_);
		connectElementArrayChanged(app()->car()->elements<Wiper>(selected_position_));
		onArrayChanged();
	}
}

void WiperView::onArrayChanged()
{
	ElementArray& wipers = app()->car()->elements<Wiper>(selected_position_);

	total_price_entry_->setText(doubleToString(wipers.totalPrice(), 2));
	mean_price_entry_->setText(doubleToString(wipers.meanPrice(), 2));
	mean_duration_entry_->setText(uintToString(wipers.meanDuration()));
	mean_distance_entry_->setText(doubleToString(wipers.meanDistance(), 0));
}
