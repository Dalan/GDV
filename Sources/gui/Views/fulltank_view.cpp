/*!
 * \file    fulltank_view.cpp
 * \author  Remi BERTHO
 * \date    15/04/18
 * \version 1.0.0
 */

/*
 * fulltank_view.cpp
 *
 * Copyright 2016-2018 Remi BERTHO <remi.bertho@dalan.fr>
 *
 * This file is part of GDV.
 *
 * GDV is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * GDV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */


#include "log.hpp"

#include "fulltank_view.hpp"

#include "internationalization.hpp"
#include <iostream>
#include "gdv_application.hpp"
#include "exception.hpp"
#include "share.hpp"
#include "Windows/main_window.hpp"
#include "Place/gas_station.hpp"


using namespace Gtk;
using namespace Glib;
using namespace std;
using namespace core;
using namespace sigc;
using namespace database;
using namespace car;
using namespace vehicle;
using namespace place;
using namespace generic;



///////////////////////////////////////////////////////////////////////////////////
///////////////////////// GasStationWindow ////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////

FulltankView::GasStationWindow::GasStationWindow(Window::BaseObjectType* cobject, const Glib::RefPtr<Builder>& refGlade)
		: PlaceWindow(cobject)
{
	refGlade->get_widget("gas_station_ok", apply_button_);
	refGlade->get_widget("gas_station_cancel", cancel_button_);

	refGlade->get_widget("gas_station_name_entry", name_entry_);
	refGlade->get_widget("gas_station_brand_entry", brand_entry_);
	refGlade->get_widget("gas_station_address_entry", address_entry_);
	refGlade->get_widget("gas_station_cheap_switch", cheap_switch_);

	connectButtons();
}

void FulltankView::GasStationWindow::launch(Window* parent_window, bool cancellable, std::shared_ptr<place::Place> place)
{
	auto station = dynamic_pointer_cast<GasStation>(place);

	name_entry_->set_text(station->name());
	brand_entry_->set_text(station->brand());
	address_entry_->set_text(station->address());
	cheap_switch_->set_active(station->cheap());

	if (cancellable)
	{
		cancel_button_->show();
	}
	else
	{
		cancel_button_->hide();
	}

	bool apply = internalLaunch(parent_window);

	if (apply == true)
	{
		station->setName(name_entry_->get_text());
		station->setBrand(brand_entry_->get_text());
		station->setAddress(address_entry_->get_text());
		station->setCheap(cheap_switch_->get_active());
	}
}




///////////////////////////////////////////////////////////////////////////////////
/////////////////////////// Constructor ///////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////
FulltankView::FulltankView(BaseObjectType* cobject, const RefPtr<Builder>& refGlade, MainWindow& window) : View(window), Grid(cobject)
{
	// Get widgets
	attach(list_, 0, 2, 1, 2);
	list_.set_size_request(250, -1);

	refGlade->get_widget_derived("gas_station_window", station_window_);

	refGlade->get_widget_derived("fulltank_total_price_entry", total_price_entry_, "€");
	refGlade->get_widget_derived("fulltank_mean_price_entry", mean_price_entry_, "€");
	refGlade->get_widget_derived("fulltank_total_volume_entry", total_volume_entry_, "L");
	refGlade->get_widget_derived("fulltank_mean_volume_entry", mean_volume_entry_, "L");
	refGlade->get_widget_derived("fulltank_mean_distance_entry", mean_distance_entry_, "km");
	refGlade->get_widget_derived("fulltank_mean_duration_entry", mean_duration_entry_, _("day"));
	refGlade->get_widget_derived("fulltank_mean_consumption_per_month_entry", mean_consumption_per_month_entry_, "L");
	refGlade->get_widget_derived("fulltank_mean_consumption_entry", mean_consumption_entry_, "L/100km");

	refGlade->get_widget("selected_fulltank_grid", selected_grid_);

	refGlade->get_widget_derived("fulltank_price_per_liter_spinbutton", price_per_liter_spinbutton_, "€/L");
	refGlade->get_widget_derived("fulltank_distance_spinbutton", distance_spinbutton_, "km");
	refGlade->get_widget_derived("fulltank_volume_spinbutton", volume_spinbutton_, "L");
	refGlade->get_widget_derived("fulltank_bio_spinbutton", bio_spinbutton_, "%");
	refGlade->get_widget("fulltank_additive_switch", additive_switch_);

	refGlade->get_widget_derived("fulltank_consumption_entry", consumption_entry_, "L/100km");
	refGlade->get_widget_derived("fulltank_price_entry", price_entry_, "€");
	refGlade->get_widget_derived("fulltank_age_entry", age_entry_, _("day"));
	refGlade->get_widget("fulltank_end_date_entry", end_date_entry_);
	refGlade->get_widget_derived("fulltank_price_per_100km_entry", price_per_100km_entry_, "€");

	refGlade->get_widget("fulltank_age_label", age_duration_label_);


	// Set gas station
	station_selector_ = manage(new ObjectSelector<place::GasStation, false>());
	selected_grid_->attach(*station_selector_, 1, 6, 1, 1);


	begin_date_button_ = manage(new CalendarButton(CalendarButton::DOWN));
	selected_grid_->attach(*begin_date_button_, 1, 0, 1, 1);


	// Event
	connectionAdd(list_.signalSelected().connect(mem_fun(*this, &FulltankView::onFulltankSelected)));

	connectSpinButton(*price_per_liter_spinbutton_);
	connectSpinButton(*distance_spinbutton_);
	connectSpinButton(*volume_spinbutton_);
	connectSpinButton(*bio_spinbutton_);
	connectSwitch(*additive_switch_);
	connectCalendarButton(*begin_date_button_);
	connectObjectSelector(*station_selector_, *station_window_);

	show_all();
}



///////////////////////////////////////////////////////////////////////////////////
/////////////////////////// Slots /////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////
void FulltankView::onApplicationVehicleChanged(VehiclePtr vehicle)
{
	if (vehicle != nullptr)
	{
		list_.set_sensitive(true);

		list_.setArray();
		connectElementArrayChanged(app()->car()->elements<FullTank>());
	}
	else
	{
		list_.set_sensitive(false);

		onFulltankSelected(nullptr);
	}
}

void FulltankView::onGuiChange()
{
	FullTank* selected_fulltank = static_cast<FullTank*>(selectedDbi());

	double old_distance = selected_fulltank->distance();
	double new_distance = distance_spinbutton_->get_value();

	selected_fulltank->setBeginDate(begin_date_button_->getDate(), Element::ALL);
	selected_fulltank->setDistance(new_distance);
	selected_fulltank->setPricePerLiter(price_per_liter_spinbutton_->get_value());
	selected_fulltank->setVolume(volume_spinbutton_->get_value());
	selected_fulltank->setBio(static_cast<guint>(bio_spinbutton_->get_value_as_int()));
	selected_fulltank->setAdditive(additive_switch_->get_active());
	selected_fulltank->setStationId(station_selector_->getSelectedId());

	if (selected_fulltank->currentlyUsed())
	{
		guint car_distance = static_cast<guint>(app()->vehicle()->distance() + new_distance - old_distance);
		app()->vehicle()->setDistance(car_distance);
	}
}

void FulltankView::onItemChanged(Item& dbi)
{
	const FullTank& fulltank = static_cast<FullTank&>(dbi);

	onArrayChanged();

	begin_date_button_->setDate(fulltank.beginDate());
	distance_spinbutton_->set_value(fulltank.distance());
	price_per_liter_spinbutton_->set_value(fulltank.pricePerLiter());
	volume_spinbutton_->set_value(fulltank.volume());
	bio_spinbutton_->set_value(fulltank.bio());
	additive_switch_->set_active(fulltank.additive());
	end_date_entry_->set_text(fulltank.endDate().format_string("%x"));
	price_per_100km_entry_->setText(doubleToString(fulltank.pricePer100Km(), 2));
	price_entry_->setText(doubleToString(fulltank.price(), 2));
	station_selector_->setSelectedId(fulltank.stationId());
	if (fulltank.currentlyUsed())
	{
		age_duration_label_->set_text(_("Age in day"));
		age_entry_->setText(uintToString(fulltank.age()));
		consumption_entry_->setText("-");
	}
	else
	{
		age_duration_label_->set_text(_("Duration in day"));
		age_entry_->setText(uintToString(fulltank.duration()));
		consumption_entry_->setText(doubleToString(fulltank.consumption(), 2));
	}
}

void FulltankView::onArrayChanged()
{
	ElementArray& fulltanks = app()->car()->elements<FullTank>();

	total_price_entry_->setText(doubleToString(fulltanks.totalPrice(), 2));
	mean_price_entry_->setText(doubleToString(fulltanks.meanPrice(), 2));
	total_volume_entry_->setText(doubleToString(app()->car()->fullTanksTotalVolume()));
	mean_volume_entry_->setText(doubleToString(app()->car()->fullTanksMeanVolume(), 1));
	mean_duration_entry_->setText(uintToString(fulltanks.meanDuration()));
	mean_distance_entry_->setText(doubleToString(fulltanks.meanDistance(), 0));
	mean_consumption_per_month_entry_->setText(doubleToString(app()->car()->fullTanksMeanConsumptionPerMonth(), 2));
	mean_consumption_entry_->setText(doubleToString(app()->car()->fullTanksMeanConsumption(), 2));
}

void FulltankView::onFulltankSelected(Element* ci)
{
	setSelectedDbi(ci);

	if (ci == nullptr)
	{
		begin_date_button_->set_sensitive(false);
		price_per_liter_spinbutton_->set_sensitive(false);
		distance_spinbutton_->set_sensitive(false);
		volume_spinbutton_->set_sensitive(false);
		bio_spinbutton_->set_sensitive(false);
		additive_switch_->set_sensitive(false);
		station_selector_->set_sensitive(false);
	}
	else
	{
		// Block GUI signals to avoid getting them when GUI init
		connectionBlock();

		begin_date_button_->set_sensitive(true);
		price_per_liter_spinbutton_->set_sensitive(true);
		distance_spinbutton_->set_sensitive(true);
		volume_spinbutton_->set_sensitive(true);
		bio_spinbutton_->set_sensitive(true);
		additive_switch_->set_sensitive(true);
		station_selector_->set_sensitive(true);

		onItemChanged(*selectedDbi());

		connectionUnblock();

		connectCurrentDbiChanged();
	}
}
