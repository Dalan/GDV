/*!
 * \file    fulltank_view.h
 * \author  Remi BERTHO
 * \date    15/04/18
 * \version 1.0.0
 */

/*
 * fulltank_view.h
 *
 * Copyright 2016-2018 Remi BERTHO <remi.bertho@dalan.fr>
 *
 * This file is part of GDV.
 *
 * GDV is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * GDV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */


#ifndef FULLTANK_VIEW_H_INCLUDED
#define FULLTANK_VIEW_H_INCLUDED

#include <gtkmm.h>

#include "Car/car.hpp"
#include "Car/fulltank.hpp"

#include "Windows/place_window.hpp"
#include "Widgets/element_list.hpp"
#include "view.hpp"
#include "Widgets/calendar_button.hpp"
#include "Widgets/unit_entry.hpp"
#include "Widgets/unit_spinbutton.hpp"

/*! \class FulltankView
 *   \brief This class represent the fulltank view
 */
class FulltankView : public View, public Gtk::Grid
{
	class GasStationWindow : public PlaceWindow
	{
		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Attributes ////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	private:
		Gtk::Entry*	 name_entry_;
		Gtk::Entry*	 brand_entry_;
		Gtk::Entry*	 address_entry_;
		Gtk::Switch* cheap_switch_;

		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Constructor ///////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	public:
		GasStationWindow(BaseObjectType* cobject, const Glib::RefPtr<Gtk::Builder>& refGlade);



		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Function //////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	public:
		void launch(Gtk::Window* parent_window, bool cancellable, std::shared_ptr<place::Place> place) override;
	};




	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Attributes ////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
private:
	UnitEntry* total_price_entry_;
	UnitEntry* mean_price_entry_;
	UnitEntry* total_volume_entry_;
	UnitEntry* mean_volume_entry_;
	UnitEntry* mean_distance_entry_;
	UnitEntry* mean_duration_entry_;
	UnitEntry* mean_consumption_per_month_entry_;
	UnitEntry* mean_consumption_entry_;

	Gtk::Grid* selected_grid_;

	CalendarButton*							  begin_date_button_;
	UnitSpinButton*							  price_per_liter_spinbutton_;
	UnitSpinButton*							  distance_spinbutton_;
	UnitSpinButton*							  volume_spinbutton_;
	UnitSpinButton*							  bio_spinbutton_;
	Gtk::Switch*							  additive_switch_;
	ObjectSelector<place::GasStation, false>* station_selector_;

	UnitEntry*	consumption_entry_;
	UnitEntry*	age_entry_;
	Gtk::Entry* end_date_entry_;
	UnitEntry*	price_entry_;
	UnitEntry*	price_per_100km_entry_;

	Gtk::Label* age_duration_label_;

	ElementList<car::FullTank> list_;

	GasStationWindow* station_window_;

	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Constructor ///////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
public:
	/*!
	 *  \brief Constructor with builder
	 *  \param cobject the C object
	 *  \param refGlade the builder
	 */
	FulltankView(BaseObjectType* cobject, const Glib::RefPtr<Gtk::Builder>& refGlade, MainWindow& window);

	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Functions /////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
	inline virtual Glib::ustring name() override
	{
		return car::FullTank::DATABASE_TABLE_NAME;
	}

	inline virtual Glib::ustring title() override
	{
		return car::FullTank::userName(2);
	}

	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Slots /////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
protected:
	/**
	 * @brief Function called when the vehicle is changed in the application
	 * @param vehicle the vehicle
	 */
	void onApplicationVehicleChanged(vehicle::VehiclePtr vehicle) override;

	/**
	 * @brief Function called when the user change somesthing in the UI
	 */
	virtual void onGuiChange() override;

	/**
	 * @brief Function called when the fulltank changed
	 * @param dbi the fulltank as Item
	 */
	virtual void onItemChanged(database::Item& dbi) override;

	/**
	 * @brief Function called when the array of elements changed
	 */
	virtual void onArrayChanged() override;

	/**
	 * @brief Function called when the user select a fulltank
	 * @param need the selected need
	 */
	void onFulltankSelected(generic::Element* ci);
};



#endif	  // FULLTANK_VIEW_H_INCLUDED
