/*!
 * \file    view.cpp
 * \author  Remi BERTHO
 * \date    06/05/18
 * \version 1.0.0
 */

/*
 * view.cpp
 *
 * Copyright 2016-2018 Remi BERTHO <remi.bertho@dalan.fr>
 *
 * This file is part of GDV.
 *
 * GDV is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * GDV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */


#include "log.hpp"

#include "view.hpp"

#include "internationalization.hpp"
#include <iostream>
#include "gdv_application.hpp"
#include "exception.hpp"
#include "share.hpp"
#include "Windows/main_window.hpp"
#include "Widgets/calendar_button.hpp"


using namespace Gtk;
using namespace Glib;
using namespace std;
using namespace core;
using namespace sigc;
using namespace database;
using namespace car;
using namespace vehicle;
using namespace generic;

///////////////////////////////////////////////////////////////////////////////////
/////////////////////////// Constructor ///////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////
View::View(MainWindow& window) : GdvWidget()
{
	// Event
	app()->signalVehicleChanged().connect(mem_fun(*this, &View::onApplicationVehicleChanged));

	window.signalViewChanged().connect(mem_fun(*this, &View::onMainWindowViewChanged));
}

View::~View()
{
}

///////////////////////////////////////////////////////////////////////////////////
/////////////////////////// Connections ///////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////
void View::connectionAdd(connection connection)
{
	signal_connection_list_.push_back(connection);
}

void View::connectionBlock()
{
	for (auto& connection : signal_connection_list_)
		connection.block();
}

void View::connectionUnblock()
{
	for (auto& connection : signal_connection_list_)
		connection.unblock();
}

void View::connectionClean()
{
	signal_connection_list_.clear();
}

void View::connectCurrentDbiChanged()
{
	dbi_changed_signal_connection_.disconnect();
	dbi_changed_signal_connection_ = selectedDbi()->signalChanged().connect(mem_fun(*this, &View::onInternalItemChanged));
}

void View::connectElementArrayChanged(ElementArray& element_array)
{
	connectionAdd(element_array.signalAdded().connect(mem_fun(*this, &View::onInternalElementArrayAdded)));
	connectionAdd(element_array.signalRemoved().connect(mem_fun(*this, &View::onInternalElementArrayRemoved)));
}

void View::connectEntry(Entry& entry)
{
	connectionAdd(entry.signal_activate().connect(mem_fun(*this, &View::onGuiChange)));
	connectionAdd(entry.signal_focus_out_event().connect(mem_fun(*this, &View::onMyFocusOutEventEntry)));
}

void View::connectSpinButton(SpinButton& spin_button)
{
	connectionAdd(spin_button.signal_value_changed().connect(mem_fun(*this, &View::onGuiChange)));
	connectionAdd(spin_button.signal_focus_out_event().connect(mem_fun(*this, &View::onMyFocusOutEventEntry)));
}

void View::connectCalendarButton(CalendarButton& button)
{
	connectionAdd(button.signalDateSelected().connect(mem_fun(*this, &View::onGuiChange)));
}

void View::connectComboBoxText(ComboBoxText& combobox)
{
	signal_connection_list_.push_back(combobox.signal_changed().connect(mem_fun(*this, &View::onGuiChange)));
}

void View::connectSwitch(Switch& sw)
{
	signal_connection_list_.push_back(sw.property_active().signal_changed().connect(mem_fun(*this, &View::onGuiChange)));
}



///////////////////////////////////////////////////////////////////////////////////
/////////////////////////// Slots /////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////
bool View::onMyFocusOutEventEntry([[maybe_unused]] GdkEventFocus* gdk_event)
{
	onGuiChange();
	return false;
}

void View::onInternalItemChanged(Item& dbi)
{
	if (!isCurrentView())
		return;

	onItemChanged(dbi);
}

void View::onInternalElementArrayAdded([[maybe_unused]] Element& item)
{
	onArrayChanged();
}

void View::onInternalElementArrayRemoved([[maybe_unused]] ItemPtr item, [[maybe_unused]] guint index)
{
	onArrayChanged();
}

void View::onMainWindowViewChanged(const Glib::ustring view_visible_name)
{
	if (isEnable())
	{
		if (view_visible_name == name())
		{
			setIsCurrentView(true);
			if (selected_dbi_ != nullptr)
				onItemChanged(*selected_dbi_);
		}
		else
			setIsCurrentView(false);
	}
}

void View::onPlaceSelected([[maybe_unused]] const gint64 id)
{
	onGuiChange();
}
