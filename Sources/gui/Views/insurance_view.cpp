/*!
 * \file    insurance_view.cpp
 * \author  Remi BERTHO
 * \date    15/04/18
 * \version 1.0.0
 */

/*
 * insurance_view.cpp
 *
 * Copyright 2016-2018 Remi BERTHO <remi.bertho@dalan.fr>
 *
 * This file is part of GDV.
 *
 * GDV is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * GDV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */


#include "log.hpp"

#include "insurance_view.hpp"

#include "internationalization.hpp"
#include <iostream>
#include "gdv_application.hpp"
#include "exception.hpp"
#include "share.hpp"
#include "Windows/main_window.hpp"


using namespace Gtk;
using namespace Glib;
using namespace std;
using namespace core;
using namespace sigc;
using namespace database;
using namespace car;
using namespace vehicle;
using namespace generic;


///////////////////////////////////////////////////////////////////////////////////
///////////////////////// AgencyWindow ////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////

InsuranceView::AgencyWindow::AgencyWindow(Window::BaseObjectType* cobject, const Glib::RefPtr<Builder>& refGlade) : PlaceWindow(cobject)
{
	refGlade->get_widget("insurance_agency_ok", apply_button_);
	refGlade->get_widget("insurance_agency_cancel", cancel_button_);

	refGlade->get_widget("insurance_agency_name_entry", name_entry_);
	refGlade->get_widget("insurance_agency_brand_entry", brand_entry_);
	refGlade->get_widget("insurance_agency_address_entry", address_entry_);
	refGlade->get_widget("insurance_agency_online_switch", online_switch_);
	refGlade->get_widget("insurance_agency_web_address_entry", web_address_entry_);

	connectButtons();
}

void InsuranceView::AgencyWindow::launch(Window* parent_window, bool cancellable, std::shared_ptr<place::Place> place)
{
	auto agency = dynamic_pointer_cast<place::Insurance>(place);

	name_entry_->set_text(agency->name());
	brand_entry_->set_text(agency->brand());
	address_entry_->set_text(agency->address());
	online_switch_->set_active(agency->online());
	web_address_entry_->set_text(agency->webAddress());

	if (cancellable)
	{
		cancel_button_->show();
	}
	else
	{
		cancel_button_->hide();
	}

	bool apply = internalLaunch(parent_window);

	if (apply == true)
	{
		agency->setName(name_entry_->get_text());
		agency->setBrand(brand_entry_->get_text());
		agency->setAddress(address_entry_->get_text());
		agency->setOnline(online_switch_->get_active());
		agency->setWebAddress(web_address_entry_->get_text());
	}
}



///////////////////////////////////////////////////////////////////////////////////
/////////////////////////// Constructor ///////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////
InsuranceView::InsuranceView(BaseObjectType* cobject, const RefPtr<Builder>& refGlade, MainWindow& window) : View(window), Grid(cobject)
{
	// Get widgets
	attach(list_, 0, 2, 1, 2);
	list_.set_size_request(250, -1);


	refGlade->get_widget_derived("insurance_agency_window", agency_window_);


	refGlade->get_widget_derived("insurance_total_price_entry", total_price_entry_, "€");
	refGlade->get_widget_derived("insurance_mean_price_entry", mean_price_entry_, "€");
	refGlade->get_widget_derived("insurance_mean_distance_entry", mean_distance_entry_, "km");
	refGlade->get_widget_derived("insurance_mean_duration_entry", mean_duration_entry_, _("day"));

	refGlade->get_widget("selected_insurance_grid", selected_grid_);

	refGlade->get_widget("insurance_name_entry", name_entry_);
	refGlade->get_widget_derived("insurance_begin_km_spinbutton", begin_km_spinbutton_, "km");
	refGlade->get_widget_derived("insurance_price_spinbutton", price_spinbutton_, "€");
	refGlade->get_widget("insurance_bonus_spinbutton", bonus_spinbutton_);
	refGlade->get_widget_derived("price_without_bonus_spinbutton", price_without_bonus_spinbutton_, "€");
	refGlade->get_widget("insurance_emergency_phone_entry", emergency_phone_entry_);
	refGlade->get_widget_derived("insurance_delay_year_spinbutton", delay_year_spinbutton_, "year");
	refGlade->get_widget_derived("insurance_delay_km_spinbutton", delay_km_spinbutton_, "km");

	refGlade->get_widget_derived("insurance_end_km_spinbutton", end_km_spinbutton_, "km");

	refGlade->get_widget_derived("insurance_age_entry", age_entry_, _("day"));
	refGlade->get_widget_derived("insurance_day_before_entry", day_before_entry_, _("day"));
	refGlade->get_widget_derived("insurance_km_entry", km_entry_, "km");
	refGlade->get_widget_derived("insurance_km_before_entry", km_before_entry_, "km");
	refGlade->get_widget_derived("insurance_price_per_month_entry", price_per_month_entry_, "€");
	refGlade->get_widget_derived("insurance_price_per_100km_entry", price_per_100km_entry_, "€");

	refGlade->get_widget("insurance_end_km_label", end_km_label_);
	refGlade->get_widget("insurance_end_date_label", end_date_label_);
	refGlade->get_widget("insurance_age_label", age_duration_label_);


	agency_selector_ = manage(new ObjectSelector<place::Insurance, false>());
	selected_grid_->attach(*agency_selector_, 1, 2, 1, 1);

	begin_date_button_ = manage(new CalendarButton(CalendarButton::DOWN));
	selected_grid_->attach(*begin_date_button_, 1, 1, 1, 1);

	end_date_button_ = manage(new CalendarButton(CalendarButton::UP));
	selected_grid_->attach(*end_date_button_, 1, 13, 1, 1);

	guarantee_text_list_ = manage(new ArticleList<Insurance, Insurance::Guarantee>(ArticleList<Insurance, Insurance::Guarantee>::UP));
	selected_grid_->attach(*guarantee_text_list_, 1, 7, 1, 1);


	// Event
	connectionAdd(list_.signalSelected().connect(mem_fun(*this, &InsuranceView::onInsuranceSelected)));

	connectEntry(*name_entry_);
	connectObjectSelector(*agency_selector_, *agency_window_);
	connectSpinButton(*begin_km_spinbutton_);
	connectSpinButton(*price_spinbutton_);
	connectSpinButton(*bonus_spinbutton_);
	connectEntry(*emergency_phone_entry_);
	connectSpinButton(*delay_year_spinbutton_);
	connectSpinButton(*delay_km_spinbutton_);
	connectCalendarButton(*begin_date_button_);
	connectSpinButton(*end_km_spinbutton_);
	connectCalendarButton(*end_date_button_);

	show_all();
}



///////////////////////////////////////////////////////////////////////////////////
/////////////////////////// Slots /////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////
void InsuranceView::onApplicationVehicleChanged(VehiclePtr vehicle)
{
	if (vehicle != nullptr)
	{
		list_.set_sensitive(true);

		list_.setArray();
		connectElementArrayChanged(app()->car()->elements<Insurance>());
	}
	else
	{
		list_.set_sensitive(false);

		onInsuranceSelected(nullptr);
	}
}

void InsuranceView::onGuiChange()
{
	Insurance* selected_insurance = static_cast<Insurance*>(selectedDbi());

	guint begin_km = static_cast<guint>(begin_km_spinbutton_->get_value_as_int());
	guint delay_km = static_cast<guint>(delay_km_spinbutton_->get_value_as_int());

	selected_insurance->setName(name_entry_->get_text());
	selected_insurance->setBeginDate(begin_date_button_->getDate(), Element::END);
	selected_insurance->setAgencyId(agency_selector_->getSelectedId());
	selected_insurance->setBeginKm(begin_km, Element::END);
	selected_insurance->setPrice(price_spinbutton_->get_value());
	selected_insurance->setBonus(static_cast<guint>(bonus_spinbutton_->get_value() * 100));
	selected_insurance->setEmergencyPhone(emergency_phone_entry_->get_text());
	selected_insurance->setDelayYear(static_cast<guint>(delay_year_spinbutton_->get_value_as_int()), Element::END);
	selected_insurance->setDelayKm(delay_km, Element::END);
	if (!selected_insurance->currentlyUsed())
	{
		selected_insurance->setEndKm(static_cast<guint>(end_km_spinbutton_->get_value_as_int()));
		selected_insurance->setEndDate(end_date_button_->getDate());
	}
}

void InsuranceView::onItemChanged(Item& dbi)
{
	Insurance& insurance	= static_cast<Insurance&>(dbi);
	guint	   car_distance = app()->vehicle()->distance();

	onArrayChanged();

	name_entry_->set_text(insurance.name());
	begin_date_button_->setDate(insurance.beginDate());
	agency_selector_->setSelectedId(insurance.agencyId());
	begin_km_spinbutton_->set_value(insurance.beginKm());
	price_spinbutton_->set_value(insurance.price());
	bonus_spinbutton_->set_value(static_cast<double>(insurance.bonus()) / 100);
	price_without_bonus_spinbutton_->set_value(insurance.priceWithoutBonus());
	emergency_phone_entry_->set_text(insurance.emergencyPhone());
	delay_year_spinbutton_->set_value(insurance.delayYear());
	delay_km_spinbutton_->set_value(insurance.delayKm());
	end_date_button_->setDate(insurance.endDate());
	end_km_spinbutton_->set_value(insurance.endKm());
	price_per_month_entry_->setText(doubleToString(insurance.pricePerMonth(), 2));
	price_per_100km_entry_->setText(doubleToString(insurance.pricePer100Km(), 2));
	if (insurance.currentlyUsed())
	{
		end_km_label_->set_text(_("Maximum km before the next"));
		end_date_label_->set_text(_("Maximum date before the next"));
		age_duration_label_->set_text(_("Age in day"));
		day_before_entry_->setText(intToString(insurance.dayBefore()));
		km_before_entry_->setText(intToString(insurance.kmBefore(car_distance)));
		age_entry_->setText(uintToString(insurance.age()));
		km_entry_->setText(uintToString(insurance.km(car_distance)));

		end_km_spinbutton_->set_sensitive(false);
		end_date_button_->set_sensitive(false);
	}
	else
	{
		end_km_label_->set_text(_("End distance"));
		end_date_label_->set_text(_("End date"));
		age_duration_label_->set_text(_("Duration in day"));
		day_before_entry_->setText("-");
		km_before_entry_->setText("-");
		age_entry_->setText(uintToString(insurance.duration()));
		km_entry_->setText(doubleToString(insurance.distance(), 0));

		end_km_spinbutton_->set_sensitive(true);
		end_date_button_->set_sensitive(true);
	}
}

void InsuranceView::onArrayChanged()
{
	ElementArray& insurances = app()->car()->elements<Insurance>();

	total_price_entry_->setText(doubleToString(insurances.totalPrice(), 2));
	mean_price_entry_->setText(doubleToString(insurances.meanPrice(), 2));
	mean_duration_entry_->setText(uintToString(insurances.meanDuration()));
	mean_distance_entry_->setText(doubleToString(insurances.meanDistance(), 0));
}

void InsuranceView::onInsuranceSelected(Element* ci)
{
	setSelectedDbi(ci);

	if (ci == nullptr)
	{
		name_entry_->set_sensitive(false);
		agency_selector_->set_sensitive(false);
		begin_km_spinbutton_->set_sensitive(false);
		price_spinbutton_->set_sensitive(false);
		bonus_spinbutton_->set_sensitive(false);
		emergency_phone_entry_->set_sensitive(false);
		delay_year_spinbutton_->set_sensitive(false);
		delay_km_spinbutton_->set_sensitive(false);
		begin_date_button_->set_sensitive(false);
		end_km_spinbutton_->set_sensitive(false);
		end_date_button_->set_sensitive(false);
		guarantee_text_list_->set_sensitive(false);

		guarantee_text_list_->setElement(nullptr);
	}
	else
	{
		// Block GUI signals to avoid getting them when GUI init
		connectionBlock();

		name_entry_->set_sensitive(true);
		agency_selector_->set_sensitive(true);
		begin_km_spinbutton_->set_sensitive(true);
		price_spinbutton_->set_sensitive(true);
		bonus_spinbutton_->set_sensitive(true);
		emergency_phone_entry_->set_sensitive(true);
		delay_year_spinbutton_->set_sensitive(true);
		delay_km_spinbutton_->set_sensitive(true);
		begin_date_button_->set_sensitive(true);
		end_km_spinbutton_->set_sensitive(true);
		end_date_button_->set_sensitive(true);
		guarantee_text_list_->set_sensitive(true);

		guarantee_text_list_->setElement(static_cast<Insurance*>(ci));

		onItemChanged(*selectedDbi());

		connectionUnblock();

		connectCurrentDbiChanged();
	}
}
