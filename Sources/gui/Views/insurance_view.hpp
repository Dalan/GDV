/*!
 * \file    insurance_view.h
 * \author  Remi BERTHO
 * \date    15/04/18
 * \version 1.0.0
 */

/*
 * insurance_view.h
 *
 * Copyright 2016-2018 Remi BERTHO <remi.bertho@dalan.fr>
 *
 * This file is part of GDV.
 *
 * GDV is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * GDV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */


#ifndef INSURANCE_VIEW_H_INCLUDED
#define INSURANCE_VIEW_H_INCLUDED

#include <gtkmm.h>

#include "Car/car.hpp"

#include "Widgets/element_list.hpp"
#include "view.hpp"
#include "Widgets/calendar_button.hpp"
#include "Widgets/unit_entry.hpp"
#include "Widgets/unit_spinbutton.hpp"
#include "Widgets/article_list.hpp"

/*! \class InsuranceView
 *   \brief This class represent the insurance view
 */
class InsuranceView : public View, public Gtk::Grid
{
private:
	class AgencyWindow : public PlaceWindow
	{
		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Attributes ////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	private:
		Gtk::Entry*	 name_entry_;
		Gtk::Entry*	 brand_entry_;
		Gtk::Entry*	 address_entry_;
		Gtk::Switch* online_switch_;
		Gtk::Entry*	 web_address_entry_;

		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Constructor ///////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	public:
		AgencyWindow(BaseObjectType* cobject, const Glib::RefPtr<Gtk::Builder>& refGlade);



		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Function //////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	public:
		void launch(Gtk::Window* parent_window, bool cancellable, std::shared_ptr<place::Place> place) override;
	};




	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Attributes ////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
private:
	UnitEntry* total_price_entry_;
	UnitEntry* mean_price_entry_;
	UnitEntry* mean_distance_entry_;
	UnitEntry* mean_duration_entry_;

	Gtk::Grid* selected_grid_;

	Gtk::Entry*												name_entry_;
	CalendarButton*											begin_date_button_;
	ObjectSelector<place::Insurance, false>*				agency_selector_;
	UnitSpinButton*											begin_km_spinbutton_;
	UnitSpinButton*											price_spinbutton_;
	Gtk::SpinButton*										bonus_spinbutton_;
	UnitSpinButton*											price_without_bonus_spinbutton_;
	Gtk::Entry*												emergency_phone_entry_;
	UnitSpinButton*											delay_year_spinbutton_;
	UnitSpinButton*											delay_km_spinbutton_;
	ArticleList<car::Insurance, car::Insurance::Guarantee>* guarantee_text_list_;

	UnitSpinButton* end_km_spinbutton_;

	UnitEntry*		age_entry_;
	UnitEntry*		day_before_entry_;
	CalendarButton* end_date_button_;
	UnitEntry*		km_entry_;
	UnitEntry*		km_before_entry_;
	UnitEntry*		price_per_month_entry_;
	UnitEntry*		price_per_100km_entry_;

	Gtk::Label* end_km_label_;
	Gtk::Label* end_date_label_;
	Gtk::Label* age_duration_label_;

	ElementList<car::Insurance> list_;

	AgencyWindow* agency_window_;

	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Constructor ///////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
public:
	/*!
	 *  \brief Constructor with builder
	 *  \param cobject the C object
	 *  \param refGlade the builder
	 */
	InsuranceView(BaseObjectType* cobject, const Glib::RefPtr<Gtk::Builder>& refGlade, MainWindow& window);

	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Functions /////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
	inline virtual Glib::ustring name() override
	{
		return car::Insurance::DATABASE_TABLE_NAME;
	}

	inline virtual Glib::ustring title() override
	{
		return car::Insurance::userName(2);
	}

	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Slots /////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
protected:
	/**
	 * @brief Function called when the vehicle is changed in the application
	 * @param vehicle the vehicle
	 */
	virtual void onApplicationVehicleChanged(vehicle::VehiclePtr vehicle) override;

	/**
	 * @brief Function called when the user change somesthing in the UI
	 */
	virtual void onGuiChange() override;

	/**
	 * @brief Function called when the insurance changed
	 * @param dbi the insurance as Item
	 */
	virtual void onItemChanged(database::Item& dbi) override;

	/**
	 * @brief Function called when the array of elements changed
	 */
	virtual void onArrayChanged() override;

	/**
	 * @brief Function called when the user select a insurance
	 * @param need the selected need
	 */
	void onInsuranceSelected(generic::Element* ci);
};



#endif	  // INSURANCE_VIEW_H_INCLUDED
