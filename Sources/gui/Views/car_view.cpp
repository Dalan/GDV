/*!
 * \file    general_view.cpp
 * \author  Remi BERTHO
 * \date    27/11/17
 * \version 1.0.0
 */

/*
 * general_view.cpp
 *
 * Copyright 2016-2017 Remi BERTHO <remi.bertho@dalan.fr>
 *
 * This file is part of GDV.
 *
 * GDV is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * GDV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */


#include "log.hpp"

#include "car_view.hpp"

#include "internationalization.hpp"
#include <iostream>
#include "gdv_application.hpp"
#include "exception.hpp"
#include "share.hpp"


using namespace Gtk;
using namespace Glib;
using namespace std;
using namespace core;
using namespace database;
using namespace car;
using namespace vehicle;

///////////////////////////////////////////////////////////////////////////////////
/////////////////////////// Constructor ///////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////
CarView::CarView(BaseObjectType* cobject, const RefPtr<Builder>& refGlade, MainWindow& window) : View(window), ScrolledWindow(cobject)
{
	// Get widgets
	refGlade->get_widget("car_grid", main_grid_);

	refGlade->get_widget("car_name_entry", name_entry_);
	refGlade->get_widget("car_brand_entry", brand_entry_);
	refGlade->get_widget("car_model_entry", model_entry_);
	refGlade->get_widget("car_plate_entry", plate_entry_);
	refGlade->get_widget("car_owner_entry", owner_entry_);
	refGlade->get_widget_derived("car_price_spinbutton", price_spinbutton_, "€");
	refGlade->get_widget("car_engine_type_combobox", engine_type_comboboxtext_);
	refGlade->get_widget_derived("car_distance_spinbutton", distance_spinbutton_, "km");
	refGlade->get_widget_derived("car_price_month_entry", price_month_entry_, "€");
	refGlade->get_widget_derived("car_price_100km_entry", price_100km_entry_, "€");
	refGlade->get_widget_derived("car_price_year_entry", price_year_entry_, "€");
	refGlade->get_widget_derived("car_total_price_entry", total_price_entry_, "€");

	refGlade->get_widget("car_name_label", name_label_);

	first_ownership_calendar_button_ = manage(new CalendarButton(CalendarButton::DOWN));
	main_grid_->attach(*first_ownership_calendar_button_, 1, 5, 1, 1);

	// Event
	connectEntry(*name_entry_);
	connectEntry(*brand_entry_);
	connectEntry(*model_entry_);
	connectEntry(*plate_entry_);
	connectEntry(*owner_entry_);
	connectCalendarButton(*first_ownership_calendar_button_);
	connectSpinButton(*price_spinbutton_);
	connectComboBoxText(*engine_type_comboboxtext_);
	connectSpinButton(*distance_spinbutton_);

	show_all();
}



///////////////////////////////////////////////////////////////////////////////////
/////////////////////////// Slots /////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////
void CarView::onApplicationVehicleChanged(VehiclePtr vehicle)
{
	auto car = static_pointer_cast<Car>(vehicle);

	if (car != nullptr)
	{
		setSelectedDbi(car.get());

		already_used_cars_ = app()->database()->objectsId<Car>();
		for (auto it = already_used_cars_.begin(); it != already_used_cars_.end(); it++)
		{
			if ((*it).first == car->name())
			{
				already_used_cars_.erase(it);
				break;
			}
		}

		// Block GUI signals to avoid getting them when GUI init
		connectionBlock();

		name_entry_->set_sensitive(true);
		brand_entry_->set_sensitive(true);
		model_entry_->set_sensitive(true);
		plate_entry_->set_sensitive(true);
		owner_entry_->set_sensitive(true);
		first_ownership_calendar_button_->set_sensitive(true);
		price_spinbutton_->set_sensitive(true);
		distance_spinbutton_->set_sensitive(true);
		engine_type_comboboxtext_->set_sensitive(true);

		onItemChanged(*car);

		connectCurrentDbiChanged();

		connectionUnblock();
	}
	else
	{
		name_entry_->set_sensitive(false);
		brand_entry_->set_sensitive(false);
		model_entry_->set_sensitive(false);
		plate_entry_->set_sensitive(false);
		owner_entry_->set_sensitive(false);
		first_ownership_calendar_button_->set_sensitive(false);
		price_spinbutton_->set_sensitive(false);
		distance_spinbutton_->set_sensitive(false);
		engine_type_comboboxtext_->set_sensitive(false);
	}
}


void CarView::onItemChanged(Item& dbi)
{
	Car& car = static_cast<Car&>(dbi);
	name_entry_->set_text(car.name());
	brand_entry_->set_text(car.brand());
	model_entry_->set_text(car.model());
	plate_entry_->set_text(car.plate());
	owner_entry_->set_text(car.owner());
	first_ownership_calendar_button_->setDate(car.dateOfOwnership());
	price_spinbutton_->set_value(car.price());
	engine_type_comboboxtext_->set_active(car.engineType());
	distance_spinbutton_->set_value(car.distance());
	price_100km_entry_->setText(doubleToString(car.pricePer100Km(), 2));
	price_month_entry_->setText(doubleToString(car.pricePerMonth(), 2));
	price_year_entry_->setText(doubleToString(car.pricePerYear(), 2));
	total_price_entry_->setText(doubleToString(car.totalPrice(), 2));
}

void CarView::onGuiChange()
{
	bool car_name_already_used = false;

	// Empty name
	if (name_entry_->get_text() == "")
		car_name_already_used = true;

	// Already used name
	for (auto& car_name : already_used_cars_)
	{
		if (car_name.first == name_entry_->get_text())
		{
			car_name_already_used = true;
			break;
		}
	}

	if (!car_name_already_used)
	{
		app()->vehicle()->setName(name_entry_->get_text());
		name_label_->set_markup(_("Name"));
	}
	else
	{
		name_label_->set_markup(ustring::compose("%1%2%3", "<span foreground=\"red\">", _("Name"), "</span>"));
	}


	app()->car()->setBrand(brand_entry_->get_text());
	app()->car()->setModel(model_entry_->get_text());
	app()->car()->setPlate(plate_entry_->get_text());
	app()->car()->setOwner(owner_entry_->get_text());
	app()->car()->setPrice(price_spinbutton_->get_value());
	app()->car()->setDistance(static_cast<guint>(distance_spinbutton_->get_value_as_int()));
	if (engine_type_comboboxtext_->get_active_id() == "gpl")
		app()->car()->setEngineType(EngineType::GPL);
	else if (engine_type_comboboxtext_->get_active_id() == "diesel")
		app()->car()->setEngineType(EngineType::DIESEL);
	else if (engine_type_comboboxtext_->get_active_id() == "gas")
		app()->car()->setEngineType(EngineType::GAS);
	else if (engine_type_comboboxtext_->get_active_id() == "electricity")
		app()->car()->setEngineType(EngineType::ELECTRICITY);
	app()->car()->setDateOfOwnership(first_ownership_calendar_button_->getDate());
}

void CarView::onArrayChanged()
{
	// No array here
}
