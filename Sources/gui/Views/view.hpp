/*!
 * \file    view.h
 * \author  Remi BERTHO
 * \date    06/05/18
 * \version 1.0.0
 */

/*
 * view.h
 *
 * Copyright 2016-2018 Remi BERTHO <remi.bertho@dalan.fr>
 *
 * This file is part of GDV.
 *
 * GDV is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * GDV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */


#ifndef VIEW_H_INCLUDED
#define VIEW_H_INCLUDED

#include <gtkmm.h>

#include "Car/car.hpp"
#include "Place/place.hpp"

#include "Widgets/object_selector.hpp"
#include "Utilities/gdv_widget.hpp"
#include "Windows/place_window.hpp"

class CalendarButton;
class TextList;
class MainWindow;

/*! \class View
 *   \brief This class represent the service view
 */
class View : public GdvWidget
{
	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Attributes ////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
private:
	std::vector<sigc::connection> signal_connection_list_;
	sigc::connection			  dbi_changed_signal_connection_;

	database::Item* selected_dbi_ = nullptr;

	bool is_current_view_ = false;
	bool is_enable_		  = false;

	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Constructor ///////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
protected:
	/*!
	 *  \brief Constructor
	 */
	View(MainWindow& window);

	virtual ~View();

	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Connections ///////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
	void connectionAdd(sigc::connection connection);
	void connectionBlock();
	void connectionUnblock();
	void connectionClean();

	void connectCurrentDbiChanged();
	void connectElementArrayChanged(generic::ElementArray& element_array);

	void								connectEntry(Gtk::Entry& entry);
	void								connectSpinButton(Gtk::SpinButton& spin_button);
	void								connectCalendarButton(CalendarButton& button);
	void								connectComboBoxText(Gtk::ComboBoxText& combobox);
	void								connectSwitch(Gtk::Switch& sw);
	template <typename PlaceClass> void connectObjectSelector(ObjectSelector<PlaceClass, false>& selector, PlaceWindow& place_window)
	{
		connectionAdd(selector.signalSelected().connect(sigc::mem_fun(*this, &View::onPlaceSelected)));
		connectionAdd(selector.signalRemove().connect(sigc::mem_fun(*this, &View::onPlaceRemoveButton<PlaceClass>)));
		connectionAdd(selector.signalAdd().connect(sigc::bind<ObjectSelector<PlaceClass, false>&, PlaceWindow&>(
				sigc::mem_fun(*this, &View::onPlaceNewButton<PlaceClass>), selector, place_window)));
		connectionAdd(selector.signalModify().connect(sigc::bind<ObjectSelector<PlaceClass, false>&, PlaceWindow&>(
				sigc::mem_fun(*this, &View::onPlaceModifyButton<PlaceClass>), selector, place_window)));
	}

	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Getter and setter /////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
private:
	inline bool isCurrentView() const
	{
		return is_current_view_;
	}

	inline void setIsCurrentView(bool is_current_view)
	{
		is_current_view_ = is_current_view;
	}

protected:
	inline database::Item* selectedDbi() const
	{
		return selected_dbi_;
	}

	inline void setSelectedDbi(database::Item* selected_dbi)
	{
		selected_dbi_ = selected_dbi;
	}

public:
	inline bool isEnable() const
	{
		return is_enable_;
	}

	inline void enable(bool is_enable)
	{
		is_enable_ = is_enable;
	}

	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Functions /////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////

	virtual Glib::ustring name()  = 0;
	virtual Glib::ustring title() = 0;

	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Slots /////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
private:
	/**
	 * @brief Function called when the user changed the view in the main window
	 * @param view_visible_name the visible view
	 */
	void onMainWindowViewChanged(const Glib::ustring view_visible_name);

	/**
	 * @brief Function called when the focus on an entry is out
	 * @param gdk_event The Gdk::EventFocus which triggered this signal.
	 * @return true to stop other handlers from being invoked for the event. false to propagate the event further
	 */
	bool onMyFocusOutEventEntry(GdkEventFocus* gdk_event);

	void								onPlaceSelected(const gint64 id);
	template <typename PlaceClass> void onPlaceRemoveButton(gint64 id)
	{
		try
		{
			app()->database()->removeSingleItem<PlaceClass>(id);
		}
		catch (core::Exception& e)
		{
			GDV_APP_SEND_ERROR(_("The place cannot be found."), e.what());
		}
	}
	template <typename PlaceClass> void onPlaceNewButton(ObjectSelector<PlaceClass, false>& selector, PlaceWindow& place_window)
	{
		Glib::ustring new_name = Glib::ustring::compose(_("New %1"), PlaceClass::userName());
		try
		{
			auto station = app()->database()->newPlace<PlaceClass>(new_name);
			place_window.launch(app()->mainWindow(), false, station);
			selector.objectAdded(station->objectId());

			onGuiChange();
		}
		catch (core::Exception& e)
		{
			GDV_APP_SEND_ERROR(Glib::ustring::compose(_("Error when creating %1."), new_name), e.what());
		}
	}
	template <typename PlaceClass>
	void onPlaceModifyButton(gint64 id, ObjectSelector<PlaceClass, false>& selector, PlaceWindow& place_window)
	{
		try
		{
			auto place = app()->database()->getPlace<PlaceClass>(id);
			place_window.launch(app()->mainWindow(), true, place);
			selector.objectChanged(place->objectId());
		}
		catch (core::Exception& e)
		{
			GDV_APP_SEND_ERROR(Glib::ustring::compose(_("Error when creating gas station %1."), id), e.what());
		}
	}


	/**
	 * @brief Function called when the service changed
	 * @param dbi the service as Item
	 */
	void onInternalItemChanged(database::Item& dbi);

	void onInternalElementArrayAdded(generic::Element& item);
	void onInternalElementArrayRemoved(database::ItemPtr item, guint index);

protected:
	/**
	 * @brief Function called when the vehicle is changed in the application
	 * @param vehicle the vehicle
	 */
	virtual void onApplicationVehicleChanged(vehicle::VehiclePtr vehicle) = 0;

	/**
	 * @brief Function called when the user change somesthing in the UI
	 */
	virtual void onGuiChange() = 0;

	/**
	 * @brief Function called when the service changed
	 * @param dbi the service as Item
	 */
	virtual void onItemChanged(database::Item& dbi) = 0;

	/**
	 * @brief Function called when the array of elements changed
	 */
	virtual void onArrayChanged() = 0;
};



#endif	  // VIEW_H_INCLUDED
