/*!
 * \file    general_view.h
 * \author  Remi BERTHO
 * \date    27/11/17
 * \version 1.0.0
 */

/*
 * general_view.h
 *
 * Copyright 2016-2017 Remi BERTHO <remi.bertho@dalan.fr>
 *
 * This file is part of GDV.
 *
 * GDV is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * GDV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */


#ifndef CAR_VIEW_H_INCLUDED
#define CAR_VIEW_H_INCLUDED

#include <gtkmm.h>

#include "view.hpp"
#include "DataBase/database.hpp"
#include "Widgets/calendar_button.hpp"
#include "Widgets/unit_entry.hpp"
#include "Widgets/unit_spinbutton.hpp"

/*! \class GeneralView
 *   \brief This class represent the main window
 */
class CarView : public View, public Gtk::ScrolledWindow
{
	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Attributes ////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
private:
	Gtk::Grid* main_grid_;

	Gtk::Entry*		   name_entry_;
	Gtk::Entry*		   brand_entry_;
	Gtk::Entry*		   model_entry_;
	Gtk::Entry*		   plate_entry_;
	Gtk::Entry*		   owner_entry_;
	CalendarButton*	   first_ownership_calendar_button_;
	UnitSpinButton*	   price_spinbutton_;
	UnitSpinButton*	   distance_spinbutton_;
	Gtk::ComboBoxText* engine_type_comboboxtext_;
	UnitEntry*		   price_month_entry_;
	UnitEntry*		   price_100km_entry_;
	UnitEntry*		   price_year_entry_;
	UnitEntry*		   total_price_entry_;

	Gtk::Label* name_label_;

	std::vector<std::pair<std::string, gint64>> already_used_cars_;

	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Constructor ///////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
public:
	/*!
	 *  \brief Constructor with builder
	 *  \param cobject the C object
	 *  \param refGlade the builder
	 */
	CarView(BaseObjectType* cobject, const Glib::RefPtr<Gtk::Builder>& refGlade, MainWindow& window);


	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Functions /////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
	inline virtual Glib::ustring name() override
	{
		return car::Car::DATABASE_TABLE_NAME;
	}

	inline virtual Glib::ustring title() override
	{
		return car::Car::userName();
	}

	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Slots /////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
protected:
	/**
	 * @brief Function called when the vehicle is changed in the application
	 * @param vehicle the vehicle
	 */
	virtual void onApplicationVehicleChanged(vehicle::VehiclePtr vehicle) override;

	/**
	 * @brief Function called when the something in the car is changed
	 * @param dbi the car as Item
	 */
	virtual void onItemChanged(database::Item& dbi) override;

	/**
	 * @brief Function called when the user change somesthing in the UI
	 */
	virtual void onGuiChange() override;

	/**
	 * @brief Function called when the array of elements changed
	 */
	virtual void onArrayChanged() override;
};



#endif	  // CAR_VIEW_H_INCLUDED
