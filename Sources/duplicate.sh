#!/bin/bash
old=$1
new=$2
old_lower="${old,,}"
new_lower="${new,,}"
old_upper="${old^^}"
new_upper="${new^^}"

echo "Duplicate from $old to $new"
cp "$old_lower".hpp "$new_lower".hpp
cp "$old_lower".cpp "$new_lower".cpp

sd "$old" "$new" "$new_lower".hpp
sd "$old" "$new" "$new_lower".cpp
sd "$old_lower" "$new_lower" "$new_lower".hpp
sd "$old_lower" "$new_lower" "$new_lower".cpp
sd "$old_upper" "$new_upper" "$new_lower".hpp
sd "$old_upper" "$new_upper" "$new_lower".cpp
