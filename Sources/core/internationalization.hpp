/*!
 * \file
 */

/*
 * internationalization.hpp
 *
 * Copyright 2020 dalan <remi.bertho@dalan.fr>
 *
 * GDV is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * GDV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

#ifndef INTERNATIONALIZATION_HPP
#define INTERNATIONALIZATION_HPP

#include <glibmm/i18n.h>
#define P_(SINGULAR, PLURAL, N) g_dngettext(NULL, SINGULAR, PLURAL, N)

#endif
