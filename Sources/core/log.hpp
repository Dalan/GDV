/*!
 * \file    log.h
 * \author  Remi BERTHO
 * \date    21/11/17
 * \version 1.0.0
 */

/*
 * log.h
 *
 * Copyright 2017 Rémi BERTHO <remi.bertho@dalan.fr>
 *
 * This file is part of GDV.
 *
 * GDVis free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * GDV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

#ifndef LOG_H
#define LOG_H

///////////////////////////////////////////////////////////////////////////////////
/////////////////////////////// Defines ///////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////
#define G_LOG_USE_STRUCTURED
#define GDV_DEBUG(str) log(LogLevel::DEBUG, __FILE__, __LINE__, std::string(G_STRFUNC), str)
#define GDV_INFO(str) log(LogLevel::INFO, __FILE__, __LINE__, std::string(G_STRFUNC), str)
#define GDV_WARNING(str) log(LogLevel::WARNING, __FILE__, __LINE__, std::string(G_STRFUNC), str)
#define GDV_ERROR(str) log(LogLevel::ERROR, __FILE__, __LINE__, std::string(G_STRFUNC), str)
#define GDV_FATAL(str) log(LogLevel::FATAL, __FILE__, __LINE__, std::string(G_STRFUNC), str)

#include <string>

namespace core
{
	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Enumeration ///////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
	enum LogLevel
	{
		DEBUG,
		INFO,
		WARNING,
		ERROR,
		FATAL
	};

	/**
	 * @brief Log function
	 * @param level tge log level
	 * @param file the file
	 * @param line the line
	 * @param fn the function
	 * @param msg the message
	 */
	void log(LogLevel level, const std::string& file, unsigned int line, const std::string& fn, const std::string& msg);

	/**
	 * @brief Init the logs
	 */
	void initLog();
}	 // namespace core


#endif	  // LOG_H
