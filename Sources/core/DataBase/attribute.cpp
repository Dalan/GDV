/*!
 * \file    attribute.cpp
 * \author  Remi BERTHO
 * \date    26/06/18
 * \version 1.0.0
 */

/*
 * attribute.cpp
 *
 * Copyright 2018 Rémi BERTHO <remi.bertho@dalan.fr>
 *
 * This file is part of GDV.
 *
 * GDV is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * GDV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

#include "log.hpp"

#include "attribute.hpp"
#include "item.hpp"

#include "exception.hpp"

using namespace std;
using namespace Glib;

namespace database
{
	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Constructor and destructor ////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
	Attribute::Attribute(const Attribute& dbp, Item& dbi) : Attribute(dbi, dbp.name(), dbp.databaseVersion(), dbp.nullable())
	{
	}


	Attribute::~Attribute() = default;


	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Function //////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
	bool Attribute::sameDbiId(const Attribute& dbp)
	{
		return dbi_.sameDatabaseId(dbp.dbi_);
	}

	bool Attribute::sameDbiType(const Attribute& dbp)
	{
		return typeid(dbi_) == typeid(dbp.dbi_);
	}


}	 // namespace database
