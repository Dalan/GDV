/*!
 * \file    property.h
 * \author  Remi BERTHO
 * \date    26/06/18
 * \version 1.0.0
 */

/*
 * property.h
 *
 * Copyright 2018 Rémi BERTHO <remi.bertho@dalan.fr>
 *
 * This file is part of GDV.
 *
 * GDVis free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * GDV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */


#ifndef DATABASEPROPERTY_H
#define DATABASEPROPERTY_H

#include <glibmm.h>
#include <SQLiteCpp/SQLiteCpp.h>
#include <any>
#include <typeinfo>
#include <format>

#include "exception.hpp"
#include "item.hpp"
#include "version.hpp"
#include "item.hpp"
#include "attribute.hpp"

using namespace core;

namespace database
{

	/*! \class Property
	 *   \brief This class is a property
	 */
	class Property : public Attribute
	{
		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Types /////////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	private:
		inline static const std::type_info& INT_TYPE		   = typeid(int);
		inline static const std::type_info& UINT_TYPE		   = typeid(unsigned int);
		inline static const std::type_info& INT64_TYPE		   = typeid(int64_t);
		inline static const std::type_info& BOOL_TYPE		   = typeid(bool);
		inline static const std::type_info& DOUBLE_TYPE		   = typeid(double);
		inline static const std::type_info& DATE_TYPE		   = typeid(Glib::Date);
		inline static const std::type_info& STRING_TYPE		   = typeid(std::string);
		inline static const std::type_info& VECTOR_STRING_TYPE = typeid(std::vector<std::string>);

		static const constexpr char VECTOR_SEPARATOR = '|';


		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Friendship ////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
		friend class Item;
		friend class AttributeGroup;
		friend class DataBase;


		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Attributes ////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	protected:
		std::any value_;


		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Constructor ///////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	public:
		template <typename T>
		explicit Property(Item& item, const std::string& name, const Version& databaseVersion, T value, const bool nullable = false)
				: Attribute(item, name, databaseVersion, nullable)
		{
			if constexpr (std::is_enum_v<T>)
				value_ = static_cast<std::underlying_type_t<T>>(value);
			else
				value_ = value;

			item.properties().add(*this);
		}
		explicit Property(const Property& property) = delete;
		explicit Property(const Property& property, Item& item);

		virtual ~Property();


		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Function //////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	protected:
		virtual void bindSqlStatement(SQLite::Statement& statement, const int index) const;

		virtual std::string getSqlType() const;

	public:
		template <typename T> inline bool equal(const Property& p) const
		{
			return getValue<T>() == p.getValue<T>();
		}

		template <typename T> inline bool operator==(const T& t) const
		{
			static_assert(!std::is_same_v<T, Property>, "Use equal function instead of operator ==");

			return getValue<T>() == t;
		}

		using Attribute::toString;
		template <typename T> std::string toString() const
		{
			return std::format("Property `{}` type `{}` value `{}`", name(), type().name(), getValue<T>());
		}


		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Getter ////////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	public:
		inline const std::type_info& type() const
		{
			return value_.type();
		}

		// Operator T
		template <typename T> inline operator T() const
		{
			return getValue<T>();
		}

		// Get Value
		template <typename T> auto getValue() const
		{
			try
			{
				if constexpr (std::is_enum_v<T>)
					return static_cast<const T>(std::any_cast<const std::underlying_type_t<T>&>(value_));
				else
					return std::any_cast<const T&>(value_);
			}
			catch ([[maybe_unused]] std::bad_any_cast& e)
			{
				throw core::WrongPropertyType(std::format("Type of property `{}` is `{}` not `{}`", name(), type().name(), typeid(T).name()));
			}
		}

		virtual std::string getAsString() const;

		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Setter ////////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	protected:
		// Set from sql
		virtual void setFromSqlColumn(const SQLite::Column& column);

	public:
		// Operator =
		template <typename T> inline Property& operator=(T t)
		{
			setValue<T>(t);
			return *this;
		}
		using Attribute::operator=;
		Property& operator=(const Property& dbp)
		{
			if (type() == dbp.type())
			{
				dbi_.setOld();

				value_ = dbp.value_;

				emitSignalChanged();
			}
			return *this;
		}

		// Set Value
		template <typename T> void setValue(T value)
		{
			if (std::not_equal_to<T>()(getValue<T>(), value))
			{
				dbi_.setOld();

				if constexpr (std::is_enum_v<T>)
					value_ = static_cast<std::underlying_type_t<T>>(value);
				else
					value_ = value;

				emitSignalChanged();
			}
		}
	};


}	 // namespace database


#endif /* DATABASEPROPERTY_H */
