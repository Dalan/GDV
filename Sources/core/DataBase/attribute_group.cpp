/*!
 * \file    property_group.cpp
 * \author  Remi BERTHO
 * \date    26/06/18
 * \version 1.0.0
 */

/*
 * property_group.cpp
 *
 * Copyright 2018 Rémi BERTHO <remi.bertho@dalan.fr>
 *
 * This file is part of GDV.
 *
 * GDV is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * GDV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

#include "log.hpp"

#include "attribute_group.hpp"

#include "exception.hpp"
#include "item.hpp"
#include "property.hpp"
#include "link.hpp"

using namespace std;
using namespace Glib;
using namespace sigc;
using namespace core;

namespace database
{
	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Constructor and destructor ////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
	AttributeGroup::AttributeGroup(const Type type) : type_(type)
	{
	}

	AttributeGroup::~AttributeGroup() = default;

	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Function //////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
	AttributeGroup& AttributeGroup::operator=(const AttributeGroup& dbpg)
	{
		if (size() != dbpg.size())
		{
			throw DifferentPropertyGroup("Different number of Properties");
		}

		if (size() == 0)
		{
			return *this;
		}

		if (!attributes_.front().get().sameDbiType(dbpg.attributes_.front()))
		{
			throw DifferentPropertyGroup("Different Item");
		}

		auto it_1 = attributes_.begin();
		auto it_2 = dbpg.attributes_.begin();
		for (; it_1 != attributes_.end(); it_1++, it_2++)
		{
			switch (type_)
			{
			case PROPERTY:
			{
				auto& p1 = dynamic_cast<Property&>((*it_1).get());
				auto  l1 = dynamic_cast<Link*>(&p1);
				auto& p2 = dynamic_cast<Property&>((*it_2).get());
				auto  l2 = dynamic_cast<Link*>(&p2);
				if ((l1 != nullptr) && (l2 != nullptr))
					l1->setId(l2->id());
				else
					p1 = p2;
				break;
			}
			}
		}

		return *this;
	}

	void AttributeGroup::add(Attribute& a)
	{
		attributes_.emplace_back(a);
		a.signalChanged().connect(mem_fun(*this, &AttributeGroup::onPropertyChanged));
	}

	void AttributeGroup::blockSignal()
	{
		for_each(attributes_.begin(), attributes_.end(), [](Attribute& dbp) { dbp.signalChanged().block(); });
	}

	void AttributeGroup::unblockSignal()
	{
		for_each(attributes_.begin(), attributes_.end(), [](Attribute& dbp) { dbp.signalChanged().unblock(); });
	}

	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Slot //////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
	void AttributeGroup::onPropertyChanged(Item& dbi, [[maybe_unused]] Attribute& dbp)
	{
		signalChanged().emit(dbi);
	}


	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Getter and setter /////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
	Attribute& AttributeGroup::at(const string& name)
	{
		auto it_find = std::find_if(attributes_.begin(), attributes_.end(), [&name](const Attribute& a) { return a.name() == name; });
		if (it_find == attributes_.end())
		{
			throw NotFound(format("Property `{}` is not found in the property group.", name));
		}
		return *it_find;
	}

	const Attribute& AttributeGroup::at(const string& name) const
	{
		auto it_find = std::find_if(attributes_.begin(), attributes_.end(), [&name](const Attribute& a) { return a.name() == name; });
		if (it_find == attributes_.end())
		{
			throw NotFound(format("Property `{}` is not found in the property group.", name));
		}
		return *it_find;
	}
}	 // namespace database
