/*!
 * \file    attribute.h
 * \author  Remi BERTHO
 * \date    26/06/18
 * \version 1.0.0
 */

/*
 * attribute.h
 *
 * Copyright 2018 Rémi BERTHO <remi.bertho@dalan.fr>
 *
 * This file is part of GDV.
 *
 * GDVis free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * GDV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */


#ifndef DATABASEATTRIBUTE_H
#define DATABASEATTRIBUTE_H

#include <glibmm.h>

#include "exception.hpp"
#include "version.hpp"

using namespace core;

namespace database
{
	class Item;

	/*! \class Attribute
	 *   \brief This class is an attribute
	 */
	class Attribute
	{
		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Friendship ////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
		friend class Item;
		friend class AttributeGroup;

		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Signals ///////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	public:
		typedef sigc::signal<void, Item&, Attribute&> type_signal_changed;

		/*!
		 *  \brief Return the changed signal
		 *  \return the signal
		 */
		inline type_signal_changed signalChanged()
		{
			return signal_changed_;
		}


	protected:
		/*!
		 *  \brief Emit the changed signal
		 */
		inline void emitSignalChanged()
		{
			signalChanged().emit(dbi_, *this);
		}

	private:
		type_signal_changed signal_changed_; /*!< The signal when changed*/



		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Attributes ////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	protected:
		Item&		dbi_;
		std::string name_;
		Version		database_version_;
		bool		nullable_;


		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Constructor ///////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	public:
		explicit Attribute(Item& dbi, const std::string& name, const Version& databaseVersion, const bool nullable)
				: dbi_(dbi), name_(name), database_version_(databaseVersion), nullable_(nullable)
		{
		}
		explicit Attribute(const Attribute& attribute) = delete;
		explicit Attribute(const Attribute& attribute, Item& item);

		virtual ~Attribute();


		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Function //////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	public:
		bool sameDbiId(const Attribute& attribute);

		bool sameDbiType(const Attribute& attribute);

		virtual std::string toString() const
		{
			return format("Attribute `{}` since version {}", name(), database_version_.toString());
		}


		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Getter ////////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	public:
		inline std::string name() const
		{
			return name_;
		}

		inline Version databaseVersion() const
		{
			return database_version_;
		}

		inline bool nullable() const
		{
			return nullable_;
		}

		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Setter ////////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	public:
		virtual Attribute& operator=([[maybe_unused]] const Attribute& attribute)
		{
			return *this;
		}
	};


}	 // namespace database


#endif /* DATABASEATTRIBUTE_H */
