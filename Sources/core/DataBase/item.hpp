/*!
 * \file    item.h
 * \author  Remi BERTHO
 * \date    14/03/16
 * \version 1.0.0
 */

/*
 * item.h
 *
 * Copyright 2017-2020 Rémi BERTHO <remi.bertho@dalan.fr>
 *
 * This file is part of GDV.
 *
 * GDVis free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * GDV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */


#ifndef DATABASEITEM_H
#define DATABASEITEM_H

#include <SQLiteCpp/SQLiteCpp.h>
#include <sigc++/sigc++.h>
#include <glibmm.h>

#include "share.hpp"
#include "version.hpp"

#include "attribute_group.hpp"


namespace action
{
	class Action;
}	 // namespace action


namespace database
{
	class Item;

	/** Item smart pointer */
	typedef std::shared_ptr<Item> ItemPtr;


	/*! \class Item
	 *   \brief This class is a item
	 */
	class Item
	{
		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Friendship ////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
		friend class DataBase;
		friend class action::Action;
		friend class Attribute;
		friend class Property;
		friend class Link;

		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Constants /////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	private:
		static const constexpr int DEFAULT_SQL_STATEMENT_SIZE = 250;


		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Attributes ////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	private:
		int64_t id_; /**< The ID of the item in the database */
		bool	linked_to_database_;

		AttributeGroup properties_;

		ItemPtr old_value_ = nullptr;


		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Signals ///////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	public:
		typedef AttributeGroup::type_signal_changed type_signal_changed;

		/*!
		 *  \brief Return the changed signal
		 *  \return the signal
		 */
		inline type_signal_changed signalChanged()
		{
			return properties_.signalChanged();
		}


	protected:
		/*!
		 *  \brief Emit the changed signal
		 */
		inline void emitSignalChanged()
		{
			signalChanged().emit(*this);
		}


		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Constructor ///////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	public:
		/*!
		 *  \brief Default constructor
		 *  \param id the id
		 */
		explicit Item(const int64_t id);

		/*!
		 *  \brief Destructor
		 */
		virtual ~Item();

	protected:
		/*!
		 *  \brief Copy constructor
		 */
		explicit Item(const Item& dbi);


		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Function //////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	public:
		/*!
		 *  \brief Convert to a string
		 *  \return the string
		 */
		virtual std::string toString() const = 0;

		/*!
		 *  \brief Operator <<
		 *  \param os the ostream
		 *  \param dbi the Item
		 *  \return the ostream
		 */
		friend std::ostream& operator<<(std::ostream& os, const Item& dbi);

		/*!
		 *  \brief Copy datas
		 *  \param dbi a Item
		 *  \return a reference to the object
		 */
		Item& operator=(const Item& dbi);

		/**
		 * @brief operator ==, check if all the data except id and old_value are the same
		 * @param dbi another Item
		 * @return true
		 */
		inline bool operator==([[maybe_unused]] const Item& dbi) const
		{
			return true;
		}

		/**
		 * @brief Clone the dbi
		 * @return the clone in a shared_ptr
		 */
		virtual ItemPtr clone() const = 0;

		/**
		 * @brief databaseName
		 * @return the name used in database
		 */
		virtual std::string dataBaseTableName() const = 0;

		/**
		 * @brief Check if two dbi has the same id
		 * @param dbi
		 * @return
		 */
		inline bool sameDatabaseId(const Item& dbi) const
		{
			return id() == dbi.id();
		}

		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Database //////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	protected:
		bool	createTableInDatabase(SQLite::Database& db) const;
		bool	updateTableInDatabase(SQLite::Database& db, const core::Version& current_version, const core::Version& last_version) const;
		void	addInDatabase(SQLite::Database& db) const;
		void	deleteInDatabase(SQLite::Database& db) const;
		void	saveInDatabase(SQLite::Database& db) const;
		void	setFromDatabase(SQLite::Database& db);
		int64_t getNextIdInDatabase(SQLite::Database& db) const;
		int		resetOrphanLink(SQLite::Database& db) const;

		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Getter and setter /////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	private:
		/*!
		 *  \brief Set the id
		 *  \param id the id
		 */
		inline void setId(const int64_t id)
		{
			id_ = id;
		}

		inline AttributeGroup& properties()
		{
			return properties_;
		}

	protected:
		inline ItemPtr getOld() const
		{
			return old_value_;
		}

		inline void setOld()
		{
			old_value_ = nullptr;
			old_value_ = clone();
		}

	public:
		/*!
		 *  \brief Return the id
		 *  \return the id
		 */
		inline int64_t id() const
		{
			return id_;
		}

		/**
		 * @brief isLinkedToDatabase
		 * @return true if the object is linked to the detabase, false otherwise
		 */
		inline bool linkedToDatabase() const
		{
			return linked_to_database_;
		}

		inline const AttributeGroup& properties() const
		{
			return properties_;
		}
	};
}	 // namespace database


#endif /* DATABASEITEM_H */
