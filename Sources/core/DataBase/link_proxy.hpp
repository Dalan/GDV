/*!
 * \file
 */

/*
 * link_proxy.h
 *
 * Copyright 2019 Rémi BERTHO <remi.bertho@dalan.fr>
 *
 * This file is part of GDV.
 *
 * GDVis free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * GDV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */


#ifndef DATABASELINK_PROXY_H
#define DATABASELINK_PROXY_H

#include "link.hpp"

using namespace core;

namespace database
{

	/*! \class LinkProxy
	 *   \brief This class is a link_proxy
	 */
	template <const char* name, const char* databaseVersion, const char* database_name, const bool nullable = false> class LinkProxy
	{
		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Attributes ////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	protected:
		Link link_;

		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Constructor ///////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	public:
		explicit LinkProxy(Item& item, Item* link_item = nullptr)
				: link_(item, name, Version(databaseVersion), link_item, database_name, nullable)
		{
		}
		explicit LinkProxy(Item& item, const int64_t id = Link::EMPTY_VALUE)
				: link_(item, name, Version(databaseVersion), id, database_name, nullable)
		{
		}
		explicit LinkProxy(const LinkProxy& link_proxy) = delete;
		explicit LinkProxy(const LinkProxy& link_proxy, Item& item) : link_(link_proxy.link_, item)
		{
		}

		virtual ~LinkProxy() = default;


		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Function //////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	public:
		inline bool equal(const LinkProxy& p) const
		{
			return link_.equal(p.link_);
		}

		inline bool operator==(const int64_t& t) const
		{
			return link_ == t;
		}

		inline bool operator==(Item* t) const
		{
			return link_ == t;
		}

		std::string toString() const
		{
			return link_.toString();
		}


		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Getter ////////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	public:
		// Operator T
		inline operator int64_t() const
		{
			return link_;
		}
		inline operator Item*() const
		{
			return link_;
		}

		// Get Value
		int64_t getId() const
		{
			return link_.id();
		}
		Item* getItem() const
		{
			return link_.item();
		}

		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Setter ////////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	public:
		// Operator =
		inline LinkProxy& operator=(int64_t t)
		{
			link_ = t;
			return *this;
		}
		inline LinkProxy& operator=(Item* t)
		{
			link_ = t;
			return *this;
		}
		LinkProxy& operator=(const LinkProxy& dbp)
		{
			link_ = dbp.link_;
		}

		// Set Value
		void setId(int64_t value)
		{
			link_.setValue(value);
		}
		void setItem(Item* value)
		{
			link_.setValue(value);
		}
	};


}	 // namespace database


#endif /* DATABASELINK_PROXY_H */
