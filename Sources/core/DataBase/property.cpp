/*!
 * \file    property.cpp
 * \author  Remi BERTHO
 * \date    26/06/18
 * \version 1.0.0
 */

/*
 * property.cpp
 *
 * Copyright 2018 Rémi BERTHO <remi.bertho@dalan.fr>
 *
 * This file is part of GDV.
 *
 * GDV is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * GDV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

#include "log.hpp"

#include "property.hpp"

#include "exception.hpp"

using namespace std;
using namespace Glib;

namespace database
{
	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Constructor and destructor ////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
	Property::Property(const Property& dbp, Item& dbi) : Property(dbi, dbp.name(), dbp.databaseVersion(), dbp.value_)
	{
	}


	Property::~Property() = default;



	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Function //////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
	void Property::bindSqlStatement(SQLite::Statement& statement, const int index) const
	{
		const type_info& real_type = type();

		if (real_type == INT_TYPE)
		{
			auto value = getValue<int>();
			statement.bind(index, value);
		}
		else if (real_type == UINT_TYPE)
		{
			auto value = getValue<unsigned int>();
			statement.bind(index, value);
		}
		else if (real_type == INT64_TYPE)
		{
			auto value = getValue<int64_t>();
			statement.bind(index, value);
		}
		else if (real_type == BOOL_TYPE)
		{
			auto value = getValue<bool>();
			statement.bind(index, value);
		}
		else if (real_type == DOUBLE_TYPE)
		{
			auto value = getValue<double>();
			statement.bind(index, value);
		}
		else if (real_type == DATE_TYPE)
		{
			const auto& value = getValue<Date>();
			statement.bind(index, value.get_julian());
		}
		else if (real_type == STRING_TYPE)
		{
			const auto& value = getValue<string>();
			statement.bind(index, value);
		}
		else if (real_type == VECTOR_STRING_TYPE)
		{
			const auto& vec = getValue<vector<string>>();
			string		value;
			for (auto& str : vec)
			{
				value += (str + VECTOR_SEPARATOR);
			}
			statement.bind(index, value);
		}
		else
		{
			string error = format("Cannot bind type `{}` in SQL", real_type.name());
			GDV_ERROR(error);
			throw UnknownPropertyType(error);
		}
	}

	string Property::getSqlType() const
	{
		const type_info& real_type = type();
		if (real_type == INT_TYPE)
		{
			return "INTEGER";
		}
		if (real_type == UINT_TYPE)
		{
			return "INTEGER";
		}
		if (real_type == INT64_TYPE)
		{
			return "INTEGER";
		}
		if (real_type == BOOL_TYPE)
		{
			return "INTEGER";
		}
		if (real_type == DOUBLE_TYPE)
		{
			return "REAL";
		}
		if (real_type == DATE_TYPE)
		{
			return "INTEGER";
		}
		if (real_type == STRING_TYPE)
		{
			return "TEXT";
		}
		if (real_type == VECTOR_STRING_TYPE)
		{
			return "TEXT";
		}

		string error = format("Cannot get sql type for type `{}` in SQL", real_type.name());
		GDV_ERROR(error);
		throw UnknownPropertyType(error);
	}

	string Property::getAsString() const
	{
		const type_info& real_type = type();

		if (real_type == INT_TYPE)
		{
			auto value = getValue<int>();
			return i64tostr(value);
		}
		if (real_type == UINT_TYPE)
		{
			auto value = getValue<unsigned int>();
			return u64tostr(value);
		}
		if (real_type == INT64_TYPE)
		{
			auto value = getValue<int64_t>();
			return i64tostr(value);
		}
		if (real_type == BOOL_TYPE)
		{
			auto value = getValue<bool>();
			return itostr(static_cast<bool>(value));
		}
		if (real_type == DOUBLE_TYPE)
		{
			auto value = getValue<double>();
			return dtostr(value);
		}
		if (real_type == DATE_TYPE)
		{
			const auto& value = getValue<Date>();
			return u64tostr(value.get_julian());
		}
		if (real_type == STRING_TYPE)
		{
			const auto& value = getValue<string>();
			return "'" + value + "'";
		}
		else if (real_type == VECTOR_STRING_TYPE)
		{
			const auto& vec = getValue<vector<string>>();
			string		value;
			for (auto& str : vec)
			{
				value += (str + VECTOR_SEPARATOR);
			}
			return "'" + value + "'";
		}

		string error = format("Cannot bind type `{}` in SQL", real_type.name());
		GDV_ERROR(error);
		throw UnknownPropertyType(error);
	}

	void Property::setFromSqlColumn(const SQLite::Column& column)
	{
		if (column.getType() != SQLite::Null)
		{
			const type_info& real_type = type();

			if (real_type == INT_TYPE)
			{
				setValue(static_cast<int>(column.getInt()));
			}
			else if (real_type == UINT_TYPE)
			{
				setValue(static_cast<unsigned int>(column.getInt()));
			}
			else if (real_type == INT64_TYPE)
			{
				setValue(static_cast<int64_t>(column.getInt64()));
			}
			else if (real_type == BOOL_TYPE)
			{
				setValue(static_cast<bool>(column.getInt()));
			}
			else if (real_type == DOUBLE_TYPE)
			{
				setValue(column.getDouble());
			}
			else if (real_type == DATE_TYPE)
			{
				setValue(Date(static_cast<uint32_t>(column.getInt())));
			}
			else if (real_type == STRING_TYPE)
			{
				setValue(string(column.getText()));
			}
			else if (real_type == VECTOR_STRING_TYPE)
			{
				vector<string> vec;
				stringstream   ss(column.getText());
				string		   tok;

				while (getline(ss, tok, VECTOR_SEPARATOR))
				{
					vec.push_back(tok);
				}

				setValue(vec);
			}
			else
			{
				string error = format("Cannot set type `{}` in SQL", real_type.name());
				GDV_ERROR(error);
				throw UnknownPropertyType(error);
			}
		}
	}

}	 // namespace database
