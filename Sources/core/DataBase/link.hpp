/*!
 * \file    link.h
 * \author  Remi BERTHO
 * \date    26/06/18
 * \version 1.0.0
 */

/*
 * link.h
 *
 * Copyright 2018 Rémi BERTHO <remi.bertho@dalan.fr>
 *
 * This file is part of GDV.
 *
 * GDVis free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * GDV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */


#ifndef DATABASELINK_H
#define DATABASELINK_H

#include <glibmm.h>
#include <vector>

#include "exception.hpp"
#include "item.hpp"
#include "version.hpp"
#include "item.hpp"
#include "property.hpp"

using namespace core;

namespace database
{

	/*! \class Link
	 *   \brief This class is a link
	 */
	class Link : public Property
	{
		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Constants /////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	public:
		static const constexpr int64_t EMPTY_VALUE = -1;

		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Attributes ////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	private:
		std::string reference_table_;
		int64_t		no_ptr_link_id_ = EMPTY_VALUE;

		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Constructor ///////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	public:
		explicit Link(Item&		   dbi,
				const std::string& name,
				const Version&	   databaseVersion,
				Item*			   item,
				const std::string& database_name,
				const bool		   nullable = false)
				: Property(dbi, name, databaseVersion, item, nullable), reference_table_(database_name)
		{
			if (item != nullptr)
				no_ptr_link_id_ = item->id();
		}
		explicit Link(Item&		   dbi,
				const std::string& name,
				const Version&	   databaseVersion,
				const int64_t	   id,
				const std::string& database_name,
				const bool		   nullable = false)
				: Property(dbi, name, databaseVersion, static_cast<Item*>(nullptr), nullable), reference_table_(database_name),
				  no_ptr_link_id_(id)
		{
		}
		explicit Link(const Link& dbp) = delete;
		explicit Link(const Link& l, Item& item);

		virtual ~Link();


		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Function //////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	protected:
		virtual void bindSqlStatement(SQLite::Statement& statement, const int index) const;

		virtual std::string getSqlType() const;

	public:
		virtual std::string toString() const
		{
			return format("Link `{}` link id `{}` table name `{}`", name(), id(), referenceTable());
		}

		inline bool equal(const Link& link) const
		{
			return id() == link.id();
		}


		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Getter ////////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	public:
		// Get Value
		const std::string& referenceTable() const
		{
			return reference_table_;
		}

		inline Item* item() const
		{
			return getValue<Item*>();
		}

		inline int64_t id() const
		{
			if (item() != nullptr)
				return item()->id();
			return no_ptr_link_id_;
		}

		inline operator int64_t() const
		{
			return id();
		}

		inline operator Item*() const
		{
			return item();
		}

		virtual std::string getAsString() const;

		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Setter ////////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	protected:
		// Set from sql
		virtual void setFromSqlColumn(const SQLite::Column& column);

	public:
		Link& operator=(const Link& l);

		Property& operator=(int64_t t);

		Property& operator=(Item* t);

		void setId(int64_t id);

		void setItem(Item* t);
	};


}	 // namespace database


#endif /* DATABASELINK_H */
