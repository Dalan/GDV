/*!
 * \file    link.cpp
 * \author  Remi BERTHO
 * \date    26/06/18
 * \version 1.0.0
 */

/*
 * link.cpp
 *
 * Copyright 2018 Rémi BERTHO <remi.bertho@dalan.fr>
 *
 * This file is part of GDV.
 *
 * GDV is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * GDV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

#include "log.hpp"

#include "link.hpp"

#include "exception.hpp"

using namespace std;
using namespace Glib;

namespace database
{
	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Constructor and destructor ////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
	Link::Link(const Link& l, Item& dbi) : Property(l, dbi), reference_table_(l.reference_table_), no_ptr_link_id_(l.no_ptr_link_id_)
	{
		if (l.item() != nullptr)
		{
			no_ptr_link_id_ = l.id();
			value_			= static_cast<Item*>(nullptr);
		}
	}


	Link::~Link() = default;




	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Function //////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
	void Link::bindSqlStatement(SQLite::Statement& statement, const int index) const
	{
		auto	item = getValue<Item*>();
		int64_t id;
		if (item != nullptr)
		{
			id = item->id();
		}
		else
		{
			id = no_ptr_link_id_;
		}
		if (id == -1)
		{
			statement.bind(index);
		}
		else
		{
			statement.bind(index, id);
		}
	}

	string Link::getSqlType() const
	{
		return "INTEGER";
	}

	string Link::getAsString() const
	{
		auto item = getValue<Item*>();
		if (item != nullptr)
		{
			return i64tostr(item->id());
		}
		else
		{
			return i64tostr(no_ptr_link_id_);
		}
	}

	void Link::setFromSqlColumn(const SQLite::Column& column)
	{
		if (column.isNull())
			no_ptr_link_id_ = -1;
		else
			no_ptr_link_id_ = column.getInt64();
		setValue<Item*>(nullptr);
	}

	Property& Link::operator=(Item* t)
	{
		no_ptr_link_id_ = EMPTY_VALUE;
		setValue<Item*>(t);
		return *this;
	}

	Property& Link::operator=(int64_t t)
	{
		setId(t);
		return *this;
	}

	Link& Link::operator=(const Link& l)
	{
		Property::operator= <Item*>(l);
		reference_table_ = l.reference_table_;
		no_ptr_link_id_	 = l.no_ptr_link_id_;
		return *this;
	}

	void Link::setId(int64_t id)
	{
		if (item() != nullptr)
		{
			if (item()->id() == id)
			{
				no_ptr_link_id_ = id;
				return;
			}
		}

		if (id != no_ptr_link_id_)
		{
			no_ptr_link_id_ = id;
			if (item() != nullptr)
				setValue<Item*>(nullptr);
			else
				emitSignalChanged();
		}
	}

	void Link::setItem(Item* t)
	{
		if (t->id() != no_ptr_link_id_)
		{
			no_ptr_link_id_ = EMPTY_VALUE;
			setValue<Item*>(t);
		}
		else
			value_ = t;
	}

}	 // namespace database
