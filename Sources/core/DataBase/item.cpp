/*!
 * \file    item.cpp
 * \author  Remi BERTHO
 * \date    14/03/16
 * \version 1.0.0
 */

/*
 * item.cpp
 *
 * Copyright 2017-2020 Rémi BERTHO <remi.bertho@dalan.fr>
 *
 * This file is part of GDV.
 *
 * GDV is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * GDV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

#include "log.hpp"
#include <fmt/core.h>

#include "item.hpp"

#include "property.hpp"
#include "link.hpp"

using namespace std;
using namespace Glib;
using namespace SQLite;

namespace database
{
	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Constructor and destructor ////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
	Item::Item(const int64_t id) : id_(id), linked_to_database_(true), properties_(AttributeGroup::PROPERTY)
	{
	}

	Item::Item(const Item& dbi)
			: id_(dbi.id()), linked_to_database_(false), properties_(AttributeGroup::PROPERTY), old_value_(dbi.old_value_)
	{
	}


	Item::~Item() = default;




	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Function //////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
	ostream& operator<<(ostream& os, const Item& dbi)
	{
		os << dbi.toString();
		return os;
	}

	Item& Item::operator=(const Item& dbi)
	{
		setId(dbi.id());

		properties().blockSignal();
		properties() = dbi.properties();
		properties().unblockSignal();

		emitSignalChanged();

		return *this;
	}

	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Database //////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
	void Item::addInDatabase(SQLite::Database& db) const
	{
		string sql;
		sql.reserve(DEFAULT_SQL_STATEMENT_SIZE);

		sql += "INSERT INTO `" + dataBaseTableName() + "` (`id`";
		for (database::Attribute& attribute : properties())
		{
			sql += ",`" + attribute.name() + "`";
		}

		sql += ") VALUES (?";
		for (size_t i = 0; i < properties().size(); i++)
		{
			sql += ",?";
		}

		sql += ")";
		Statement st(db, sql);

		st.bind(1, id());
		int index = 2;
		for (const database::Attribute& attribute : properties())
		{
			dynamic_cast<const database::Property&>(attribute).bindSqlStatement(st, index++);
		}

		GDV_DEBUG("Execute \"" + st.getExpandedSQL() + "\"");
		st.exec();
	}

	void Item::deleteInDatabase(SQLite::Database& db) const
	{
		Statement st(db, format("DELETE FROM `{}` WHERE `id`=?", dataBaseTableName()));
		st.bind(1, id());
		GDV_DEBUG("Execute \"" + st.getExpandedSQL() + "\"");
		st.exec();
	}

	void Item::saveInDatabase(SQLite::Database& db) const
	{
		string sql;
		sql.reserve(DEFAULT_SQL_STATEMENT_SIZE);

		sql += "UPDATE `" + dataBaseTableName() + "` SET ";
		for (database::Attribute& attribute : properties())
		{
			sql += attribute.name() + "=?,";
		}
		sql.erase(sql.length() - 1, 1);

		sql += " WHERE `id`=?";

		Statement st(db, sql);
		int		  index = 1;
		for (const database::Attribute& attribute : properties())
		{
			dynamic_cast<const database::Property&>(attribute).bindSqlStatement(st, index++);
		}
		st.bind(index, id());

		GDV_DEBUG("Execute \"" + st.getExpandedSQL() + "\"");
		st.exec();
	}

	void Item::setFromDatabase(Database& db)
	{
		string	  sql = format("SELECT * FROM `{}` WHERE `id` = ?", dataBaseTableName());
		Statement query(db, sql);
		query.bind(1, id());

		query.executeStep();

		for (Attribute& attribute : properties())
		{
			dynamic_cast<Property&>(attribute).setFromSqlColumn(query.getColumn(attribute.name().c_str()));
		}
	}

	bool Item::createTableInDatabase(Database& db) const
	{
		if (db.tableExists(dataBaseTableName()))
		{
			return false;
		}

		string sql;
		sql.reserve(DEFAULT_SQL_STATEMENT_SIZE);
		vector<const Link*> links;

		sql += "CREATE TABLE  `" + dataBaseTableName() + "` (`id` INTEGER NOT NULL PRIMARY KEY UNIQUE";
		for (database::Attribute& attribute : properties())
		{
			const auto& property = dynamic_cast<const Property&>(attribute);
			sql += format(", `{}` {} DEFAULT {}", attribute.name(), property.getSqlType(), property.getAsString());
			if (!attribute.nullable())
			{
				sql += " NOT NULL";
			}

			const Link* link = dynamic_cast<Link*>(&attribute);
			if (link != nullptr)
			{
				links.push_back(link);
			}
		}
		if (!links.empty())
		{
			for (auto link : links)
			{
				sql += format(", FOREIGN KEY(`{}`) REFERENCES `{}`(`id`)", link->name(), link->referenceTable());
			}
		}

		sql += ") WITHOUT ROWID";


		GDV_DEBUG("Execute \"" + sql + "\"");
		db.exec(sql);

		GDV_INFO(format("Create table `{}`", dataBaseTableName()));

		return true;
	}

	int64_t Item::getNextIdInDatabase(Database& db) const
	{
		return db.execAndGet(format("SELECT max(`id`) FROM `{}`", dataBaseTableName())).getInt64() + 1;
	}

	bool Item::updateTableInDatabase(Database& db, const core::Version& current_version, const core::Version& last_version) const
	{
		bool update = false;

		for (database::Attribute& attribute : properties())
		{
			if ((last_version >= attribute.databaseVersion()) && (attribute.databaseVersion() > current_version))
			{
				const auto& property = dynamic_cast<const Property&>(attribute);
				const Link* link	 = dynamic_cast<const Link*>(&property);
				string sql = format("ALTER TABLE `{}` ADD COLUMN `{}` {}", dataBaseTableName(), property.name(), property.getSqlType());
				if (((link != nullptr) && (link->id() != -1)) || (link == nullptr))
				{
					sql += format(" DEFAULT {}", property.getAsString());
				}
				if (!attribute.nullable())
				{
					sql += " NOT NULL";
				}

				if (link != nullptr)
				{
					sql += format(" REFERENCES `{}`(`id`)", link->referenceTable());
				}

				GDV_DEBUG("Execute \"" + sql + "\"");
				db.exec(sql);

				update = true;
			}
		}

		return update;
	}

	int Item::resetOrphanLink(Database& db) const
	{
		int nb_reset = 0;

		for (database::Attribute& attribute : properties())
		{
			const Link* link = dynamic_cast<Link*>(&attribute);
			if (link != nullptr)
			{
				string sql = fmt::format("UPDATE `{0}` SET `{1}`=NULL WHERE NOT EXISTS "
										 "(SELECT NULL FROM `{2}` WHERE `{0}`.`{1}` = `{2}`.`id`)"
										 " AND `{0}`.`{1}` IS NOT NULL AND `{0}`.`{1}` != \"-1\"",
						dataBaseTableName(),
						link->name(),
						link->referenceTable());
				GDV_DEBUG("Execute \"" + sql + "\"");
				nb_reset += db.exec(sql);
			}
		}

		return nb_reset;
	}



}	 // namespace database
