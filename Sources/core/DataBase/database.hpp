/*!
 * \file    database.h
 * \author  Remi BERTHO
 * \date    17/03/16
 * \version 1.0.0
 */

/*
 * database.h
 *
 * Copyright 2017-2020 Rémi BERTHO <remi.bertho@dalan.fr>
 *
 * This file is part of GDV.
 *
 * GDVis free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * GDV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */


#ifndef DATABASE_H_INCLUDED
#define DATABASE_H_INCLUDED

#include <typeindex>
#include <atomic>
#include <optional>

#include "Generic/object.hpp"
#include "Place/place.hpp"
#include "Place/gas_station.hpp"
#include "Car/car.hpp"
#include "Car/insurance.hpp"
#include "Car/service.hpp"
#include "Actions/list.hpp"
#include "log.hpp"

namespace place
{
	typedef std::shared_ptr<place::GasStation> GasStationPtr;
}	 // namespace place

namespace database
{
	class DataBase;

	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Typedef ///////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
	typedef std::shared_ptr<DataBase>	   DataBasePtr;
	typedef std::vector<generic::ObjectId> ObjectIds;



	/*! \class DataBase
	 *   \brief This class represent a database
	 */
	class DataBase
	{
		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// DB filename ///////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	private:
		static const constexpr char* DB_FILENAME = "gdv.db3"; /**< The database filename */
#ifdef PORTABLE
		inline const std::string DB_FULL_FILENAME()
		{
			return DB_FILENAME;
		}
#else
		inline const std::string DB_FULL_FILENAME()
		{
			return Glib::build_filename(Glib::locale_to_utf8((Glib::get_home_dir())), ".local", "share", "gdv", DB_FILENAME);
		}
#endif	  // PORTABLE

		static const constexpr int MAJOR_VERSION = 0;
		static const constexpr int MINOR_VERSION = 2;
		static const constexpr int MICRO_VERSION = 9;


		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Enumeration ///////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	public:
		enum FunctionDone
		{
			SAVE,
			DELETE_CHANGES,
			UNDO,
			REDO
		};


		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Signals ///////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	public:
		typedef sigc::signal<void, const bool>	 type_signal_change_request;
		typedef sigc::signal<void, FunctionDone> type_signal_function_done;

		/*!
		 *  \brief Return the changed signal
		 *  \return the signal
		 */
		inline type_signal_change_request signalChangeRequest()
		{
			return signal_change_request_;
		}

		inline type_signal_function_done signalFunctionDone()
		{
			return signal_function_done_;
		}


	protected:
		/*!
		 *  \brief Emit the changed signal
		 */
		inline void emitSignalChangeRequest()
		{
			signalChangeRequest().emit(needSave());
		}

		inline void emitSignalFunctionDone(FunctionDone fn)
		{
			signalFunctionDone().emit(fn);
		}

	private:
		type_signal_change_request signal_change_request_; /*!< The signal when changed*/
		type_signal_function_done  signal_function_done_;  /*!< The signal when changed*/

		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Attributes ////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	private:
		SQLite::Database										  db_; /**< The Sqlite database */
		action::List											  actions_;
		std::unordered_map<std::type_index, std::atomic<int64_t>> nextID_;
		std::vector<sigc::connection>							  signal_connection_list_;


		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Constructor ///////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	private:
		/*!
		 *  \brief Default constructor
		 */
		DataBase();

	public:
		/*!
		 *  \brief Initialize the database
		 *  \return The Database in a smart pointer
		 *  \exception SQLError if an error occurred
		 */
		static DataBasePtr initialize();



		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Functions /////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	private:
		/**
		 * @brief Add an action in the action list
		 * @param action
		 */
		void addAction(action::Action* action);

		/**
		 * @brief Add a slot in the slot list
		 */
		inline void slotAdd(sigc::connection slot_connexion)
		{
			signal_connection_list_.push_back(slot_connexion);
		}

		/**
		 * @brief Block all the slot
		 */
		void slotBlock();

		/**
		 * @brief Unblock all the slot
		 */
		void slotUnblock();

		/**
		 * @brief Connect all the signals for a car
		 * @param c the car
		 */
		void connectCarSignals(const car::CarPtr& c);

		/**
		 * @brief Connect all the signals for a car
		 * @param c the car
		 */
		void connectElementSignals(generic::Element* element);

		/**
		 * @brief Get the last version of the Database available
		 * @return the last version of the Database available
		 */
		static Version getLastVersion();

		template <typename OwnedObjectClass> void cleanOrphanObjects()
		{
			static_assert(std::is_base_of_v<generic::OwnedObject, OwnedObjectClass>, "The class must be derived from OwnedObject");
			OwnedObjectClass item(1);

			std::string sql = std::format("DELETE FROM `{0}` WHERE NOT EXISTS "
										  "(SELECT NULL FROM `{1}` WHERE `{0}`.`owner_id` = `{1}`.`id`)",
					OwnedObjectClass::DATABASE_TABLE_NAME,
					item.ownerTable());
			GDV_DEBUG("Execute \"" + sql + "\"");
			int nb_delete = db().exec(sql);
			if (nb_delete > 0)
			{
				GDV_INFO(std::format("Delete {} orphan from {} table", nb_delete, OwnedObjectClass::DATABASE_TABLE_NAME));
			}
		}

		template <typename ItemClass> void cleanOrphanLinks()
		{
			static_assert(std::is_base_of_v<Item, ItemClass>, "The class must be derived from Item");
			ItemClass item(1);
			int		  nb_reset = item.resetOrphanLink(db());
			if (nb_reset > 0)
			{
				GDV_INFO(std::format("Clean {} orphan links from {} table", nb_reset, ItemClass::DATABASE_TABLE_NAME));
			}
		}

		/**
		 * @brief Initialize an Item type in the Database
		 */
		template <typename ItemClass> void initItem(const core::Version& current_version, const core::Version& last_version)
		{
			static_assert(std::is_base_of_v<Item, ItemClass>, "The class must be derived from Item");
			ItemClass item(1);
			if (item.createTableInDatabase(db()) == false)
				item.updateTableInDatabase(db(), current_version, last_version);
			nextId<ItemClass>() = item.getNextIdInDatabase(db());
		}

	public:
		/**
		 * @brief Get the current version of the Database
		 * @return the current version of the Database
		 */
		Version getCurrentVersion();

		/**
		 * @brief Delete all object with no owner and reset link with no object
		 */
		void cleanOrphans();

		/*!
		 *  \brief Get the object name and id
		 *  \return The object names and ids
		 */
		template <typename ObjectClass> auto objectsId()
		{
			static_assert(std::is_base_of_v<generic::Object, ObjectClass>, "The class must be derived from generic::Object");

			ObjectIds list;

			std::string sql = std::format("SELECT `name`,`id`  FROM `{}` ORDER BY `name`", ObjectClass::DATABASE_TABLE_NAME);

			SQLite::Statement query(db(), sql);

			while (query.executeStep())
			{
				std::pair<std::string, int64_t> car_id = std::make_pair(query.getColumn(0).getText(), query.getColumn(1).getInt64());
				list.push_back(car_id);
			}

			return list;
		}

		/*!
		 *  \brief Get the object names
		 *  \return The object names
		 */
		template <typename ObjectClass> auto objectsName()
		{
			static_assert(std::is_base_of_v<generic::Object, ObjectClass>, "The class must be derived from generic::Object");

			std::vector<std::string> list;

			std::string sql = std::format("SELECT `name` FROM `{}` GROUP BY `name` ORDER BY `name`", ObjectClass::DATABASE_TABLE_NAME);

			SQLite::Statement query(db(), sql);

			while (query.executeStep())
			{
				list.push_back(query.getColumn(0).getText());
			}

			return list;
		}


		/*!
		 *  \brief Get the fuel price
		 *  \param fuel the fuel type
		 *  \return All the fuel price
		 */
		template <typename VehicleClass>
		std::map<Glib::Date, double> fuelPrice(
				EngineType engine, std::optional<bool> additive = std::optional<bool>(), std::optional<bool> cheap = std::optional<bool>())
		{
			static_assert(std::is_base_of_v<vehicle::Vehicle, VehicleClass>, "The class must be derived from vehicle::Vehicle");

			std::map<Glib::Date, double> prices;
			int							 bind_index = 2;

			try
			{

				std::string sql = std::format("SELECT `begin_date`, `price_per_liter` FROM `{}_FullTanks`, `{}`",
						VehicleClass::DATABASE_ITEM_TABLE_PREFIX,
						VehicleClass::DATABASE_TABLE_NAME);
				if (cheap)
				{
					sql += std::format(", `{}` ", place::GasStation::DATABASE_TABLE_NAME);
				}
				sql += std::format("WHERE `{1}`.`id` = `{0}_FullTanks`.`owner_id` "
								   "and `{1}`.`engine_type`=? ",
						VehicleClass::DATABASE_ITEM_TABLE_PREFIX,
						VehicleClass::DATABASE_TABLE_NAME);

				if (additive)
				{
					sql += std::format(" and `{}_FullTanks`.`additive`=?", VehicleClass::DATABASE_ITEM_TABLE_PREFIX);
				}
				if (cheap)
				{
					sql += std::format(" and `{1}_FullTanks`.`station` = `{0}`.`id` and `{0}`.`cheap`=? ",
							place::GasStation::DATABASE_TABLE_NAME,
							VehicleClass::DATABASE_ITEM_TABLE_PREFIX);
				}
				sql += std::format("ORDER BY `{}_FullTanks`.`begin_date`", VehicleClass::DATABASE_ITEM_TABLE_PREFIX);

				SQLite::Statement query(db(), sql);
				query.bind(1, engine);
				if (additive)
				{
					query.bind(bind_index++, *additive);
				}
				if (cheap)
				{
					query.bind(bind_index++, *cheap);
				}

				while (query.executeStep())
				{
					int		   begin_date = query.getColumn(0).getInt();
					double	   price	  = query.getColumn(1).getDouble();
					Glib::Date date(static_cast<uint32_t>(begin_date));
					prices[date] = price;
				}
			}
			catch (SQLite::Exception& e)
			{
				GDV_WARNING(e.what());
				throw SQLError(std::format("Error when getting fuel price on the database for engine {}.", static_cast<int>(engine)));
			}

			GDV_DEBUG("Get fuel price");

			return prices;
		}


		/*!
		 *  \brief Get the mean fuel price
		 *  \param fuel the fuel type
		 *  \return The mean fuel price
		 */
		template <typename VehicleClass> double meanFuelPrice(EngineType engine)
		{
			static_assert(std::is_base_of_v<vehicle::Vehicle, VehicleClass>, "The class must be derived from vehicle::Vehicle");

			double price;

			try
			{
				std::string		  sql = std::format("SELECT AVG(`price_per_liter`) FROM `{0}_FullTanks`, `{1}`"
													" WHERE `{1}`.`id` = `{0}_FullTanks`.`owner_id` and `{1}`.`engine_type`=?",
						  VehicleClass::DATABASE_ITEM_TABLE_PREFIX,
						  VehicleClass::DATABASE_TABLE_NAME);
				SQLite::Statement query(db(), sql);
				query.bind(1, engine);

				query.executeStep();
				price = query.getColumn(0).getDouble();
			}
			catch (SQLite::Exception& e)
			{
				GDV_WARNING(e.what());
				throw SQLError(std::format("Error when getting mean fuel price en the database for type {}.", static_cast<int>(engine)));
			}

			GDV_DEBUG("Get mean fuel price");

			return price;
		}

		/**
		 * @brief Save all the change in the database
		 */
		void save();

		/**
		 * @brief deleteChanges
		 */
		void deleteChanges();

		/**
		 * @brief needSave
		 * @return
		 */
		bool needSave() const;

		/**
		 * @brief Delete all the current slot that track all the change on the Item in order to save it in the database
		 */
		void deleteCurrentTracking();

		/**
		 * @brief canUndo
		 * @return
		 */
		inline bool canUndo() const
		{
			return actions_.canUndo();
		}

		/**
		 * @brief canRedo
		 * @return
		 */
		inline bool canRedo() const
		{
			return actions_.canRedo();
		}

		/**
		 * @brief undo
		 */
		void undo(const vehicle::VehiclePtr& vehicle);

		/**
		 * @brief redo
		 */
		void redo(const vehicle::VehiclePtr& vehicle);



		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// New ///////////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	public:
		car::CarPtr newCar(const std::string& car_name);

		template <typename PlaceClass> auto newPlace(const std::string& name)
		{
			try
			{
				int64_t id	  = getNextId<PlaceClass>();
				auto	place = std::make_shared<PlaceClass>(id, name);

				place->addInDatabase(db());

				slotAdd(place->signalChanged().connect(sigc::mem_fun(*this, &DataBase::onDbiChanged)));

				GDV_DEBUG("Create new place");

				return place;
			}
			catch (SQLite::Exception& e)
			{
				GDV_WARNING(e.what());
				throw SQLError("Error during creating a place, a place with the same name is"
							   " probably already existing in the database.");
			}
		}



		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Get ///////////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	private:
		template <typename ItemClass, typename Position = std::nullptr_t> void getVehicleElements(car::CarPtr c, Position pos = nullptr)
		{
			static_assert(std::is_base_of_v<generic::Element, ItemClass>, "The class must be part of a vehicle");

			generic::ElementArray& array = c->elements<ItemClass, Position>(pos);

			std::string sql = std::format("SELECT * FROM `{}` WHERE `owner_id` = ?", ItemClass::DATABASE_TABLE_NAME);
			if constexpr (std::is_null_pointer_v<Position> == false)
				sql += " AND `position`=?";
			SQLite::Statement query(db(), sql);
			query.bind(1, c->id());
			if constexpr (std::is_null_pointer_v<Position> == false)
				query.bind(2, pos);

			while (query.executeStep())
			{
				int64_t id = query.getColumn(0);

				ItemClass* ci = new ItemClass(id);

				int index = 1;
				for (Attribute& attribute : ci->properties())
					static_cast<Property&>(attribute).setFromSqlColumn(query.getColumn(index++));

				ci->setOwner(c.get());

				if constexpr (std::is_same_v<ItemClass, car::Insurance> == true)
				{
					getElementsObject<ItemClass, car::Insurance::Guarantee>(ci);
				}
				else if constexpr (std::is_same_v<ItemClass, car::Service> == true)
				{
					getElementsObject<ItemClass, car::Service::Operation>(ci);
				}

				connectElementSignals(ci);
				array.add(ci);
			}
		}


		template <typename ItemClass, typename ObjectClass, typename Position = std::nullptr_t> void getElementsObject(ItemClass* item)
		{
			static_assert(std::is_base_of_v<generic::Element, ItemClass>, "The class must be part of a vehicle");
			static_assert(std::is_base_of_v<generic::Object, ObjectClass>, "The class must be an object");

			generic::ObjectArray& array = item->template objects<ObjectClass>();

			std::string		  sql = std::format("SELECT * FROM `{}` WHERE `owner_id` = ?", ObjectClass::DATABASE_TABLE_NAME);
			SQLite::Statement query(db(), sql);
			query.bind(1, item->id());

			while (query.executeStep())
			{
				int64_t id = query.getColumn(0);

				ObjectClass* object = new ObjectClass(id);

				int index = 1;
				for (Attribute& attribute : object->properties())
					static_cast<Property&>(attribute).setFromSqlColumn(query.getColumn(index++));

				object->setOwner(item);

				slotAdd(object->signalChanged().connect(sigc::mem_fun(*this, &DataBase::onDbiChanged)));
				array.add(object);
			}
		}

	public:
		car::CarPtr getCar(int64_t id);

		template <typename PlaceClass> auto getPlace(int64_t id)
		{
			static_assert(std::is_base_of_v<place::Place, PlaceClass>, "The class must be a place");
			try
			{
				auto place = std::make_shared<PlaceClass>(id, "");

				place->setFromDatabase(db());

				slotAdd(place->signalChanged().connect(sigc::mem_fun(*this, &DataBase::onDbiChanged)));

				GDV_DEBUG(std::format("Get place {}", id));

				return place;
			}
			catch (SQLite::Exception& e)
			{
				GDV_WARNING(e.what());
				throw SQLError(std::format("Error during getting a place, the place {}"
										   " is probably not existing in the database.",
						id));
			}
		}




		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Remove ////////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	private:
		/*!
		 *  \brief Remove a element for a vehicle
		 *  \param car_id the car id
		 */
		template <typename ItemClass> void removeVehicleElements(int64_t vehicle_id)
		{
			static_assert(std::is_base_of_v<generic::Element, ItemClass>, "The class must be derived from generic::Element");
			std::string		  sql = std::format("DELETE FROM `{}` WHERE `owner_id`=?", ItemClass::DATABASE_TABLE_NAME);
			SQLite::Statement st(db(), sql);
			st.bind(1, vehicle_id);
			st.exec();
		}

	public:
		void removeCar(int64_t id);

		template <typename ItemClass> void removeSingleItem(int64_t id)
		{
			try
			{
				std::string		  sql = std::format("DELETE FROM `{}` WHERE `id`=?", ItemClass::DATABASE_TABLE_NAME);
				SQLite::Statement st(db(), sql);
				st.bind(1, id);
				st.exec();

				GDV_DEBUG(std::format("Remove in table {} item {}", ItemClass::DATABASE_TABLE_NAME, id));
			}
			catch (SQLite::Exception& e)
			{
				GDV_WARNING(e.what());
				throw SQLError(std::format("Error during removing an item, the item {}"
										   " is probably not existing in the database.",
						id));
			}
		}


		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Slots /////////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	private:
		/*!
		 *  \brief Made the change to the database
		 *  \param item the Item
		 *  \param type the type of Item
		 */
		void onDbiChanged(database::Item& item);

		/**
		 * @brief Called when a car element is deleted from a car
		 * @param car the car element as ItemPtr
		 * @param index the index of the item
		 */
		void onVehicleElementDeleted(const database::ItemPtr& car, unsigned int index);

		/**
		 * @brief Called when a element is added in a car
		 * @param element the Service
		 * @param type the type of Item
		 */
		void onVehicleElementAdded(generic::Element& element);

		/**
		 * @brief Called when a car element is deleted from an element
		 * @param item the element as ItemPtr
		 * @param index the index of the object
		 */
		void onObjectDeleted(const database::ItemPtr& element, unsigned int index);

		/**
		 * @brief Called when an object is added in an element
		 * @param object the object added
		 */
		void onObjectAdded(generic::OwnedObject& object);


		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Getter and setter /////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	private:
		/*!
		 *  \brief Get the SQLite Database
		 *  \return Database
		 */
		inline SQLite::Database& db()
		{
			return db_;
		}

		template <typename ItemClass> inline auto& nextId()
		{
			static_assert(std::is_base_of_v<Item, ItemClass>, "The class must be derived from Item");
			return nextID_[std::type_index(typeid(ItemClass))];
		}

		template <typename ItemClass> inline int64_t getNextId()
		{
			static_assert(std::is_base_of_v<Item, ItemClass>, "The class must be derived from Item");
			return nextID_[std::type_index(typeid(ItemClass))]++;
		}
		inline int64_t getNextId(const Item& item)
		{
			return nextID_[std::type_index(typeid(item))]++;
		}
	};
}	 // namespace database

#endif	  // DATABASE_H_INCLUDED
