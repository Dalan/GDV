/*!
 * \file
 */

/*
 * property_proxy.h
 *
 * Copyright 2019 Rémi BERTHO <remi.bertho@dalan.fr>
 *
 * This file is part of GDV.
 *
 * GDVis free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * GDV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */


#ifndef DATABASEPROPERTY_PROXY_H
#define DATABASEPROPERTY_PROXY_H

#include "property.hpp"

using namespace core;

namespace database
{

	/*! \class PropertyProxy
	 *   \brief This class is a property_proxy
	 */
	template <typename T, const char* name, const char* databaseVersion, const bool nullable = false> class PropertyProxy
	{
		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Attributes ////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	protected:
		Property property_;

		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Constructor ///////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	public:
		explicit PropertyProxy(Item& item, T value = T()) : property_(item, name, Version(databaseVersion), value, nullable)
		{
		}
		explicit PropertyProxy(const PropertyProxy& property_proxy) = delete;
		explicit PropertyProxy(const PropertyProxy& property_proxy, Item& item) : property_(property_proxy.property_, item)
		{
		}

		virtual ~PropertyProxy() = default;


		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Function //////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	public:
		inline bool equal(const PropertyProxy& p) const
		{
			return property_.equal<T>(p.property_);
		}

		inline bool operator==(const T& t) const
		{
			return property_ == t;
		}

		std::string toString() const
		{
			return property_.toString<T>();
		}


		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Getter ////////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	public:
		// Operator T
		inline operator T() const
		{
			return property_;
		}

		// Get Value
		T getValue() const
		{
			return property_.getValue<T>();
		}

		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Setter ////////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	public:
		// Operator =
		inline PropertyProxy& operator=(T t)
		{
			property_ = t;
			return *this;
		}
		PropertyProxy& operator=(const PropertyProxy& dbp)
		{
			property_ = dbp.property_;
		}

		// Set Value
		void setValue(T value)
		{
			property_.setValue<T>(value);
		}
	};


}	 // namespace database


#endif /* DATABASEPROPERTY_PROXY_H */
