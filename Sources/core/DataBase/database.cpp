/*!
 * \file    database.cpp
 * \author  Remi BERTHO
 * \date    17/03/16
 * \version 1.0.0
 */

/*
 * database.cpp
 *
 * Copyright 2017 Rémi BERTHO <remi.bertho@dalan.fr>
 *
 * This file is part of GDV.
 *
 * GDVis free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * GDV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

#include "log.hpp"

#include "database.hpp"

#include "internationalization.hpp"
#include <iostream>
#include <filesystem>

#include "exception.hpp"
#include "Car/tyre.hpp"
#include "Car/wiper.hpp"
#include "Car/fulltank.hpp"
#include "Car/inspection.hpp"
#include "Actions/car.hpp"
#include "Actions/car_element.hpp"
#include "Actions/vehicule_need_object.hpp"
#include "Place/gas_station.hpp"
#include "Place/insurance.hpp"
#include "Place/inspection_center.hpp"

using namespace SQLite;
using namespace std;
using namespace Glib;
using namespace sigc;
using namespace car;
using namespace vehicle;
using namespace place;
using namespace generic;

namespace database
{
	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Constructor and destructor ////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
	DataBase::DataBase() : db_(DB_FULL_FILENAME(), OPEN_READWRITE | OPEN_CREATE)
	{
		GDV_DEBUG(format("Open database {}", DB_FULL_FILENAME()));
	}


	DataBasePtr DataBase::initialize()
	{
		try
		{
			filesystem::create_directories(build_filename((get_home_dir()), ".local", "share", "gdv"));
			DataBasePtr db(new DataBase());
			Version		last_version = getLastVersion();
			Transaction transaction(db->db());

			if (!db->db().tableExists("Informations"))
			{

				db->db().exec("CREATE TABLE `Informations` ("
							  "`version`	TEXT NOT NULL"
							  ")");

				Statement st(db->db(), "INSERT INTO `Informations`(`Version`) VALUES (?)");
				st.bind(1, last_version.toString());
				st.exec();
			}

			Version current_version = db->getCurrentVersion();
			db->initItem<Car>(current_version, last_version);
			db->initItem<FullTank>(current_version, last_version);
			db->initItem<Inspection>(current_version, last_version);
			db->initItem<car::Insurance>(current_version, last_version);
			db->initItem<car::Insurance::Guarantee>(current_version, last_version);
			db->initItem<Service>(current_version, last_version);
			db->initItem<Service::Operation>(current_version, last_version);
			db->initItem<Tyre>(current_version, last_version);
			db->initItem<Wiper>(current_version, last_version);
			db->initItem<GasStation>(current_version, last_version);
			db->initItem<place::Insurance>(current_version, last_version);
			db->initItem<Garage>(current_version, last_version);
			db->initItem<InspectionCenter>(current_version, last_version);

			if (current_version < last_version)
			{
				Statement st(db->db(), "UPDATE `Informations` SET `version`=? WHERE _rowid_='1'");
				st.bind(1, last_version.toString());
				st.exec();
			}

			transaction.commit();

			return db;
		}
		catch (SQLite::Exception& e)
		{
			GDV_WARNING(e.what());
			throw SQLError("Error during the initialization of the database. "
						   "The database may be already opened in a different window. "
						   "If not, please report the bug to a developer.");
		}
	}


	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Functions /////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
	void DataBase::addAction(action::Action* action)
	{
		actions_.add(action);
		emitSignalChangeRequest();
	}

	void DataBase::slotBlock()
	{
		for (auto& connection : signal_connection_list_)
		{
			connection.block();
		}
	}

	void DataBase::slotUnblock()
	{
		for (auto& connection : signal_connection_list_)
		{
			connection.unblock();
		}
	}

	void DataBase::connectCarSignals(const CarPtr& c)
	{
		slotAdd(c->signalChanged().connect(mem_fun(*this, &DataBase::onDbiChanged)));

		slotAdd(c->signalRemoved<FullTank>().connect(mem_fun(*this, &DataBase::onVehicleElementDeleted)));
		slotAdd(c->signalAdded<FullTank>().connect(mem_fun(*this, &DataBase::onVehicleElementAdded)));

		slotAdd(c->signalRemoved<Service>().connect(mem_fun(*this, &DataBase::onVehicleElementDeleted)));
		slotAdd(c->signalAdded<Service>().connect(mem_fun(*this, &DataBase::onVehicleElementAdded)));

		slotAdd(c->signalRemoved<car::Insurance>().connect(mem_fun(*this, &DataBase::onVehicleElementDeleted)));
		slotAdd(c->signalAdded<car::Insurance>().connect(mem_fun(*this, &DataBase::onVehicleElementAdded)));

		slotAdd(c->signalRemoved<Inspection>().connect(mem_fun(*this, &DataBase::onVehicleElementDeleted)));
		slotAdd(c->signalAdded<Inspection>().connect(mem_fun(*this, &DataBase::onVehicleElementAdded)));

		slotAdd(c->signalRemoved<Tyre>(Tyre::BOTTOM_LEFT).connect(mem_fun(*this, &DataBase::onVehicleElementDeleted)));
		slotAdd(c->signalAdded<Tyre>(Tyre::BOTTOM_LEFT).connect(mem_fun(*this, &DataBase::onVehicleElementAdded)));
		slotAdd(c->signalRemoved<Tyre>(Tyre::BOTTOM_RIGHT).connect(mem_fun(*this, &DataBase::onVehicleElementDeleted)));
		slotAdd(c->signalAdded<Tyre>(Tyre::BOTTOM_RIGHT).connect(mem_fun(*this, &DataBase::onVehicleElementAdded)));
		slotAdd(c->signalRemoved<Tyre>(Tyre::FRONT_LEFT).connect(mem_fun(*this, &DataBase::onVehicleElementDeleted)));
		slotAdd(c->signalAdded<Tyre>(Tyre::FRONT_LEFT).connect(mem_fun(*this, &DataBase::onVehicleElementAdded)));
		slotAdd(c->signalRemoved<Tyre>(Tyre::FRONT_RIGHT).connect(mem_fun(*this, &DataBase::onVehicleElementDeleted)));
		slotAdd(c->signalAdded<Tyre>(Tyre::FRONT_RIGHT).connect(mem_fun(*this, &DataBase::onVehicleElementAdded)));

		slotAdd(c->signalRemoved<Wiper>(Wiper::BOTTOM).connect(mem_fun(*this, &DataBase::onVehicleElementDeleted)));
		slotAdd(c->signalAdded<Wiper>(Wiper::BOTTOM).connect(mem_fun(*this, &DataBase::onVehicleElementAdded)));
		slotAdd(c->signalRemoved<Wiper>(Wiper::FRONT).connect(mem_fun(*this, &DataBase::onVehicleElementDeleted)));
		slotAdd(c->signalAdded<Wiper>(Wiper::FRONT).connect(mem_fun(*this, &DataBase::onVehicleElementAdded)));
	}

	void DataBase::connectElementSignals(Element* element)
	{
		slotAdd(element->signalChanged().connect(sigc::mem_fun(*this, &DataBase::onDbiChanged)));

		for (auto& pair_array : element->objects())
		{
			slotAdd(pair_array.second.signalAdded().connect(mem_fun(*this, &DataBase::onObjectAdded)));
			slotAdd(pair_array.second.signalRemoved().connect(mem_fun(*this, &DataBase::onObjectDeleted)));
		}
	}

	Version DataBase::getLastVersion()
	{
		return Version(MAJOR_VERSION, MINOR_VERSION, MICRO_VERSION);
	}

	void DataBase::cleanOrphans()
	{
		cleanOrphanObjects<car::Insurance>();
		cleanOrphanObjects<car::Insurance::Guarantee>();
		cleanOrphanObjects<FullTank>();
		cleanOrphanObjects<Inspection>();
		cleanOrphanObjects<Service>();
		cleanOrphanObjects<Service::Operation>();
		cleanOrphanObjects<Tyre>();
		cleanOrphanObjects<Wiper>();

		cleanOrphanLinks<FullTank>();
		cleanOrphanLinks<car::Insurance>();
		cleanOrphanLinks<Inspection>();
		cleanOrphanLinks<Service>();
		cleanOrphanLinks<Tyre>();
		cleanOrphanLinks<Wiper>();
	}

	Version DataBase::getCurrentVersion()
	{
		return Version(db().execAndGet("SELECT max(`version`) FROM `Informations`").getString());
	}


	void DataBase::save()
	{
		actions_.execute(db());
		emitSignalFunctionDone(SAVE);
	}

	void DataBase::deleteChanges()
	{
		actions_.clear();
		emitSignalFunctionDone(DELETE_CHANGES);
	}

	bool DataBase::needSave() const
	{
		return actions_.canUndo();
	}

	void DataBase::deleteCurrentTracking()
	{
		for (auto& connection : signal_connection_list_)
		{
			connection.disconnect();
		}
	}

	void DataBase::undo(const VehiclePtr& vehicle)
	{
		slotBlock();
		actions_.undo(vehicle);
		slotUnblock();
		emitSignalFunctionDone(UNDO);
	}

	void DataBase::redo(const VehiclePtr& vehicle)
	{
		slotBlock();
		actions_.redo(vehicle);
		slotUnblock();
		emitSignalFunctionDone(REDO);
	}



	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// New ///////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
	CarPtr DataBase::newCar(const string& car_name)
	{
		try
		{
			int64_t id = getNextId<Car>();
			CarPtr	c  = make_shared<Car>(id, car_name);

			c->addInDatabase(db());

			connectCarSignals(c);

			GDV_DEBUG("Create new car");

			return c;
		}
		catch (SQLite::Exception& e)
		{
			GDV_WARNING(e.what());
			throw SQLError("Error during creating a car, a car without name is"
						   " probably already existing in the database.");
		}
	}




	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Get ///////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
	CarPtr DataBase::getCar(int64_t id)
	{
		try
		{
			auto c = make_shared<Car>(id, "");

			c->setFromDatabase(db());


			getVehicleElements<FullTank>(c);

			getVehicleElements<Service>(c);
			getVehicleElements<Inspection>(c);
			getVehicleElements<car::Insurance>(c);

			getVehicleElements<Wiper, Wiper::Position>(c, Wiper::BOTTOM);
			getVehicleElements<Wiper, Wiper::Position>(c, Wiper::FRONT);

			getVehicleElements<Tyre, Tyre::Position>(c, Tyre::BOTTOM_RIGHT);
			getVehicleElements<Tyre, Tyre::Position>(c, Tyre::BOTTOM_LEFT);
			getVehicleElements<Tyre, Tyre::Position>(c, Tyre::FRONT_RIGHT);
			getVehicleElements<Tyre, Tyre::Position>(c, Tyre::FRONT_LEFT);

			connectCarSignals(c);

			GDV_DEBUG(format("Get car {}", id));

			return c;
		}
		catch (SQLite::Exception& e)
		{
			GDV_WARNING(e.what());
			throw SQLError(format("Error during getting a car,the car {}"
								  " is probably not existing in the database.",
					id));
		}
	}


	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Remove ////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
	void DataBase::removeCar(int64_t id)
	{
		try
		{
			Transaction transaction(db());

			removeVehicleElements<Service>(id);
			removeVehicleElements<Inspection>(id);
			removeVehicleElements<car::Insurance>(id);
			removeVehicleElements<Wiper>(id);
			removeVehicleElements<Tyre>(id);
			removeVehicleElements<FullTank>(id);

			string			  sql = format("DELETE FROM `{}` WHERE `id`=?", Car::DATABASE_TABLE_NAME);
			SQLite::Statement st(db(), sql);
			st.bind(1, id);
			st.exec();

			transaction.commit();

			GDV_DEBUG(format("Remove car {}", id));
		}
		catch (SQLite::Exception& e)
		{
			GDV_WARNING(e.what());
			throw SQLError(format("Error during removing a car,the car {}"
								  " is probably not existing in the database.",
					id));
		}
	}


	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Slots /////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
	void DataBase::onDbiChanged(Item& item)
	{
		action::Action* action = nullptr;
		if (typeid(item) == typeid(Car))
		{
			action = new action::Car(item.clone(), action::Action::MODIFY);
		}
		else if (dynamic_cast<Element*>(&item) != nullptr)
		{
			action = new action::CarElement(item.clone(), action::Action::MODIFY);
		}
		else if (dynamic_cast<OwnedObject*>(&item) != nullptr)
		{
			action = new action::VehiculeNeedObject(item.clone(), action::Action::MODIFY);
		}
		else if (dynamic_cast<Place*>(&item) != nullptr)
		{
			item.saveInDatabase(db());
		}
		else
		{
			string error = format("Type `{}` is not a database item", typeid(item).name());
			GDV_ERROR(error);
			throw WrongDbiType(error);
		}

		if (action != nullptr)
		{
			addAction(action);
		}
	}

	void DataBase::onObjectDeleted(const ItemPtr& element, [[maybe_unused]] unsigned int index)
	{
		action::Action* action = new action::VehiculeNeedObject(element, action::Action::DELETE);
		addAction(action);
	}

	void DataBase::onObjectAdded(OwnedObject& object)
	{
		int64_t id = getNextId(object);
		object.setId(id);

		action::Action* action = new action::VehiculeNeedObject(object.clone(), action::Action::ADD);
		addAction(action);

		slotAdd(object.signalChanged().connect(mem_fun(*this, &DataBase::onDbiChanged)));
	}

	void DataBase::onVehicleElementDeleted(const ItemPtr& element, [[maybe_unused]] unsigned int index)
	{
		action::Action* action = new action::CarElement(element, action::Action::DELETE);
		addAction(action);
	}

	void DataBase::onVehicleElementAdded(Element& element)
	{
		int64_t id = getNextId(element);
		element.setId(id);

		action::Action* action = new action::CarElement(element.clone(), action::Action::ADD);
		addAction(action);

		connectElementSignals(&element);
	}
}	 // namespace database
