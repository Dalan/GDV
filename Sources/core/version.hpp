/*!
 * \file    version.h
 * \author  Remi BERTHO
 * \date    05/07/18
 * \version 1.0.0
 */

/*
 * version.h
 *
 * Copyright 2018 Rémi BERTHO <remi.bertho@dalan.fr>
 *
 * This file is part of GDV.
 *
 * GDVis free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * GDV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */


#ifndef VERSION_H_INCLUDED
#define VERSION_H_INCLUDED

#include <glibmm.h>
#include <string>
#include <AsynchronousExecution.h>
#include "exception.hpp"

#undef minor
#undef major

namespace core
{

	/*! \class Version
	 *   \brief This class define a version
	 */
	class Version
	{
		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Attributes ////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	private:
		uint32_t major_ = 0; /*!< The major version */
		uint32_t minor_ = 0; /*!< The minor version */
		uint32_t micro_ = 0; /*!< The micro version */

		Glib::DateTime time_;

		std::string git_branch_;
		std::string git_commit_hash_;


		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Constructor ///////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	public:
		/*!
		 *  \brief Default constructor
		 */
		Version();

		/*!
		 *  \brief Constructor with the version in string
		 *  \param version the version
		 */
		explicit Version(const std::string& version, Glib::DateTime time = Glib::DateTime());

		/*!
		 *  \brief Constructor with the version in string
		 *  \param version the version
		 */
		explicit Version(const uint32_t major,
				const uint32_t			minor,
				const uint32_t			micro,
				Glib::DateTime			time			= Glib::DateTime(),
				std::string				git_branch		= "",
				std::string				git_commit_hash = "");

		/**
		 * @brief Het the software version of GDV
		 * @return the version
		 */
		static Version GetSoftwareCurrent();

		/**
		 * @brief Get the last version available on the internet
		 * @return the Version
		 * @exception InternetError if internet or the website is not available
		 */
		static Version getSoftwareLast();

		/**
		 * @brief Get the last version available of GDV on the internet asynchronously
		 * @param return_function the function that will receive the version
		 * @param exception_function the function that will receive the exception
		 */
		static void getSoftwareLastAsynchronously(const std::function<void(const Version&)>& return_function,
				const std::function<void(const core::Exception&)>&							 exception_function);


		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Function //////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	public:
		/*!
		 *  \brief Get the major version
		 *  \return the major version
		 */
		inline uint32_t major() const
		{
			return major_;
		}

		/*!
		 *  \brief Get the minor version
		 *  \return the minor version
		 */
		inline uint32_t minor() const
		{
			return minor_;
		}

		/*!
		 *  \brief Get the micro version
		 *  \return the micro version
		 */
		inline uint32_t micro() const
		{
			return micro_;
		}

		inline Glib::DateTime time() const
		{
			return time_;
		}

		std::string git() const;

		/*!
		 *  \brief Convert to a string
		 *  \return the string
		 */
		std::string toString() const;


		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Operators /////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	public:
		/*!
		 *  \brief Operator ==
		 *  \param version an other version
		 *  \return true if equal, false otherwise
		 */
		bool operator==(const Version& version) const;

		/*!
		 *  \brief Operator !=
		 *  \param version an other version
		 *  \return true if different, false otherwise
		 */
		bool operator!=(const Version& version) const;

		/*!
		 *  \brief Operator >
		 *  \param version an other version
		 *  \return true if superior, false otherwise
		 */
		bool operator>(const Version& version) const;

		/*!
		 *  \brief Operator <
		 *  \param version an other version
		 *  \return true if inferior, false otherwise
		 */
		bool operator<(const Version& version) const;

		/*!
		 *  \brief Operator >=
		 *  \param version an other version
		 *  \return true if superior or equal, false otherwise
		 */
		bool operator>=(const Version& version) const;

		/*!
		 *  \brief Operator <=
		 *  \param version an other version
		 *  \return true if inferior or equal, false otherwise
		 */
		bool operator<=(const Version& version) const;
	};
}	 // namespace core


#endif	  // VERSION_H_INCLUDED
