/*!
 * \file    need.h
 * \author  Remi BERTHO
 * \date    14/03/16
 * \version 1.0.0
 */

/*
 * need.h
 *
 * Copyright 2017 Rémi BERTHO <remi.bertho@dalan.fr>
 *
 * This file is part of GDV.
 *
 * GDVis free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * GDV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

#ifndef NEED_H_INCLUDED
#define NEED_H_INCLUDED

#include "Generic/element.hpp"

#include "DataBase/property.hpp"

namespace vehicle
{
	/*! \class Need
	 *   \brief This class represent a need for the car
	 */
	class Need : public generic::Element
	{
		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// DB infos //////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	private:
		static const constexpr char BEGIN_KM_DB_NAME[]	  = "begin_km";
		static const constexpr char BEGIN_KM_DB_VERSION[] = "0.1.0";

		static const constexpr char END_KM_DB_NAME[]	= "end_km";
		static const constexpr char END_KM_DB_VERSION[] = "0.1.0";

		static const constexpr char			DELAY_KM_DB_NAME[]	  = "delay_km";
		static const constexpr char			DELAY_KM_DB_VERSION[] = "0.1.0";
		static const constexpr unsigned int DEFAULT_DELAY_KM	  = 1000;

		static const constexpr char			DELAY_YEAR_DB_NAME[]	= "delay_year";
		static const constexpr char			DELAY_YEAR_DB_VERSION[] = "0.1.0";
		static const constexpr unsigned int DEFAULT_DELAY_YEAR		= 1;

		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Attributes ////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	protected:
		database::PropertyProxy<uint32_t, BEGIN_KM_DB_NAME, BEGIN_KM_DB_VERSION>   begin_km_;
		database::PropertyProxy<uint32_t, END_KM_DB_NAME, END_KM_DB_VERSION>	   end_km_;
		database::PropertyProxy<uint32_t, DELAY_KM_DB_NAME, DELAY_YEAR_DB_VERSION> delay_km_;
		database::PropertyProxy<uint32_t, DELAY_YEAR_DB_NAME, DELAY_KM_DB_VERSION> delay_year_;

		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Constructor ///////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	public:
		/*!
		 *  \brief Default constructor
		 */
		explicit Need(const int64_t id, const OwnerInfos& owner_info);

	protected:
		/*!
		 *  \brief Copy constructor
		 */
		explicit Need(const Need& need);


		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Function //////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	protected:
		/**
		 * @brief Check if the dbi changed with the old value
		 * @return true if it change, false otherwise
		 */
		virtual bool changed() const;


	public:
		/*!
		 *  \brief operator =
		 *  \param need a Need
		 *  \return a reference to the object
		 */
		Need& operator=(const Need& need);

		/**
		 * @brief operator ==
		 * @param need another Need
		 * @return true if it has the same data, false otherwise
		 */
		bool operator==(const Need& need) const;

		/** \brief Test if it it need to renew the the need or not
		 * \param car the km of the car
		 * \return true if it need, false otherwise
		 */
		bool need(const unsigned int km = 0) const;

		/*!
		 *  \brief Convert to a string
		 *  \return the string
		 */
		virtual std::string toString() const;

		/** \brief Return the number of km before the new need
		 * \return The number of km before the new need
		 */
		int kmBefore(const unsigned int km) const;


		/** \brief Return the price per month
		 * \return The price per month
		 */
		double pricePerMonth() const;


		/** \brief Return the price per 100 km
		 * \return The price per 100 km
		 */
		double pricePer100Km() const;

		/** \brief Return the number of km of the need
		 * \return The number of km of the need
		 */
		inline unsigned int km(unsigned int km) const
		{
			return km - beginKm();
		}

		virtual double distance() const
		{
			return endKm() - beginKm();
		}


		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Getter ////////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	public:
		/*!
		 *  \brief Return the last_km
		 *  \return the last_km
		 */
		inline unsigned int beginKm() const
		{
			return begin_km_;
		}


		/*!
		 *  \brief Return the last_km
		 *  \return the last_km
		 */
		inline unsigned int endKm() const
		{
			return end_km_;
		}


		/*!
		 *  \brief Return the delay_km
		 *  \return the delay_km
		 */
		inline unsigned int delayKm() const
		{
			return delay_km_;
		}


		/*!
		 *  \brief Return the delay_year
		 *  \return the delay_year
		 */
		inline unsigned int delayYear() const
		{
			return delay_year_;
		}


		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Setter ////////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	public:
		/*!
		 *  \brief Set the last_date
		 *  \param last_date the last_date
		 */
		virtual void setBeginDate(const Glib::Date& date, const LinkType link_type = NONE);

		/*!
		 *  \brief Set the last_km
		 *  \param last_km the last_km
		 */
		void setBeginKm(const unsigned int km, const LinkType link_type = NONE);

		/*!
		 *  \brief Set the last_km
		 *  \param last_km the last_km
		 */
		inline void setEndKm(const unsigned int km)
		{
			end_km_ = km;
		}

		/*!
		 *  \brief Set the delay_km
		 *  \param delay_km the delay_km
		 */
		void setDelayKm(const unsigned int delay_km, const LinkType link_type = NONE);

		/*!
		 *  \brief Set the delay_year
		 *  \param delay_year the delay_year
		 */
		void setDelayYear(const unsigned int delay_year, const LinkType link_type = NONE);
	};
}	 // namespace vehicle


#endif	  // NEED_H_INCLUDED
