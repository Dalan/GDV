/*!
 * \file    vehicle.cpp
 * \author  Remi BERTHO
 * \date    27/11/18
 * \version 1.0.0
 */

/*
 * vehicle.cpp
 *
 * Copyright 2018 Rémi BERTHO <remi.bertho@dalan.fr>
 *
 * This file is part of GDV.
 *
 * GDVis free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * GDV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

#include "log.hpp"

#include <fmt/core.h>
#include <numeric>
#include "internationalization.hpp"
#include <iostream>

#include "vehicle.hpp"
#include "exception.hpp"

using namespace Glib;
using namespace std;

namespace vehicle
{
	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Constructor and destructor ////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
	Vehicle::Vehicle(const int64_t id, const string& name)
			: Object(id, name), brand_(*this, ""), model_(*this, ""), owner_(*this, ""), date_of_ownership_(*this, Date(1)),
			  price_(*this, 0), engine_(*this, EngineType::GAS), distance_(*this, 0)
	{
	}


	Vehicle::~Vehicle()
	{
		for (auto& element_array : items_)
		{
			element_array.second.deleteAll();
		}
	}

	Vehicle::Vehicle(const Vehicle& vehicle)
			: Object(vehicle), brand_(vehicle.brand_, *this), model_(vehicle.model_, *this), owner_(vehicle.owner_, *this),
			  date_of_ownership_(vehicle.date_of_ownership_, *this), price_(vehicle.price_, *this), engine_(vehicle.engine_, *this),
			  distance_(vehicle.distance_, *this)
	{
	}


	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Function //////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
	string Vehicle::toString() const
	{
		string res = Object::toString();

		res += fmt::format(fmt::runtime(_("Vehicle:\nBrand: {}\nModel: {}km\n"
										  "Owner: {}\nDate of ownership: {}\nPrice: {}\n"
										  "Fuel: {}\nDistance: {}")),
				brand(),
				model(),
				owner(),
				dateOfOwnership().format_string("%x").c_str(),
				price(),
				engineToString(engineType()),
				distance());


		for (auto& element_array : items_)
		{
			res += element_array.second.toString();
		}

		return res;
	}

	Vehicle& Vehicle::operator=(const Vehicle& vehicle)
	{
		if (this == &vehicle)
		{
			return *this;
		}

		Item::operator=(vehicle);

		return *this;
	}

	bool Vehicle::operator==(const Vehicle& vehicle) const
	{
		if (Item::operator==(vehicle))
		{
			if ((name() == vehicle.name()) && (brand() == vehicle.brand()) && (model() == vehicle.model()) &&
					(owner() == vehicle.owner()) && (dateOfOwnership() == vehicle.dateOfOwnership()) && (price() == vehicle.price()) &&
					(engineType() == vehicle.engineType()) && (distance() == vehicle.distance()))
			{
				return true;
			}
		}

		return false;
	}


	double Vehicle::totalPrice() const
	{
		double total = price();


		for (auto& element_array : items_)
		{
			total += element_array.second.totalPrice();
		}

		return total;
	}


	double Vehicle::pricePerYear() const
	{
		if (dateOfOwnership() == Date(1))
			return 0;

		double total = totalPrice();

		Date now;
		now.set_time_current();
		double nb_year = static_cast<double>(dateOfOwnership().days_between(now)) / NB_DAY_PER_YEAR;

		if (nb_year == 0.)
		{
			return 0;
		}

		return total / nb_year;
	}


	double Vehicle::pricePer100Km() const
	{
		double total = totalPrice();

		if (distance() == 0)
		{
			return 0;
		}

		return total / ((double)distance() / NB_KM_IN_CONSUMPTION);
	}

}	 // namespace vehicle
