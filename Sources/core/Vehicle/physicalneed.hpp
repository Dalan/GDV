/*!
 * \file    physicalneed.h
 * \author  Remi BERTHO
 * \date    14/03/16
 * \version 1.0.0
 */

/*
 * physicalneed.h
 *
 * Copyright 2017 Rémi BERTHO <remi.bertho@dalan.fr>
 *
 * This file is part of GDV.
 *
 * GDVis free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * GDV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

#ifndef PHYSICALNEED_H_INCLUDED
#define PHYSICALNEED_H_INCLUDED

#include "need.hpp"

namespace vehicle
{
	/*! \class PhysicalNeed
	 *   \brief This class represent a physical need for the car
	 */
	class PhysicalNeed : public Need
	{
		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// DB infos //////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	private:
		static const constexpr char BRAND_DB_NAME[]	   = "brand";
		static const constexpr char BRAND_DB_VERSION[] = "0.1.0";

		static const constexpr char MODEL_DB_NAME[]	   = "model";
		static const constexpr char MODEL_DB_VERSION[] = "0.1.0";

		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Attributes ////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	protected:
		database::PropertyProxy<std::string, BRAND_DB_NAME, BRAND_DB_VERSION> brand_;
		database::PropertyProxy<std::string, MODEL_DB_NAME, MODEL_DB_VERSION> model_;

		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Constructor ///////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	public:
		/*!
		 *  \brief Default constructor
		 */
		explicit PhysicalNeed(const int64_t id, const OwnerInfos& owner_info);

	protected:
		/*!
		 *  \brief Copy constructor
		 */
		explicit PhysicalNeed(const PhysicalNeed& pn);


		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Function //////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	protected:
		/**
		 * @brief Check if the dbi changed with the old value
		 * @return true if it change, false otherwise
		 */
		virtual bool changed() const;


	public:
		/*!
		 *  \brief operator =
		 *  \param pn a PhysicalNeed
		 *  \return a reference to the object
		 */
		PhysicalNeed& operator=(const PhysicalNeed& pn);

		/**
		 * @brief operator ==
		 * @param pn another PhysicalNeed
		 * @return true if it has the same data, false otherwise
		 */
		bool operator==(const PhysicalNeed& pn) const;

		/*!
		 *  \brief Convert to a string
		 *  \return the string
		 */
		virtual std::string toString() const;


		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Getter ////////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	public:
		/*!
		 *  \brief Return the brand
		 *  \return the brand
		 */
		inline std::string brand() const
		{
			return brand_;
		}


		/*!
		 *  \brief Return the model
		 *  \return the model
		 */
		inline std::string model() const
		{
			return model_;
		}

		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Setter ////////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	public:
		/*!
		 *  \brief Set the brand
		 *  \param brand the brand
		 */
		inline void setBrand(const std::string& brand)
		{
			brand_ = brand;
		}


		/*!
		 *  \brief Set the model
		 *  \param model the model
		 */
		inline void setModel(const std::string& model)
		{
			model_ = model;
		}
	};
}	 // namespace vehicle

#endif	  // PHYSICALNEED_H_INCLUDED
