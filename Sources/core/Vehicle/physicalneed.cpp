/*!
 * \file    physicalneed.cpp
 * \author  Remi BERTHO
 * \date    14/03/16
 * \version 1.0.0
 */

/*
 * physicalneed.cpp
 *
 * Copyright 2017 Rémi BERTHO <remi.bertho@dalan.fr>
 *
 * This file is part of GDV.
 *
 * GDVis free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * GDV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

#include "internationalization.hpp"

#include <fmt/core.h>

#include "physicalneed.hpp"

using namespace std;
using namespace Glib;

namespace vehicle
{
	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Constructor and destructor ////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
	PhysicalNeed::PhysicalNeed(const int64_t id, const OwnerInfos& owner_info) : Need(id, owner_info), brand_(*this, ""), model_(*this, "")
	{
	}

	PhysicalNeed::PhysicalNeed(const PhysicalNeed& pn) : Need(pn), brand_(pn.brand_, *this), model_(pn.model_, *this)
	{
	}



	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Function //////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
	bool PhysicalNeed::changed() const
	{
		if (getOld() != nullptr)
		{
			return !(*this == *dynamic_cast<PhysicalNeed*>(getOld().get()));
		}

		return false;
	}

	PhysicalNeed& PhysicalNeed::operator=(const PhysicalNeed& pn)
	{
		Need::operator=(pn);

		return *this;
	}

	bool PhysicalNeed::operator==(const PhysicalNeed& pn) const
	{
		if (Need::operator==(pn))
		{
			if ((brand() == pn.brand()) && (model() == pn.model()))
			{
				return true;
			}
		}

		return false;
	}

	string PhysicalNeed::toString() const
	{
		string res = Need::toString();

		res += fmt::format(fmt::runtime(_("\nBrand: {}\nModel: {}")), brand(), model());

		return res;
	}

}	 // namespace vehicle
