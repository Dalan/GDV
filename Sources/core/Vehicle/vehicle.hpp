/*!
 * \file    vehicle.h
 * \author  Remi BERTHO
 * \date    27/11/18
 * \version 1.0.0
 */

/*
 * vehicle.h
 *
 * Copyright 2018 Rémi BERTHO <remi.bertho@dalan.fr>
 *
 * This file is part of GDV.
 *
 * GDVis free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * GDV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

#ifndef VEHICLE_H_INCLUDED
#define VEHICLE_H_INCLUDED

#include "share.hpp"
#include "physicalneed.hpp"
#include "Generic/element_array.hpp"
#include "Generic/object.hpp"

namespace vehicle
{
	class Vehicle;

	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Typedef ///////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
	/** Vehicle smart pointer */
	typedef std::shared_ptr<Vehicle> VehiclePtr;


	/*! \class Vehicle
	 *   \brief This class represent a vehicle
	 */
	class Vehicle : public generic::Object
	{
		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// DB infos //////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	private:
		static const constexpr char BRAND_DB_NAME[]	   = "brand";
		static const constexpr char BRAND_DB_VERSION[] = "0.1.0";

		static const constexpr char MODEL_DB_NAME[]	   = "model";
		static const constexpr char MODEL_DB_VERSION[] = "0.1.0";

		static const constexpr char OWNER_DB_NAME[]	   = "owner";
		static const constexpr char OWNER_DB_VERSION[] = "0.1.0";

		static const constexpr char DATE_OF_FIRST_OWNERSHIP_DB_NAME[]	 = "date_of_first_ownership";
		static const constexpr char DATE_OF_FIRST_OWNERSHIP_DB_VERSION[] = "0.1.0";

		static const constexpr char PRICE_DB_NAME[]	   = "price";
		static const constexpr char PRICE_DB_VERSION[] = "0.1.0";

		static const constexpr char ENGINE_TYPE_DB_NAME[]	 = "engine_type";
		static const constexpr char ENGINE_TYPE_DB_VERSION[] = "0.1.0";

		static const constexpr char DISTANCE_DB_NAME[]	  = "distance";
		static const constexpr char DISTANCE_DB_VERSION[] = "0.1.0";

		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Attributes ////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	protected:
		database::PropertyProxy<std::string, BRAND_DB_NAME, BRAND_DB_VERSION>									 brand_;
		database::PropertyProxy<std::string, MODEL_DB_NAME, MODEL_DB_VERSION>									 model_;
		database::PropertyProxy<std::string, OWNER_DB_NAME, OWNER_DB_VERSION>									 owner_;
		database::PropertyProxy<Glib::Date, DATE_OF_FIRST_OWNERSHIP_DB_NAME, DATE_OF_FIRST_OWNERSHIP_DB_VERSION> date_of_ownership_;
		database::PropertyProxy<double, PRICE_DB_NAME, PRICE_DB_VERSION>										 price_;
		database::PropertyProxy<EngineType, ENGINE_TYPE_DB_NAME, ENGINE_TYPE_DB_VERSION>						 engine_;
		database::PropertyProxy<unsigned int, DISTANCE_DB_NAME, DISTANCE_DB_VERSION>							 distance_;
		std::map<std::string, generic::ElementArray>															 items_;


		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Signals ///////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	public:
		template <typename ElementClass, typename Position = std::nullptr_t>
		inline generic::ElementArray::SignalAdded signalAdded([[maybe_unused]] Position pos = nullptr)
		{
			return elements<ElementClass, Position>(pos).signalAdded();
		}
		template <typename ElementClass, typename Position = std::nullptr_t>
		inline generic::ElementArray::SignalRemoved signalRemoved([[maybe_unused]] Position pos = nullptr)
		{
			return elements<ElementClass, Position>(pos).signalRemoved();
		}

		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Constructor ///////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	public:
		Vehicle(const int64_t id, const std::string& name);


		/*!
		 *  \brief Destructor
		 */
		virtual ~Vehicle();

	protected:
		/*!
		 *  \brief Copy constructor
		 * You need to call copyElements after a copy constructor
		 */
		explicit Vehicle(const Vehicle& vehicle);

		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Function //////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	public:
		/*!
		 *  \brief Convert to a string
		 *  \return The string
		 */
		virtual std::string toString() const;


		/*!
		 *  \brief operator =
		 *  \param vehicle a Vehicle
		 *  \return a reference to the object
		 */
		Vehicle& operator=(const Vehicle& vehicle);

		/**
		 * @brief operator ==
		 * @param vehicle another Vehicle
		 * @return true if it has the same data, false otherwise
		 */
		bool operator==(const Vehicle& vehicle) const;

		/*!
		 *  \brief Calculate the estimate total price of the vehicle from the beginning
		 *  \return The total price
		 */
		double totalPrice() const;

		/*!
		 *  \brief Calculate the estimate price per month
		 *  \return The price per month
		 */
		inline double pricePerMonth() const
		{
			return pricePerYear() / 12;
		}


		/*!
		 *  \brief Calculate the estimate price per year
		 *  \return The price per year
		 */
		double pricePerYear() const;


		/*!
		 *  \brief Calculate the average price per 100 km
		 *  \return The average price per 100 km
		 */
		double pricePer100Km() const;



		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Getter ////////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	public:
		/*!
		 *  \brief Return the brand
		 *  \return The brand
		 */
		inline std::string brand() const
		{
			return brand_;
		}


		/*!
		 *  \brief Return the model
		 *  \return The model
		 */
		inline std::string model() const
		{
			return model_;
		}


		/*!
		 *  \brief Return the owner
		 *  \return The owner
		 */
		inline std::string owner() const
		{
			return owner_;
		}


		/*!
		 *  \brief Return the date_of_ownership
		 *  \return The date_of_ownership
		 */
		inline Glib::Date dateOfOwnership() const
		{
			return date_of_ownership_;
		}


		/*!
		 *  \brief Return the price
		 *  \return The price
		 */
		inline double price() const
		{
			return price_;
		}


		/*!
		 *  \brief Return the engine used
		 *  \return The engine used
		 */
		inline EngineType engineType() const
		{
			return engine_;
		}


		/*!
		 *  \brief Return the distance
		 *  \return the distance
		 */
		inline unsigned int distance() const
		{
			return distance_;
		}



		template <typename ElementClass, typename Position = std::nullptr_t>
		const generic::ElementArray& elements([[maybe_unused]] Position pos = nullptr) const
		{
			static_assert(std::is_base_of_v<generic::Element, ElementClass>, "The class must be part of a vehicle");

			if constexpr (std::is_same_v<Position, std::nullptr_t>)
				return items_.at(ElementClass::DATABASE_TABLE_NAME);
			else
			{
				try
				{
					return items_.at(std::format("{}.{}", ElementClass::DATABASE_TABLE_NAME, static_cast<uint32_t>(pos)));
				}
				catch (...)
				{
					throw OutOfRange(std::format("Wrong {} position", ElementClass::userName()));
				}
			}
		}


		template <typename ElementClass, typename Position = std::nullptr_t>
		const ElementClass& element(const unsigned int index = 0, [[maybe_unused]] Position pos = nullptr) const
		{
			static_assert(std::is_base_of_v<generic::Element, ElementClass>, "The class must be part of a vehicle");

			if constexpr (std::is_same_v<Position, std::nullptr_t>)
				return static_cast<const ElementClass&>(elements<ElementClass>().at(index));
			else
			{
				return static_cast<const ElementClass&>(elements<ElementClass>(pos).at(index));
			}
		}

		template <typename ElementClass, typename Position = std::nullptr_t>
		unsigned int nbElements([[maybe_unused]] Position pos = nullptr) const
		{
			static_assert(std::is_base_of_v<generic::Element, ElementClass>, "The class must be part of a vehicle");

			if constexpr (std::is_same_v<Position, std::nullptr_t>)
				return static_cast<unsigned int>(elements<ElementClass, Position>(pos).size());
			else
			{
				try
				{
					return static_cast<unsigned int>(elements<ElementClass, Position>(pos).size());
				}
				catch (...)
				{
					throw OutOfRange(std::format("Wrong {} position", ElementClass::userName()));
				}
			}
		}


		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Setter ////////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	public:
		/*!
		 *  \brief Set the brand
		 *  \param brand the brand
		 */
		inline void setBrand(const std::string& brand)
		{
			brand_ = brand;
		}


		/*!
		 *  \brief Set the model
		 *  \param model the model
		 */
		inline void setModel(const std::string& model)
		{
			model_ = model;
		}


		/*!
		 *  \brief Set the owner
		 *  \param owner the owner
		 */
		inline void setOwner(const std::string& owner)
		{
			owner_ = owner;
		}


		/*!
		 *  \brief Set the date_of_ownership
		 *  \param date_of_ownership the date_of_ownership
		 */
		inline void setDateOfOwnership(const Glib::Date& date_of_ownership)
		{
			date_of_ownership_ = date_of_ownership;
		}


		/*!
		 *  \brief Set the distance
		 *  \param distance the distance
		 */
		inline void setDistance(const unsigned int distance)
		{
			distance_ = distance;
		}


		/*!
		 *  \brief Set the price
		 *  \param price the price
		 */
		inline void setPrice(const double price)
		{
			price_ = price;
		}


		/*!
		 *  \brief Set the engine
		 *  \param fuel the engine
		 */
		inline void setEngineType(const EngineType fuel)
		{
			engine_ = fuel;
		}

		template <typename ElementClass, typename Position = std::nullptr_t>
		generic::ElementArray& elements([[maybe_unused]] Position pos = nullptr)
		{
			static_assert(std::is_base_of_v<generic::Element, ElementClass>, "The class must be part of a vehicle");

			if constexpr (std::is_same_v<Position, std::nullptr_t>)
				return items_[ElementClass::DATABASE_TABLE_NAME];
			else
				return items_[std::format("{}.{}", ElementClass::DATABASE_TABLE_NAME, static_cast<uint32_t>(pos))];
		}

		template <typename ElementClass, typename Position = std::nullptr_t>
		ElementClass& addElement([[maybe_unused]] Position pos = nullptr, [[maybe_unused]] const bool copy_objects = false)
		{
			static_assert(std::is_base_of_v<generic::Element, ElementClass>, "The class must be part of a vehicle");

			ElementClass* e = nullptr;

			if (nbElements<ElementClass>(pos) > 0)
			{
				if constexpr (std::is_same_v<Position, std::nullptr_t>)
					e = dynamic_cast<ElementClass*>(element<ElementClass>(0).createNext(copy_objects));
				else
					e = dynamic_cast<ElementClass*>(element<ElementClass>(0, pos).createNext());
			}
			else
			{
				if constexpr (std::is_same_v<Position, std::nullptr_t>)
					e = new ElementClass(0, this);
				else
					e = new ElementClass(0, this, pos);
				elements<ElementClass>(pos).add(e);
			}
			return *e;
		}

		template <typename ElementClass> void removeElement(ElementClass& element)
		{
			static_assert(std::is_base_of_v<generic::Element, ElementClass>, "The class must be part of a vehicle");

			if constexpr (std::is_base_of_v<vehicle::PhysicalNeed, ElementClass>)
				elements<ElementClass>(element.position()).remove(element);
			else
				elements<ElementClass>().remove(element);
		}
	};
}	 // namespace vehicle

#endif	  // VEHICLE_H_INCLUDED
