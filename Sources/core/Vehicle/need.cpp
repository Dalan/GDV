/*!
 * \file    need.h
 * \author  Remi BERTHO
 * \date    14/03/16
 * \version 1.0.0
 */

/*
 * need.h
 *
 * Copyright 2017 Rémi BERTHO <remi.bertho@dalan.fr>
 *
 * This file is part of GDV.
 *
 * GDVis free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * GDV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

#include <fmt/core.h>
#include "internationalization.hpp"

#include "need.hpp"
#include "vehicle.hpp"

using namespace std;
using namespace Glib;

namespace vehicle
{
	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Constructor and destructor ////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
	Need::Need(const int64_t id, const OwnerInfos& owner_info)
			: Element(id, owner_info), begin_km_(*this, 0), end_km_(*this, DEFAULT_DELAY_KM), delay_km_(*this, DEFAULT_DELAY_KM),
			  delay_year_(*this, DEFAULT_DELAY_YEAR)
	{
	}

	Need::Need(const Need& need)
			: Element(need), begin_km_(need.begin_km_, *this), end_km_(need.end_km_, *this), delay_km_(need.delay_km_, *this),
			  delay_year_(need.delay_year_, *this)
	{
	}



	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Function //////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
	bool Need::changed() const
	{
		if (getOld() != nullptr)
		{
			return !(*this == *dynamic_cast<Need*>(getOld().get()));
		}

		return false;
	}

	Need& Need::operator=(const Need& need)
	{
		Element::operator=(need);

		return *this;
	}

	bool Need::operator==(const Need& need) const
	{
		if (Element::operator==(need))
		{
			if ((beginKm() == need.beginKm()) && (endKm() == need.endKm()) && (delayKm() == need.delayKm()) &&
					(delayYear() == need.delayYear()))
			{
				return true;
			}
		}

		return false;
	}

	bool Need::need(const unsigned int km) const
	{
		bool res = false;
		if (kmBefore(km) <= 0)
		{
			res = true;
		}
		else
		{
			if (dayBefore() <= 0)
			{
				res = true;
			}
		}

		return res;
	}

	string Need::toString() const
	{
		string res = Element::toString();

		res += fmt::format(fmt::runtime(_("Begin km: {}\nEnd km: {}\nDelay km: {}\n"
										  "Delay year: {}\nPrice: {}")),
				beginKm(),
				endKm(),
				delayKm(),
				delayYear(),
				price());

		return res;
	}

	int Need::kmBefore(const unsigned int km) const
	{
		return static_cast<int>(beginKm() + delayKm() - km);
	}

	double Need::pricePerMonth() const
	{
		if (currentlyUsed())
		{
			if (delayYear() != 0)
			{
				return price() / static_cast<double>(delayYear() * NB_MONTH_PER_YEAR);
			}

			return 0;
		}


		double month = static_cast<double>(beginDate().days_between(endDate())) / (NB_DAY_PER_YEAR / NB_MONTH_PER_YEAR);
		return price() / month;
	}

	double Need::pricePer100Km() const
	{
		if (currentlyUsed())
		{
			if (delayKm() != 0)
			{
				return (price() / (double)delayKm()) * NB_KM_IN_CONSUMPTION;
			}

			return 0;
		}


		return (price() / (double)(endKm() - beginKm())) * NB_KM_IN_CONSUMPTION;
	}




	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Setter ////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
	void Need::setBeginDate(const Date& date, const LinkType link_type)
	{
		if (beginDate() != date)
		{
			begin_date_ = date;
			emitSignalNeedSort();
			switch (link_type)
			{
			case END:
				if (currentlyUsed())
				{
					Date new_date = beginDate();
					new_date.add_years(static_cast<int>(delayYear()));
					setEndDate(new_date);
				}
				break;

			case ALL:
				if (previous() != nullptr)
				{
					setEndDate(dynamic_cast<Need*>(previous())->beginDate());
				}
				else
				{
					Date new_end = begin_date_;
					new_end.add_years(static_cast<int>(delayYear()));
					setEndDate(new_end);
				}

				if (next() != nullptr)
				{
					next()->setEndDate(beginDate());
				}
				break;

			case NONE:
				break;
			}
		}
	}

	void Need::setBeginKm(const unsigned int km, const LinkType link_type)
	{
		if (beginKm() != km)
		{
			begin_km_ = km;
			switch (link_type)
			{
			case END:
				if (currentlyUsed())
				{
					end_km_ = km + delayKm();
				}
				break;

			case ALL:
				if (previous() != nullptr)
				{
					end_km_ = dynamic_cast<Need*>(previous())->beginKm();
				}
				else
				{
					end_km_ = km + delayKm();
				}

				if (next() != nullptr)
				{
					dynamic_cast<Need*>(next())->setEndKm(beginKm());
				}
				break;

			case NONE:
				break;
			}
		}
	}



	void Need::setDelayKm(const unsigned int delay_km, const LinkType link_type)
	{
		if (delayKm() != delay_km)
		{
			delay_km_ = delay_km;
			switch (link_type)
			{
			case ALL:
			case END:
				if (currentlyUsed())
				{
					end_km_ = beginKm() + delay_km;
				}
				break;
			case NONE:
				break;
			}
		}
	}

	void Need::setDelayYear(const unsigned int delay_year, const LinkType link_type)
	{
		if (delayYear() != delay_year)
		{
			delay_year_ = delay_year;
			switch (link_type)
			{
			case ALL:
			case END:
				if (currentlyUsed())
				{
					setEndDate(beginDate().add_years(static_cast<int>(delay_year)));
				}
				break;
			case NONE:
				break;
			}
		}
	}


}	 // namespace vehicle
