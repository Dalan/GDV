/*!
 * \file    exception_list.cpp
 * \brief   Thread safe exception list
 * \author  Remi BERTHO
 * \date    28/08/16
 * \version 4.3.2
 */

/*
 * exception_list.cpp
 *
 * Copyright 2014-2024 Remi BERTHO <remi.bertho@dalan.fr>
 *
 * This file is part of GDV.
 *
 * LibCsuper is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * LibCsuper is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

#include "log.hpp"

#include "exception_list.hpp"
#include <thread>

namespace core
{
	using namespace std;
	using namespace Glib;

	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Constructor ///////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
	ExceptionList::ExceptionList() = default;

	ExceptionList::~ExceptionList() = default;

	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Function //////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
	void ExceptionList::lock() const
	{
		while (lock_.test_and_set())
		{
			this_thread::yield();
		}
	}

	void ExceptionList::unlock() const
	{
		lock_.clear();
	}

	bool ExceptionList::empty() const
	{
		bool res;

		lock();
		res = list_.empty();
		unlock();

		return res;
	}

	void ExceptionList::add(const core::Exception& exception, const string& msg)
	{
		lock();
		list_.emplace_back(exception.clone(), msg);
		unlock();
		GDV_DEBUG("Exception added in list");
		signal_added_.emit();
	}

	std::pair<ExceptionPtr, string> ExceptionList::get()
	{
		lock();
		auto res = list_.front();
		list_.pop_front();
		unlock();
		GDV_DEBUG("Exception removed from list");

		return res;
	}
}	 // namespace core
