/*!
 * \file    action.cpp
 * \author  Remi BERTHO
 * \date    07/02/18
 * \version 1.0.0
 */

/*
 * action.cpp
 *
 * Copyright 2018 Rémi BERTHO <remi.bertho@dalan.fr>
 *
 * This file is part of GDV.
 *
 * GDV is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * GDV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

#include "log.hpp"

#include "action.hpp"

#include <utility>

#include "exception.hpp"

using namespace std;
using namespace SQLite;
using namespace Glib;
using namespace database;

namespace action
{
	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Constructor and destructor ////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
	Action::Action(ItemPtr dbi, Type type) : dbi_(std::move(dbi)), type_(type)
	{
	}


	Action::~Action() = default;


	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Function //////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
	ostream& operator<<(ostream& os, const Action& dbi)
	{
		os << dbi.toString();
		return os;
	}

	bool Action::sameDbi(const Action& action)
	{
		if (dbi_->sameDatabaseId(*(action.dbi_)))
		{
			if (typeid(dbi_) == typeid((action.dbi_)))
			{
				return true;
			}
		}
		return false;
	}

	void Action::executeSql(Database& db)
	{
		try
		{
			switch (type())
			{
			case DELETE:
				getDbi()->deleteInDatabase(db);
				break;
			case ADD:
				getDbi()->addInDatabase(db);
				break;
			case MODIFY:
				getDbi()->saveInDatabase(db);
			}
		}
		catch (SQLite::Exception& e)
		{
			GDV_WARNING(e.what());
			throw SQLError(format("Error during executing the SQL in the database : {}", e.what()));
		}
	}
}	 // namespace action
