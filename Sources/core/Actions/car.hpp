/*!
 * \file    car.h
 * \author  Remi BERTHO
 * \date    07/02/18
 * \version 1.0.0
 */

/*
 * car.h
 *
 * Copyright 2018 Rémi BERTHO <remi.bertho@dalan.fr>
 *
 * This file is part of GDV.
 *
 * GDVis free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * GDV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */


#ifndef CAR_ACTION_H
#define CAR_ACTION_H

#include "action.hpp"

namespace action
{
	/*! \class Action
	 *   \brief This class is a action
	 */
	class Car : public Action
	{
		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Constructor ///////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	public:
		/*!
		 *  \brief Default constructor
		 *  \param id the id
		 */
		explicit Car(const database::ItemPtr& car, const Type type);

		/*!
		 *  \brief Destructor
		 */
		virtual ~Car() override;


		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Function //////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	protected:
	public:
		/*!
		 *  \brief Convert to a string
		 *  \return the string
		 */
		std::string toString() const override;

		/**
		 * @brief Undo the action on the dbi
		 */
		void undo(vehicle::VehiclePtr car) override;

		/**
		 * @brief Redo the action on the dbi
		 */
		void redo(vehicle::VehiclePtr car) override;
	};
}	 // namespace action


#endif /* CAR_ACTION_H */
