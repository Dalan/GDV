/*!
 * \file
 */

/*
 * vehicule_need_object.cpp
 *
 * Copyright 2019 Rémi BERTHO <remi.bertho@dalan.fr>
 *
 * This file is part of GDV.
 *
 * GDV is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * GDV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */


#include "vehicule_need_object.hpp"

#include "Car/insurance.hpp"
#include "Car/service.hpp"
#include "exception.hpp"
#include "log.hpp"
#include <iostream>
#include <utility>

using namespace std;
using namespace Glib;
using namespace database;
using namespace car;
using namespace vehicle;
using namespace generic;

namespace action
{
	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Constructor and destructor ////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
	VehiculeNeedObject::VehiculeNeedObject(ItemPtr dbi, Type type) : Action(std::move(dbi), type)
	{
	}


	VehiculeNeedObject::~VehiculeNeedObject() = default;

	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Function //////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
	string VehiculeNeedObject::toString() const
	{
		return format("VehiculeNeedObject type {}", static_cast<uint32_t>(type()));
	}

	void VehiculeNeedObject::undo(vehicle::VehiclePtr vehicle)
	{
		generic::Object* obj = dynamic_cast<generic::Object*>(getDbi().get());
		auto			 car = static_pointer_cast<Car>(vehicle);

		if (typeid(*obj) == typeid(Insurance::Guarantee))
		{
			undoInternal<Insurance, Insurance::Guarantee>(car);
		}
		else if (typeid(*obj) == typeid(Service::Operation))
		{
			undoInternal<Service, Service::Operation>(car);
		}
		else
		{
			string error = format("Type `{}` is not a car need object", typeid(obj).name());
			GDV_ERROR(error);
			throw WrongDbiType(error);
		}
	}

	void VehiculeNeedObject::redo(vehicle::VehiclePtr vehicle)
	{
		generic::Object* obj = dynamic_cast<generic::Object*>(getDbi().get());
		auto			 car = static_pointer_cast<Car>(vehicle);

		if (typeid(*obj) == typeid(Insurance::Guarantee))
		{
			redoInternal<Insurance, Insurance::Guarantee>(car);
		}
		else if (typeid(*obj) == typeid(Service::Operation))
		{
			redoInternal<Service, Service::Operation>(car);
		}
		else
		{
			string error = format("Type `{}` is not a car need object", typeid(obj).name());
			GDV_ERROR(error);
			throw WrongDbiType(error);
		}
	}
}	 // namespace action
