/*!
 * \file    list.cpp
 * \author  Remi BERTHO
 * \date    25/02/2018
 * \version 1.0.0
 */

/*
 * list.h
 *
 * Copyright 2016-2018 Remi BERTHO <remi.bertho@dalan.fr>
 *
 * This file is part of Csuper-gtk.
 *
 * Csuper-gtk is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * Csuper-gtk is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

#include "log.hpp"

#include "internationalization.hpp"

#include <utility>

#include "list.hpp"
#include "exception.hpp"

using namespace std;
using namespace Glib;


namespace action
{
	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Constructor and destructor ////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
	List::List()
	{
		GDV_DEBUG("List created");
	}


	List::~List()
	{
		clear();
		GDV_DEBUG("List destroyed");
	}


	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Function //////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
	bool List::canUndo() const
	{
		return (index_ < actions_group_.size());
	}


	bool List::canRedo() const
	{
		return (index_ > 0);
	}

	void List::clear()
	{
		for (auto& group : actions_group_)
		{
			group.clear();
		}

		actions_group_.clear();

		index_ = 0;

		GDV_DEBUG("List cleared done");
	}

	void List::execute(SQLite::Database& db)
	{
		if (!actions_group_.empty())
		{
			for (int64_t i = static_cast<int64_t>(actions_group_.size()) - 1; i >= index_ && i >= 0; i--)
			{
				actions_group_[static_cast<size_t>(i)].execute(db);
			}

			clear();

			GDV_DEBUG("List execute done");
		}
	}

	void List::add(Action* action)
	{
		// Delete the last redo available
		for (unsigned int i = 0; i < index_; i++)
		{
			auto group = actions_group_.front();
			group.clear();
			actions_group_.pop_front();
		}
		index_	   = 0;
		bool added = false;

		if (!actions_group_.empty())
		{
			Group& last_group = actions_group_[0];
			if (last_group.canAdd(Group::Clock::now()))
			{
				last_group.add(action);
				added = true;
				GDV_DEBUG("List add in group done");
			}
		}

		if (!added)
		{
			Group new_group;
			new_group.add(action);
			actions_group_.push_front(new_group);
			GDV_DEBUG("List add done");
		}
	}

	void List::undo(const vehicle::VehiclePtr& vehicle)
	{
		if (!(canUndo()))
		{
			throw UndoRedoError(_("You cannot undo, there is no previous action."));
		}

		actions_group_[index_].undo(vehicle);

		index_++;

		GDV_DEBUG("List undo done");
	}

	void List::redo(const vehicle::VehiclePtr& vehicle)
	{
		if (!(canRedo()))
		{
			throw UndoRedoError(_("You cannot redo, there is no further action."));
		}

		index_--;

		actions_group_[index_].redo(vehicle);

		GDV_DEBUG("List redo done");
	}
}	 // namespace action
