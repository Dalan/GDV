/*!
 * \file    group.h
 * \author  Remi BERTHO
 * \date    07/06/2018
 * \version 1.0.0
 */

/*
 * group.h
 *
 * Copyright 2016-2018 Remi BERTHO <remi.bertho@dalan.fr>
 *
 * This file is part of Csuper-gtk.
 *
 * Csuper-gtk is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * Csuper-gtk is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

#ifndef ACTION_GROUP_H_INCLUDED
#define ACTION_GROUP_H_INCLUDED

#include <vector>
#include <chrono>

#include "action.hpp"


namespace action
{
	/*! \class Group
	 *   \brief This class is an action group
	 */
	class Group
	{
		///////////////////////////////////////////////////////////////////////////////////
		////////////////////////////// Typedef ////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	public:
		typedef std::chrono::steady_clock Clock;
		typedef Clock::time_point		  Time;


		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Constants /////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	private:
		static const constexpr int ADD_TIME_MUX_MS = 100;

		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Attributes ////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	private:
		std::vector<Action*> actions_; /*!< The group of aciton to be done */
		Time				 time_ = Clock::now();


		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Constructor ///////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	public:
		/*!
		 *  \brief Default constructor
		 */
		Group();

		/*!
		 *  \brief Destructor
		 */
		~Group();


		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Function //////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	public:
		/*!
		 *  \brief Test if the user can add an Action in the group
		 *  \return true if the user can, false otherwise
		 */
		bool canAdd(const Time& time);

		/*!
		 *  \brief Clear all the Actions
		 */
		void clear();

		/*!
		 *  \brief Execute all the Actions
		 */
		void execute(SQLite::Database& db);

		/*!
		 *  \brief Add a game
		 *  \param game the game
		 */
		inline void add(Action* action)
		{
			actions_.push_back(action);
		}

		/*!
		 *  \brief Undo the last action
		 */
		void undo(const vehicle::VehiclePtr& vehicle);

		/*!
		 *  \brief Redo the last action
		 */
		void redo(const vehicle::VehiclePtr& vehicle);
	};
}	 // namespace action

#endif	  // ACTION_GROUP_H_INCLUDED
