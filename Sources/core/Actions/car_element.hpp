/*!
 * \file    car_element.h
 * \author  Remi BERTHO
 * \date    07/02/18
 * \version 1.0.0
 */

/*
 * car_element.h
 *
 * Copyright 2018 Rémi BERTHO <remi.bertho@dalan.fr>
 *
 * This file is part of GDV.
 *
 * GDVis free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * GDV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */


#ifndef CAR_ELEMENT_ACTION_H
#define CAR_ELEMENT_ACTION_H

#include "action.hpp"
#include "Car/car.hpp"

namespace action
{
	/*! \class Action
	 *   \brief This class is a action
	 */
	class CarElement : public Action
	{
		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Constructor ///////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	public:
		/*!
		 *  \brief Default constructor
		 *  \param id the id
		 */
		explicit CarElement(database::ItemPtr dbi, const Type type);

		/*!
		 *  \brief Destructor
		 */
		~CarElement() override;


		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Function //////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	protected:
		template <typename ElementClass, typename Position = std::nullptr_t> void undoInternal(car::CarPtr car)
		{
			static_assert(std::is_base_of_v<generic::Element, ElementClass>, "The class must be derived from CarElement");

			ElementClass& old_element = static_cast<ElementClass&>(*getOldDbi());

			Position pos;
			if constexpr (std::is_null_pointer_v<Position> == true)
				pos = nullptr;

			switch (type())
			{
			case MODIFY:
				if constexpr (std::is_null_pointer_v<Position> == false)
					pos = old_element.position();
				for (auto& ci : car->elements<ElementClass, Position>(pos))
				{
					if (ci.sameDatabaseId(old_element))
					{
						ElementClass& element = static_cast<ElementClass&>(ci);
						element				  = old_element;
						break;
					}
				}
				break;
			case ADD:
				car->removeElement<ElementClass>(static_cast<ElementClass&>(*getDbi()));
				break;
			case DELETE:
				ElementClass& original_element = static_cast<ElementClass&>(*getDbi());
				if constexpr (std::is_null_pointer_v<Position> == false)
					pos = original_element.position();
				ElementClass& new_element = car->addElement<ElementClass, Position>(pos);
				new_element				  = original_element;
				break;
			}
		}

		template <typename ElementClass, typename Position = std::nullptr_t> void redoInternal(car::CarPtr car)
		{
			ElementClass& newer_element = static_cast<ElementClass&>(*getDbi());

			Position pos;
			if constexpr (std::is_null_pointer_v<Position> == false)
				pos = newer_element.position();
			else
				pos = nullptr;

			switch (type())
			{
			case MODIFY:
				for (auto& ci : car->elements<ElementClass, Position>(pos))
				{
					if (ci.sameDatabaseId(newer_element))
					{
						ElementClass& element = static_cast<ElementClass&>(ci);
						element				  = newer_element;
						break;
					}
				}
				break;
			case ADD:
			{
				ElementClass& new_element = car->addElement<ElementClass, Position>(pos);
				new_element				  = static_cast<ElementClass&>(*getDbi());
			}
			break;
			case DELETE:
				car->removeElement<ElementClass>(static_cast<ElementClass&>(*getDbi()));
				break;
			}
		}

	public:
		/*!
		 *  \brief Convert to a string
		 *  \return the string
		 */
		std::string toString() const override;

		/**
		 * @brief Undo the action on the dbi
		 */
		void undo(vehicle::VehiclePtr car) override;

		/**
		 * @brief Redo the action on the dbi
		 */
		void redo(vehicle::VehiclePtr car) override;
	};
}	 // namespace action


#endif /* CAR_ELEMENT_ACTION_H */
