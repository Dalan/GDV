/*!
 * \file    car_element.cpp
 * \author  Remi BERTHO
 * \date    07/02/18
 * \version 1.0.0
 */

/*
 * car_element.cpp
 *
 * Copyright 2018 Rémi BERTHO <remi.bertho@dalan.fr>
 *
 * This file is part of GDV.
 *
 * GDV is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * GDV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */


#include "car_element.hpp"

#include "exception.hpp"
#include "log.hpp"
#include <iostream>
#include <utility>

#include "Car/fulltank.hpp"
#include "Car/inspection.hpp"
#include "Car/insurance.hpp"
#include "Car/service.hpp"
#include "Car/tyre.hpp"
#include "Car/wiper.hpp"

using namespace std;
using namespace Glib;
using namespace database;
using namespace car;
using namespace vehicle;
using namespace generic;

namespace action
{
	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Constructor and destructor ////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
	CarElement::CarElement(ItemPtr dbi, Type type) : Action(std::move(dbi), type)
	{
	}


	CarElement::~CarElement() = default;

	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Function //////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
	string CarElement::toString() const
	{
		return format("CarElement type {}", static_cast<uint32_t>(type()));
	}

	void CarElement::undo(vehicle::VehiclePtr vehicle)
	{
		Element* el	 = dynamic_cast<Element*>(getDbi().get());
		auto	 car = static_pointer_cast<Car>(vehicle);

		if (typeid(*el) == typeid(FullTank))
		{
			undoInternal<FullTank>(car);
		}
		else if (typeid(*el) == typeid(Inspection))
		{
			undoInternal<Inspection>(car);
		}
		else if (typeid(*el) == typeid(Insurance))
		{
			undoInternal<Insurance>(car);
		}
		else if (typeid(*el) == typeid(Service))
		{
			undoInternal<Service>(car);
		}
		else if (typeid(*el) == typeid(Tyre))
		{
			undoInternal<Tyre, Tyre::Position>(car);
		}
		else if (typeid(*el) == typeid(Wiper))
		{
			undoInternal<Wiper, Wiper::Position>(car);
		}
		else
		{
			string error = format("Type `{}` is not a car element", typeid(el).name());
			GDV_ERROR(error);
			throw WrongDbiType(error);
		}
	}

	void CarElement::redo(vehicle::VehiclePtr vehicle)
	{
		Element* el	 = dynamic_cast<Element*>(getDbi().get());
		auto	 car = static_pointer_cast<Car>(vehicle);

		if (typeid(*el) == typeid(FullTank))
		{
			redoInternal<FullTank>(car);
		}
		else if (typeid(*el) == typeid(Inspection))
		{
			redoInternal<Inspection>(car);
		}
		else if (typeid(*el) == typeid(Insurance))
		{
			redoInternal<Insurance>(car);
		}
		else if (typeid(*el) == typeid(Service))
		{
			redoInternal<Service>(car);
		}
		else if (typeid(*el) == typeid(Tyre))
		{
			redoInternal<Tyre, Tyre::Position>(car);
		}
		else if (typeid(*el) == typeid(Wiper))
		{
			redoInternal<Wiper, Wiper::Position>(car);
		}
		else
		{
			string error = format("Type `{}` is not a car element", typeid(el).name());
			GDV_ERROR(error);
			throw WrongDbiType(error);
		}
	}
}	 // namespace action
