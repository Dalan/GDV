/*!
 * \file    group.cpp
 * \author  Remi BERTHO
 * \date    07/06/2018
 * \version 1.0.0
 */

/*
 * group.h
 *
 * Copyright 2016-2018 Remi BERTHO <remi.bertho@dalan.fr>
 *
 * This file is part of Csuper-gtk.
 *
 * Csuper-gtk is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * Csuper-gtk is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

#include "log.hpp"

#include "internationalization.hpp"

#include "group.hpp"
#include "exception.hpp"

using namespace std;
using namespace Glib;
using namespace SQLite;

namespace action
{
	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Constructor and destructor ////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
	Group::Group() = default;


	Group::~Group() = default;


	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Function //////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
	bool Group::canAdd(const Time& time)
	{
		auto diff	 = std::chrono::duration_cast<std::chrono::milliseconds>(time - time_);
		bool can_add = diff < std::chrono::milliseconds(ADD_TIME_MUX_MS);
		if (can_add)
		{
			time_ = time;
		}
		return can_add;
	}

	void Group::clear()
	{
		for (auto& action : actions_)
		{
			delete action;
		}
	}

	void Group::execute(Database& db)
	{
		Transaction transaction(db);
		for (auto& action : actions_)
		{
			action->executeSql(db);
		}
		transaction.commit();
	}

	void Group::undo(const vehicle::VehiclePtr& vehicle)
	{
		for (auto it = actions_.rbegin(); it != actions_.rend(); it++)
		{
			(*it)->undo(vehicle);
		}
	}

	void Group::redo(const vehicle::VehiclePtr& vehicle)
	{
		for (auto& action : actions_)
		{
			action->redo(vehicle);
		}
	}
}	 // namespace action
