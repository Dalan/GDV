/*!
 * \file    list.h
 * \author  Remi BERTHO
 * \date    25/02/2018
 * \version 1.0.0
 */

/*
 * list.h
 *
 * Copyright 2016-2018 Remi BERTHO <remi.bertho@dalan.fr>
 *
 * This file is part of Csuper-gtk.
 *
 * Csuper-gtk is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * Csuper-gtk is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

#ifndef ACTION_LIST_H_INCLUDED
#define ACTION_LIST_H_INCLUDED

#include <deque>

#include "group.hpp"


namespace action
{
	/*! \class List
	 *   \brief This class is a undo redo manager
	 */
	class List
	{
		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Attributes ////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	private:
		std::deque<Group> actions_group_; /*!< The list of aciton to be done */
		unsigned int	  index_ = 0;	  /*!< The index of the last gaction to be done */



		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Constructor ///////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	public:
		/*!
		 *  \brief Default constructor
		 */
		List();

		/*!
		 *  \brief Destructor
		 */
		~List();


		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Function //////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	public:
		/*!
		 *  \brief Test if the user can do an undo or not
		 *  \return true if the user can undo, false otherwise
		 */
		bool canUndo() const;

		/*!
		 *  \brief Test if the user can do an redo or not
		 *  \return true if the user can redo, false otherwise
		 */
		bool canRedo() const;

		/*!
		 *  \brief Clear all the Actions
		 */
		void clear();

		/*!
		 *  \brief Execute all the Actions
		 */
		void execute(SQLite::Database& db);

		/*!
		 *  \brief Add a game
		 *  \param game the game
		 */
		void add(Action* action);

		/*!
		 *  \brief Undo the last action
		 */
		void undo(const vehicle::VehiclePtr& vehicle);
		/*!
		 *  \brief Redo the last action
		 */
		void redo(const vehicle::VehiclePtr& vehicle);
	};
}	 // namespace action

#endif	  // ACTION_LIST_H_INCLUDED
