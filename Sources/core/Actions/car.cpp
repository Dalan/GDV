/*!
 * \file    car.cpp
 * \author  Remi BERTHO
 * \date    07/02/18
 * \version 1.0.0
 */

/*
 * car.cpp
 *
 * Copyright 2018 Rémi BERTHO <remi.bertho@dalan.fr>
 *
 * This file is part of GDV.
 *
 * GDV is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * GDV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */


#include "car.hpp"

#include "exception.hpp"
#include "log.hpp"
#include "Car/car.hpp"

using namespace std;
using namespace Glib;
using namespace database;

namespace action
{
	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Constructor and destructor ////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
	Car::Car(const ItemPtr& car, Type type) : Action(car, type)
	{
		Item* pDbi = car.get();

		if (typeid(*pDbi) != typeid(car::Car))
		{
			string error_log(format("This function need a Car*, not a {}", typeid(*pDbi).name()));
			GDV_WARNING(error_log);
			throw WrongDbiType(error_log);
		}
	}


	Car::~Car() = default;

	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Function //////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
	string Car::toString() const
	{
		car::Car& car = dynamic_cast<car::Car&>(*getOldDbi());
		return format("Car on car {} type {}", car.name(), static_cast<uint32_t>(type()));
	}

	void Car::undo(vehicle::VehiclePtr vehicle)
	{
		auto			car		= static_pointer_cast<car::Car>(vehicle);
		const car::Car& dbi_car = dynamic_cast<car::Car&>(*getOldDbi());
		switch (type())
		{
		case MODIFY:
			*car = dbi_car;
			break;
		default:
			throw OperationNotSupported("Car can only be modified");
		}
	}

	void Car::redo(vehicle::VehiclePtr vehicle)
	{
		auto			car		= static_pointer_cast<car::Car>(vehicle);
		const car::Car& dbi_car = dynamic_cast<car::Car&>(*getDbi());
		switch (type())
		{
		case MODIFY:
			*car = dbi_car;
			break;
		default:
			throw OperationNotSupported("Car can only be modified");
		}
	}
}	 // namespace action
