/*!
 * \file    action.h
 * \author  Remi BERTHO
 * \date    07/02/18
 * \version 1.0.0
 */

/*
 * action.h
 *
 * Copyright 2018 Rémi BERTHO <remi.bertho@dalan.fr>
 *
 * This file is part of GDV.
 *
 * GDVis free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * GDV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */


#ifndef ACTION_H
#define ACTION_H

#include <glibmm.h>
#include <SQLiteCpp/SQLiteCpp.h>

#include "DataBase/item.hpp"
#include "Vehicle/vehicle.hpp"


namespace action
{
	/*! \class Action
	 *   \brief This class is a action
	 */
	class Action
	{
		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Enumeration ///////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	public:
		enum Type
		{
			ADD,
			DELETE,
			MODIFY
		};

		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Friendship ////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
		friend class DataBase;


		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Attributes ////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	protected:
		database::ItemPtr dbi_;
		Type			  type_;


		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Signals ///////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////



		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Constructor ///////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	public:
		/*!
		 *  \brief Default constructor
		 *  \param id the id
		 */
		explicit Action(database::ItemPtr dbi, const Type type);

		/*!
		 *  \brief Destructor
		 */
		virtual ~Action();


		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Function //////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	public:
		/*!
		 *  \brief Convert to a string
		 *  \return the string
		 */
		virtual std::string toString() const = 0;

		/*!
		 *  \brief Operator <<
		 *  \param os the ostream
		 *  \param dbi the Item
		 *  \return the ostream
		 */
		friend std::ostream& operator<<(std::ostream& os, const database::Item& dbi);

		/**
		 * @brief Undo the action on the dbi
		 */
		virtual void undo(vehicle::VehiclePtr vehicle) = 0;

		/**
		 * @brief Redo the action on the dbi
		 */
		virtual void redo(vehicle::VehiclePtr vehicle) = 0;

		/**
		 * @brief Execute the SQL statement for the action
		 */
		void executeSql(SQLite::Database& db);

		/**
		 * @brief Check if the action is based on the same dbi
		 * @param Another action
		 * @return true or false
		 */
		bool sameDbi(const Action& action);


		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Getter and setter /////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	public:
		/**
		 * @brief Get the type
		 * @return the type
		 */
		inline Type type() const
		{
			return type_;
		}

	protected:
		/**
		 * @brief Get the dbi
		 * @return the dbi
		 */
		inline database::ItemPtr getDbi() const
		{
			return dbi_;
		}

		/**
		 * @brief Get the old dbi
		 * @return the dbi
		 */
		inline database::ItemPtr getOldDbi() const
		{
			return dbi_->getOld();
		}
	};
}	 // namespace action


#endif /* ACTION_H */
