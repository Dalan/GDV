/*!
 * \file
 */

/*
 * vehicule_need_object.h
 *
 * Copyright 2019 Rémi BERTHO <remi.bertho@dalan.fr>
 *
 * This file is part of GDV.
 *
 * GDVis free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * GDV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */


#ifndef VEHICULE_NEED_OBJECT_ACTION_H
#define VEHICULE_NEED_OBJECT_ACTION_H

#include "action.hpp"
#include "Car/car.hpp"

namespace action
{
	/*! \class Action
	 *   \brief This class is a action
	 */
	class VehiculeNeedObject : public Action
	{
		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Constructor ///////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	public:
		/*!
		 *  \brief Default constructor
		 *  \param id the id
		 */
		explicit VehiculeNeedObject(database::ItemPtr dbi, const Type type);

		/*!
		 *  \brief Destructor
		 */
		~VehiculeNeedObject() override;


		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Function //////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	protected:
		template <typename ElementClass, typename ObjectClass, typename Position = std::nullptr_t> void undoInternal(car::CarPtr car)
		{
			static_assert(std::is_base_of_v<generic::Element, ElementClass>, "The class must be derived from generic::Element");
			static_assert(std::is_base_of_v<generic::Object, ObjectClass>, "The class must be derived from generic::Object");

			ObjectClass* old_object		 = static_cast<ObjectClass*>(getOldDbi().get());
			ObjectClass* original_object = static_cast<ObjectClass*>(getDbi().get());

			Position pos = nullptr;
			if constexpr (std::is_null_pointer_v<Position> == false)
				pos = static_cast<ElementClass*>(old_object->owner())->position();

			int64_t owner_id = -1;
			if (old_object != nullptr)
				owner_id = old_object->ownerId();
			if (owner_id == -1)
				owner_id = original_object->ownerId();

			ElementClass* owner = nullptr;
			for (generic::Element& ci : car->elements<ElementClass, Position>(pos))
			{
				if (ci.id() == owner_id)
				{
					owner = static_cast<ElementClass*>(&ci);
					break;
				}
			}

			switch (type())
			{
			case MODIFY:
				for (auto object : owner->template objects<ObjectClass>())
				{
					if (object->sameDatabaseId(*old_object))
					{
						ObjectClass* obj = static_cast<ObjectClass*>(object);
						*obj			 = *old_object;
						break;
					}
				}
				break;
			case ADD:
				owner->template removeObject<ObjectClass>(*original_object);
				break;
			case DELETE:
				ObjectClass& new_object = owner->template addObject<ObjectClass>();
				new_object				= *original_object;
				break;
			}
		}

		template <typename ElementClass, typename ObjectClass, typename Position = std::nullptr_t> void redoInternal(car::CarPtr car)
		{
			ObjectClass& newer_object = static_cast<ObjectClass&>(*getDbi());

			Position pos = nullptr;
			if constexpr (std::is_null_pointer_v<Position> == false)
				pos = static_cast<ElementClass*>(newer_object.owner())->position();

			ElementClass* owner = nullptr;
			for (generic::Element& ci : car->elements<ElementClass, Position>(pos))
			{
				if (ci.id() == newer_object.ownerId())
				{
					owner = static_cast<ElementClass*>(&ci);
					break;
				}
			}

			switch (type())
			{
			case MODIFY:
				for (auto object : owner->template objects<ObjectClass>())
				{
					if (object->sameDatabaseId(newer_object))
					{
						ObjectClass* obj = static_cast<ObjectClass*>(object);
						*obj			 = newer_object;
						break;
					}
				}
				break;
			case ADD:
			{
				ObjectClass& new_object = owner->template addObject<ObjectClass>();
				new_object				= static_cast<ObjectClass&>(*getDbi());
			}
			break;
			case DELETE:
				owner->template removeObject<ObjectClass>(static_cast<ObjectClass&>(*getDbi()));
				break;
			}
		}

	public:
		/*!
		 *  \brief Convert to a string
		 *  \return the string
		 */
		std::string toString() const override;

		/**
		 * @brief Undo the action on the dbi
		 */
		void undo(vehicle::VehiclePtr car) override;

		/**
		 * @brief Redo the action on the dbi
		 */
		void redo(vehicle::VehiclePtr car) override;
	};
}	 // namespace action


#endif /* VEHICULE_NEED_OBJECT_ACTION_H */
