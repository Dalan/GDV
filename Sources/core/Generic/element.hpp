/*!
 * \file    element.h
 * \author  Remi BERTHO
 * \date    15/05/18
 * \version 1.0.0
 */

/*
 * element.h
 *
 * Copyright 2018 Rémi BERTHO <remi.bertho@dalan.fr>
 *
 * This file is part of GDV.
 *
 * GDVis free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * GDV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

#ifndef GENERIC_ELEMENT_H_INCLUDED
#define GENERIC_ELEMENT_H_INCLUDED

#include "article.hpp"
#include "object_array.hpp"
#include "DataBase/property_proxy.hpp"
#include "DataBase/link_proxy.hpp"

namespace database
{
	class DataBase;
}

namespace generic
{
	class Vehicle;

	/*! \class Element
	 *   \brief This class represent a need with a location
	 */
	class Element : public Article
	{
		friend class ElementArray;
		friend class database::DataBase;

		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// DB infos //////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	private:
		static const constexpr char BEGIN_DATE_DB_NAME[]	= "begin_date";
		static const constexpr char BEGIN_DATE_DB_VERSION[] = "0.1.0";

		static const constexpr char END_DATE_DB_NAME[]	  = "end_date";
		static const constexpr char END_DATE_DB_VERSION[] = "0.1.0";

		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Enumeration ///////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	public:
		enum LinkType
		{
			NONE,
			END,
			ALL
		};

		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Signals ///////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	public:
		typedef sigc::signal<void, Element&> type_signal_need_sort;

		/*!
		 *  \brief Return the changed signal
		 *  \return the signal
		 */
		inline type_signal_need_sort signalNeedSort()
		{
			return signal_need_sort_;
		}

		template <typename ObjectClass> inline generic::ObjectArray::SignalAdded signalObjectAdded()
		{
			static_assert(std::is_base_of_v<generic::Object, ObjectClass>, "The class must be an object");

			return objects<ObjectClass>().signalAdded();
		}

		template <typename ObjectClass> inline generic::ObjectArray::SignalRemoved signalObjectRemoved()
		{
			static_assert(std::is_base_of_v<generic::Object, ObjectClass>, "The class must be an object");

			return objects<ObjectClass>().signalRemoved();
		}


	protected:
		/*!
		 *  \brief Emit the changed signal
		 */
		inline void emitSignalNeedSort()
		{
			signalNeedSort().emit(*this);
		}

	private:
		type_signal_need_sort signal_need_sort_;

		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Attributes ////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	private:
		Element* next_	   = nullptr;
		Element* previous_ = nullptr;

	protected:
		database::PropertyProxy<Glib::Date, BEGIN_DATE_DB_NAME, BEGIN_DATE_DB_VERSION> begin_date_;
		database::PropertyProxy<Glib::Date, END_DATE_DB_NAME, END_DATE_DB_VERSION>	   end_date_;
		std::map<std::string, generic::ObjectArray>									   objects_;

		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Constructor ///////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	public:
		/*!
		 *  \brief Default constructor
		 */
		explicit Element(const int64_t id, const OwnerInfos& owner_info);

		~Element();

	protected:
		/*!
		 *  \brief Copy constructor
		 */
		explicit Element(const Element& ci);


		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Function //////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	protected:
		/**
		 * @brief Check if the dbi changed with the old value
		 * @return true if it change, false otherwise
		 */
		virtual bool changed() const;

		/**
		 * @brief Exchange the next and previous with another Element
		 * @param ci a Element
		 */
		void exchangeRelated(Element& ci);


	public:
		/*!
		 *  \brief operator =
		 *  \param pn a Element
		 *  \return a reference to the object
		 */
		Element& operator=(const Element& ci);

		/**
		 * @brief operator ==
		 * @param pn another Element
		 * @return true if it has the same data, false otherwise
		 */
		bool operator==(const Element& ci) const;

		/**
		 * @brief Create the next element for the owner
		 * @return the next element
		 */
		virtual Element* createNext(const bool copy_objects = false) const = 0;

		/*!
		 *  \brief Convert to a string
		 *  \return the string
		 */
		virtual std::string toString() const;

		/**
		 * @brief Get the name as ID of the specific need
		 * @return the name
		 */
		virtual std::string nameId() const = 0;

		/**
		 * @brief Inform if the need need is currently used or not
		 * @return
		 */
		bool currentlyUsed() const;

		/** \brief Return the number of day before the new need
		 * \return The number of day before the new need
		 */
		int dayBefore() const;

		/** \brief Return the age of the need in days
		 * \return The age of the need in days
		 */
		unsigned int age() const;

		/*!
		 *  \brief Get the duration in days
		 *  \return The duration in days
		 */
		inline unsigned int duration() const
		{
			return static_cast<unsigned int>(beginDate().days_between(endDate()));
		}

		/*!
		 *  \brief Get the mean date
		 *  \return The mean date
		 */
		Glib::Date meanDate() const;

		/**
		 * @brief Compare two car item based on mean date
		 * @param ci
		 * @return 0 if equal, < 0 if before, > 0 if after
		 */
		int compareDate(const Element& ci) const;



		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Getter ////////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	private:
		inline auto& objects()
		{
			return objects_;
		}

	protected:
		inline Element* next()
		{
			return next_;
		}
		inline Element* previous()
		{
			return previous_;
		}

		inline const Element* next() const
		{
			return next_;
		}
		inline const Element* previous() const
		{
			return previous_;
		}

	public:
		template <typename ObjectClass> const generic::ObjectArray& objects() const
		{
			static_assert(std::is_base_of_v<generic::Object, ObjectClass>, "The class must be an object");

			return objects_.at(ObjectClass::DATABASE_TABLE_NAME);
		}

		template <typename ObjectClass> unsigned int nbObjects() const
		{
			static_assert(std::is_base_of_v<generic::Object, ObjectClass>, "The class must be an object");

			return objects<ObjectClass>().size();
		}

		inline Glib::Date beginDate() const
		{
			return begin_date_;
		}

		inline Glib::Date endDate() const
		{
			return end_date_;
		}

		virtual double distance() const = 0;

		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Setter ////////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	protected:
		void setNext(Element* ci);
		void setPrevious(Element* ci);

	public:
		template <typename ObjectClass> generic::ObjectArray& objects()
		{
			static_assert(std::is_base_of_v<generic::Object, ObjectClass>, "The class must be an object");

			return objects_[ObjectClass::DATABASE_TABLE_NAME];
		}

		template <typename ObjectClass> ObjectClass& addObject()
		{
			static_assert(std::is_base_of_v<generic::Object, ObjectClass>, "The class must be an object");

			ObjectClass* o = new ObjectClass(0, this);
			objects<ObjectClass>().add(o);
			return *o;
		}

		template <typename ObjectClass> void removeObject(ObjectClass& object)
		{
			static_assert(std::is_base_of_v<generic::Object, ObjectClass>, "The class must be an object");

			objects<ObjectClass>().remove(object);
		}

		/*!
		 *  \brief Set the last_date
		 *  \param last_date the last_date
		 */
		virtual void setBeginDate(const Glib::Date& date, const LinkType link_type = NONE);

		/*!
		 *  \brief Set the last_date
		 *  \param last_date the last_date
		 */
		void setEndDate(const Glib::Date& date);
	};
}	 // namespace generic

#endif	  // VEHICLE_ITEM_H_INCLUDED
