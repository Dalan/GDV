/*!
 * \file
 */

/*
 * object.h
 *
 * Copyright 2019 Rémi BERTHO <remi.bertho@dalan.fr>
 *
 * This file is part of GDV.
 *
 * GDVis free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * GDV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

#ifndef OBJECT_H_INCLUDED
#define OBJECT_H_INCLUDED

#include "share.hpp"
#include "DataBase/item.hpp"
#include "DataBase/property_proxy.hpp"

namespace generic
{
	typedef std::pair<std::string, int64_t> ObjectId;


	/*! \class Object
	 *   \brief This class represent a object
	 */
	class Object : public database::Item
	{

		friend class DataBase;

		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// DB infos //////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	private:
		static const constexpr char NAME_DB_NAME[]	  = "name";
		static const constexpr char NAME_DB_VERSION[] = "0.1.0";

		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Attributes ////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	protected:
		database::PropertyProxy<std::string, NAME_DB_NAME, NAME_DB_VERSION> name_;


		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Constructor ///////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	public:
		explicit Object(const int64_t id);
		Object(const int64_t id, const std::string& name);

		virtual ~Object() = default;

	protected:
		explicit Object(const Object& object);

		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Function //////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	public:
		virtual std::string toString() const;

		Object& operator=(const Object& object);

		bool operator==(const Object& object) const;

		inline ObjectId objectId() const
		{
			return std::make_pair(name(), id());
		}


		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Getter ////////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	public:
		inline std::string name() const
		{
			return name_;
		}


		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Setter ////////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	public:
		inline void setName(const std::string& name)
		{
			name_ = name;
		}
	};
}	 // namespace generic

#endif	  // OBJECT_H_INCLUDED
