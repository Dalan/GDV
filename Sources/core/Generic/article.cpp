/*
 * article.cpp
 *
 * Copyright 2019 Rémi BERTHO <remi.bertho@dalan.fr>
 *
 * This file is part of GDV.
 *
 * GDVis free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * GDV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

#include "log.hpp"

#include <fmt/core.h>
#include <numeric>
#include "internationalization.hpp"
#include <iostream>

#include "article.hpp"
#include "exception.hpp"

using namespace Glib;
using namespace std;

namespace generic
{
	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Constructor and destructor ////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
	Article::Article(const int64_t id, const OwnerInfos& owner_info) : OwnedObject(id, owner_info), price_(*this)
	{
	}

	Article::Article(const Article& article) : OwnedObject(article), price_(article.price_, *this)
	{
	}


	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Function //////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
	string Article::toString() const
	{
		string res = OwnedObject::toString();

		res += fmt::format(fmt::runtime(_("\nPrice: {}")), price());

		return res;
	}

	Article& Article::operator=(const Article& article)
	{
		if (this == &article)
		{
			return *this;
		}

		Item::operator=(article);

		return *this;
	}

	bool Article::operator==(const Article& article) const
	{
		if (OwnedObject::operator==(article))
		{
			if ((price() == article.price()))
			{
				return true;
			}
		}

		return false;
	}
}	 // namespace generic
