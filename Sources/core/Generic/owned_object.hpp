/*!
 * \file
 */

/*
 * ownedobject.h
 *
 * Copyright 2019 Rémi BERTHO <remi.bertho@dalan.fr>
 *
 * This file is part of GDV.
 *
 * GDVis free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * GDV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

#ifndef OWNEDOBJECT_H_INCLUDED
#define OWNEDOBJECT_H_INCLUDED

#include "share.hpp"
#include "object.hpp"
#include "DataBase/link_proxy.hpp"

namespace generic
{
	typedef std::pair<std::string, int64_t> OwnedObjectId;


	/*! \class OwnedObject
	 *   \brief This class represent a ownedobject
	 */
	class OwnedObject : public Object
	{
		friend class DataBase;

	public:
		typedef std::tuple<database::Item*, std::string> OwnerInfos;

		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// DB infos //////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	private:
		static const constexpr char OWNER_ID_DB_NAME[]	  = "owner_id";
		static const constexpr char OWNER_ID_DB_VERSION[] = "0.1.0";

		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Attributes ////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	protected:
		database::Link owner_;


		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Constructor ///////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	public:
		OwnedObject(const int64_t id, const OwnerInfos& owner_info);

		virtual ~OwnedObject() override = default;

	protected:
		explicit OwnedObject(const OwnedObject& ownedobject);

		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Function //////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	public:
		virtual std::string toString() const override;

		OwnedObject& operator=(const OwnedObject& ownedobject);

		bool operator==(const OwnedObject& ownedobject) const;

		inline const std::string& ownerTable() const
		{
			return owner_.referenceTable();
		}

		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Getter and setter /////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	public:
		inline int64_t ownerId() const
		{
			return owner_;
		}
		inline void setOwnerId(const int64_t id)
		{
			owner_ = id;
		}

		inline database::Item* owner() const
		{
			return owner_;
		}
		inline void setOwner(database::Item* owner)
		{
			owner_ = owner;
		}
	};
}	 // namespace generic

#endif	  // OWNEDOBJECT_H_INCLUDED
