/*!
 * \file    element.cpp
 * \author  Remi BERTHO
 * \date    15/05/18
 * \version 1.0.0
 */

/*
 * element.cpp
 *
 * Copyright 2018 Rémi BERTHO <remi.bertho@dalan.fr>
 *
 * This file is part of GDV.
 *
 * GDVis free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * GDV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

#include "internationalization.hpp"

#include <fmt/core.h>

#include "element.hpp"
#include "log.hpp"
#include "Vehicle/vehicle.hpp"

using namespace Glib;
using namespace database;
using namespace std;

namespace generic
{
	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Constructor and destructor ////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
	Element::Element(const int64_t id, const OwnerInfos& owner_info)
			: Article(id, owner_info), begin_date_(*this, Date(1, Date::JANUARY, 2020)), end_date_(*this, Date(1, Date::JANUARY, 2021))
	{
	}

	Element::~Element()
	{
		for (auto& object_array : objects_)
		{
			object_array.second.deleteAll();
		}
	}

	Element::Element(const Element& ci) : Article(ci), begin_date_(ci.begin_date_, *this), end_date_(ci.end_date_, *this)
	{
	}



	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Function //////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
	bool Element::changed() const
	{
		if (getOld() != nullptr)
		{
			return !(*this == *dynamic_cast<Element*>(getOld().get()));
		}

		return false;
	}

	void Element::exchangeRelated(Element& ci)
	{
		Element* old_first;
		Element* old_second;
		Element* next_item;
		Element* previous_item;

		if (next() == &ci)
		{
			old_first  = this;
			old_second = &ci;
		}
		else
		{
			old_first  = &ci;
			old_second = this;
		}
		next_item	  = old_second->next();
		previous_item = old_first->previous();

		if (previous_item != nullptr)
		{
			previous_item->setNext(old_second);
		}
		if (next_item != nullptr)
		{
			next_item->setPrevious(old_first);
		}
		old_first->setPrevious(old_second);
		old_second->setNext(old_first);
		old_first->setNext(next_item);
		old_second->setPrevious(previous_item);
	}

	Element& Element::operator=(const Element& ci)
	{
		Item::operator=(ci);
		emitSignalNeedSort();

		return *this;
	}

	bool Element::operator==(const Element& ci) const
	{
		if (Article::operator==(ci))
		{
			if (beginDate() == ci.beginDate())
			{
				if (endDate().valid() && ci.endDate().valid())
				{
					if (endDate() == ci.endDate())
					{
						return true;
					}
				}
				else
				{
					if (!endDate().valid() && !ci.endDate().valid())
					{
						return true;
					}
				}
			}
		}

		return false;
	}

	string Element::toString() const
	{
		string res = Article::toString();

		res += fmt::format(fmt::runtime(_("Begin date: {}\nEnd date: {}\n")),
				beginDate().format_string("%x").c_str(),
				endDate().format_string("%x").c_str());

		for (auto& object_array : objects_)
		{
			res += object_array.second.toString();
		}

		return res;
	}

	bool Element::currentlyUsed() const
	{
		return previous_ == nullptr;
	}

	int Element::dayBefore() const
	{
		Date now;
		now.set_time_current();
		return now.days_between(endDate());
	}

	unsigned int Element::age() const
	{
		Date compare_date;
		compare_date.set_time_current();
		return static_cast<unsigned int>(beginDate().days_between(compare_date));
	}

	Date Element::meanDate() const
	{
		Date mean(beginDate());
		mean.add_days(static_cast<int>(duration() / 2));
		return mean;
	}

	int Element::compareDate(const Element& ci) const
	{
		return beginDate().compare(ci.beginDate());
	}


	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Setter ////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
	void Element::setNext(Element* ci)
	{
		next_ = ci;
	}

	void Element::setPrevious(Element* ci)
	{
		previous_ = ci;
	}

	void Element::setBeginDate(const Date& date, const LinkType link_type)
	{
		if (beginDate() != date)
		{
			begin_date_ = date;
			emitSignalNeedSort();
			if (link_type == ALL)
			{
				if (next() != nullptr)
				{
					next()->setEndDate(beginDate());
				}
			}
		}
	}

	void Element::setEndDate(const Date& date)
	{
		if (!endDate().valid() || endDate() != date)
		{
			end_date_ = date;
		}
	}
}	 // namespace generic
