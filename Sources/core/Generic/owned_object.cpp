/*
 * ownedobject.cpp
 *
 * Copyright 2019 Rémi BERTHO <remi.bertho@dalan.fr>
 *
 * This file is part of GDV.
 *
 * GDVis free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * GDV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

#include "log.hpp"

#include <fmt/core.h>
#include <numeric>
#include "internationalization.hpp"
#include <iostream>

#include "owned_object.hpp"
#include "exception.hpp"

using namespace Glib;
using namespace std;

namespace generic
{
	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Constructor and destructor ////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
	OwnedObject::OwnedObject(const int64_t id, const OwnerInfos& owner_info)
			: Object(id), owner_(*this, OWNER_ID_DB_NAME, Version(OWNER_ID_DB_VERSION), get<0>(owner_info), get<1>(owner_info))
	{
	}

	OwnedObject::OwnedObject(const OwnedObject& ownedobject) : Object(ownedobject), owner_(ownedobject.owner_, *this)
	{
	}


	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Function //////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
	string OwnedObject::toString() const
	{
		string res = Object::toString();

		res += fmt::format(fmt::runtime(_("\nOwnerId: {}")), ownerId());

		return res;
	}

	OwnedObject& OwnedObject::operator=(const OwnedObject& ownedobject)
	{
		if (this == &ownedobject)
		{
			return *this;
		}

		Item::operator=(ownedobject);

		return *this;
	}

	bool OwnedObject::operator==(const OwnedObject& ownedobject) const
	{
		if (Object::operator==(ownedobject))
		{
			if ((ownerId() == ownedobject.ownerId()))
			{
				return true;
			}
		}

		return false;
	}
}	 // namespace generic
