/*!
 * \file    element_array.h
 * \author  Remi BERTHO
 * \date    01/04/2018
 * \version 1.0.0
 */

/*
 * Remi.h
 *
 * Copyright 2018 Rémi BERTHO <remi.bertho@dalan.fr>
 *
 * This file is part of GDV.
 *
 * GDV is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * GDV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

#ifndef CAR_ITEM_ARRAY_H_INCLUDED
#define CAR_ITEM_ARRAY_H_INCLUDED

#include <glibmm.h>
#include <vector>
#include "element.hpp"
#include "exception.hpp"

namespace generic
{
	/**
	 * @class ElementArray
	 * @brief An array withe signal and exception handling
	 */
	class ElementArray
	{
		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Attributes ////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	private:
		Element*	 begin_ = nullptr;
		Element*	 end_	= nullptr;
		unsigned int size_	= 0;

		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Signals ///////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	public:
		typedef sigc::signal<void, database::ItemPtr, unsigned int> SignalRemoved;
		typedef sigc::signal<void, Element&>						SignalAdded;

		inline SignalAdded signalAdded()
		{
			return signal_added_;
		}
		inline SignalRemoved signalRemoved()
		{
			return signal_removed_;
		}

	private:
		SignalAdded	  signal_added_;
		SignalRemoved signal_removed_;

		///////////////////////////////////////////////////////////////////////////////////
		///////////////////////////// Iterators ///////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	public:
		class iterator
		{
		private:
			Element* it_;

		public:
			iterator(Element* ci);
			Element&  operator*() const;
			iterator& operator++();
			iterator  operator++(int);
			iterator& operator--();
			iterator  operator--(int);
			Element*  operator->() const;
			bool	  operator==(const iterator& it);
			bool	  operator!=(const iterator& it);
		};

		typedef iterator const_iterator;

		inline iterator begin()
		{
			return iterator(begin_);
		}

		inline const_iterator begin() const
		{
			return cbegin();
		}

		inline const_iterator cbegin() const
		{
			return iterator(begin_);
		}

		inline iterator end()
		{
			return iterator(nullptr);
		}

		inline const_iterator end() const
		{
			return cend();
		}

		inline const_iterator cend() const
		{
			return iterator(nullptr);
		}


		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Constructor ///////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	public:
		/*!
		 *  \brief Default constructor
		 */
		explicit ElementArray();

		~ElementArray();

		///////////////////////////////////////////////////////////////////////////////////
		///////////////////////////// Getter //////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	public:
		Element&	   at(unsigned int index);
		const Element& at(unsigned int index) const;

		inline Element& operator[](unsigned int index)
		{
			return at(index);
		}

		inline const Element& operator[](unsigned int index) const
		{
			return at(index);
		}

		unsigned int getIndex(const Element& element) const;


		///////////////////////////////////////////////////////////////////////////////////
		///////////////////////////// Capacity ////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	public:
		inline bool empty() const
		{
			return size() == 0;
		}

		inline unsigned int size() const
		{
			return size_;
		}


		///////////////////////////////////////////////////////////////////////////////////
		///////////////////////////// Modifiers ///////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	public:
		void clear();

		void deleteAll();

		void add(Element* element);

		void remove(Element& element);


		///////////////////////////////////////////////////////////////////////////////////
		///////////////////////////// Functions ///////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	public:
		double totalPrice() const;
		double meanPrice() const;

		unsigned int meanDuration() const;

		double meanDistance() const;

		std::string toString() const;

		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Update sort ///////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	private:
		void onElementNeedSort(Element& ci);
	};
}	 // namespace generic


#endif	  // CAR_ITEM_ARRAY_H_INCLUDED
