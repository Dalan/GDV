/*!
 * \file    object_array.h
 * \author  Remi BERTHO
 * \date    01/04/2018
 * \version 1.0.0
 */

/*
 * Remi.h
 *
 * Copyright 2018 Rémi BERTHO <remi.bertho@dalan.fr>
 *
 * This file is part of GDV.
 *
 * GDV is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * GDV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

#ifndef OBJECT_ARRAY_H_INCLUDED
#define OBJECT_ARRAY_H_INCLUDED

#include <glibmm.h>
#include <vector>
#include "owned_object.hpp"
#include "exception.hpp"

namespace generic
{
	/**
	 * @class ObjectArray
	 * @brief An array withe signal and exception handling
	 */
	class ObjectArray
	{
		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Attributes ////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	private:
		std::vector<OwnedObject*> objects_;

		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Signals ///////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	public:
		typedef sigc::signal<void, database::ItemPtr, unsigned int> SignalRemoved;
		typedef sigc::signal<void, OwnedObject&>					SignalAdded;

		inline SignalAdded signalAdded()
		{
			return signal_added_;
		}
		inline SignalRemoved signalRemoved()
		{
			return signal_removed_;
		}

	private:
		SignalAdded	  signal_added_;
		SignalRemoved signal_removed_;


		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Constructor ///////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	public:
		/*!
		 *  \brief Default constructor
		 */
		explicit ObjectArray();

		~ObjectArray();

		///////////////////////////////////////////////////////////////////////////////////
		///////////////////////////// Getter //////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	public:
		OwnedObject&	   at(unsigned int index);
		const OwnedObject& at(unsigned int index) const;

		inline OwnedObject& operator[](unsigned int index)
		{
			return at(index);
		}

		inline const OwnedObject& operator[](unsigned int index) const
		{
			return at(index);
		}


		///////////////////////////////////////////////////////////////////////////////////
		///////////////////////////// Capacity ////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	public:
		inline bool empty() const
		{
			return objects_.empty();
		}

		inline unsigned int size() const
		{
			return static_cast<unsigned int>(objects_.size());
		}


		///////////////////////////////////////////////////////////////////////////////////
		///////////////////////////// Modifiers ///////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	public:
		void clear();
		void deleteAll();
		void add(OwnedObject* object);
		void remove(OwnedObject& object);

		//////////////////////////////////////////////////////////////////////////////////
		///////////////////////////// Iterators ///////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	public:
		typedef std::vector<OwnedObject*>::iterator		  iterator;
		typedef std::vector<OwnedObject*>::const_iterator const_iterator;

		inline iterator begin()
		{
			return objects_.begin();
		}

		inline const_iterator begin() const
		{
			return objects_.begin();
		}

		inline const_iterator cbegin() const
		{
			return objects_.cbegin();
		}

		inline iterator end()
		{
			return objects_.end();
		}

		inline const_iterator end() const
		{
			return objects_.end();
		}

		inline const_iterator cend() const
		{
			return objects_.cend();
		}


		///////////////////////////////////////////////////////////////////////////////////
		///////////////////////////// Functions ///////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	public:
		std::string toString() const;
	};
}	 // namespace generic


#endif	  // CAR_ITEM_ARRAY_H_INCLUDED
