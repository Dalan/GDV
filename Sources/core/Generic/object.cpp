/*
 * object.cpp
 *
 * Copyright 2019 Rémi BERTHO <remi.bertho@dalan.fr>
 *
 * This file is part of GDV.
 *
 * GDVis free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * GDV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

#include "log.hpp"

#include <fmt/core.h>
#include <numeric>
#include "internationalization.hpp"
#include <iostream>

#include "object.hpp"
#include "exception.hpp"

using namespace Glib;
using namespace std;

namespace generic
{
	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Constructor and destructor ////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
	Object::Object(const int64_t id) : Item(id), name_(*this)
	{
	}

	Object::Object(const int64_t id, const string& name) : Item(id), name_(*this, name)
	{
	}

	Object::Object(const Object& object) : Item(object), name_(object.name_, *this)
	{
	}


	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Function //////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
	string Object::toString() const
	{
		string res;

		res += fmt::format(fmt::runtime(_("Object:\nName: {}")), name());

		return res;
	}

	Object& Object::operator=(const Object& object)
	{
		if (this == &object)
		{
			return *this;
		}

		Item::operator=(object);

		return *this;
	}

	bool Object::operator==(const Object& object) const
	{
		if (Item::operator==(object))
		{
			if ((name() == object.name()))
			{
				return true;
			}
		}

		return false;
	}
}	 // namespace generic
