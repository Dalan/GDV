/*!
 * \file    element_array.h
 * \author  Remi BERTHO
 * \date    02/04/2018
 * \version 1.0.0
 */

/*
 * element_array.h
 *
 * Copyright 2018 Rémi BERTHO <remi.bertho@dalan.fr>
 *
 * This file is part of GDV.
 *
 * GDVis free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * GDV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

#include "log.hpp"

#include <numeric>
#include "internationalization.hpp"

#include "element_array.hpp"


using namespace Glib;
using namespace std;
using namespace sigc;
using namespace database;

namespace generic
{
	///////////////////////////////////////////////////////////////////////////////////
	///////////////////////////// Iterators ///////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
	ElementArray::iterator::iterator(Element* ci) : it_(ci)
	{
	}

	Element& ElementArray::iterator::operator*() const
	{
		return *it_;
	}

	ElementArray::iterator& ElementArray::iterator::operator++()
	{
		if (it_ != nullptr)
		{
			it_ = it_->next();
		}
		return *this;
	}

	ElementArray::iterator ElementArray::iterator::operator++(int)
	{
		iterator res(*this);
		++(*this);
		return res;
	}

	ElementArray::iterator& ElementArray::iterator::operator--()
	{
		if (it_ != nullptr)
		{
			it_ = it_->previous();
		}
		return *this;
	}

	ElementArray::iterator ElementArray::iterator::operator--(int)
	{
		iterator res(*this);
		--(*this);
		return res;
	}

	Element* ElementArray::iterator::operator->() const
	{
		return it_;
	}

	bool ElementArray::iterator::operator==(const ElementArray::iterator& it)
	{
		return it_ == it.it_;
	}

	bool ElementArray::iterator::operator!=(const ElementArray::iterator& it)
	{
		return it_ != it.it_;
	}

	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Constructor ///////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
	ElementArray::ElementArray() = default;

	ElementArray::~ElementArray() = default;

	///////////////////////////////////////////////////////////////////////////////////
	///////////////////////////// Getter //////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
	Element& ElementArray::at(unsigned int index)
	{
		Element*	 tmp_item = begin_;
		unsigned int counter  = 0;

		if (index >= size())
		{
			throw OutOfRange(format("You cannot access to the {}th element"
									" because there is only {} element.",
					index,
					size()));
		}

		while (tmp_item != nullptr)
		{
			if (counter == index)
			{
				return *tmp_item;
			}

			counter++;
			tmp_item = tmp_item->next();
		}

		GDV_WARNING("Null pointer car item in array.");
		throw RuntimeError("Null pointer car item in array.");
	}

	const Element& ElementArray::at(unsigned int index) const
	{
		Element*	 tmp_item = begin_;
		unsigned int counter  = 0;

		if (index >= size())
		{
			throw OutOfRange(format("You cannot access to the {}th element"
									" because there is only {} element.",
					index,
					size()));
		}

		while (tmp_item != nullptr)
		{
			if (counter == index)
			{
				return *tmp_item;
			}

			counter++;
			tmp_item = tmp_item->next();
		}

		GDV_WARNING("Null pointer car item in array.");
		throw RuntimeError("Null pointer car item in array.");
	}

	unsigned int ElementArray::getIndex(const Element& element) const
	{
		unsigned int counter = 0;
		for (auto& it : *this)
		{
			if (element.sameDatabaseId(it))
			{
				return counter;
			}
			counter++;
		}

		throw NotFound("Element not founded in ElementArray");
	}

	///////////////////////////////////////////////////////////////////////////////////
	///////////////////////////// Modifiers ///////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
	void ElementArray::clear()
	{
		size_  = 0;
		begin_ = nullptr;
		end_   = nullptr;
	}

	void ElementArray::deleteAll()
	{
		if (size() > 0)
		{
			Element* current_item;
			Element* next_item = begin_;
			do
			{
				current_item = next_item;
				next_item	 = current_item->next();
				delete current_item;
			} while (next_item != nullptr);

			clear();
		}
	}

	void ElementArray::add(Element* element)
	{
		if (size() == 0)
		{
			begin_ = end_ = element;
			element->setNext(nullptr);
			element->setPrevious(nullptr);
		}
		else
		{
			Element* current_item = begin_;

			do
			{
				if (element->compareDate(*current_item) > 0)
				{
					break;
				}
				current_item = current_item->next();
			} while (current_item != nullptr);

			if (current_item == begin_)
			{
				element->setNext(current_item);
				element->setPrevious(nullptr);
				current_item->setPrevious(element);
				begin_ = element;
			}
			else if (current_item == nullptr)
			{
				element->setPrevious(end_);
				element->setNext(nullptr);
				end_->setNext(element);
				end_ = element;
			}
			else
			{
				element->setNext(current_item);
				element->setPrevious(current_item->previous());
				current_item->previous()->setNext(element);
				current_item->setPrevious(element);
			}
		}
		size_++;
		element->signalNeedSort().connect(mem_fun(*this, &ElementArray::onElementNeedSort));

		signalAdded().emit(*element);
	}

	void ElementArray::remove(Element& element)
	{
		if (size() > 0)
		{
			Element*	 current_item = begin_;
			unsigned int index		  = 0;

			do
			{
				if (element.sameDatabaseId(*current_item))
				{
					break;
				}
				index++;
				current_item = current_item->next();
			} while (current_item != nullptr);

			if (current_item != nullptr)
			{
				if (current_item == begin_)
				{
					if (current_item == end_)
					{
						begin_ = end_ = nullptr;
					}
					else
					{
						begin_ = current_item->next();
						begin_->setPrevious(nullptr);
					}
				}
				else if (current_item == end_)
				{
					end_ = current_item->previous();
					end_->setNext(nullptr);
				}
				else
				{
					current_item->previous()->setNext(current_item->next());
					current_item->next()->setPrevious(current_item->previous());
				}

				size_--;
				ItemPtr copy = element.clone();
				delete current_item;
				signalRemoved().emit(copy, index);
				return;
			}
		}

		throw NotFound("Element not founded in ElementArray");
	}

	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Function //////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////

	double ElementArray::totalPrice() const
	{
		return accumulate(begin(), end(), 0., [](double a, Element& b) { return a + b.price(); });
	}

	double ElementArray::meanPrice() const
	{
		if (size() > 0)
		{
			return totalPrice() / size();
		}

		return 0;
	}

	unsigned int ElementArray::meanDuration() const
	{
		if (size() == 0)
		{
			return 0;
		}

		if (size() == 1)
		{
			return at(0).duration();
		}

		unsigned int total = accumulate(++begin(), end(), 0U, [](unsigned int a, Element& b) { return a + b.duration(); });
		return total / (size() - 1);
	}

	double ElementArray::meanDistance() const
	{
		if (size() == 0)
		{
			return 0;
		}

		if (size() == 1)
		{
			return at(0).distance();
		}

		double total = accumulate(++begin(), end(), 0., [](double a, Element& b) { return a + b.distance(); });
		return total / (size() - 1);
	}

	string ElementArray::toString() const
	{
		return accumulate(begin(), end(), string(), [](const string& a, const Element& b) { return a + b.toString() + "\n\n"; });
	}

	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Update sort ///////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
	void ElementArray::onElementNeedSort(Element& element)
	{
		Element* current_item;
		// Move next
		current_item = element.next();
		while ((current_item != nullptr) && (element.compareDate(*current_item) <= 0))
		{
			element.exchangeRelated(*current_item);

			if (&element == begin_)
			{
				begin_ = current_item;
			}
			if (current_item == end_)
			{
				end_ = &element;
			}

			current_item = element.next();
		}

		// Move previous
		current_item = element.previous();
		while ((current_item != nullptr) && (element.compareDate(*current_item) > 0))
		{
			element.exchangeRelated(*current_item);

			if (&element == end_)
			{
				end_ = current_item;
			}
			if (current_item == begin_)
			{
				begin_ = &element;
			}

			current_item = element.previous();
		}
	}

}	 // namespace generic
