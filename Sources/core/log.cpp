/*!
 * \file    log.cpp
 * \author  Remi BERTHO
 * \date    21/11/17
 * \version 1.0.0
 */

/*
 * log.cpp
 *
 * Copyright 2017 Rémi BERTHO <remi.bertho@dalan.fr>
 *
 * This file is part of GDV.
 *
 * GDVis free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * GDV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

#include "log.hpp"

#include <glibmm.h>
#include <map>
#include <iomanip>
#include <iostream>

#include "config.h"

#include "share.hpp"

using namespace std;
using namespace Glib;

namespace core
{
	///////////////////////////////////////////////////////////////////////////////////
	//////////////////////////// Prototypes ///////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
	/*!
	 *  \brief Function called when log occured
	 *  \param log_level the log level
	 *  \param fields the log fiels
	 *  \param n_fields the number of fields
	 *  \param user_data the user data (string)
	 */
	static GLogWriterOutput logWriterFunction(GLogLevelFlags log_level, const GLogField* fields, gsize n_fields, gpointer user_data);


	///////////////////////////////////////////////////////////////////////////////////
	////////////////////////////// Functions //////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
	GLogWriterOutput logWriterFunction(GLogLevelFlags log_level, const GLogField* fields, gsize n_fields, gpointer user_data)
	{
		string				print_log_level = *static_cast<string*>(user_data);
		map<string, string> str_fields;
		DateTime			current_time = DateTime::create_now_local();
		ostringstream		log_message;

// Log to systemd
#if (!defined PORTABLE) && (defined NDEBUG)
		g_log_writer_journald(log_level, fields, n_fields, user_data);
#endif

		// Check if log need to be written
		if (((print_log_level == "warning") && (log_level > G_LOG_LEVEL_WARNING)) ||
				((print_log_level == "info") && (log_level > G_LOG_LEVEL_INFO)))
		{
			return G_LOG_WRITER_HANDLED;
		}

		// Convert to map
		for (gsize i = 0; i < n_fields; i++)
		{
			str_fields[fields[i].key] = static_cast<const char*>(fields[i].value);
		}

		// Return if it's not my domain
		if (str_fields["GLIB_DOMAIN"] != GDV_PROGRAM_NAME)
		{
			return G_LOG_WRITER_HANDLED;
		}

		// Print date
		log_message << "[" << current_time.format("%X.") << setw(3) << setfill('0') << current_time.get_microsecond() / 1000 << "] ";

		// Print severity
		switch (log_level)
		{
		case G_LOG_FLAG_RECURSION:
			log_message << "Emergency ";
			break;
		case G_LOG_FLAG_FATAL:
			log_message << "Fatal     ";
			break;
		case G_LOG_LEVEL_ERROR:
			log_message << "Error     ";
			break;
		case G_LOG_LEVEL_CRITICAL:
			log_message << "Critical  ";
			break;
		case G_LOG_LEVEL_WARNING:
			log_message << "Warning   ";
			break;
		case G_LOG_LEVEL_MESSAGE:
			log_message << "Message   ";
			break;
		case G_LOG_LEVEL_INFO:
			log_message << "Info      ";
			break;
		case G_LOG_LEVEL_DEBUG:
			log_message << "Debug     ";
			break;
		default:
			break;
		}

		// Print domain
		log_message << str_fields["GLIB_DOMAIN"];

		// Print file
		if (str_fields.count("CODE_FILE") == 1)
		{
			log_message << " - " << string(path_get_basename(str_fields["CODE_FILE"])) << ":" << str_fields["CODE_LINE"];
		}

		// Print function
		if (str_fields.count("CODE_FUNC") == 1)
		{
			log_message << " - " << str_fields["CODE_FUNC"] << " ";
		}

		// Print message
		log_message << ": ";
		log_message << removeCharacterInString(str_fields["MESSAGE"], '\n');

		// Print the message to the stream
		cerr << log_message.str() << endl;

		return G_LOG_WRITER_HANDLED;
	}

	void log(LogLevel level, const std::string& file, unsigned int line, const std::string& fn, const std::string& msg)
	{
		GLogLevelFlags glevel = G_LOG_LEVEL_ERROR;
		switch (level)
		{
		case DEBUG:
			glevel = G_LOG_LEVEL_DEBUG;
			break;
		case INFO:
			glevel = G_LOG_LEVEL_INFO;
			break;
		case WARNING:
			glevel = G_LOG_LEVEL_WARNING;
			break;
		case ERROR:
			glevel = G_LOG_LEVEL_CRITICAL;
			break;
		case FATAL:
			glevel = G_LOG_LEVEL_ERROR;
			break;
		}

		g_log_structured(GDV_PROGRAM_NAME,
				glevel,
				"CODE_FILE",
				file.c_str(),
				"CODE_LINE",
				to_string(line).c_str(),
				"CODE_FUNC",
				fn.c_str(),
				"MESSAGE",
				msg.c_str());
	}

	void initLog()
	{
		static string log_level = Glib::getenv("GDV_LOG_LEVEL");

		if (log_level.empty() || ((log_level != "debug") && (log_level != "warning") && (log_level != "info")))
		{
			log_level = "warning";
		}
		g_log_set_writer_func(&logWriterFunction, &log_level, nullptr);
	}
}	 // namespace core
