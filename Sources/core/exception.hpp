/*!
 * \file    exception.h
 * \author  Remi BERTHO
 * \date    16/03/16
 * \version 1.0.0
 */

/*
 * exception.h
 *
 * Copyright 2017-2024 Rémi BERTHO <remi.bertho@dalan.fr>
 *
 * This file is part of GDV.
 *
 * GDVis free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * GDV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

#ifndef EXCEPTION_H_INCLUDED
#define EXCEPTION_H_INCLUDED

#include <exception>
#include <string>

namespace core
{
	class Exception;

	/** Exception smart pointer */
	typedef std::shared_ptr<Exception> ExceptionPtr;


	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Base exception ////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
	/*! \class Exception
	 *   \brief This class represent a csuper Exception
	 */
	class Exception : public std::exception
	{
	private:
		std::string msg_;

	public:
		/*!
		 *  \brief Constructor with the string parameter
		 *  \param msg the message
		 */
		explicit Exception(std::string msg);

		/*!
		 *  \brief Return the message
		 *  \return msg the message
		 */
		virtual const char* what() const noexcept;

		/**
		 * @brief Clone the exception
		 * @return the clone in a shared_ptr
		 */
		virtual ExceptionPtr clone() const = 0;
	};



	/**
	 * @class ExceptionCRTP
	 * @brief An excption with CRTP implementation of clone
	 */
	template <typename Derived> class ExceptionCRTP : public Exception
	{
	public:
		/*!
		 *  \brief Constructor with the string parameter
		 *  \param msg the string of the error
		 */
		explicit ExceptionCRTP(const std::string msg) : Exception(msg)
		{
		}

		/**
		 * @brief Clone the exception
		 * @return the clone in a shared_ptr
		 */
		virtual ExceptionPtr clone() const override
		{
			return std::make_shared<Derived>(static_cast<Derived const&>(*this));
		}
	};



	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Specialized exception /////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
	class NotFound : public ExceptionCRTP<NotFound>
	{
	public:
		explicit NotFound(const std::string& msg);
	};


	class OutOfRange : public ExceptionCRTP<OutOfRange>
	{
	public:
		explicit OutOfRange(const std::string& msg);
	};


	class SQLError : public ExceptionCRTP<SQLError>
	{
	public:
		explicit SQLError(const std::string& msg);
	};


	class OperationNotSupported : public ExceptionCRTP<OperationNotSupported>
	{
	public:
		explicit OperationNotSupported(const std::string& msg);
	};


	class WrongDbiType : public ExceptionCRTP<WrongDbiType>
	{
	public:
		explicit WrongDbiType(const std::string& msg);
	};


	class UndoRedoError : public ExceptionCRTP<UndoRedoError>
	{
	public:
		explicit UndoRedoError(const std::string& msg);
	};


	class RuntimeError : public ExceptionCRTP<RuntimeError>
	{
	public:
		explicit RuntimeError(const std::string& msg);
	};


	class WrongPropertyType : public ExceptionCRTP<WrongPropertyType>
	{
	public:
		explicit WrongPropertyType(const std::string& msg);
	};


	class UnknownPropertyType : public ExceptionCRTP<UnknownPropertyType>
	{
	public:
		explicit UnknownPropertyType(const std::string& msg);
	};


	class DifferentPropertyGroup : public ExceptionCRTP<DifferentPropertyGroup>
	{
	public:
		explicit DifferentPropertyGroup(const std::string& msg);
	};


	class InternetError : public ExceptionCRTP<InternetError>
	{
	public:
		explicit InternetError(const std::string& msg);
	};


	class OptionUnusedError : public ExceptionCRTP<OptionUnusedError>
	{
	public:
		explicit OptionUnusedError(const std::string& msg);
	};
}	 // namespace core


#endif	  // EXCEPTION_H_INCLUDED
