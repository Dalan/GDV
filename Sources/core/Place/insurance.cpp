/*
 * insurance.cpp
 *
 * Copyright 2020 Rémi BERTHO <remi.bertho@dalan.fr>
 *
 * This file is part of GDV.
 *
 * GDVis free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * GDV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

#include <fmt/core.h>
#include "internationalization.hpp"

#include "insurance.hpp"

using namespace Glib;
using namespace database;
using namespace std;
namespace place
{
	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Constructor and destructor ////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
	Insurance::Insurance(const int64_t id, const string& name) : Place(id, name), online_(*this, false), web_address_(*this, "")
	{
	}

	Insurance::Insurance(const Insurance& insurance)
			: Place(insurance), online_(insurance.online_, *this), web_address_(insurance.web_address_, *this)
	{
	}



	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Function //////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
	bool Insurance::changed() const
	{
		if (getOld() != nullptr)
		{
			return !(*this == *dynamic_cast<Insurance*>(getOld().get()));
		}

		return false;
	}

	Insurance& Insurance::operator=(const Insurance& insurance)
	{
		Place::operator=(insurance);

		return *this;
	}

	bool Insurance::operator==(const Insurance& insurance) const
	{
		if (Place::operator==(insurance))
		{
			if ((webAddress() == insurance.webAddress()) && (online() == insurance.online()))
			{
				return true;
			}
		}

		return false;
	}

	string Insurance::toString() const
	{
		string res = Object::toString();

		res += fmt::format(fmt::runtime(_("\nOnline: {}\nInternet adresse {}")), online(), webAddress());

		return res;
	}

	string Insurance::userName(gulong number, Determiner determiner)
	{
		switch (determiner)
		{
		case Determiner::NONE:
			return P_("insurance agency", "insurance agency", number);
		case Determiner::A:
			return P_("a insurance agency", "insurance agency", number);
		case Determiner::THE:
			return P_("the insurance agency", "the insurance agency", number);
		case Determiner::YOUR:
			return P_("your insurance agency", "your insurance agency", number);
		case Determiner::OF:
			return P_("of insurance agency", "of insurance agency", number);
		}
		return "";
	}

}	 // namespace place
