/*
 * gas_station.cpp
 *
 * Copyright 2019 Rémi BERTHO <remi.bertho@dalan.fr>
 *
 * This file is part of GDV.
 *
 * GDVis free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * GDV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

#include "internationalization.hpp"
#include <fmt/core.h>

#include "gas_station.hpp"

using namespace Glib;
using namespace database;
using namespace std;

namespace place
{
	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Constructor and destructor ////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
	GasStation::GasStation(const int64_t id, const string& name) : Place(id, name), cheap_(*this, false)
	{
	}

	GasStation::GasStation(const GasStation& gas_station) : Place(gas_station), cheap_(gas_station.cheap_, *this)
	{
	}



	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Function //////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
	bool GasStation::changed() const
	{
		if (getOld() != nullptr)
		{
			return !(*this == *dynamic_cast<GasStation*>(getOld().get()));
		}

		return false;
	}

	GasStation& GasStation::operator=(const GasStation& gas_station)
	{
		Place::operator=(gas_station);

		return *this;
	}

	bool GasStation::operator==(const GasStation& gas_station) const
	{
		if (Place::operator==(gas_station))
		{
			if (cheap() == gas_station.cheap())
			{
				return true;
			}
		}

		return false;
	}

	string GasStation::toString() const
	{
		string res = Object::toString();

		res += fmt::format(fmt::runtime(_("\nCheap: {}")), cheap());

		return res;
	}

	string GasStation::userName(gulong number, Determiner determiner)
	{
		switch (determiner)
		{
		case Determiner::NONE:
			return P_("gas station", "gas stations", number);
		case Determiner::A:
			return P_("a gas station", "gas stations", number);
		case Determiner::THE:
			return P_("the gas station", "the gas stations", number);
		case Determiner::YOUR:
			return P_("your gas station", "your gas stations", number);
		case Determiner::OF:
			return P_("of gas station", "of gas stations", number);
		}
		return "";
	}

}	 // namespace place
