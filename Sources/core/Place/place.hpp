/*!
 * \file
 */

/*
 * place.h
 *
 * Copyright 2019 Rémi BERTHO <remi.bertho@dalan.fr>
 *
 * This file is part of GDV.
 *
 * GDVis free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * GDV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

#ifndef PLACE_H_INCLUDED
#define PLACE_H_INCLUDED

#include "Generic/object.hpp"

namespace place
{
	class Place : public generic::Object
	{
		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// DB infos //////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	private:
		static const constexpr char BRAND_DB_NAME[]	   = "brand";
		static const constexpr char BRAND_DB_VERSION[] = "0.2.1";

		static const constexpr char ADDRESS_DB_NAME[]	 = "address";
		static const constexpr char ADDRESS_DB_VERSION[] = "0.2.1";

		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Attributes ////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	protected:
		database::PropertyProxy<std::string, BRAND_DB_NAME, BRAND_DB_VERSION>	  brand_;
		database::PropertyProxy<std::string, ADDRESS_DB_NAME, ADDRESS_DB_VERSION> address_;

		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Constructor ///////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	public:
		explicit Place(const int64_t id, const std::string& name = "");

	protected:
		explicit Place(const Place& place);


		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Function //////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	public:
		Place& operator=(const Place& place);

		bool operator==(const Place& place) const;

		virtual std::string toString() const;


		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Getter ////////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	public:
		inline std::string brand() const
		{
			return brand_;
		}


		inline std::string address() const
		{
			return address_;
		}

		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Setter ////////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	public:
		inline void setBrand(const std::string& brand)
		{
			brand_ = brand;
		}


		inline void setAddress(const std::string& address)
		{
			address_ = address;
		}
	};
}	 // namespace place

#endif	  // PLACE_H_INCLUDED
