/*
 * garage.cpp
 *
 * Copyright 2020 Rémi BERTHO <remi.bertho@dalan.fr>
 *
 * This file is part of GDV.
 *
 * GDVis free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * GDV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

#include "internationalization.hpp"

#include <fmt/core.h>

#include "garage.hpp"

using namespace Glib;
using namespace database;
using namespace std;

namespace place
{
	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Constructor and destructor ////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
	Garage::Garage(const int64_t id, const string& name) : Place(id, name), web_address_(*this, "")
	{
	}

	Garage::Garage(const Garage& garage) : Place(garage), web_address_(garage.web_address_, *this)
	{
	}



	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Function //////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
	bool Garage::changed() const
	{
		if (getOld() != nullptr)
		{
			return !(*this == *dynamic_cast<Garage*>(getOld().get()));
		}

		return false;
	}

	Garage& Garage::operator=(const Garage& garage)
	{
		Place::operator=(garage);

		return *this;
	}

	bool Garage::operator==(const Garage& garage) const
	{
		if (Place::operator==(garage))
		{
			if (webAddress() == garage.webAddress())
			{
				return true;
			}
		}

		return false;
	}

	string Garage::toString() const
	{
		string res = Object::toString();

		res += fmt::format(fmt::runtime(_("\nInternet adresse {}")), webAddress());

		return res;
	}

	string Garage::userName(gulong number, Determiner determiner)
	{
		switch (determiner)
		{
		case Determiner::NONE:
			return P_("garage", "garages", number);
		case Determiner::A:
			return P_("a garage", "garages", number);
		case Determiner::THE:
			return P_("the garage", "the garages", number);
		case Determiner::YOUR:
			return P_("your garage", "your garages", number);
		case Determiner::OF:
			return P_("of garage", "of garages", number);
		}
		return "";
	}

}	 // namespace place
