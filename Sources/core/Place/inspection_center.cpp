/*
 * inspection_center.cpp
 *
 * Copyright 2020 Rémi BERTHO <remi.bertho@dalan.fr>
 *
 * This file is part of GDV.
 *
 * GDVis free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * GDV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

#include <fmt/core.h>
#include "internationalization.hpp"

#include "inspection_center.hpp"

using namespace Glib;
using namespace database;
using namespace std;

namespace place
{
	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Constructor and destructor ////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
	InspectionCenter::InspectionCenter(const int64_t id, const string& name) : Place(id, name), web_address_(*this, "")
	{
	}

	InspectionCenter::InspectionCenter(const InspectionCenter& inspection_center)
			: Place(inspection_center), web_address_(inspection_center.web_address_, *this)
	{
	}



	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Function //////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
	bool InspectionCenter::changed() const
	{
		if (getOld() != nullptr)
		{
			return !(*this == *dynamic_cast<InspectionCenter*>(getOld().get()));
		}

		return false;
	}

	InspectionCenter& InspectionCenter::operator=(const InspectionCenter& inspection_center)
	{
		Place::operator=(inspection_center);

		return *this;
	}

	bool InspectionCenter::operator==(const InspectionCenter& inspection_center) const
	{
		if (Place::operator==(inspection_center))
		{
			if (webAddress() == inspection_center.webAddress())
			{
				return true;
			}
		}

		return false;
	}

	string InspectionCenter::toString() const
	{
		string res = Object::toString();

		res += fmt::format(fmt::runtime(_("\nInternet adresse {}")), webAddress());

		return res;
	}

	string InspectionCenter::userName(gulong number, Determiner determiner)
	{
		switch (determiner)
		{
		case Determiner::NONE:
			return P_("inspection center", "inspection centers", number);
		case Determiner::A:
			return P_("an inspection center", "inspection centers", number);
		case Determiner::THE:
			return P_("the inspection center", "the inspection centers", number);
		case Determiner::YOUR:
			return P_("your inspection center", "your inspection centers", number);
		case Determiner::OF:
			return P_("of inspection center", "of inspection centers", number);
		}
		return "";
	}

}	 // namespace place
