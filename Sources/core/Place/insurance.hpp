/*!
 * \file
 */

/*
 * insurance.h
 *
 * Copyright 2020 Rémi BERTHO <remi.bertho@dalan.fr>
 *
 * This file is part of GDV.
 *
 * GDVis free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * GDV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

#ifndef INSURANCE_H_INCLUDED
#define INSURANCE_H_INCLUDED

#include "place.hpp"

namespace place
{
	class Insurance;
	typedef std::shared_ptr<Insurance> InsurancePtr;

	class Insurance : public Place
	{

		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Constants /////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	public:
		static const constexpr char DATABASE_TABLE_NAME[] = "InsurancePlace";

		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// DB infos //////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	private:
		static const constexpr char online_db_name[]	= "online";
		static const constexpr char online_db_version[] = "0.2.5";

		static const constexpr char web_address_db_name[]	 = "web_address";
		static const constexpr char web_address_db_version[] = "0.2.5";

		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Attributes ////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	protected:
		database::PropertyProxy<bool, online_db_name, online_db_version>				  online_;
		database::PropertyProxy<std::string, web_address_db_name, web_address_db_version> web_address_;

		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Constructor ///////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	public:
		explicit Insurance(const int64_t id, const std::string& name = "");

	protected:
		explicit Insurance(const Insurance& insurance);


		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Function //////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	protected:
		virtual bool changed() const;


	public:
		Insurance& operator=(const Insurance& insurance);

		bool operator==(const Insurance& insurance) const;

		virtual std::string toString() const override;

		virtual database::ItemPtr clone() const override
		{
			return std::shared_ptr<database::Item>(new Insurance(*this));
		}

		virtual std::string dataBaseTableName() const override
		{
			return DATABASE_TABLE_NAME;
		}

		static std::string userName(gulong number = 1, Determiner determiner = Determiner::NONE);


		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Getter ////////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	public:
		inline bool online() const
		{
			return online_;
		}

		inline std::string webAddress() const
		{
			return web_address_;
		}

		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Setter ////////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	public:
		inline void setOnline(const bool online)
		{
			online_ = online;
		}

		inline void setWebAddress(const std::string& web_address)
		{
			web_address_ = web_address;
		}
	};
}	 // namespace place

#endif	  // INSURANCE_H_INCLUDED
