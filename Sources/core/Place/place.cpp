/*
 * place.cpp
 *
 * Copyright 2019 Rémi BERTHO <remi.bertho@dalan.fr>
 *
 * This file is part of GDV.
 *
 * GDVis free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * GDV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

#include <fmt/core.h>
#include "internationalization.hpp"

#include "place.hpp"

using namespace Glib;
using namespace database;
using namespace std;

namespace place
{
	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Constructor and destructor ////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
	Place::Place(const int64_t id, const string& name) : Object(id, name), brand_(*this, ""), address_(*this, "")
	{
	}

	Place::Place(const Place& place) : Object(place), brand_(place.brand_, *this), address_(place.address_, *this)
	{
	}



	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Function //////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
	Place& Place::operator=(const Place& place)
	{
		if (this == &place)
		{
			return *this;
		}

		Object::operator=(place);

		return *this;
	}

	bool Place::operator==(const Place& place) const
	{
		if (Object::operator==(place))
		{
			if ((brand() == place.brand()) && (address() == place.address()))
			{
				return true;
			}
		}

		return false;
	}

	string Place::toString() const
	{
		string res = Object::toString();

		res += fmt::format(fmt::runtime(_("\nBrand: {}\nAddress: {}")), brand(), address());

		return res;
	}

}	 // namespace place
