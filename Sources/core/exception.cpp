/*!
 * \file    exception.cpp
 * \author  Remi BERTHO
 * \date    16/03/16
 * \version 1.0.0
 */

/*
 * exception.cpp
 *
 * Copyright 2017 Rémi BERTHO <remi.bertho@dalan.fr>
 *
 * This file is part of GDV.
 *
 * GDVis free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * GDV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

#include "exception.hpp"

#include <utility>

using namespace std;

namespace core
{
	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Base exception ////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
	Exception::Exception(string msg) : msg_(std::move(msg))
	{
	}

	const char* Exception::what() const noexcept
	{
		return msg_.c_str();
	}


	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Specialized exception /////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
	NotFound::NotFound(const string& msg) : ExceptionCRTP<NotFound>("Not found: " + msg)
	{
	}


	OutOfRange::OutOfRange(const string& msg) : ExceptionCRTP<OutOfRange>("Out of range: " + msg)
	{
	}


	SQLError::SQLError(const string& msg) : ExceptionCRTP<SQLError>("SQL error: " + msg)
	{
	}


	OperationNotSupported::OperationNotSupported(const string& msg)
			: ExceptionCRTP<OperationNotSupported>("Operation not supported: " + msg)
	{
	}


	WrongDbiType::WrongDbiType(const string& msg) : ExceptionCRTP<WrongDbiType>("Wrong dbi type: " + msg)
	{
	}


	UndoRedoError::UndoRedoError(const string& msg) : ExceptionCRTP<UndoRedoError>("Undo/redo error: " + msg)
	{
	}


	RuntimeError::RuntimeError(const string& msg) : ExceptionCRTP<RuntimeError>("Runtime error: " + msg)
	{
	}


	WrongPropertyType::WrongPropertyType(const string& msg) : ExceptionCRTP<WrongPropertyType>("Wrong database property type: " + msg)
	{
	}


	UnknownPropertyType::UnknownPropertyType(const string& msg)
			: ExceptionCRTP<UnknownPropertyType>("Unknown database property type: " + msg)
	{
	}


	DifferentPropertyGroup::DifferentPropertyGroup(const string& msg)
			: ExceptionCRTP<DifferentPropertyGroup>("Different database property group: " + msg)
	{
	}


	InternetError::InternetError(const string& msg) : ExceptionCRTP<InternetError>("Internet error: " + msg)
	{
	}

	OptionUnusedError::OptionUnusedError(const string& msg) : ExceptionCRTP<OptionUnusedError>("Option unused error: " + msg)
	{
	}

}	 // namespace core
