/*!
 * \file    version.cpp
 * \author  Remi BERTHO
 * \date    05/07/18
 * \version 1.0.0
 */

/*
 * version.cpp
 *
 * Copyright 2018 Rémi BERTHO <remi.bertho@dalan.fr>
 *
 * This file is part of GDV.
 *
 * GDVis free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * GDV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

#include "log.hpp"

#include "internationalization.hpp"
#include <giomm.h>

#include <utility>

#include "version.hpp"
#include "share.hpp"
#include "config.h"

using namespace Glib;
using namespace std;
using namespace AsynchronousExecution;
using namespace Gio;

namespace core
{
	//
	// Constructor
	//
	Version::Version() = default;

	Version::Version(const string& version, Glib::DateTime time) : time_(std::move(time))
	{
		stringstream ss(version);
		char		 tmp_1;
		char		 tmp_2;
		ss >> major_ >> tmp_1 >> minor_ >> tmp_2 >> micro_;
	}

	Version::Version(const uint32_t major,
			const uint32_t			minor,
			const uint32_t			micro,
			Glib::DateTime			time,
			string					git_branch,
			string					git_commit_hash)
			: major_(major), minor_(minor), micro_(micro), time_(std::move(time)), git_branch_(std::move(git_branch)),
			  git_commit_hash_(std::move(git_commit_hash))
	{
	}

	Version Version::GetSoftwareCurrent()
	{
		DateTime	 time;
		int			 year;
		int			 month;
		int			 day;
		int			 hour;
		int			 minute;
		int			 second;
		char		 tmp_1;
		char		 tmp_2;
		stringstream ss_date(GDV_BUILD_DATE);
		ss_date >> month >> tmp_1 >> day >> tmp_2 >> year;
		stringstream ss_time(GDV_BUILD_TIME);
		ss_time >> hour >> tmp_1 >> minute >> tmp_2 >> second;
		time = DateTime::create_local(year, month, day, hour, minute, second);
		return Version(GDV_VERSION_MAJOR, GDV_VERSION_MINOR, GDV_VERSION_MICRO, time, GDV_GIT_BRANCH, GDV_GIT_COMMIT_HASH);
	}


	Version Version::getSoftwareLast()
	{
		char* data = nullptr;

		try
		{
			gsize		 length;
			RefPtr<File> file = File::create_for_uri("https://www.binaries.dalan.fr/GDV/latest_stable/version.txt");
			file->load_contents(data, length);
		}
		catch (Glib::Exception& ex)
		{
			throw InternetError(format("Cannot access to the latest version file on the internet ({}).", ex.what().c_str()));
		}

		return Version(data);
	}

	void Version::getSoftwareLastAsynchronously(const std::function<void(const Version&)>& return_function,
			const std::function<void(const core::Exception&)>&							   exception_function)
	{
		execFunction<Version, core::Exception>(getSoftwareLast, return_function, exception_function);
	}

	string Version::git() const
	{
		return "branch " + git_branch_ + " commit " + git_commit_hash_;
	}

	//
	// Function
	//
	string Version::toString() const
	{
		return format("{}.{}.{}", major(), minor(), micro());
	}


	//
	// operator
	//
	bool Version::operator==(const Version& version) const
	{
		return major() == version.major() && minor() == version.minor() && micro() == version.micro();
	}


	bool Version::operator!=(const Version& version) const
	{
		return major() != version.major() || minor() != version.minor() || micro() != version.micro();
	}


	bool Version::operator>(const Version& version) const
	{
		return (major() > version.major()) || (major() == version.major() && minor() > version.minor()) ||
			   (major() == version.major() && minor() == version.minor() && micro() > version.micro());
	}


	bool Version::operator<(const Version& version) const
	{
		return (major() < version.major()) || (major() == version.major() && minor() < version.minor()) ||
			   (major() == version.major() && minor() == version.minor() && micro() < version.micro());
	}


	bool Version::operator>=(const Version& version) const
	{
		return (*this == version || *this > version);
	}


	bool Version::operator<=(const Version& version) const
	{
		return (*this == version || *this < version);
	}
}	 // namespace core
