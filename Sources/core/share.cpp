/*!
 * \file    share.h
 * \author  Remi BERTHO
 * \date    15/03/16
 * \version 1.0.0
 */

/*
 * share.h
 *
 * Copyright 2017 Rémi BERTHO <remi.bertho@dalan.fr>
 *
 * This file is part of GDV.
 *
 * GDVis free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * GDV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

#include "share.hpp"

#include <iomanip>
#include "internationalization.hpp"
#include <array>
#include <string>
#include <cmath>

using namespace Glib;
using namespace std;

namespace core
{
	///////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////// Fuel ///////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
	string engineToString(EngineType type)
	{
		switch (type)
		{
		case DIESEL:
			return _("Diesel");
		case GAS:
			return _("Gas");
		case ELECTRICITY:
			return _("Electricity");
		case GPL:
			return _("GPL");
		}
		return "";
	}

	EngineType stringToEngine(string str)
	{
		if (str == _("Diesel"))
			return DIESEL;
		if (str == _("Gas"))
			return GAS;
		if (str == _("Electricity"))
			return ELECTRICITY;
		return GPL;
	}

	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Conversion ////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////

	string boolToYesNo(bool b)
	{
		if (b)
		{
			return _("yes");
		}

		return _("no");
	}

	string boolToString(bool b)
	{
		if (b)
		{
			return "yes";
		}

		return "no";
	}

	bool stringToBool(const string& str)
	{
		return str == "yes";
	}

	string i64tostr(const int64_t i)
	{
		string res = to_string(i);

		removeCharacterInString(res, ' ');

		return res;
	}

	string u64tostr(const uint64_t i)
	{
		string res = to_string(i);

		removeCharacterInString(res, ' ');

		return res;
	}

	string intToString(const int i, int width)
	{
		return ustring::format(setw(width), i);
	}

	string uintToString(const unsigned int i, int width)
	{
		return ustring::format(setw(width), i);
	}

	int stringToInt(const string& str)
	{
		lconv* lc = localeconv();
		removeCharacterInString(str, *(lc->thousands_sep));
		return atoi(str.c_str());
	}


	string doubleToString(const double d, const int decimals, int width)
	{
		if (decimals < 0)
		{
			if (width == 0)
			{
				return ustring::format(d);
			}

			return ustring::format(fixed, setw(width), d);
		}


		if (width == 0)
		{
			return ustring::format(fixed, setprecision(decimals), d);
		}

		return ustring::format(fixed, setprecision(decimals), setw(width), d);
	}

	string dtostr(const double d)
	{
#ifdef G_OS_WIN32
		if (d == INFINITY)
			return "inf";
		else
#endif	  // G_OS_WIN32
			return Ascii::dtostr(d);
	}


	double stringToDouble(const string& str)
	{
		string copy_str(str);
		lconv* lc = localeconv();

		// Remove the thousands separator
		removeCharacterInString(copy_str, *(lc->thousands_sep));
		replaceCharacterInString(copy_str, *(lc->decimal_point), '.');

#ifdef _WIN32
		if (copy_str == "inf")
			return INFINITY;
		else
#endif	  // _WIN32

			return Ascii::strtod(copy_str);
	}

	string replaceCharacterInString(const string& str, const char old_character, const char new_character)
	{
		string ret(str);
		if (old_character != new_character)
		{
			string::size_type pos;
			while ((pos = ret.find(old_character)) != string::npos)
			{
				ret.replace(pos, 1, string(1, new_character));
			}
		}
		return ret;
	}

	string& replaceCharacterInString(string& str, const char old_character, const char new_character)
	{
		if (old_character != new_character)
		{
			string::size_type pos;
			while ((pos = str.find(old_character)) != string::npos)
			{
				str.replace(pos, 1, string(1, new_character));
			}
		}
		return str;
	}

	string removeCharacterInString(const string& str, const char character)
	{
		string::size_type pos;
		string			  ret(str);
		while ((pos = ret.find(character)) != string::npos)
		{
			ret.erase(pos, 1);
		}
		return ret;
	}

	string& removeCharacterInString(string& str, const char character)
	{
		string::size_type pos;
		while ((pos = str.find(character)) != string::npos)
		{
			str.erase(pos, 1);
		}
		return str;
	}

}	 // namespace core
