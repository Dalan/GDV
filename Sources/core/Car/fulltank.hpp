/*!
 * \file    fulltank.h
 * \author  Remi BERTHO
 * \date    16/03/16
 * \version 1.0.0
 */

/*
 * fulltank.h
 *
 * Copyright 2017 Rémi BERTHO <remi.bertho@dalan.fr>
 *
 * This file is part of GDV.
 *
 * GDVis free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * GDV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

#ifndef FULLTANK_H_INCLUDED
#define FULLTANK_H_INCLUDED

#include "Generic/element.hpp"
#include "Place/gas_station.hpp"

namespace car
{
	/*! \class FullTank
	 *   \brief This class represent a full tank
	 */
	class FullTank : public generic::Element
	{
		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Constants /////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	public:
		static const constexpr char* DATABASE_TABLE_NAME = "Car_FullTanks";

		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// DB infos //////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	private:
		static const constexpr char DISTANCE_DB_NAME[]	  = "distance";
		static const constexpr char DISTANCE_DB_VERSION[] = "0.1.0";

		static const constexpr char VOLUME_DB_NAME[]	= "volume";
		static const constexpr char VOLUME_DB_VERSION[] = "0.1.0";

		static const constexpr char PRICE_PER_LITER_DB_NAME[]	 = "price_per_liter";
		static const constexpr char PRICE_PER_LITER_DB_VERSION[] = "0.1.0";

		static const constexpr char ADDITIVE_DB_NAME[]	  = "additive";
		static const constexpr char ADDITIVE_DB_VERSION[] = "0.2.0";

		static const constexpr char BIO_DB_NAME[]	 = "bio";
		static const constexpr char BIO_DB_VERSION[] = "0.2.0";

		static const constexpr char STATION_DB_NAME[]	 = "station";
		static const constexpr char STATION_DB_VERSION[] = "0.2.2";

		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Attributes ////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	protected:
		database::PropertyProxy<double, DISTANCE_DB_NAME, DISTANCE_DB_VERSION>								   distance_;
		database::PropertyProxy<double, VOLUME_DB_NAME, VOLUME_DB_VERSION>									   volume_;
		database::PropertyProxy<double, PRICE_PER_LITER_DB_NAME, PRICE_PER_LITER_DB_VERSION>				   price_per_liter_;
		database::PropertyProxy<bool, ADDITIVE_DB_NAME, ADDITIVE_DB_VERSION>								   additive_;
		database::PropertyProxy<unsigned int, BIO_DB_NAME, BIO_DB_VERSION>									   bio_;
		database::LinkProxy<STATION_DB_NAME, STATION_DB_VERSION, place::GasStation::DATABASE_TABLE_NAME, true> station_;

		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Constructor ///////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	public:
		/*!
		 *  \brief Default constructor
		 */
		explicit FullTank(const int64_t id);

		/*!
		 *  \brief Constructor with all components
		 */
		FullTank(const int64_t id, database::Item* car);

	protected:
		/*!
		 *  \brief Copy constructor
		 */
		explicit FullTank(const FullTank& ft);


		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Operator //////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	public:
		/*!
		 *  \brief She operator < which is based on the mean date
		 *  \param ft another FullTank
		 */
		bool operator<(FullTank& ft);

		/*!
		 *  \brief She operator > which is based on the mean date
		 *  \param ft another FullTank
		 */
		bool operator>(FullTank& ft);

		/*!
		 *  \brief operator =
		 *  \param ft a FullTank
		 *  \return a reference to the object
		 */
		FullTank& operator=(const FullTank& ft);

		/**
		 * @brief operator ==
		 * @param ft another FullTank
		 * @return true if it has the same data, false otherwise
		 */
		bool operator==(const FullTank& ft) const;

		Element* createNext(const bool copy_objects = false) const override;



		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Function //////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	public:
		/*!
		 *  \brief Convert to a string
		 *  \return The string
		 */
		virtual std::string toString() const override;

		/**
		 * @brief Clone the exception
		 * @return the clone in a shared_ptr
		 */
		virtual database::ItemPtr clone() const override
		{
			return std::shared_ptr<database::Item>(new FullTank(*this));
		}

		/**
		 * @brief Get the element name
		 * @return the element name
		 */
		static std::string userName(gulong number = 1, Determiner determiner = Determiner::NONE);

		/**
		 * @brief databaseName
		 * @return the name used in database
		 */
		virtual std::string dataBaseTableName() const override
		{
			return DATABASE_TABLE_NAME;
		}

		/**
		 * @brief Get the name of the specific need
		 * @return the name
		 */
		virtual std::string nameId() const override;

		/*!
		 *  \brief Get the total price
		 *  \return The total price
		 */
		virtual inline double price() const override
		{
			return volume() * pricePerLiter();
		}

		/*!
		 *  \brief Get the consumption (L/100km)
		 *  \return The consumption (L/100km)
		 */
		double consumption() const;

		/*!
		 *  \brief Get the price per 100 km
		 *  \return The price per 100 km
		 */
		inline double pricePer100Km() const
		{
			return (consumption() * pricePerLiter());
		}


		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Getter ////////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	public:
		virtual inline double distance() const override
		{
			return distance_;
		}


		inline double volume() const
		{
			return volume_;
		}


		inline double pricePerLiter() const
		{
			return price_per_liter_;
		}


		inline bool additive() const
		{
			return additive_;
		}


		inline unsigned int bio() const
		{
			return bio_;
		}


		inline int64_t stationId() const
		{
			return station_;
		}


		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Setter ////////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	public:
		virtual void setBeginDate(const Glib::Date& date, const LinkType link_type = NONE) override;

		inline void setDistance(const double distance)
		{
			distance_ = distance;
		}

		inline void setVolume(const double volume)
		{
			volume_ = volume;
		}

		inline void setPricePerLiter(const double price_per_liter)
		{
			price_per_liter_ = price_per_liter;
		}

		inline void setAdditive(const bool additive)
		{
			additive_ = additive;
		}

		inline void setBio(const unsigned int bio)
		{
			bio_ = bio;
		}

		inline void setStationId(const int64_t id)
		{
			station_ = id;
		}

		double setPrice(double price) = delete;
	};
}	 // namespace car

#endif	  // FULLTANK_H_INCLUDED
