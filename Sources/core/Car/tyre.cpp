/*!
 * \file    tyre.cpp
 * \author  Remi BERTHO
 * \date    15/03/16
 * \version 1.0.0
 */

/*
 * tyre.cpp
 *
 * Copyright 2017 Rémi BERTHO <remi.bertho@dalan.fr>
 *
 * This file is part of GDV.
 *
 * GDVis free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * GDV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

#include <fmt/core.h>

#include "internationalization.hpp"

#include "tyre.hpp"

#include "car.hpp"

using namespace Glib;
using namespace vehicle;
using namespace std;

namespace car
{

	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Constructor and destructor ////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
	Tyre::Tyre(const int64_t id) : PhysicalNeed(id, make_tuple(nullptr, Car::DATABASE_TABLE_NAME)), position_(*this, Position::BOTTOM_RIGHT)
	{
	}

	Tyre::Tyre(const int64_t id, database::Item* car, Position position)
			: PhysicalNeed(id, make_tuple(car, Car::DATABASE_TABLE_NAME)), position_(*this, position)
	{
	}

	Tyre::Tyre(const Tyre& tyre) : PhysicalNeed(tyre), position_(tyre.position_, *this)
	{
	}



	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Function //////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
	string Tyre::toString() const
	{
		string res = _("Tyre:\n") + PhysicalNeed::toString();

		res += fmt::format(fmt::runtime(_("\nPosition: {}")), static_cast<uint32_t>(position()));

		return res;
	}

	Tyre& Tyre::operator=(const Tyre& t)
	{
		PhysicalNeed::operator=(t);

		return *this;
	}

	bool Tyre::operator==(const Tyre& tyre) const
	{
		if (PhysicalNeed::operator==(tyre))
		{
			if (position() == tyre.position())
			{
				return true;
			}
		}

		return false;
	}

	generic::Element* Tyre::createNext([[maybe_unused]] const bool copy_objects) const
	{
		Date		 end_date;
		unsigned int end_km;

		(end_date = endDate()).add_years(static_cast<int>(delayYear()));
		end_km = endKm() + delayKm();

		auto t = new Tyre(*this);
		t->setOwner(owner());
		t->setBeginDate(endDate());
		t->setEndDate(end_date);
		t->setBeginKm(endKm());
		t->setEndKm(end_km);

		auto car = dynamic_cast<Car*>(owner());
		if (car != nullptr)
		{
			car->elements<Tyre>(position()).add(t);
		}

		return t;
	}

	string Tyre::userName(gulong number, Determiner determiner)
	{
		switch (determiner)
		{
		case Determiner::NONE:
			return P_("tyre", "tyres", number);
		case Determiner::A:
			return P_("a tyre", "tyres", number);
		case Determiner::THE:
			return P_("the tyre", "the tyres", number);
		case Determiner::YOUR:
			return P_("your tyre", "your tyres", number);
		case Determiner::OF:
			return P_("of tyre", "of tyres", number);
		}
		return "";
	}

	string Tyre::nameId() const
	{
		return fmt::format(fmt::runtime(_("Put the {}")), beginDate().format_string("%x").c_str());
	}
}	 // namespace car
