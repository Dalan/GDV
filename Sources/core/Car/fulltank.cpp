/*!
 * \file    fulltank.h
 * \author  Remi BERTHO
 * \date    16/03/16
 * \version 1.0.0
 */

/*
 * fulltank.h
 *
 * Copyright 2017 Rémi BERTHO <remi.bertho@dalan.fr>
 *
 * This file is part of GDV.
 *
 * GDVis free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * GDV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

#include <fmt/core.h>
#include "log.hpp"
#include "internationalization.hpp"
#include "fulltank.hpp"
#include "car.hpp"

using namespace Glib;
using namespace vehicle;
using namespace std;
using namespace place;

namespace car
{
	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Constructor and destructor ////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
	FullTank::FullTank(const int64_t id)
			: Element(id, make_tuple(nullptr, Car::DATABASE_TABLE_NAME)), distance_(*this, 0), volume_(*this, 0),
			  price_per_liter_(*this, 0), additive_(*this, 0), bio_(*this, false), station_(*this, nullptr)
	{
	}

	FullTank::FullTank(const int64_t id, database::Item* car)
			: Element(id, make_tuple(car, Car::DATABASE_TABLE_NAME)), distance_(*this, 0), volume_(*this, 0), price_per_liter_(*this, 0),
			  additive_(*this, 0), bio_(*this, false), station_(*this, nullptr)
	{
	}

	FullTank::FullTank(const FullTank& ft)
			: Element(ft), distance_(ft.distance_, *this), volume_(ft.volume_, *this), price_per_liter_(ft.price_per_liter_, *this),
			  additive_(ft.additive_, *this), bio_(ft.bio_, *this), station_(ft.station_, *this)
	{
	}

	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Operator //////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
	bool FullTank::operator<(FullTank& ft)
	{
		return (meanDate().compare(ft.meanDate()) < 0);
	}

	bool FullTank::operator>(FullTank& ft)
	{
		return (meanDate().compare(ft.meanDate()) > 0);
	}

	FullTank& FullTank::operator=(const FullTank& ft)
	{
		Element::operator=(ft);

		return *this;
	}

	bool FullTank::operator==(const FullTank& ft) const
	{
		if (Element::operator==(ft))
		{
			if ((distance() == ft.distance()) && (volume() == ft.volume()) && (pricePerLiter() == ft.pricePerLiter()) &&
					(additive() == ft.additive()) && (bio() == ft.bio()))
			{
				return true;
			}
		}

		return false;
	}

	generic::Element* FullTank::createNext([[maybe_unused]] const bool copy_objects) const
	{
		Date end_date;

		(end_date = endDate()).add_months(1);

		auto f = new FullTank(*this);
		f->setOwner(owner());
		f->setBeginDate(endDate());
		f->setEndDate(end_date);
		f->setDistance(0);
		f->setVolume(0);

		auto car = dynamic_cast<Car*>(owner());
		if (car != nullptr)
		{
			car->elements<FullTank>().add(f);
		}

		return f;
	}


	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Function //////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
	string FullTank::toString() const
	{
		string res = "Fulltank:\n";
		res += Element::toString();
		res += fmt::format(fmt::runtime(_("Distance: {}km\nVolume: {}\nPrice per liter: {}\nAdditive: {}\nBio: {}")),
				distance(),
				volume(),
				pricePerLiter(),
				additive(),
				bio());

		return res;
	}

	string FullTank::userName(gulong number, Determiner determiner)
	{
		switch (determiner)
		{
		case Determiner::NONE:
			return P_("fulltank", "fulltanks", number);
		case Determiner::A:
			return P_("a fulltank", "fulltanks", number);
		case Determiner::THE:
			return P_("the fulltank", "the fulltanks", number);
		case Determiner::YOUR:
			return P_("your fulltank", "your fulltanks", number);
		case Determiner::OF:
			return P_("of fulltank", "of fulltanks", number);
		}
		return "";
	}

	string FullTank::nameId() const
	{
		return fmt::format(fmt::runtime(_("Done the {}")), beginDate().format_string("%x").c_str());
	}

	double FullTank::consumption() const
	{
		if (previous() == nullptr)
		{
			return 0;
		}
		const auto* next = dynamic_cast<const FullTank*>(previous());
		return (next->volume() / next->distance()) * NB_KM_IN_CONSUMPTION;
	}


	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Setter ////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
	void FullTank::setBeginDate(const Date& date, const LinkType link_type)
	{
		if (beginDate() != date)
		{
			begin_date_ = date;
			emitSignalNeedSort();
			switch (link_type)
			{
			case END:
				if (currentlyUsed())
				{
					Date new_date = beginDate();
					new_date.add_months(1);
					setEndDate(new_date);
				}
				break;

			case ALL:
				if (previous() != nullptr)
				{
					setEndDate(dynamic_cast<FullTank*>(previous())->beginDate());
				}
				else
				{
					Date new_end = begin_date_;
					new_end.add_months(1);
					setEndDate(new_end);
				}

				if (next() != nullptr)
				{
					next()->setEndDate(beginDate());
				}
				break;

			case NONE:
				break;
			}
		}
	}

}	 // namespace car
