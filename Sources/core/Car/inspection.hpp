/*!
 * \file    inspection.h
 * \author  Remi BERTHO
 * \date    15/03/16
 * \version 1.0.0
 */

/*
 * inspection.h
 *
 * Copyright 2017 Rémi BERTHO <remi.bertho@dalan.fr>
 *
 * This file is part of GDV.
 *
 * GDVis free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * GDV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

#ifndef INSPECTION_H_INCLUDED
#define INSPECTION_H_INCLUDED

#include "Vehicle/need.hpp"
#include "Place/inspection_center.hpp"

namespace car
{
	/*! \class Inspection
	 *   \brief This class represent an inspection
	 */
	class Inspection : public vehicle::Need
	{
		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Friendship ////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
		friend class Car;

		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Constants /////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	public:
		static const constexpr char* DATABASE_TABLE_NAME = "Car_Inspections";

		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// DB infos //////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	private:
		static const constexpr char CENTER_DB_NAME[]	= "center";
		static const constexpr char CENTER_DB_VERSION[] = "0.2.8";

		static const constexpr char REPEAT_DATE_DB_NAME[]	 = "repeate_date";
		static const constexpr char REPEAT_DATE_DB_VERSION[] = "0.2.9";

		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Attributes ////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	protected:
		database::LinkProxy<CENTER_DB_NAME, CENTER_DB_VERSION, place::InspectionCenter::DATABASE_TABLE_NAME, true> center_;
		database::PropertyProxy<Glib::Date, REPEAT_DATE_DB_NAME, REPEAT_DATE_DB_VERSION>						   repeat_date_;

		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Constructor ///////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	public:
		/*!
		 *  \brief Default constructor
		 */
		explicit Inspection(const int64_t id);

		/*!
		 *  \brief Constructor with all components
		 */
		Inspection(const int64_t id, database::Item* car);

	protected:
		/*!
		 *  \brief Copy constructor
		 */
		explicit Inspection(const Inspection& i);


		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Function //////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	public:
		/*!
		 *  \brief Convert to a string
		 *  \return The string
		 */
		std::string toString() const override;

		/**
		 * @brief Clone the exception
		 * @return the clone in a shared_ptr
		 */
		database::ItemPtr clone() const override
		{
			return std::shared_ptr<database::Item>(new Inspection(*this));
		}

		/*!
		 *  \brief operator =
		 *  \param i a Inspection
		 *  \return a reference to the object
		 */
		Inspection& operator=(const Inspection& i);

		/**
		 * @brief operator ==
		 * @param ins another Inspection
		 * @return true if it has the same data, false otherwise
		 */
		bool operator==(const Inspection& ins) const;

		Element* createNext(const bool copy_objects = false) const override;

		/**
		 * @brief Get the need name
		 * @return the need name
		 */
		static std::string userName(gulong number = 1, Determiner determiner = Determiner::NONE);

		/**
		 * @brief databaseName
		 * @return the name used in database
		 */
		std::string dataBaseTableName() const override
		{
			return DATABASE_TABLE_NAME;
		}

		/**
		 * @brief Get the name of the specific need
		 * @return the name
		 */
		std::string nameId() const override;


		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Getter ////////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	public:
		inline int64_t centerId() const
		{
			return center_;
		}

		inline Glib::Date repeatDate() const
		{
			return repeat_date_;
		}

		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Setter ////////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	public:
		inline void setCenterId(const int64_t id)
		{
			center_ = id;
		}

		inline void setRepeatDate(const Glib::Date& date)
		{
			repeat_date_ = date;
		}
	};
}	 // namespace car

#endif	  // INSPECTION_H_INCLUDED
