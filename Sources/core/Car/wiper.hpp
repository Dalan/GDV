/*!
 * \file    wiper.h
 * \author  Remi BERTHO
 * \date    15/03/16
 * \version 1.0.0
 */

/*
 * wiper.h
 *
 * Copyright 2017 Rémi BERTHO <remi.bertho@dalan.fr>
 *
 * This file is part of GDV.
 *
 * GDVis free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * GDV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

#ifndef WIPER_H
#define WIPER_H

#include "Vehicle/physicalneed.hpp"

namespace car
{
	/*! \class Wiper
	 *   \brief This class represent a wiper
	 */
	class Wiper : public vehicle::PhysicalNeed
	{
		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Friendship ////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
		friend class Car;

		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Enumeration ///////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	public:
		enum Position
		{
			BOTTOM,
			FRONT
		};

		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Constants /////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	public:
		static const constexpr char* DATABASE_TABLE_NAME = "Car_Wipers";

		static const constexpr int NB_POSITION = 2;

		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// DB infos //////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	private:
		static const constexpr char POSITION_DB_NAME[]	  = "position";
		static const constexpr char POSITION_DB_VERSION[] = "0.1.0";

		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Attributes ////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	protected:
		database::PropertyProxy<Position, POSITION_DB_NAME, POSITION_DB_VERSION> position_;


		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Constructor ///////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	public:
		/*!
		 *  \brief Default constructor
		 */
		explicit Wiper(const int64_t id);

		/*!
		 *  \brief Constructor with all components
		 */
		Wiper(const int64_t id, database::Item* car, const Position position);

	protected:
		/*!
		 *  \brief Copy constructor
		 */
		explicit Wiper(const Wiper& wiper);


		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Function //////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	public:
		/*!
		 *  \brief operator =
		 *  \param wiper a Wiper
		 *  \return a reference to the object
		 */
		Wiper& operator=(const Wiper& wiper);

		/**
		 * @brief operator ==
		 * @param wiper another Wiper
		 * @return true if it has the same data, false otherwise
		 */
		bool operator==(const Wiper& wiper) const;

		Element* createNext(const bool copy_objects = false) const override;

		/**
		 * @brief Clone the exception
		 * @return the clone in a shared_ptr
		 */
		database::ItemPtr clone() const override
		{
			return std::shared_ptr<database::Item>(new Wiper(*this));
		}

		/*!
		 *  \brief Convert to a string
		 *  \return the string
		 */
		std::string toString() const override;

		/**
		 * @brief Get the element name
		 * @return the element name
		 */
		static std::string userName(gulong number = 1, Determiner determiner = Determiner::NONE);

		/**
		 * @brief databaseName
		 * @return the name used in database
		 */
		std::string dataBaseTableName() const override
		{
			return DATABASE_TABLE_NAME;
		}


		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Getter ////////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	public:
		/*!
		 *  \brief Return the position
		 *  \return the position
		 */
		inline Position position() const
		{
			return position_;
		}

		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Setter ////////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	protected:
		/*!
		 *  \brief Set the position
		 *  \param position the position
		 */
		inline void setPosition(const Position position)
		{
			position_ = position;
		}

	public:
		/**
		 * @brief Get the name of the specific need
		 * @return the name
		 */
		std::string nameId() const override;
	};
}	 // namespace car

#endif	  // WIPER_H
