/*!
 * \file    insurance.cpp
 * \author  Remi BERTHO
 * \date    15/03/16
 * \version 1.0.0
 */

/*
 * insurance.cpp
 *
 * Copyright 2017 Rémi BERTHO <remi.bertho@dalan.fr>
 *
 * This file is part of GDV.
 *
 * GDVis free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * GDV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

#include <fmt/core.h>

#include "internationalization.hpp"

#include "insurance.hpp"

#include "car.hpp"

using namespace Glib;
using namespace vehicle;
using namespace std;
using namespace generic;

namespace car
{

	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Guarantee /////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
	Insurance::Guarantee::Guarantee(const int64_t id) : Article(id, make_tuple(nullptr, Insurance::DATABASE_TABLE_NAME))
	{
	}

	Insurance::Guarantee::Guarantee(const int64_t id, database::Item* insurance)
			: Article(id, make_tuple(insurance, Insurance::DATABASE_TABLE_NAME))
	{
	}

	Insurance::Guarantee::Guarantee(const Guarantee& ig) : Article(ig)
	{
	}


	Insurance::Guarantee& Insurance::Guarantee::operator=(const Guarantee& ig)
	{
		if (this == &ig)
		{
			return *this;
		}

		Item::operator=(ig);

		return *this;
	}

	bool Insurance::Guarantee::operator==(const Guarantee& ig) const
	{
		return Article::operator==(ig);
	}

	string Insurance::Guarantee::toString() const
	{
		string res = Article::toString();

		return res;
	}

	string Insurance::Guarantee::userName(gulong number, Determiner determiner)
	{
		switch (determiner)
		{
		case Determiner::NONE:
			return P_("guarantee", "guarantees", number);
		case Determiner::A:
			return P_("a guarantee", "guarantees", number);
		case Determiner::THE:
			return P_("the guarantee", "the guarantees", number);
		case Determiner::YOUR:
			return P_("your guarantee", "your guarantees", number);
		case Determiner::OF:
			return P_("of guarantee", "of guarantees", number);
		}
		return "";
	}




	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Constructor and destructor ////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
	Insurance::Insurance(const int64_t id)
			: Need(id, make_tuple(nullptr, Car::DATABASE_TABLE_NAME)), bonus_(*this, DEFAULT_BONUS), emergency_phone_(*this, ""),
			  agency_(*this, nullptr)
	{
		objects<Guarantee>() = ObjectArray();
	}

	Insurance::Insurance(const int64_t id, database::Item* car)
			: Need(id, make_tuple(car, Car::DATABASE_TABLE_NAME)), bonus_(*this, DEFAULT_BONUS), emergency_phone_(*this, ""),
			  agency_(*this, nullptr)
	{
		objects<Guarantee>() = ObjectArray();
	}

	Insurance::Insurance(const Insurance& i, const bool copy_objects)
			: Need(i), bonus_(i.bonus_, *this), emergency_phone_(i.emergency_phone_, *this), agency_(i.agency_, *this)
	{
		objects<Guarantee>() = ObjectArray();

		if (copy_objects)
		{
			for (auto& g : i.objects<Guarantee>())
			{
				Guarantee& new_g = addObject<Guarantee>();
				new_g			 = dynamic_cast<Guarantee&>(*g);
			}
		}
	}



	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Function //////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
	string Insurance::toString() const
	{
		string res = _("Insurance:\n") + Need::toString();

		res += fmt::format(fmt::runtime(_("\nBonus: {}\nEmergency phone: {}\nAgency: {}")), bonus(), emergencyPhone(), agencyId());

		return res;
	}

	Insurance& Insurance::operator=(const Insurance& i)
	{
		Need::operator=(i);

		return *this;
	}

	bool Insurance::operator==(const Insurance& ins) const
	{
		if (Need::operator==(ins))
		{
			if ((bonus() == ins.bonus()) && (emergencyPhone() == ins.emergencyPhone()) && (agencyId() == ins.agencyId()))
			{
				return true;
			}
		}

		return false;
	}

	Element* Insurance::createNext(const bool copy_objects) const
	{
		Date		 end_date;
		unsigned int end_km;
		ObjectArray	 new_guarantees;

		(end_date = endDate()).add_years(static_cast<int>(delayYear()));
		end_km = endKm() + delayKm();

		auto i = new Insurance(*this, false);
		i->setOwner(owner());
		i->setBeginDate(endDate());
		i->setEndDate(end_date);
		i->setBeginKm(endKm());
		i->setEndKm(end_km);
		i->setBonus((bonus() * 95) / 100);

		auto car = dynamic_cast<Car*>(owner());
		if (car != nullptr)
		{
			car->elements<Insurance>().add(i);
		}

		if (copy_objects)
		{
			for (const OwnedObject* object : objects<Guarantee>())
			{
				Insurance::Guarantee& new_guarantee = i->addObject<Guarantee>();
				auto				  guarantee		= static_cast<const Insurance::Guarantee*>(object);
				new_guarantee.setName(guarantee->name());
				new_guarantee.setPrice(guarantee->price());
			}
		}

		return i;
	}

	string Insurance::userName(gulong number, Determiner determiner)
	{
		switch (determiner)
		{
		case Determiner::NONE:
			return P_("insurance", "insurances", number);
		case Determiner::A:
			return P_("a insurance", "insurances", number);
		case Determiner::THE:
			return P_("the insurance", "the insurances", number);
		case Determiner::YOUR:
			return P_("your insurance", "your insurances", number);
		case Determiner::OF:
			return P_("of insurance", "of insurances", number);
		}
		return "";
	}

	string Insurance::nameId() const
	{
		return fmt::format(
				fmt::runtime(_("From the {} to the {}")), beginDate().format_string("%x").c_str(), endDate().format_string("%x").c_str());
	}

}	 // namespace car
