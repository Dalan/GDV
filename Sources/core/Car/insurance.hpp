/*!
 * \file    insurance.h
 * \author  Remi BERTHO
 * \date    15/03/16
 * \version 1.0.0
 */

/*
 * insurance.h
 *
 * Copyright 2017 Rémi BERTHO <remi.bertho@dalan.fr>
 *
 * This file is part of GDV.
 *
 * GDVis free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * GDV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

#ifndef INSURRANCE_H_INCLUDED
#define INSURRANCE_H_INCLUDED

#include "Vehicle/need.hpp"
#include "Place/insurance.hpp"

namespace car
{
	/*! \class Insurance
	 *   \brief This class represent an insurance
	 */
	class Insurance : public vehicle::Need
	{
		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Internal class ////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	public:
		class Guarantee : public generic::Article
		{
			///////////////////////////////////////////////////////////////////////////////////
			/////////////////////////// Constants /////////////////////////////////////////////
			///////////////////////////////////////////////////////////////////////////////////
		public:
			static const constexpr char* DATABASE_TABLE_NAME = "Car_Insurance_Guarantees";

			///////////////////////////////////////////////////////////////////////////////////
			/////////////////////////// Constructor ///////////////////////////////////////////
			///////////////////////////////////////////////////////////////////////////////////
		public:
			/*!
			 *  \brief Default constructor
			 */
			explicit Guarantee(const int64_t id);

			/*!
			 *  \brief Constructor with all components
			 */
			Guarantee(const int64_t id, database::Item* insurance);

			~Guarantee() = default;

		protected:
			/*!
			 *  \brief Copy constructor
			 */
			explicit Guarantee(const Guarantee& ig);


			///////////////////////////////////////////////////////////////////////////////////
			/////////////////////////// Function //////////////////////////////////////////////
			///////////////////////////////////////////////////////////////////////////////////
		public:
			/*!
			 *  \brief operator =
			 *  \param pn a InsuranceGuarantee
			 *  \return a reference to the object
			 */
			Guarantee& operator=(const Guarantee& ig);

			/**
			 * @brief operator ==
			 * @param pn another InsuranceGuarantee
			 * @return true if it has the same data, false otherwise
			 */
			bool operator==(const Guarantee& ig) const;

			/*!
			 *  \brief Convert to a std::string
			 *  \return the std::string
			 */
			virtual std::string toString() const;

			/**
			 * @brief Clone the exception
			 * @return the clone in a shared_ptr
			 */
			virtual database::ItemPtr clone() const
			{
				return std::shared_ptr<database::Item>(new Guarantee(*this));
			}

			/**
			 * @brief Get the name for an user
			 * @return the name
			 */
			static std::string userName(gulong number = 1, Determiner determiner = Determiner::NONE);

			/**
			 * @brief databaseName
			 * @return the name used in database
			 */
			virtual std::string dataBaseTableName() const
			{
				return DATABASE_TABLE_NAME;
			}
		};




		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Friendship ////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
		friend class Car;

		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Constants /////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	public:
		static const constexpr char* DATABASE_TABLE_NAME = "Car_Insurances";

		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// DB infos //////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	private:
		static const constexpr char			BONUS_DB_NAME[]	   = "bonus";
		static const constexpr char			BONUS_DB_VERSION[] = "0.2.5";
		static const constexpr unsigned int DEFAULT_BONUS	   = 100;

		static const constexpr char AGENCY_DB_NAME[]	= "agency";
		static const constexpr char AGENCY_DB_VERSION[] = "0.2.6";

		static const constexpr char EMERGENCY_PHONE_DB_NAME[]	 = "emergency_phone";
		static const constexpr char EMERGENCY_PHONE_DB_VERSION[] = "0.2.6";

		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Attributes ////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	protected:
		database::PropertyProxy<unsigned int, BONUS_DB_NAME, BONUS_DB_VERSION>								bonus_;
		database::PropertyProxy<std::string, EMERGENCY_PHONE_DB_NAME, EMERGENCY_PHONE_DB_VERSION>			emergency_phone_;
		database::LinkProxy<AGENCY_DB_NAME, AGENCY_DB_VERSION, place::Insurance::DATABASE_TABLE_NAME, true> agency_;

		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Constructor ///////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	public:
		/*!
		 *  \brief Default constructor
		 */
		explicit Insurance(const int64_t id);

		/*!
		 *  \brief Constructor with the car
		 */
		Insurance(const int64_t id, database::Item* car);

	protected:
		/*!
		 *  \brief Copy constructor
		 */
		explicit Insurance(const Insurance& i, const bool copy_objects = true);


		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Function //////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	public:
		/*!
		 *  \brief Convert to a std::string
		 *  \return the std::string
		 */
		std::string toString() const override;

		/*!
		 *  \brief operator =
		 *  \param i a Insurance
		 *  \return a reference to the object
		 */
		Insurance& operator=(const Insurance& i);

		/**
		 * @brief operator ==
		 * @param ins another Insurance
		 * @return true if it has the same data, false otherwise
		 */
		bool operator==(const Insurance& ins) const;

		Element* createNext(const bool copy_objects) const override;

		/**
		 * @brief Clone the exception
		 * @return the clone in a shared_ptr
		 */
		database::ItemPtr clone() const override
		{
			return std::shared_ptr<database::Item>(new Insurance(*this));
		}

		/**
		 * @brief Get the need name
		 * @return the need name
		 */
		static std::string userName(gulong number = 1, Determiner determiner = Determiner::NONE);

		/**
		 * @brief databaseName
		 * @return the name used in database
		 */
		std::string dataBaseTableName() const override
		{
			return DATABASE_TABLE_NAME;
		}

		/**
		 * @brief Get the name of the specific need
		 * @return the name
		 */
		std::string nameId() const override;


		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Getter ////////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	public:
		inline unsigned int bonus() const
		{
			return bonus_;
		}


		inline int64_t agencyId() const
		{
			return agency_;
		}

		inline std::string emergencyPhone() const
		{
			return emergency_phone_;
		}

		inline double priceWithoutBonus() const
		{
			return (price() * 100) / bonus();
		}

		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Setter ////////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	public:
		inline void setBonus(const unsigned int bonus)
		{
			bonus_ = bonus;
		}

		inline void setEmergencyPhone(const std::string& emergency_phone)
		{
			emergency_phone_ = emergency_phone;
		}

		inline void setAgencyId(const int64_t id)
		{
			agency_ = id;
		}
	};
}	 // namespace car

#endif	  // INSURRANCE_H_INCLUDED
