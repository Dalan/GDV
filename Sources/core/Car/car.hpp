/*!
 * \file    car.h
 * \author  Remi BERTHO
 * \date    16/03/16
 * \version 1.0.0
 */

/*
 * car.h
 *
 * Copyright 2017 Rémi BERTHO <remi.bertho@dalan.fr>
 *
 * This file is part of GDV.
 *
 * GDVis free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * GDV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

#ifndef CAR_H_INCLUDED
#define CAR_H_INCLUDED

#include "share.hpp"
#include "Vehicle/vehicle.hpp"

namespace car
{
	class Car;

	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Typedef ///////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
	/** Car smart pointer */
	typedef std::shared_ptr<Car> CarPtr;


	/*! \class Car
	 *   \brief This class represent a car
	 */
	class Car : public vehicle::Vehicle, public std::enable_shared_from_this<Car>
	{
		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Constants /////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	public:
		static const constexpr char* DATABASE_TABLE_NAME		= "Cars";
		static const constexpr char* DATABASE_ITEM_TABLE_PREFIX = "Car";

		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// DB infos //////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	private:
		static const constexpr char PLATE_DB_NAME[]	   = "plate";
		static const constexpr char PLATE_DB_VERSION[] = "0.1.0";


		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Attributes ////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	protected:
		database::PropertyProxy<std::string, PLATE_DB_NAME, PLATE_DB_VERSION> plate_;



		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Constructor ///////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	public:
		Car(const int64_t id, const std::string& name = "");


		/*!
		 *  \brief Destructor
		 */
		virtual ~Car();

	protected:
		/*!
		 *  \brief Copy constructor
		 */
		explicit Car(const Car& car);

		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Function //////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	public:
		/*!
		 *  \brief operator =
		 *  \param car a Car
		 *  \return a reference to the object
		 */
		Car& operator=(const Car& car);

		/**
		 * @brief operator ==
		 * @param car another Car
		 * @return true if it has the same data, false otherwise
		 */
		bool operator==(const Car& car) const;

		/**
		 * @brief Clone the exception
		 * @return the clone in a shared_ptr
		 */
		virtual database::ItemPtr clone() const
		{
			return std::shared_ptr<database::Item>(new Car(*this));
		}

		/**
		 * @brief databaseName
		 * @return the name used in database
		 */
		virtual std::string dataBaseTableName() const
		{
			return DATABASE_TABLE_NAME;
		}

		/**
		 * @brief Get the element name
		 * @return the element name
		 */
		static std::string userName(gulong number = 1, Determiner determiner = Determiner::NONE);


		/*!
		 *  \brief Calculate the average price of full tank per month
		 *  \return The average price of full tank per month
		 */
		double fullTankPricePerMonth() const;


		/*!
		 *  \brief Calculate the estimate consumption (L/100km)
		 *  \return The estimate consumption (L/100km)
		 */
		double consumption() const;


		/*!
		 *  \brief Calculate the average price of full tank per 100 km
		 *  \return The average price of full tank per 100 km
		 */
		double fullTankPricePer100Km() const;


		/*!
		 *  \brief Return the total volume of fuel
		 *  \return The total volume of fuel
		 */
		double fullTanksTotalVolume() const;


		/*!
		 *  \brief Return the mean volume per full-tanks
		 *  \return The  mean volume per full-tanks
		 */
		double fullTanksMeanVolume() const;


		/*!
		 *  \brief Return the mean price per liter per full-tanks
		 *  \return The  mean price per liter per full-tanks
		 */
		double fullTanksMeanPricePerLiter() const;


		/*!
		 *  \brief Return the mean consumption per month
		 *  \return The  mean consumption per month
		 */
		double fullTanksMeanConsumptionPerMonth() const;


		/*!
		 *  \brief Return the mean consumption per month
		 *  \return The  mean consumption per month
		 */
		double fullTanksMeanConsumption() const;


		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Getter ////////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	public:
		/*!
		 *  \brief Return the plate
		 *  \return The plate
		 */
		inline std::string plate() const
		{
			return plate_;
		}


		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Setter ////////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	public:
		/*!
		 *  \brief Set the plate
		 *  \param plate the plate
		 */
		inline void setPlate(const std::string& plate)
		{
			plate_ = plate;
		}
	};
}	 // namespace car

#endif	  // CAR_H_INCLUDED
