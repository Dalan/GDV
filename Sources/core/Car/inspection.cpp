/*!
 * \file    inspection.cpp
 * \author  Remi BERTHO
 * \date    15/03/16
 * \version 1.0.0
 */

/*
 * inspection.cpp
 *
 * Copyright 2017 Rémi BERTHO <remi.bertho@dalan.fr>
 *
 * This file is part of GDV.
 *
 * GDVis free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * GDV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

#include <fmt/core.h>
#include "internationalization.hpp"

#include "inspection.hpp"

#include "car.hpp"

using namespace Glib;
using namespace vehicle;
using namespace std;

namespace car
{
	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Constructor and destructor ////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
	Inspection::Inspection(const int64_t id)
			: Need(id, make_tuple(nullptr, Car::DATABASE_TABLE_NAME)), center_(*this, nullptr), repeat_date_(*this, Date(1))
	{
	}

	Inspection::Inspection(const int64_t id, database::Item* car)
			: Need(id, make_tuple(car, Car::DATABASE_TABLE_NAME)), center_(*this, nullptr), repeat_date_(*this, Date(1))
	{
	}

	Inspection::Inspection(const Inspection& i) : Need(i), center_(i.center_, *this), repeat_date_(i.repeat_date_, *this)
	{
	}


	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Function //////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
	string Inspection::toString() const
	{
		string res = _("Inspection:\n") + Need::toString();

		res += fmt::format(fmt::runtime(_("\nCenter: {}\nRepeat date: {}")), centerId(), repeatDate().format_string("%x").c_str());

		return res;
	}


	Inspection& Inspection::operator=(const Inspection& i)
	{
		Need::operator=(i);

		return *this;
	}

	bool Inspection::operator==(const Inspection& ins) const
	{
		if (Need::operator==(ins))
		{
			if ((centerId() == ins.centerId()) && (repeatDate() == ins.repeatDate()))
			{
				return true;
			}
		}

		return false;
	}

	generic::Element* Inspection::createNext([[maybe_unused]] const bool copy_objects) const
	{
		Date		 end_date;
		unsigned int end_km;

		(end_date = endDate()).add_years(static_cast<int>(delayYear()));
		end_km = endKm() + delayKm();

		auto i = new Inspection(*this);
		i->setOwner(owner());
		i->setBeginDate(endDate());
		i->setEndDate(end_date);
		i->setBeginKm(endKm());
		i->setEndKm(end_km);
		i->setRepeatDate(Date(1));

		auto car = dynamic_cast<Car*>(owner());
		if (car != nullptr)
		{
			car->elements<Inspection>().add(i);
		}

		return i;
	}

	string Inspection::userName(gulong number, Determiner determiner)
	{
		switch (determiner)
		{
		case Determiner::NONE:
			return P_("inspection", "inspections", number);
		case Determiner::A:
			return P_("a inspection", "inspections", number);
		case Determiner::THE:
			return P_("the inspection", "the inspections", number);
		case Determiner::YOUR:
			return P_("your inspection", "your inspections", number);
		case Determiner::OF:
			return P_("of inspection", "of inspections", number);
		}
		return "";
	}

	string Inspection::nameId() const
	{
		return fmt::format(fmt::runtime(("Done the {}")), beginDate().format_string("%x").c_str());
	}
}	 // namespace car
