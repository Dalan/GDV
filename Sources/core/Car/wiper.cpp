/*!
 * \file    wiper.cpp
 * \author  Remi BERTHO
 * \date    15/03/16
 * \version 1.0.0
 */

/*
 * wiper.cpp
 *
 * Copyright 2017 Rémi BERTHO <remi.bertho@dalan.fr>
 *
 * This file is part of GDV.
 *
 * GDVis free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * GDV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

#include <fmt/core.h>

#include "internationalization.hpp"

#include "wiper.hpp"

#include "car.hpp"

using namespace Glib;
using namespace vehicle;
using namespace std;

namespace car
{

	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Constructor and destructor ////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
	Wiper::Wiper(const int64_t id) : PhysicalNeed(id, make_tuple(nullptr, Car::DATABASE_TABLE_NAME)), position_(*this, Position::BOTTOM)
	{
	}

	Wiper::Wiper(const int64_t id, database::Item* car, const Position position)
			: PhysicalNeed(id, make_tuple(car, Car::DATABASE_TABLE_NAME)), position_(*this, position)
	{
	}

	Wiper::Wiper(const Wiper& wiper) : PhysicalNeed(wiper), position_(wiper.position_, *this)
	{
	}



	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Function //////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
	Wiper& Wiper::operator=(const Wiper& wiper)
	{
		PhysicalNeed::operator=(wiper);

		return *this;
	}

	bool Wiper::operator==(const Wiper& wiper) const
	{
		if (PhysicalNeed::operator==(wiper))
		{
			if (position() == wiper.position())
			{
				return true;
			}
		}

		return false;
	}

	generic::Element* Wiper::createNext([[maybe_unused]] const bool copy_objects) const
	{
		Date		 end_date;
		unsigned int end_km;

		(end_date = endDate()).add_years(static_cast<int>(delayYear()));
		end_km = endKm() + delayKm();

		auto w = new Wiper(*this);
		w->setOwner(owner());
		w->setBeginDate(endDate());
		w->setEndDate(end_date);
		w->setBeginKm(endKm());
		w->setEndKm(end_km);

		auto car = dynamic_cast<Car*>(owner());
		if (car != nullptr)
		{
			car->elements<Wiper>(position()).add(w);
		}

		return w;
	}

	string Wiper::toString() const
	{
		string res = _("Wiper:\n") + PhysicalNeed::toString();

		res += fmt::format(fmt::runtime(_("\nPosition: {}")), static_cast<uint32_t>(position()));

		return res;
	}

	string Wiper::userName(gulong number, Determiner determiner)
	{
		switch (determiner)
		{
		case Determiner::NONE:
			return P_("wiper", "wipers", number);
		case Determiner::A:
			return P_("a wiper", "wipers", number);
		case Determiner::THE:
			return P_("the wiper", "the wipers", number);
		case Determiner::YOUR:
			return P_("your wiper", "your wipers", number);
		case Determiner::OF:
			return P_("of wiper", "of wipers", number);
		}
		return "";
	}

	string Wiper::nameId() const
	{
		return fmt::format(fmt::runtime(_("Put the {}")), beginDate().format_string("%x").c_str());
	}
}	 // namespace car
