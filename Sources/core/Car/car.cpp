/*!
 * \file    car.cpp
 * \author  Remi BERTHO
 * \date    16/03/16
 * \version 1.0.0
 */

/*
 * car.cpp
 *
 * Copyright 2017 Rémi BERTHO <remi.bertho@dalan.fr>
 *
 * This file is part of GDV.
 *
 * GDVis free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * GDV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

#include "log.hpp"

#include <fmt/core.h>
#include <numeric>
#include "internationalization.hpp"
#include <iostream>

#include "car.hpp"
#include "wiper.hpp"
#include "tyre.hpp"
#include "service.hpp"
#include "inspection.hpp"
#include "insurance.hpp"
#include "fulltank.hpp"
#include "exception.hpp"

using namespace Glib;
using namespace std;
using namespace vehicle;
using namespace generic;

namespace car
{
	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Constructor and destructor ////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
	Car::Car(const int64_t id, const string& name) : Vehicle(id, name), plate_(*this, "")
	{
		elements<FullTank>()			   = ElementArray();
		elements<Service>()				   = ElementArray();
		elements<Inspection>()			   = ElementArray();
		elements<Insurance>()			   = ElementArray();
		elements<Wiper>(Wiper::BOTTOM)	   = ElementArray();
		elements<Wiper>(Wiper::FRONT)	   = ElementArray();
		elements<Tyre>(Tyre::BOTTOM_LEFT)  = ElementArray();
		elements<Tyre>(Tyre::BOTTOM_RIGHT) = ElementArray();
		elements<Tyre>(Tyre::FRONT_LEFT)   = ElementArray();
		elements<Tyre>(Tyre::FRONT_RIGHT)  = ElementArray();
	}


	Car::~Car() = default;

	Car::Car(const Car& car) : Vehicle(car), plate_(car.plate_, *this)

	{
		elements<FullTank>()			   = ElementArray();
		elements<Service>()				   = ElementArray();
		elements<Inspection>()			   = ElementArray();
		elements<Insurance>()			   = ElementArray();
		elements<Wiper>(Wiper::BOTTOM)	   = ElementArray();
		elements<Wiper>(Wiper::FRONT)	   = ElementArray();
		elements<Tyre>(Tyre::BOTTOM_LEFT)  = ElementArray();
		elements<Tyre>(Tyre::BOTTOM_RIGHT) = ElementArray();
		elements<Tyre>(Tyre::FRONT_LEFT)   = ElementArray();
		elements<Tyre>(Tyre::FRONT_RIGHT)  = ElementArray();

		for (auto& s : car.elements<Service>())
		{
			Service& new_s = addElement<Service>();
			new_s		   = dynamic_cast<Service&>(s);
		}
		for (auto& i : car.elements<Inspection>())
		{
			Inspection& new_i = addElement<Inspection>();
			new_i			  = dynamic_cast<Inspection&>(i);
		}
		for (auto& i : car.elements<Insurance>())
		{
			Insurance& new_i = addElement<Insurance>();
			new_i			 = dynamic_cast<Insurance&>(i);
		}

		for (int pos = Tyre::BOTTOM_LEFT; pos < Tyre::FRONT_LEFT; pos++)
		{
			auto tyre_pos = static_cast<Tyre::Position>(pos);
			for (auto& t : car.elements<Tyre>(tyre_pos))
			{
				Tyre& new_t = addElement<Tyre>(tyre_pos);
				new_t		= dynamic_cast<Tyre&>(t);
			}
		}
		for (int pos = Wiper::BOTTOM; pos < Wiper::FRONT; pos++)
		{
			auto wiper_pos = static_cast<Wiper::Position>(pos);
			for (auto& w : car.elements<Wiper>(wiper_pos))
			{
				Wiper& new_w = addElement<Wiper>(wiper_pos);
				new_w		 = dynamic_cast<Wiper&>(w);
			}
		}
	}


	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Function //////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
	Car& Car::operator=(const Car& car)
	{
		if (this == &car)
		{
			return *this;
		}

		Item::operator=(car);

		return *this;
	}

	bool Car::operator==(const Car& car) const
	{
		if (Vehicle::operator==(car))
		{
			if (plate() == car.plate())
			{
				return true;
			}
		}

		return false;
	}


	double Car::fullTankPricePerMonth() const
	{
		if (nbElements<FullTank>() == 0)
		{
			return 0;
		}

		double		 total_price = 0;
		unsigned int nb_day		 = 0;

		for (auto& ft : elements<FullTank>())
		{
			if (!ft.currentlyUsed())
			{
				total_price += ft.price();
				nb_day += ft.duration();
			}
		}

		double nb_month = static_cast<double>(nb_day - 1) / MEAN_DAY_PER_MONTH;

		return total_price / nb_month;
	}


	double Car::consumption() const
	{
		if (nbElements<FullTank>() == 0)
		{
			return 0;
		}

		unsigned int distance = 0;
		double		 volume	  = 0;

		for (auto& ci : elements<FullTank>())
		{
			auto& ft = dynamic_cast<FullTank&>(ci);
			distance += static_cast<unsigned int>(ft.distance());
			volume += ft.volume();
		}

		return (volume / distance) * NB_KM_IN_CONSUMPTION;
	}


	double Car::fullTankPricePer100Km() const
	{
		if (nbElements<FullTank>() == 0)
		{
			return 0;
		}

		unsigned int distance = 0;
		double		 price	  = 0;

		for (auto& ft : elements<FullTank>())
		{
			distance += static_cast<unsigned int>(ft.distance());
			price += ft.price();
		}

		return (price / distance) * NB_KM_IN_CONSUMPTION;
	}

	double Car::fullTanksTotalVolume() const
	{
		return accumulate(elements<FullTank>().begin(),
				elements<FullTank>().end(),
				0.,
				[](double a, Element& b) { return a + dynamic_cast<FullTank&>(b).volume(); });
	}


	double Car::fullTanksMeanVolume() const
	{
		if (nbElements<FullTank>() == 0)
		{
			return 0;
		}

		return fullTanksTotalVolume() / nbElements<FullTank>();
	}


	double Car::fullTanksMeanPricePerLiter() const
	{
		if (nbElements<FullTank>() == 0)
		{
			return 0;
		}

		double total = accumulate(elements<FullTank>().begin(),
				elements<FullTank>().end(),
				0.,
				[](double a, Element& b) { return a + dynamic_cast<FullTank&>(b).pricePerLiter(); });
		return total / nbElements<FullTank>();
	}


	double Car::fullTanksMeanConsumptionPerMonth() const
	{
		if (nbElements<FullTank>() == 0)
		{
			return 0;
		}

		unsigned int duration = 0;
		double		 volume	  = 0;

		for (auto& ci : elements<FullTank>())
		{
			auto& ft = dynamic_cast<FullTank&>(ci);
			if (!ft.currentlyUsed())
			{
				duration += ft.duration();
				volume += ft.volume();
			}
		}

		return (volume / duration) * (NB_DAY_PER_YEAR / NB_MONTH_PER_YEAR);
	}

	double Car::fullTanksMeanConsumption() const
	{
		if (nbElements<FullTank>() == 0)
		{
			return 0;
		}

		double distance = 0;
		double volume	= 0;

		for (auto& ci : elements<FullTank>())
		{
			auto& ft = dynamic_cast<FullTank&>(ci);
			distance += ft.distance();
			volume += ft.volume();
		}

		return (volume / distance) * NB_KM_IN_CONSUMPTION;
	}


	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Getter and Setter /////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
	string Car::userName(gulong number, Determiner determiner)
	{
		switch (determiner)
		{
		case Determiner::NONE:
			return P_("car", "cars", number);
		case Determiner::A:
			return P_("a car", "cars", number);
		case Determiner::THE:
			return P_("the car", "the cars", number);
		case Determiner::YOUR:
			return P_("your car", "your cars", number);
		case Determiner::OF:
			return P_("of car", "of cars", number);
		}
		return "";
	}

}	 // namespace car
