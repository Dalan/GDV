/*!
 * \file    service.h
 * \author  Remi BERTHO
 * \date    15/03/16
 * \version 1.0.0
 */

/*
 * service.h
 *
 * Copyright 2017 Rémi BERTHO <remi.bertho@dalan.fr>
 *
 * This file is part of GDV.
 *
 * GDVis free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * GDV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

#include <fmt/core.h>

#include "internationalization.hpp"

#include "service.hpp"

#include "car.hpp"

using namespace Glib;
using namespace vehicle;
using namespace std;
using namespace generic;

namespace car
{

	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Operation /////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
	Service::Operation::Operation(const int64_t id) : Article(id, make_tuple(nullptr, Service::DATABASE_TABLE_NAME))
	{
	}

	Service::Operation::Operation(const int64_t id, database::Item* operation)
			: Article(id, make_tuple(operation, Service::DATABASE_TABLE_NAME))
	{
	}

	Service::Operation::Operation(const Operation& op) : Article(op)
	{
	}


	Service::Operation& Service::Operation::operator=(const Operation& op)
	{
		if (this == &op)
		{
			return *this;
		}

		Item::operator=(op);

		return *this;
	}

	bool Service::Operation::operator==(const Operation& op) const
	{
		return Article::operator==(op);
	}

	string Service::Operation::toString() const
	{
		std::string res = Article::toString();

		return res;
	}

	std::string Service::Operation::userName(gulong number, Determiner determiner)
	{
		switch (determiner)
		{
		case Determiner::NONE:
			return P_("operation", "operations", number);
		case Determiner::A:
			return P_("an operation", "operations", number);
		case Determiner::THE:
			return P_("the operation", "the operations", number);
		case Determiner::YOUR:
			return P_("your operation", "your operations", number);
		case Determiner::OF:
			return P_("of operation", "of operations", number);
		}
		return "";
	}



	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Constructor and destructor ////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
	Service::Service(const int64_t id) : Need(id, make_tuple(nullptr, Car::DATABASE_TABLE_NAME)), garage_(*this, nullptr)
	{
		objects<Operation>() = ObjectArray();
	}

	Service::Service(const int64_t id, database::Item* car) : Need(id, make_tuple(car, Car::DATABASE_TABLE_NAME)), garage_(*this, nullptr)
	{
		objects<Operation>() = ObjectArray();
	}

	Service::Service(const Service& s, const bool copy_objects) : Need(s), garage_(s.garage_, *this)
	{
		objects<Operation>() = ObjectArray();

		if (copy_objects)
		{
			for (auto& o : s.objects<Operation>())
			{
				Operation& new_o = addObject<Operation>();
				new_o			 = dynamic_cast<Operation&>(*o);
			}
		}
	}


	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Function //////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
	string Service::toString() const
	{
		string res = _("Service:\n") + Need::toString();

		res += fmt::format(fmt::runtime(_("\nAgency: {}")), garageId());

		return res;
	}

	Service& Service::operator=(const Service& s)
	{
		Need::operator=(s);

		return *this;
	}

	bool Service::operator==(const Service& ser) const
	{
		if (Need::operator==(ser))
		{
			if (garageId() == ser.garageId())
			{
				return true;
			}
		}

		return false;
	}

	generic::Element* Service::createNext([[maybe_unused]] const bool copy_objects) const
	{
		Date		 end_date;
		unsigned int end_km;

		(end_date = endDate()).add_years(static_cast<int>(delayYear()));
		end_km = endKm() + delayKm();

		auto s = new Service(*this, false);
		s->setOwner(owner());
		s->setBeginDate(endDate());
		s->setEndDate(end_date);
		s->setBeginKm(endKm());
		s->setEndKm(end_km);

		auto car = dynamic_cast<Car*>(owner());
		if (car != nullptr)
		{
			car->elements<Service>().add(s);
		}

		if (copy_objects)
		{
			for (const OwnedObject* object : objects<Operation>())
			{
				Operation& new_operation = s->addObject<Operation>();
				auto	   operation	 = static_cast<const Operation*>(object);
				new_operation.setName(operation->name());
				new_operation.setPrice(operation->price());
			}
		}

		return s;
	}

	std::string Service::userName(gulong number, Determiner determiner)
	{
		switch (determiner)
		{
		case Determiner::NONE:
			return P_("service", "services", number);
		case Determiner::A:
			return P_("a service", "services", number);
		case Determiner::THE:
			return P_("the service", "the services", number);
		case Determiner::YOUR:
			return P_("your service", "your services", number);
		case Determiner::OF:
			return P_("of service", "of services", number);
		}
		return "";
	}

	std::string Service::nameId() const
	{
		return fmt::format(fmt::runtime(_("Done the {}")), beginDate().format_string("%x").c_str());
	}
}	 // namespace car
