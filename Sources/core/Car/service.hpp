/*!
 * \file    service.h
 * \author  Remi BERTHO
 * \date    15/03/16
 * \version 1.0.0
 */

/*
 * service.h
 *
 * Copyright 2017 Rémi BERTHO <remi.bertho@dalan.fr>
 *
 * This file is part of GDV.
 *
 * GDVis free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * GDV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

#ifndef SERVICE_H_INCLUDED
#define SERVICE_H_INCLUDED

#include "Vehicle/need.hpp"
#include "Place/garage.hpp"

namespace car
{
	/*! \class Service
	 *   \brief This class represent a service
	 */
	class Service : public vehicle::Need
	{
		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Internal class ////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	public:
		class Operation : public generic::Article
		{
			///////////////////////////////////////////////////////////////////////////////////
			/////////////////////////// Constants /////////////////////////////////////////////
			///////////////////////////////////////////////////////////////////////////////////
		public:
			static const constexpr char* DATABASE_TABLE_NAME = "Car_Service_Operations";

			///////////////////////////////////////////////////////////////////////////////////
			/////////////////////////// Constructor ///////////////////////////////////////////
			///////////////////////////////////////////////////////////////////////////////////
		public:
			/*!
			 *  \brief Default constructor
			 */
			explicit Operation(const int64_t id);

			/*!
			 *  \brief Constructor with all components
			 */
			Operation(const int64_t id, database::Item* operation);

			~Operation() = default;

		protected:
			/*!
			 *  \brief Copy constructor
			 */
			explicit Operation(const Operation& op);


			///////////////////////////////////////////////////////////////////////////////////
			/////////////////////////// Function //////////////////////////////////////////////
			///////////////////////////////////////////////////////////////////////////////////
		public:
			/*!
			 *  \brief operator =
			 *  \param pn a OperationOperation
			 *  \return a reference to the object
			 */
			Operation& operator=(const Operation& op);

			/**
			 * @brief operator ==
			 * @param pn another OperationOperation
			 * @return true if it has the same data, false otherwise
			 */
			bool operator==(const Operation& op) const;

			/*!
			 *  \brief Convert to a string
			 *  \return the string
			 */
			virtual std::string toString() const;

			/**
			 * @brief Clone the exception
			 * @return the clone in a shared_ptr
			 */
			virtual database::ItemPtr clone() const
			{
				return std::shared_ptr<database::Item>(new Operation(*this));
			}

			/**
			 * @brief Get the need name
			 * @return the need name
			 */
			static std::string userName(gulong number = 1, Determiner determiner = Determiner::NONE);

			/**
			 * @brief databaseName
			 * @return the name used in database
			 */
			virtual std::string dataBaseTableName() const
			{
				return DATABASE_TABLE_NAME;
			}
		};


		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Friendship ////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
		friend class Car;

		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Constants /////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	public:
		static const constexpr char* DATABASE_TABLE_NAME = "Car_Services";

		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// DB infos //////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	private:
		static const constexpr char GARAGE_DB_NAME[]	= "garage";
		static const constexpr char GARAGE_DB_VERSION[] = "0.2.7";

		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Attributes ////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	protected:
		database::LinkProxy<GARAGE_DB_NAME, GARAGE_DB_VERSION, place::Garage::DATABASE_TABLE_NAME, true> garage_;

		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Constructor ///////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	public:
		/*!
		 *  \brief Default constructor
		 */
		explicit Service(const int64_t id);

		/*!
		 *  \brief Constructor with all components
		 */
		Service(const int64_t id, database::Item* car);

	protected:
		/*!
		 *  \brief Copy constructor
		 */
		explicit Service(const Service& s, const bool copy_objects = true);


		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Function //////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	public:
		/*!
		 *  \brief Convert to a string
		 *  \return The string
		 */
		std::string toString() const override;

		/*!
		 *  \brief operator =
		 *  \param s a Service
		 *  \return a reference to the object
		 */
		Service& operator=(const Service& s);

		/**
		 * @brief operator ==
		 * @param ser another Service
		 * @return true if it has the same data, false otherwise
		 */
		bool operator==(const Service& ser) const;

		Element* createNext(const bool copy_objects = false) const override;

		/**
		 * @brief Clone the exception
		 * @return the clone in a shared_ptr
		 */
		database::ItemPtr clone() const override
		{
			return std::shared_ptr<database::Item>(new Service(*this));
		}

		/**
		 * @brief Get the need name
		 * @return the need name
		 */
		static std::string userName(gulong number = 1, Determiner determiner = Determiner::NONE);

		/**
		 * @brief databaseName
		 * @return the name used in database
		 */
		std::string dataBaseTableName() const override
		{
			return DATABASE_TABLE_NAME;
		}

		/**
		 * @brief Get the name of the specific need
		 * @return the name
		 */
		std::string nameId() const override;


		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Getter ////////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	public:
		inline int64_t garageId() const
		{
			return garage_;
		}

		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Setter ////////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	public:
		inline void setGarageId(const int64_t id)
		{
			garage_ = id;
		}
	};
}	 // namespace car


#endif	  // SERVICE_H_INCLUDED
