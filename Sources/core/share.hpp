/*!
 * \file    share.h
 * \author  Remi BERTHO
 * \date    15/03/16
 * \version 1.0.0
 */

/*
 * share.h
 *
 * Copyright 2017-2024 Rémi BERTHO <remi.bertho@dalan.fr>
 *
 * This file is part of GDV.
 *
 * GDVis free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * GDV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

#ifndef SHARE_H_INCLUDED
#define SHARE_H_INCLUDED

#include <string>

namespace core
{
	///////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////// Fuel ///////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
	enum EngineType
	{
		DIESEL,
		GAS,
		ELECTRICITY,
		GPL
	};

	enum Determiner
	{
		NONE,
		A,
		THE,
		YOUR,
		OF
	};

	const constexpr int	   ENGINE_TYPE_NB		= 4;
	const constexpr int	   NB_KM_IN_CONSUMPTION = 100;
	const constexpr double MEAN_DAY_PER_MONTH	= 30.4377;
	const constexpr double NB_DAY_PER_YEAR		= 365.2524;
	const constexpr double NB_MONTH_PER_YEAR	= 12.;

	std::string engineToString(EngineType type);
	EngineType	stringToEngine(std::string str);


	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Conversion ////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
	/*!
	 *  Convert a Boolean into a string with yes or no which can be translated
	 *  \param b
	 *  \return the string
	 */
	std::string boolToYesNo(const bool b);

	/*!
	 *  Convert a Boolean into a string
	 *  \param b
	 *  \return the string
	 */
	std::string boolToString(const bool b);

	/*!
	 *  Convert a string to a bool
	 *  \param str the string
	 *  \return the bool
	 */
	bool stringToBool(const std::string& str);

	/*!
	 *  Convert a int64_t into a string without locales
	 *  \return the string
	 */
	std::string i64tostr(const int64_t i);

	/*!
	 *  Convert a uint64_t into a string without locales
	 *  \return the string
	 */
	std::string u64tostr(const uint64_t i);

	/*!
	 *  Convert a unsigned int into a string without locales
	 *  \return the string
	 */
	inline std::string utostr(const unsigned int i)
	{
		return i64tostr(i);
	}

	/*!
	 *  Convert a int into a string without locales
	 *  \return the string
	 */
	inline std::string itostr(const int i)
	{
		return i64tostr(i);
	}

	/*!
	 *  Convert a double into a string
	 *  \param d the double
	 *  \param decimals the number of decimals
	 *  \return the string
	 */
	std::string doubleToString(const double d, const int decimals = -1, int width = 0);

	/*!
	 *  Convert a double into a string without locales
	 *  \return the string
	 */
	std::string dtostr(const double d);

	/*!
	 *  Convert a string into a double
	 *  \param str the string
	 *  \return the double
	 */
	double stringToDouble(const std::string& str);

	/*!
	 *  Convert a int into a string
	 *  \param str the string
	 *  \return the int
	 */
	int stringToInt(const std::string& str);

	/*!
	 *  Convert an int into a string
	 *  \param i the int
	 *  \return the string
	 */
	std::string intToString(const int i, int width = 0);

	/*!
	 *  Convert an int into a string
	 *  \param i the int
	 *  \return the string
	 */
	std::string uintToString(const unsigned int i, int width = 0);

	/*!
	 *  Change the decimal place of a double in a string
	 *  \param str the string
	 *  \param old_character the old character
	 *  \param new_character the new character
	 *  \return the string
	 */
	std::string replaceCharacterInString(const std::string& str, const char old_character, const char new_character);

	/*!
	 *  Change the decimal place of a double in a string
	 *  \param str the string
	 *  \param old_character the character
	 *  \param new_character the character
	 *  \return the string
	 */
	std::string& replaceCharacterInString(std::string& str, const char old_character, const char new_character);

	/*!
	 *  Remove a character in a string
	 *  \param str the string
	 *  \param character
	 *  \return the string
	 */
	std::string removeCharacterInString(const std::string& str, const char character);

	/*!
	 *  Remove a character in a string
	 *  \param str the string
	 *  \param character
	 *  \return the string
	 */
	std::string& removeCharacterInString(std::string& str, const char character);
}	 // namespace core


#endif	  // SHARE_H_INCLUDED
