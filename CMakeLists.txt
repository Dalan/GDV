cmake_minimum_required(VERSION 3.11)

project(gdv LANGUAGES C CXX VERSION 0.1.0)
STRING(TIMESTAMP DATE "%m %d %Y")
STRING(TIMESTAMP TIME "%H:%M:%S")

#------------------------------------------------------------------------------
# Custom options
#------------------------------------------------------------------------------
# Options
option(PCH "Pre-compiled header" ON)
option(BUILD_TESTS "Build test programs" ON)
option(CODE_COVERAGE "Build with code coverage enable" OFF)
option(INSTALL "To be used when compiling for install" OFF)
option(PORTABLE "Set as portable application" OFF)
option(FORMAT "Format code with clang-format" OFF)
option(TRANSLATION "Update translation" ON)


# Windows install
IF(INSTALL)
	add_definitions(-DINSTALL)
ENDIF()

# Portable
IF(PORTABLE)
	add_definitions(-DPORTABLE)
ENDIF()

#------------------------------------------------------------------------------
# General settings
#------------------------------------------------------------------------------

# Add Cmake dependencies
set (CMAKE_MODULE_PATH "${CMAKE_SOURCE_DIR}/CMake")
list(APPEND CMAKE_MODULE_PATH "${CMAKE_SOURCE_DIR}/CMake/GCR/macros")
include (GNUInstallDirs)
include(GlibCompileResourcesSupport)
include(cleanCppExtensions)
IF(TRANSLATION)
	include(Gettext_helpers)
ENDIF()

# C++ 17
set (CMAKE_CXX_STANDARD 23)

# Policies
if(POLICY CMP0058)
	cmake_policy(SET CMP0058 NEW)
endif()

# Be nice and export compile commands by default, this is handy for clang-tidy and for other tools.
set(CMAKE_EXPORT_COMPILE_COMMANDS ON)

# Set default install location to dist folder in build dir we do not want to install to /usr by default
if (CMAKE_INSTALL_PREFIX_INITIALIZED_TO_DEFAULT)
	set (CMAKE_INSTALL_PREFIX "${CMAKE_BINARY_DIR}/dist" CACHE PATH "Install path prefix, prepended onto install directories." FORCE )
endif()

# Enable helpfull warnings for all files
if(MSVC)
	add_compile_options(/W1 )
else()
	add_compile_options(-Wall)
endif()

SOURCE_GROUP(TREE ${CMAKE_CURRENT_SOURCE_DIR})


#------------------------------------------------------------------------------
# Dependencies
#------------------------------------------------------------------------------
if(MSVC)
	find_package(Sqlite3 REQUIRED)
	find_path(SQLITE_INCLUDE_DIRS sqlite3.h)
	find_library(SQLITE_LIBRARY sqlite3.lib)
    get_filename_component(SQLITE_LIBRARY_DIRS ${SQLITE_LIBRARY} DIRECTORY CACHE)
	set(SQLITE_LIBRARIES sqlite3)

	find_package(plplot REQUIRED)
	find_path(PLPLOT-CPP_INCLUDE_DIRS plplot.h PATH_SUFFIXES plplot )
	set(PLPLOT-CPP_LIBRARIES plplot csirocsa qsastime plplotcxx)
	
	set(
		GTKMM_LIBRARIES
		gtkmm
		atkmm
		gdkmm
		giomm
		pangomm
		glibmm
		cairomm-1.0
		gtk-3.0
		gdk-3.0
		pangocairo-1.0
		pango-1.0
		atk-1.0
		cairo-gobject
		gio-2.0
		cairo
		sigc-2.0
		gdk_pixbuf-2.0
		gobject-2.0
		glib-2.0
		libintl
	)
ELSE()
	# GTKmm
	find_package(
		PkgConfig
		REQUIRED
	)

	pkg_check_modules(
		GTKMM
		REQUIRED gtkmm-3.0>=3.18
	)

	pkg_check_modules(
		GLIBMM
		REQUIRED glibmm-2.4
	)

	pkg_check_modules(
		GIOMM
		REQUIRED giomm-2.4
	)

	pkg_check_modules(
		PLPLOT-CPP
		REQUIRED plplot-c++
	)

	pkg_check_modules(
		SQLITE
		REQUIRED sqlite3
	)
ENDIF()

find_package(
	Threads
	REQUIRED
)

find_package(
	fmt
	REQUIRED
)

#------------------------------------------------------------------------------
# Configuration
#------------------------------------------------------------------------------
# Set data dir and loale dire
IF(INSTALL)
	IF(WIN32)
		SET(DATA_DIR .)
		SET(LOCALE_DIR Locales)
	ELSE()
		SET(DATA_DIR ${CMAKE_INSTALL_FULL_DATADIR}/${CMAKE_PROJECT_NAME})
		SET(LOCALE_DIR ${CMAKE_INSTALL_FULL_LOCALEDIR})
	ENDIF()
ELSE()
	SET(DATA_DIR ${CMAKE_SOURCE_DIR})
	SET(LOCALE_DIR "${CMAKE_BINARY_DIR}/Locales")
ENDIF()

# Get the current working branch
execute_process(
	COMMAND git rev-parse --abbrev-ref HEAD
	WORKING_DIRECTORY ${CMAKE_SOURCE_DIR}
	OUTPUT_VARIABLE GIT_BRANCH
	OUTPUT_STRIP_TRAILING_WHITESPACE
)

# Get the latest abbreviated commit hash of the working branch
execute_process(
	COMMAND git log -1 --format=%h
	WORKING_DIRECTORY ${CMAKE_SOURCE_DIR}
	OUTPUT_VARIABLE GIT_COMMIT_HASH
	OUTPUT_STRIP_TRAILING_WHITESPACE
)

# Set program name
SET(PROJECT_NAME ${CMAKE_PROJECT_NAME})
string(TOUPPER ${CMAKE_PROJECT_NAME} PROGRAM_NAME)

configure_file (
	"${CMAKE_CURRENT_SOURCE_DIR}/config.h.in"
	"${CMAKE_CURRENT_BINARY_DIR}/config.h"
)

#------------------------------------------------------------------------------
# Data
#------------------------------------------------------------------------------
add_subdirectory("Data")

#------------------------------------------------------------------------------
# Sources
#------------------------------------------------------------------------------
add_subdirectory("Externals_libraries")

# Code coverage
IF(CODE_COVERAGE)
	SET(CMAKE_BUILD_TYPE Coverage)
ENDIF()

# Sources
add_subdirectory("Sources")

# Translations
add_subdirectory("Translations")

#------------------------------------------------------------------------------
# Installation
#------------------------------------------------------------------------------
add_subdirectory("Installation")

#------------------------------------------------------------------------------
# Tests
#------------------------------------------------------------------------------
IF(BUILD_TESTS)
	enable_testing()
	add_subdirectory("Tests")
ENDIF()

#------------------------------------------------------------------------------
# Doc
#------------------------------------------------------------------------------
add_subdirectory("Documentation")
add_subdirectory("Man")
