@startuml

namespace database{
	abstract class Attribute{
		#Item dbi
		#string namespace
		#Version database_version
		#bool nullable
		--
		-signal<void, Item, Attribute>  signal_changed_;
		--
		+Attribute(Item dbi, string name, Version databaseVersion, bool nullable)
		+Attribute(Attribute attribute, Item item)
		..Functions..
		+bool sameDbiId(Attribute attribute)
		+bool sameDbiType(Attribute attribute)
		+{abstract} string toUstring()
		..Getter and setter..
		+string name()
		+ Version databaseVersion()
		+ bool nullable()
		+{abstract} Attribute operator=(Attribute attribute)
	}
	Attribute -> Item
	Attribute "1" *- "1" core.Version
	
	enum AttributeGroup::Type{
		PROPERTY
		LINK
	}
	
	class AttributeGroup{
		#list<Attribute> attributes_;
		#Type type_;
		--
		-signal<void, Item> signal_changed_
		--
		+AttributeGroup(Type type)
		..Functions..
		+void add(Attribute dbp)
		+auto size()
		+void blockSignal()
		+void unblockSignal()
		..Slot..
		void onPropertyChanged(Item item, Attribute& attribute)
		..Getter and setter..
		+Attribute at(string name)
		+const Attribute at(string name) const
		+Attribute operator[](string name)
		..Iterators..
		+auto begin()
		+auto cbegin()
		+auto end()
		+auto cend()
	}
	AttributeGroup --> AttributeGroup::Type
	AttributeGroup *- Attribute
	
	class Property{
		#std::any value_
		--
		--
		+template<T> Property(Item item, string name, Version databaseVersion, T value, bool nullable)
		+Property(Property dbp, Item item)
		..Functions..
		+template<T> bool equal(Property p)
		+template<T> bool operator==(T t)
		+string toUstring()
		#void bindSqlStatement(SQLite::Statement statement, int index)
		#string getSqlType()
		..Getter and setter..
		+std::type_info type()
		+template<T> operator T()
		+template<T> auto getValue()
		+string getAsString() 
		+template<T> Property operator=(T t)
		+Property operator=(Property dbp)
		+template<T> void setValue(T value)
		#void setFromSqlColumn(SQLite::Column column)
	}
	Attribute <|-- Property
	
	class Link{
		-string reference_table_
		--
		--
		+Link(Item item, string name, Version databaseVersion, int64 id, string database_name, bool nullable)
		+Link(Link l, Item item)
		..Functions..
		+bool equal(Link link)
		+string toUstring()
		..Getter and setter..
		+string databaseName()
		+operator int64()
		+int64 id()
		+Link operator=(int64 id)
		+Link operator=(Link link)
		+void setId(int64 id)
	}
	Property <|-- Link


	abstract class Item{
		-int64 id_
		-bool linked_to_database_
		-ItemPtr old_value_
		-AttributeGroup properties_
		--
		-signal<void, Item> signal_changed_
		--
		+Item(int64 id)
		#Item(Item item)
		..Functions..
		+{abstract} string toUstring()
		+{abstract} ItemPtr clone()
		+{abstract} string dataBaseTableName()
		+ bool sameDatabaseId(Item dbi)
		..Database..
		#void addInDatabase(SQLite::Database db)
		#void deleteInDatabase(SQLite::Database db)
		#void saveInDatabase(SQLite::Database db)
		#void setFromDatabase(SQLite::Database db)
		#bool createTableInDatabase(SQLite::Database db)
		#bool updateTableInDatabase(SQLite::Database& db, Version current_version)
		..Getter and setter..
		-int64 id()
		-setId(int64 id)
		#ItemPtr getOld()
		#void setOld()
		+bool linkedToDatabase()
		+signal<void, Item> signalChanged()
		-AttributeGroup& properties()
		+const AttributeGroup& properties() const
	}
	Item *- AttributeGroup
	

	enum DataBase::FunctionDone{
		SAVE
		DELETE_CHANGES
		UNDO
		REDO
	}

	class DataBase{
		SQLite::Database  db_
		ActionList actions_
		int64 nextID_[DataBase::DbiType::LAST]
		vector<connection> signal_connection_list_
		--
		-signal<void, const bool>  signal_change_request_;
		-signal<void, DataBase::FunctionDone>  signal_function_done_;
		--
		-DataBase()
		{static}+DataBasePtr initialize()
		..Functions..
		-void addAction(Action action)
		-void slotAdd(connection slot_connexion)
		-void slotBlock()
		-void slotUnblock()
		-void connectCarSignals(CarPtr car)
		-template<ItemClass> void initItem(Version current_version)
		-template<OwnedObjectClass> void cleanOrphan()
		-void cleanOrphans()
		-{static}Version getLastVersion()
		+Version getCurrentVersion()
		+template<ObjectClass> list<pair<string, int64>> objectsId()
		+template<ObjectClass> list<string> objectsName()
		+map<Date, double> fuelPrice(Fuel fuel)
		+double meanFuelPrice(Fuel fuel)
		+void save()
		+void deleteChanges()
		+bool needSave()
		+void deleteCurrentTracking()
		+bool canUndo()
		+bool canRedo()
		+void undo(CarPtr car)
		+void redo(CarPtr car)
		..New..
		+CarPtr newPlace(const string name)
		+template<PlaceClass> PlaceClassPtr newPlace(const string name)
		..Get..
		-template<ItemClass, Position> void getVehicleElements(car::CarPtr c, Position pos)
		-template<ItemClass> void getItemArray(generic::Element element)
		+CarPtr getCar(int64 id)
		+template<PlaceClass> PlaceClassPtr getPlace(int64 id)
		..Remove..
		-template<ItemClass>void removeVehicleElements(int64 vehicle_id)
		+void removeCar(int64 id)
		+template<ItemClass>void removeSingleItem(int64 id)
		..Slots..
		-void onDbiChanged(Item item)
		-void onVehicleElementDeleted(ItemPtr element, uint index)
		-void onVehicleElementAdded(vehicle::Element element)
		-void onItemArrayDeleted(ItemPtr item, uint index)
		-void onItemArryAdded(Item item)
		..Getter and setter..
		+signal<void, const bool> signalChangeRequest()
		+signal<void, DataBase::FunctionDone> signalFunctionDone()
		-SQLite::Database db()
		-template<ItemClass> int64 nextId()
		-int64 getNextId(Item item)
		-template<ItemClass> int64 getNextId()
	}
	DataBase --> DataBase::FunctionDone
	DataBase --> Item
	DataBase --> action.List
	
}

@endum

