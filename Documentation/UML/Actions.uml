@startuml

namespace action{
	enum Action::Type{
		ADD
		DELETE
		MODIFY
	}

	abstract class Action{
		#DataBaseItemPtr dbi_
		#ActionType type_
		--
		+Action(DataBaseItemPtr dbi, Type type);
		..
		{abstract} + void undo(VehiclePtr vehicle)
		{abstract} + void redo(VehiclePtr vehicle)
		+ void executeSql(SQLite::Database db)
		+ bool sameDbi(Action action)
		..
		+Type type()
		#DataBaseItemPtr getDbi()
		#DataBaseItemPtr getOldDbi()
	}
	Action::Type <-- Action
	
	class Group{
		-Time time_
		-vector<Action> actions_
		..
		+Group()
		..
		+bool canAdd(Time time)
		+void clear()
		+void execute(Database db)
		+void add(Action action)
		+void undo(CarPtr car)
		+void redo(CarPtr car)
	}
	Group o--> Action

	class List{
		-deque<ActionList> actions_
		-int index_
		--
		+List()
		..
		+bool canUndo()
		+bool canRedo()
		+void clear()
		+void execute(SQLite::Database db)
		+void add(Action action)
		+void undo(CarPtr car)
		+void redo(CarPtr car)
	}
	List *--> Group

	class Vehicle{
	 	--
	 	Vehicle(DataBaseItemPtr vehicle, Type type)
	 	..
		+ void undo(VehiclePtr vehicle)
		+ void redo(VehiclePtr vehicle)
	 }
	Action <|-- Vehicle
	Vehicle --> vehicle.Vehicle

	class VehicleElement{
	 	--
	 	VehicleElement(DataBaseItemPtr vehicle, Type type)
	 	..
	 	#template<ElementClass, Position> void undoInternal(VehiclePtr vehicle)
	 	#template<ElementClass, Position> void redoInternal(VehiclePtr vehicle)
		+ void undo(VehiclePtr vehicle)
		+ void redo(VehiclePtr vehicle)
	 }
	Action <|-- VehicleElement
	VehicleElement --> generic.Element 

	class Place{
	 	--
	 	Place(DataBaseItemPtr vehicle, Type type)
	 	..
	 	#template<PlaceClass> void undoInternal(VehiclePtr vehicle)
	 	#template<PlaceClass> void redoInternal(VehiclePtr vehicle)
		+ void undo(VehiclePtr vehicle)
		+ void redo(VehiclePtr vehicle)
	 }
	Action <|-- Place
	Place --> place.Place

	class VehicleNeedElement{
	 	--
	 	VehicleNeedElement(DataBaseItemPtr need, Type type)
	 	..
	 	#template<ElementClass, Position> void undoInternal(vehicle::NeedPtr need)
	 	#template<ElementClass, Position> void redoInternal(vehicle::NeedPtr need)
		+ void undo(vehicle::NeedPtr need)
		+ void redo(vehicle::NeedPtr need)
	 }
	Action <|-- VehicleNeedElement
	VehicleNeedElement --> vehicle.Need 
}

@enduml
