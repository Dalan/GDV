# GDV

GDV (Gestionnaire De Véhicule,  Vehicle Manager)

Work in progress

## Get it
### Dependencies
General :

* A C++17 compliant compiler (GCC ≥ 7.1 or Clang ≥ 5.0)
* Cmake ≥ 3.11
* Gtkmm  ≥ 3.22
* Sqlite ≥ 3.7
* PLplot (C++ and cairo) >= 5.13

For Linux :

* Pandoc (to generate man page)

### Build

```
git clone --recursive https://framagit.org/Dalan/GDV.git
cd GDV
mkdir build
cd build
cmake ..
make
```

Cmake options (Used with `-DOPTION=VALUE`):

| Option | Possible value | Default value | Description | Additional dependencies |
|--------|----------------|---------------|-------------|--------------|
| PCH | ON/OFF | ON | Use pre-compiled header |  |
| INSTALL | ON/OFF | OFF | To be used when compiling for install |  |
| PORTABLE | ON/OFF | OFF | Portable version |  |
| FORMAT | ON/OFF | OFF | Format code | clang-format |
| TRANSLATION | ON/OFF | ON | Update translation | gettext tools |
| BUILD_TESTS | ON/OFF | ON | Add unit test | Catch2 |
| CODE_COVERAGE | ON/OFF | OFF | Enable code coverage tool | gcov |
| CMAKE_BUILD_TYPE | DEBUG/RELEASE |  | Build type | |

### Tests

Run :

```
make test
```

Code coverage:

```
make test_core_coverage
xdg-open ./coverage/index.html
```

### Run

```
make run
```

### Install

```
sudo make install
```