/*
 * physicalneed.cpp
 *
 * Copyright 2017-2018 Rémi BERTHO <remi.bertho@dalan.fr>
 *
 * This file is part of GDV.
 *
 * GDVi s free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * GDV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

#include <catch2/catch_all.hpp>
#include "Vehicle/physicalneed.hpp"
#include "Car/car.hpp"

using namespace core;
using namespace database;
using namespace Glib;
using namespace vehicle;
using namespace car;
using namespace std;

// Class that dummy implement PhysicalNeed
class PhysicalNeedTest : public PhysicalNeed
{
public:
	explicit PhysicalNeedTest(const gint64 id);
	explicit PhysicalNeedTest(const gint64 id, const OwnerInfos& owner);
	explicit PhysicalNeedTest(const PhysicalNeedTest& dbi);
	virtual PhysicalNeedTest& operator=(const PhysicalNeedTest& dbi);
	virtual bool			  operator==(const PhysicalNeedTest& dbi) const;
	virtual string	  dataBaseTableName() const;
	virtual string	  toString() const;
	Element*				  createNext(const bool copy_objects = false) const;
	virtual ItemPtr			  clone() const;
	virtual string	  nameId() const;
	virtual string	  needName() const;
};


PhysicalNeedTest::PhysicalNeedTest(const gint64 id) : PhysicalNeed(id, make_tuple(nullptr, ""))
{
}

PhysicalNeedTest::PhysicalNeedTest(const gint64 id, const OwnerInfos& owner) : PhysicalNeed(id, owner)
{
}

PhysicalNeedTest::PhysicalNeedTest(const PhysicalNeedTest& dbi) : PhysicalNeed(dbi)
{
}

PhysicalNeedTest& PhysicalNeedTest::operator=(const PhysicalNeedTest& dbi)
{
	if (this == &dbi)
		return *this;

	PhysicalNeed::operator=(dbi);

	return *this;
}

bool PhysicalNeedTest::operator==(const PhysicalNeedTest& dbi) const
{
	return PhysicalNeed::operator==(dbi);
}

string PhysicalNeedTest::toString() const
{
	return PhysicalNeed::toString();
}

generic::Element* PhysicalNeedTest::createNext([[maybe_unused]] const bool copy_objects) const
{
	return nullptr;
}

ItemPtr PhysicalNeedTest::clone() const
{
	return std::shared_ptr<PhysicalNeedTest>(new PhysicalNeedTest(*this));
}

string PhysicalNeedTest::nameId() const
{
	return beginDate().format_string("%x");
}

string PhysicalNeedTest::needName() const
{
	return "PhysicalNeed";
}

string PhysicalNeedTest::dataBaseTableName() const
{
	return "Test";
}


// Tests
TEST_CASE("PhysicalNeed", "[Vehicle]")
{
	PhysicalNeedTest pn1(1);
	PhysicalNeedTest pn2(2, std::make_tuple(nullptr, ""));

	SECTION("Properties")
	{
		int nb_callback_called_1 = 0;
		int nb_callback_called_2 = 0;

		pn1.signalChanged().connect([&]([[maybe_unused]] Item& dbi) { nb_callback_called_1++; });
		pn2.signalChanged().connect([&]([[maybe_unused]] Item& dbi) { nb_callback_called_2++; });

		SECTION("Brand")
		{
			CHECK(pn1.brand() == "");
			pn1.setBrand("blop");
			pn1.setBrand("blop");
			CHECK(pn1.brand() == "blop");
			CHECK(pn2.brand() == "");
			pn2.setBrand(pn2.brand());
		}
		SECTION("Model")
		{
			CHECK(pn1.model() == "");
			pn1.setModel("blop");
			pn1.setModel("blop");
			CHECK(pn1.model() == "blop");
			CHECK(pn2.model() == "");
			pn2.setModel(pn2.model());
		}
		SECTION("Operator=")
		{
			pn2.setBrand("Blop");
			pn2.setModel("Toto");
			nb_callback_called_2 = 0;
			pn1					 = pn2;
			CHECK(pn1.brand() == "Blop");
			CHECK(pn1.model() == "Toto");
			CHECK(pn1 == pn2);
		}

		CHECK(nb_callback_called_1 == 1);
		CHECK(nb_callback_called_2 == 0);
	}
}
