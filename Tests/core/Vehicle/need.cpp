/*
 * need.cpp
 *
 * Copyright 2017-2018 Rémi BERTHO <remi.bertho@dalan.fr>
 *
 * This file is part of GDV.
 *
 * GDVi s free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * GDV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

#include <catch2/catch_all.hpp>
#include "Vehicle/need.hpp"
#include "Car/car.hpp"

#include <iostream>

using namespace core;
using namespace database;
using namespace Glib;
using namespace std;
using namespace vehicle;
using namespace car;
using namespace std;
using namespace generic;
using namespace Catch;

// Class that dummy implement DatBaseItem
class NeedTest : public Need
{
public:
	explicit NeedTest(const gint64 id);
	explicit NeedTest(const gint64 id, const OwnerInfos& owner);
	explicit NeedTest(const NeedTest& dbi);
	virtual NeedTest&	  operator=(const NeedTest& dbi);
	virtual bool		  operator==(const NeedTest& dbi) const;
	virtual string dataBaseTableName() const;
	virtual string toString() const;
	Element*			  createNext(const bool copy_objects = false) const;
	virtual ItemPtr		  clone() const;
	virtual string nameId() const;
	virtual string needName() const;
};


NeedTest::NeedTest(const gint64 id) : Need(id, make_tuple(nullptr, ""))
{
}

NeedTest::NeedTest(const gint64 id, const OwnerInfos& owner) : Need(id, owner)
{
}

NeedTest::NeedTest(const NeedTest& dbi) : Need(dbi)
{
}

NeedTest& NeedTest::operator=(const NeedTest& dbi)
{
	if (this == &dbi)
		return *this;

	Need::operator=(dbi);

	return *this;
}

bool NeedTest::operator==(const NeedTest& dbi) const
{
	return Need::operator==(dbi);
}

string NeedTest::toString() const
{
	return Need::toString();
}

Element* NeedTest::createNext([[maybe_unused]] const bool copy_objects) const
{
	return nullptr;
}

ItemPtr NeedTest::clone() const
{
	return std::shared_ptr<Need>(new NeedTest(*this));
}

string NeedTest::nameId() const
{
	return beginDate().format_string("%x");
}

string NeedTest::needName() const
{
	return "Need";
}

string NeedTest::dataBaseTableName() const
{
	return "Test";
}


// Tests
TEST_CASE("Need", "[Vehicle]")
{
	NeedTest n1(1);
	// NeedTest n2(2, "", std::make_tuple(nullptr, ""), 9750, Date(10), Date(20), 1250, 3000, 1500, 3);
	NeedTest n2(2, std::make_tuple(nullptr, ""));
	Date	 date_tomorrow;
	date_tomorrow.set_time_current();
	date_tomorrow.add_days(1);

	Date date_tomorrow_year = date_tomorrow;
	date_tomorrow_year.add_years(1);

	SECTION("Properties")
	{
		int nb_callback_called_1 = 0;
		int nb_callback_called_2 = 0;

		n1.signalChanged().connect([&]([[maybe_unused]] Item& dbi) { nb_callback_called_1++; });
		n2.signalChanged().connect([&]([[maybe_unused]] Item& dbi) { nb_callback_called_2++; });

		SECTION("Begin date None")
		{
			CHECK(n1.beginDate() == Date(1, Date::JANUARY, 2020));
			n1.setBeginDate(date_tomorrow, Element::NONE);
			n1.setBeginDate(date_tomorrow, Element::NONE);
			CHECK(n1.beginDate() == date_tomorrow);
			n1.setBeginDate(as_const(n1).beginDate(), Element::NONE);
			CHECK(nb_callback_called_1 == 1);
		}
		SECTION("Begin date End")
		{
			CHECK(n1.beginDate() == Date(1, Date::JANUARY, 2020));
			n1.setBeginDate(date_tomorrow, Element::END);
			n1.setBeginDate(date_tomorrow, Element::END);
			CHECK(n1.beginDate() == date_tomorrow);
			CHECK(n1.endDate() == date_tomorrow_year);
			n1.setBeginDate(n1.beginDate(), Element::END);
			CHECK(nb_callback_called_1 == 2);
		}
		SECTION("Begin date All")
		{
			CHECK(n1.beginDate() == Date(1, Date::JANUARY, 2020));
			n1.setBeginDate(date_tomorrow, Element::ALL);
			n1.setBeginDate(date_tomorrow, Element::ALL);
			CHECK(n1.beginDate() == date_tomorrow);
			CHECK(n1.endDate() == date_tomorrow_year);
			n1.setBeginDate(as_const(n1).beginDate(), Element::ALL);
			CHECK(nb_callback_called_1 == 2);
		}

		SECTION("Begin km None")
		{
			CHECK(n1.beginKm() == 0);
			n1.setBeginKm(1253, Element::NONE);
			n1.setBeginKm(1253, Element::NONE);
			CHECK(n1.beginKm() == 1253);
			CHECK(n1.endKm() == 1000);
			n1.setBeginKm(n1.beginKm(), Element::NONE);
			CHECK(nb_callback_called_1 == 1);
		}
		SECTION("Begin km End")
		{
			CHECK(n1.beginKm() == 0);
			n1.setBeginKm(1253, Element::END);
			n1.setBeginKm(1253, Element::END);
			CHECK(n1.beginKm() == 1253);
			CHECK(n1.endKm() == 2253);
			n1.setBeginKm(n1.beginKm(), Element::END);
			CHECK(nb_callback_called_1 == 2);
		}
		SECTION("Begin km All")
		{
			CHECK(n1.beginKm() == 0);
			n1.setBeginKm(1253, Element::ALL);
			n1.setBeginKm(1253, Element::ALL);
			CHECK(n1.beginKm() == 1253);
			CHECK(n1.endKm() == 2253);
			n1.setBeginKm(n1.beginKm(), Element::ALL);
			CHECK(nb_callback_called_1 == 2);
		}

		SECTION("End km")
		{
			CHECK(n1.endKm() == 1000);
			n1.setEndKm(1253);
			n1.setEndKm(1253);
			CHECK(n1.endKm() == 1253);
			n1.setEndKm(n1.endKm());
			CHECK(nb_callback_called_1 == 1);
		}

		SECTION("Delay km None")
		{
			CHECK(n1.delayKm() == 1000);
			n1.setDelayKm(1253);
			n1.setDelayKm(1253);
			CHECK(n1.delayKm() == 1253);
			n1.setDelayKm(n1.delayKm());
			CHECK(nb_callback_called_1 == 1);
		}
		SECTION("Delay km All")
		{
			CHECK(n1.delayKm() == 1000);
			n1.setDelayKm(1253, Element::ALL);
			n1.setDelayKm(1253, Element::ALL);
			CHECK(n1.delayKm() == 1253);
			CHECK(n1.endKm() == 1253);
			n1.setDelayKm(n1.delayKm());
			CHECK(nb_callback_called_1 == 2);
		}

		SECTION("Delay year None")
		{
			CHECK(n1.delayYear() == 1);
			n1.setDelayYear(2);
			n1.setDelayYear(2);
			CHECK(n1.delayYear() == 2);
			n1.setDelayYear(n1.delayYear());
			CHECK(nb_callback_called_1 == 1);
		}
		SECTION("Delay year All")
		{
			CHECK(n1.delayYear() == 1);
			n1.setDelayYear(2, Element::ALL);
			n1.setDelayYear(2, Element::ALL);
			CHECK(n1.delayYear() == 2);
			CHECK(as_const(n1).endDate() == Date(1, Date::JANUARY, 2022));
			n1.setDelayYear(n1.delayYear());
			CHECK(nb_callback_called_1 == 2);
		}

		SECTION("Operator=")
		{
			n2.setDelayKm(1500);
			n2.setDelayYear(3);
			n2.setBeginKm(1250);
			n2.setEndKm(3000);
			nb_callback_called_2 = 0;
			n1					 = n2;
			CHECK(n1.delayKm() == 1500);
			CHECK(n1.delayYear() == 3);
			CHECK(n1.beginKm() == 1250);
			CHECK(n1.endKm() == 3000);
			CHECK(n1 == n2);
			CHECK(nb_callback_called_1 == 1);
		}

		CHECK(nb_callback_called_2 == 0);
	}

	SECTION("Functions")
	{
		n2.setDelayKm(1500);
		n2.setDelayYear(3);
		n2.setBeginKm(1250);
		n2.setEndKm(3000);
		n2.setPrice(9750);
		n2.setBeginDate(Date(10));
		n2.setEndDate(Date(20));

		CHECK(n1.distance() == Approx(1000.));
		CHECK(n2.distance() == Approx(1750.));

		CHECK(n2.kmBefore(2000) == 750);

		CHECK(n2.need(0));
		n1.setEndDate(date_tomorrow);
		CHECK_FALSE(n1.need(100));
		CHECK(n1.need(1500));

		CHECK(n2.km(2000) == 750);

		CHECK(n2.pricePer100Km() == Approx(650.));
		CHECK(n2.pricePerMonth() == Approx(270.83).margin(0.01));
	}
}
