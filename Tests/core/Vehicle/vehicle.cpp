/*
 * car.cpp
 *
 * Copyright 2017-2018 Rémi BERTHO <remi.bertho@dalan.fr>
 *
 * This file is part of GDV.
 *
 * GDVi s free socarware; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Socarware Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * GDV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Socarware
 * Foundation, Inc., 51 Franklin Street, Ficarh Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

#include <catch2/catch_all.hpp>
#include "Vehicle/vehicle.hpp"

using namespace core;
using namespace Glib;
using namespace database;
using namespace vehicle;
using namespace Catch;
using namespace std;

// Class that dummy implement Vehicle
class VehicleTest : public Vehicle
{
public:
	explicit VehicleTest(const gint64 id, const ustring& name);
	explicit VehicleTest(const VehicleTest& dbi);
	virtual VehicleTest&  operator=(const VehicleTest& dbi);
	virtual bool		  operator==(const VehicleTest& dbi) const;
	virtual string toString() const;
	virtual ItemPtr		  clone() const;
	virtual string dataBaseTableName() const;
};


VehicleTest::VehicleTest(const gint64 id, const ustring& name) : Vehicle(id, name)
{
}

VehicleTest::VehicleTest(const VehicleTest& dbi) : Vehicle(dbi)
{
}

VehicleTest& VehicleTest::operator=(const VehicleTest& dbi)
{
	if (this == &dbi)
		return *this;

	Vehicle::operator=(dbi);

	return *this;
}

bool VehicleTest::operator==(const VehicleTest& dbi) const
{
	return Vehicle::operator==(dbi);
}

string VehicleTest::toString() const
{
	return Vehicle::toString();
}

ItemPtr VehicleTest::clone() const
{
	return std::shared_ptr<VehicleTest>(new VehicleTest(*this));
}

string VehicleTest::dataBaseTableName() const
{
	return "Test";
}

// Tests
TEST_CASE("Vehicle", "[Vehicle]")
{
	Date date_previous_year;
	date_previous_year.set_time_current();
	date_previous_year.subtract_years(1);
	VehicleTest vehicle1(1, "Blop");
	// VehicleTest vehicle2(2, "Toto", "Titi", "bépo", "wzj", date_previous_year, 3265, EngineType::DIESEL, 32150);
	VehicleTest vehicle2(2, "Toto");

	SECTION("Properties")
	{
		int nb_callback_called_1 = 0;
		int nb_callback_called_2 = 0;

		vehicle1.signalChanged().connect([&]([[maybe_unused]] Item& dbi) { nb_callback_called_1++; });
		vehicle2.signalChanged().connect([&]([[maybe_unused]] Item& dbi) { nb_callback_called_2++; });


		SECTION("Brand")
		{
			vehicle1.setBrand("Titi");
			CHECK(vehicle1.brand() == "Titi");
			vehicle1.setBrand("Titi");
			CHECK(vehicle2.brand() == "");
			vehicle1.setBrand(vehicle1.brand());
		}
		SECTION("Model")
		{
			vehicle1.setModel("Titi");
			CHECK(vehicle1.model() == "Titi");
			vehicle1.setModel("Titi");
			CHECK(vehicle2.model() == "");
			vehicle1.setModel(vehicle1.model());
		}
		SECTION("Owner")
		{
			vehicle1.setOwner("Titi");
			CHECK(vehicle1.owner() == "Titi");
			vehicle1.setOwner("Titi");
			CHECK(vehicle2.owner() == "");
			vehicle1.setOwner(vehicle1.owner());
		}
		SECTION("Date of ownership")
		{
			Date d(25, Date::DECEMBER, 2015);
			vehicle1.setDateOfOwnership(d);
			CHECK(vehicle1.dateOfOwnership() == d);
			vehicle1.setDateOfOwnership(d);
			CHECK(vehicle2.dateOfOwnership() == Date(1));
			vehicle1.setDateOfOwnership(vehicle1.dateOfOwnership());
		}
		SECTION("Price")
		{
			vehicle1.setPrice(1253.36);
			CHECK(vehicle1.price() == Approx(1253.36));
			vehicle1.setPrice(1253.36);
			CHECK(vehicle2.price() == Approx(0));
			vehicle1.setPrice(vehicle1.price());
		}
		SECTION("Fuel")
		{
			vehicle1.setEngineType(EngineType::ELECTRICITY);
			CHECK(vehicle1.engineType() == EngineType::ELECTRICITY);
			vehicle1.setEngineType(EngineType::ELECTRICITY);
			CHECK(vehicle2.engineType() == EngineType::GAS);
			vehicle1.setEngineType(vehicle1.engineType());
		}
		SECTION("Distance")
		{
			vehicle1.setDistance(125);
			CHECK(vehicle1.distance() == 125);
			vehicle1.setDistance(125);
			CHECK(vehicle2.distance() == 0);
			vehicle1.setDistance(vehicle1.distance());
		}
		SECTION("Operator==")
		{
			vehicle2.setBrand("Titi");
			vehicle2.setModel("bépo");
			vehicle2.setOwner("wzj");
			vehicle2.setDateOfOwnership(date_previous_year);
			vehicle2.setPrice(3265);
			vehicle2.setEngineType(EngineType::DIESEL);
			vehicle2.setDistance(32150);
			nb_callback_called_2 = 0;

			CHECK_FALSE(vehicle1 == vehicle2);
			vehicle1 = vehicle2;
			CHECK(vehicle1.brand() == "Titi");
			CHECK(vehicle1.model() == "bépo");
			CHECK(vehicle1.owner() == "wzj");
			CHECK(vehicle1.dateOfOwnership() == date_previous_year);
			CHECK(vehicle1.price() == Approx(3265));
			CHECK(vehicle1.engineType() == EngineType::DIESEL);
			CHECK(vehicle1.distance() == 32150);
			CHECK(vehicle1 == vehicle2);
		}

		CHECK(nb_callback_called_1 == 1);
		CHECK(nb_callback_called_2 == 0);
	}

	SECTION("Functions")
	{
		// Functions tests
		ItemPtr car1_clone = vehicle1.clone();
		CHECK(car1_clone->sameDatabaseId(vehicle1));
		CHECK(*static_cast<VehicleTest*>(car1_clone.get()) == vehicle1);


		vehicle2.setDateOfOwnership(date_previous_year);
		vehicle2.setPrice(3265);
		vehicle2.setDistance(32150);
		CHECK(vehicle2.totalPrice() == Approx(3265.));
		CHECK(vehicle2.pricePerMonth() == Approx(272.083333333).epsilon(0.05));
		CHECK(vehicle2.pricePerYear() == Approx(3265).epsilon(0.01));
		CHECK(vehicle2.pricePer100Km() == Approx(10.155520995));
	}
}
