/*
 * gas_station.cpp
 *
 * Copyright 2019 Rémi BERTHO <remi.bertho@dalan.fr>
 *
 * This file is part of GDV.
 *
 * GDVi s free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * GDV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

#include <catch2/catch_all.hpp>
#include "Place/gas_station.hpp"
using namespace core;
using namespace database;
using namespace Glib;
using namespace place;
using namespace std;


// Tests
TEST_CASE("GasStation", "[Place]")
{
	GasStation gas_station1(1, "Salsa");
	GasStation gas_station2(2);

	SECTION("Properties")
	{
		int nb_callback_called_1 = 0;
		int nb_callback_called_2 = 0;

		gas_station1.signalChanged().connect([&]([[maybe_unused]] Item& dbi) { nb_callback_called_1++; });
		gas_station2.signalChanged().connect([&]([[maybe_unused]] Item& dbi) { nb_callback_called_2++; });

		SECTION("Cheap")
		{
			gas_station1.setCheap(true);
			CHECK(gas_station1.cheap() == true);
			gas_station1.setCheap(true);
			CHECK(gas_station2.cheap() == false);
			gas_station1.setCheap(gas_station1.cheap());
		}
		SECTION("Operator=")
		{
			gas_station2.setCheap(true);
			gas_station2.setBrand("Toto");
			gas_station2.setAddress("Paris");
			nb_callback_called_2 = 0;

			gas_station1 = gas_station2;
			CHECK(gas_station1.brand() == "Toto");
			CHECK(gas_station1.address() == "Paris");
			CHECK(gas_station1.cheap() == true);
			CHECK(gas_station1 == gas_station2);
		}

		CHECK(nb_callback_called_1 == 1);
		CHECK(nb_callback_called_2 == 0);
	}
}
