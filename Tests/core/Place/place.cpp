/*
 * place.cpp
 *
 * Copyright 2019 Rémi BERTHO <remi.bertho@dalan.fr>
 *
 * This file is part of GDV.
 *
 * GDVi s free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * GDV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

#include <catch2/catch_all.hpp>
#include "Place/place.hpp"
using namespace core;
using namespace database;
using namespace Glib;
using namespace place;
using namespace std;

// Class that dummy implement Place
class PlaceTest : public Place
{
public:
	explicit PlaceTest(const gint64 id, const Glib::ustring& name);
	explicit PlaceTest(const PlaceTest& dbi);
	virtual PlaceTest&	  operator=(const PlaceTest& dbi);
	virtual bool		  operator==(const PlaceTest& dbi) const;
	virtual string dataBaseTableName() const;
	virtual string toString() const;
	virtual ItemPtr		  clone() const;
};


PlaceTest::PlaceTest(const gint64 id, const Glib::ustring& name) : Place(id, name)
{
}

PlaceTest::PlaceTest(const PlaceTest& dbi) : Place(dbi)
{
}

PlaceTest& PlaceTest::operator=(const PlaceTest& dbi)
{
	if (this == &dbi)
		return *this;

	Place::operator=(dbi);

	return *this;
}

bool PlaceTest::operator==(const PlaceTest& dbi) const
{
	return Place::operator==(dbi);
}

string PlaceTest::toString() const
{
	return Place::toString();
}

ItemPtr PlaceTest::clone() const
{
	return std::shared_ptr<PlaceTest>(new PlaceTest(*this));
}
string PlaceTest::dataBaseTableName() const
{
	return "Test";
}


// Tests
TEST_CASE("Place", "[Place]")
{
	PlaceTest place1(1, "Salsa");
	PlaceTest place2(2, "Blop");

	SECTION("Properties")
	{
		int nb_callback_called_1 = 0;
		int nb_callback_called_2 = 0;

		place1.signalChanged().connect([&]([[maybe_unused]] Item& dbi) { nb_callback_called_1++; });
		place2.signalChanged().connect([&]([[maybe_unused]] Item& dbi) { nb_callback_called_2++; });

		SECTION("Brand")
		{
			CHECK(place1.brand() == "");
			place1.setBrand("blop");
			CHECK(place1.brand() == "blop");
			place1.setBrand("blop");
			place1.setBrand(place1.brand());
		}
		SECTION("Address")
		{
			CHECK(place1.address() == "");
			place1.setAddress("blop");
			CHECK(place1.address() == "blop");
			place1.setAddress("blop");
			place1.setAddress(place1.address());
		}
		SECTION("Operator=")
		{
			place2.setBrand("Toto");
			place2.setAddress("Paris");
			nb_callback_called_2 = 0;

			place1 = place2;
			CHECK(place1.brand() == "Toto");
			CHECK(place1.address() == "Paris");
			CHECK(place1 == place2);
		}

		CHECK(nb_callback_called_1 == 1);
		CHECK(nb_callback_called_2 == 0);
	}
}
