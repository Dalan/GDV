/*
 * inspection_center.cpp
 *
 * Copyright 2020 Rémi BERTHO <remi.bertho@dalan.fr>
 *
 * This file is part of GDV.
 *
 * GDVi s free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * GDV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

#include <catch2/catch_all.hpp>
#include "Place/inspection_center.hpp"
using namespace core;
using namespace database;
using namespace Glib;
using namespace place;
using namespace std;


// Tests
TEST_CASE("InspectionCenter", "[Place]")
{
	InspectionCenter inspection_center1(1, "Salsa");
	InspectionCenter inspection_center2(2);

	SECTION("Properties")
	{
		int nb_callback_called_1 = 0;
		int nb_callback_called_2 = 0;

		inspection_center1.signalChanged().connect([&]([[maybe_unused]] Item& dbi) { nb_callback_called_1++; });
		inspection_center2.signalChanged().connect([&]([[maybe_unused]] Item& dbi) { nb_callback_called_2++; });


		SECTION("Web address")
		{
			inspection_center1.setWebAddress("http://test.org");
			CHECK(inspection_center1.webAddress() == "http://test.org");
			inspection_center1.setWebAddress("http://test.org");
			CHECK(inspection_center2.webAddress() == "");
			inspection_center1.setWebAddress(inspection_center1.webAddress());
		}
		SECTION("Operator=")
		{
			inspection_center2.setBrand("Toto");
			inspection_center2.setAddress("Paris");
			inspection_center2.setWebAddress("http://test.org");
			nb_callback_called_2 = 0;

			inspection_center1 = inspection_center2;
			CHECK(inspection_center1.brand() == "Toto");
			CHECK(inspection_center1.address() == "Paris");
			CHECK(inspection_center1.webAddress() == "http://test.org");
			CHECK(inspection_center1 == inspection_center2);
		}

		CHECK(nb_callback_called_1 == 1);
		CHECK(nb_callback_called_2 == 0);
	}
}
