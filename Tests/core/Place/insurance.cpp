/*
 * insurance.cpp
 *
 * Copyright 2020 Rémi BERTHO <remi.bertho@dalan.fr>
 *
 * This file is part of GDV.
 *
 * GDVi s free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * GDV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

#include <catch2/catch_all.hpp>
#include "Place/insurance.hpp"
using namespace core;
using namespace database;
using namespace Glib;
using namespace place;
using namespace std;


// Tests
TEST_CASE("InsurancePlace", "[Place]")
{
	Insurance insurance1(1, "Salsa");
	Insurance insurance2(2);

	SECTION("Properties")
	{
		int nb_callback_called_1 = 0;
		int nb_callback_called_2 = 0;

		insurance1.signalChanged().connect([&]([[maybe_unused]] Item& dbi) { nb_callback_called_1++; });
		insurance2.signalChanged().connect([&]([[maybe_unused]] Item& dbi) { nb_callback_called_2++; });

		SECTION("Online")
		{
			insurance1.setOnline(true);
			CHECK(insurance1.online() == true);
			insurance1.setOnline(true);
			CHECK(insurance2.online() == false);
			insurance1.setOnline(insurance1.online());
		}
		SECTION("Web address")
		{
			insurance1.setWebAddress("http://test.org");
			CHECK(insurance1.webAddress() == "http://test.org");
			insurance1.setWebAddress("http://test.org");
			CHECK(insurance2.webAddress() == "");
			insurance1.setWebAddress(insurance1.webAddress());
		}
		SECTION("Operator=")
		{
			insurance2.setBrand("Toto");
			insurance2.setAddress("Paris");
			insurance2.setOnline(true);
			insurance2.setWebAddress("http://test.org");
			nb_callback_called_2 = 0;

			insurance1 = insurance2;
			CHECK(insurance1.brand() == "Toto");
			CHECK(insurance1.address() == "Paris");
			CHECK(insurance1.online() == true);
			CHECK(insurance1.webAddress() == "http://test.org");
			CHECK(insurance1 == insurance2);
		}

		CHECK(nb_callback_called_1 == 1);
		CHECK(nb_callback_called_2 == 0);
	}
}
