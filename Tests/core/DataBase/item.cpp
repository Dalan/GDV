/*
 * item.cpp
 *
 * Copyright 2017-2018 Rémi BERTHO <remi.bertho@dalan.fr>
 *
 * This file is part of GDV.
 *
 * GDVi s free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * GDV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

#include <catch2/catch_all.hpp>
#include "DataBase/item.hpp"
#include "DataBase/property.hpp"
#include "DataBase/property_proxy.hpp"
#include "DataBase/attribute_group.hpp"
#include "DataBase/link.hpp"
#include "DataBase/link_proxy.hpp"

using namespace core;
using namespace database;
using namespace std;

// Class that dummy implement DatBaseItem
class ItemTest : public Item
{
public:
	enum e : long
	{
		e1,
		e2
	};

	static const constexpr char int_name[]		 = "int";
	static const constexpr char int_version[]	 = "1.0.0";
	static const constexpr char string_name[]	 = "string";
	static const constexpr char string_version[] = "2.1.1";
	static const constexpr char enum_name[]		 = "enum";
	static const constexpr char enum_version[]	 = "2.1.1";

	PropertyProxy<guint32, int_name, int_version>			  test_int_;
	PropertyProxy<Glib::ustring, string_name, string_version> test_string_;
	PropertyProxy<e, enum_name, enum_version>				  test_enum_;

	explicit ItemTest(const gint64 id);
	explicit ItemTest(const gint64 id, guint32 testInt, Glib::ustring test_string, e test_enum);
	explicit ItemTest(const ItemTest& dbi);
	virtual ItemTest&	  operator=(const ItemTest& dbi);
	virtual bool		  operator==(const ItemTest& dbi) const;
	virtual string dataBaseTableName() const;
	virtual string toString() const;
	virtual ItemPtr		  clone() const;
	void				  setTestInt(const guint32& testInt);
	guint32				  testInt() const;
	Glib::ustring		  testString() const;
	void				  setTestString(const Glib::ustring& test_string);
	e					  testEnum() const;
	void				  setTestEnum(const e& testEnum);
};

void ItemTest::setTestInt(const guint32& test)
{
	test_int_ = test;
}

guint32 ItemTest::testInt() const
{
	return test_int_;
}

Glib::ustring ItemTest::testString() const
{
	return test_string_;
}

void ItemTest::setTestString(const Glib::ustring& test_string)
{
	test_string_ = test_string;
}

ItemTest::e ItemTest::testEnum() const
{
	return test_enum_;
}

void ItemTest::setTestEnum(const e& test_enum)
{
	test_enum_ = test_enum;
}

ItemTest::ItemTest(const gint64 id) : Item(id), test_int_(*this, 5u), test_string_(*this, "bépo"), test_enum_(*this, e::e1)
{
}

ItemTest::ItemTest(const gint64 id, guint32 test, Glib::ustring test_string, e test_enum)
		: Item(id), test_int_(*this, test), test_string_(*this, test_string), test_enum_(*this, test_enum)
{
}

ItemTest::ItemTest(const ItemTest& dbi)
		: Item(dbi), test_int_(dbi.test_int_, *this), test_string_(dbi.test_string_, *this), test_enum_(dbi.test_enum_, *this)
{
}

ItemTest& ItemTest::operator=(const ItemTest& dbi)
{
	Item::operator=(dbi);
	return *this;
}

bool ItemTest::operator==(const ItemTest& dbi) const
{
	if (Item::operator==(dbi))
	{
		if (test_int_.equal(dbi.test_int_) && test_string_.equal(dbi.test_string_) && test_enum_.equal(dbi.test_enum_))
		{
			return true;
		}
	}

	return false;
}

string ItemTest::toString() const
{
	return "Item";
}

string ItemTest::dataBaseTableName() const
{
	return "Test";
}


ItemPtr ItemTest::clone() const
{
	return std::shared_ptr<Item>(new ItemTest(*this));
}


// Tests
TEST_CASE("Attribute", "[DataBase]")
{
	ItemTest  dbi1(1, 0, "toto", ItemTest::e1);
	ItemTest  dbi2(2, 0, "toto", ItemTest::e2);
	Attribute p1(dbi1, "int", Version(1, 0, 0), true);
	Attribute p2(dbi1, "ustring", Version(2, 2, 2), false);
	Attribute p11(dbi2, "int", Version(1, 0, 0), false);
	Attribute p12(dbi2, "ustring", Version(2, 2, 2), true);

	SECTION("Functions")
	{
		// Version
		CHECK(p1.databaseVersion() == Version("1.0.0"));
		CHECK(p2.databaseVersion() == Version("2.2.2"));

		// Dbi type
		CHECK(p1.sameDbiId(p2));
		CHECK_FALSE(p1.sameDbiId(p11));

		// Same DBI
		CHECK(p1.sameDbiType(p2));
		CHECK(p1.sameDbiType(p11));

		// Nullable
		CHECK(p1.nullable() == true);
		CHECK(p2.nullable() == false);
	}
}

TEST_CASE("Property", "[DataBase]")
{
	ItemTest dbi1(1, 0, "toto", ItemTest::e1);
	ItemTest dbi2(2, 0, "toto", ItemTest::e2);
	Property p1(dbi1, "int", Version(1, 0, 0), 1);
	Property p2(dbi1, "ustring", Version(2, 2, 2), Glib::ustring("blop"));
	Property p3(dbi2, "enum", Version(1, 0, 0), ItemTest::e1);
	Property p11(dbi2, "int", Version(1, 0, 0), 1);
	Property p12(dbi2, "ustring", Version(2, 2, 2), Glib::ustring("blop"));
	Property p13(dbi2, "enum", Version(2, 2, 2), ItemTest::e2);

	SECTION("Functions")
	{
		// Type
		CHECK(p1.type() == typeid(int));
		CHECK(p2.type() == typeid(Glib::ustring));

		// Equal
		CHECK(p1.equal<int>(p11));
		CHECK(p2.equal<Glib::ustring>(p12));

		// Get
		CHECK(p1 == 1);
		CHECK(p2.getValue<Glib::ustring>() == "blop");
		CHECK_THROWS(p1.getValue<Glib::ustring>());

		// Set
		CHECK_NOTHROW(p1.setValue(5));
		CHECK(p1.getValue<int>() == 5);
		CHECK_THROWS(p1.setValue<Glib::ustring>("t"));

		CHECK_NOTHROW(p2.setValue<Glib::ustring>("toto"));
		CHECK(p2.getValue<Glib::ustring>() == "toto");
		CHECK_THROWS(p2.setValue<int>(-5));

		// Version
		CHECK(p1.databaseVersion() == Version("1.0.0"));
		CHECK(p2.databaseVersion() == Version("2.2.2"));

		// Dbi type
		CHECK(p1.sameDbiId(p2));
		CHECK_FALSE(p1.sameDbiId(p11));

		// Same DBI
		CHECK(p1.sameDbiType(p2));
		CHECK(p1.sameDbiType(p11));

		// Type
		CHECK(p1.type() == typeid(int));
		CHECK(p2.type() == typeid(Glib::ustring));
		CHECK(p3.type() == typeid(long));
	}

	SECTION("Attributes")
	{
		int nb_callback_called = 0;

		auto callback = [&]([[maybe_unused]] Item& dbi, [[maybe_unused]] Attribute& dbp) { nb_callback_called++; };

		SECTION("Test int")
		{
			p1.signalChanged().connect(callback);
			p1 = 5;
			p1.setValue<int>(5);
			CHECK(p1.getValue<int>() == 5);
		}
		SECTION("Test string")
		{
			p2.signalChanged().connect(callback);
			p2.setValue<Glib::ustring>("toto");
			p2 = Glib::ustring("toto");
			CHECK(p2.getValue<Glib::ustring>() == "toto");
		}
		SECTION("Test enum")
		{
			p3.signalChanged().connect(callback);
			p3.setValue<ItemTest::e>(ItemTest::e2);
			p3 = ItemTest::e2;
			p3 = 1l;
			CHECK(p3.getValue<ItemTest::e>() == ItemTest::e2);
			CHECK(p3 == 1l);
		}

		CHECK(nb_callback_called == 1);
	}
}

TEST_CASE("PropertyProxy", "[DataBase]")
{
	ItemTest										dbi1(1, 0, "toto", ItemTest::e1);
	ItemTest										dbi2(2, 0, "toto", ItemTest::e2);
	static constexpr const char						name_1[]	= "int";
	static constexpr const char						name_2[]	= "ustring";
	static constexpr const char						name_3[]	= "enum";
	static constexpr const char						version_1[] = "1.0.0";
	static constexpr const char						version_2[] = "2.2.2";
	PropertyProxy<int, name_1, version_1>			p1(dbi1, 1);
	PropertyProxy<Glib::ustring, name_2, version_2> p2(dbi1, "blop");
	PropertyProxy<ItemTest::e, name_3, version_1>	p3(dbi2, ItemTest::e1);
	PropertyProxy<int, name_1, version_1>			p11(dbi2, 1);
	PropertyProxy<Glib::ustring, name_2, version_2> p12(dbi2, "blop");
	PropertyProxy<ItemTest::e, name_3, version_2>	p13(dbi2, ItemTest::e2);

	SECTION("Functions")
	{
		// Equal
		CHECK(p1.equal(p11));
		CHECK(p2.equal(p12));

		// Get
		CHECK(p1 == 1);
		CHECK(p2.getValue() == "blop");

		// Set
		CHECK_NOTHROW(p1.setValue(5));
		CHECK(p1.getValue() == 5);

		CHECK_NOTHROW(p2.setValue("toto"));
		CHECK(p2.getValue() == "toto");
	}

	SECTION("Attributes")
	{
		SECTION("Test int")
		{
			p1 = 5;
			p1.setValue(5);
			CHECK(p1.getValue() == 5);
		}
		SECTION("Test string")
		{
			p2.setValue("toto");
			p2 = Glib::ustring("toto");
			CHECK(p2.getValue() == "toto");
		}
		SECTION("Test enum")
		{
			p3.setValue(ItemTest::e2);
			p3 = ItemTest::e2;
			// p3 = 1l;
			CHECK(p3.getValue() == ItemTest::e2);
			CHECK(p3 == 1l);
		}
	}
}

TEST_CASE("Link", "[DataBase]")
{
	ItemTest dbi1(1, 0, "toto", ItemTest::e1);
	ItemTest dbi2(1);
	ItemTest dbi3(2);
	Link	 l1(dbi1, "int", Version(1, 0, 0), 1, "Test");
	Link	 l2(dbi1, "int", Version(1, 0, 0), &dbi2, "Test");

	SECTION("Functions")
	{
		// Equal
		CHECK(l1.equal(l2));

		// Type
		CHECK(l1.type() == typeid(Item*));
	}

	SECTION("Attributes")
	{
		int nb_callback_called = 0;

		auto callback = [&]([[maybe_unused]] Item& dbi, [[maybe_unused]] Attribute& dbp) { nb_callback_called++; };

		SECTION("Id")
		{
			l1.signalChanged().connect(callback);
			l1.setId(5);
			l1 = static_cast<gint64>(5);
			CHECK(l1.id() == 5);
			CHECK(l1 == static_cast<Item*>(nullptr));
			CHECK(nb_callback_called == 1);
		}
		SECTION("Item")
		{
			l1.signalChanged().connect(callback);
			l1.setItem(&dbi3);
			l1 = &dbi3;
			l1.setValue<Item*>(&dbi3);
			CHECK(l1.id() == 2);
			CHECK(l1.item() == &dbi3);
			CHECK(l1.getValue<Item*>() == &dbi3);
			CHECK(l1 == static_cast<Item*>(&dbi3));
			CHECK(nb_callback_called == 1);
		}
		SECTION("Database name")
		{
			CHECK(l1.referenceTable() == "Test");
		}
	}
}

TEST_CASE("LinkProxy", "[DataBase]")
{
	ItemTest						  dbi1(1, 0, "toto", ItemTest::e1);
	static constexpr const char		  name[]	= "int";
	static constexpr const char		  version[] = "1.0.0";
	static constexpr const char		  db_name[] = "Test";
	LinkProxy<name, version, db_name> l1(dbi1, 1);
	LinkProxy<name, version, db_name> l2(dbi1, 1);

	SECTION("Functions")
	{
		// Equal
		CHECK(l1.equal(l2));
	}
}

TEST_CASE("PropertyGroup", "[DataBase]")
{
	ItemTest	   dbi(1, 0, "toto", ItemTest::e1);
	Property	   p10(dbi, "int", Version(1, 0, 0), 1);
	Property	   p11(dbi, "int", Version(1, 0, 0), 5);
	Property	   p20(dbi, "ustring", Version(1, 0, 0), Glib::ustring("blop"));
	Property	   p21(dbi, "ustring", Version(1, 0, 0), Glib::ustring("toto"));
	Property	   p30(dbi, "double", Version(1, 0, 0), 3.14);
	AttributeGroup g1(AttributeGroup::PROPERTY);
	AttributeGroup g2(AttributeGroup::PROPERTY);
	AttributeGroup g3(AttributeGroup::PROPERTY);

	g1.add(p10);
	g1.add(p20);
	g2.add(p11);
	g2.add(p21);
	g3.add(p11);
	g3.add(p21);
	g3.add(p30);


	SECTION("Functions")
	{
		// At
		CHECK(static_cast<Property&>(g1["int"]).equal<int>(p10));
		CHECK(&g1["int"] == &p10);
		CHECK(static_cast<Property&>(g2["int"]).equal<int>(p11));
		CHECK(&g2["int"] == &p11);
		CHECK(static_cast<Property&>(g1.at("ustring")).equal<Glib::ustring>(p20));
		CHECK(&g1.at("ustring") == &p20);
		CHECK(static_cast<Property&>(g2.at("ustring")).equal<Glib::ustring>(p21));
		CHECK(&g2.at("ustring") == &p21);

		CHECK_THROWS(g1["toto"]);

		// Iterator
		int i = 0;
		for (Attribute& a : g1)
		{
			Property& p = static_cast<Property&>(a);
			switch (i)
			{
			case 0:
				CHECK(p.equal<int>(p10));
				break;
			case 1:
				CHECK(p.equal<Glib::ustring>(p20));
				break;
			}
			i++;
		}
		CHECK(i == 2);

		// Size
		CHECK(g1.size() == 2);
		CHECK(g2.size() == 2);

		// Operator=
		CHECK_NOTHROW(g1 = g2);
		CHECK_THROWS(g1 = g3);
	}

	SECTION("Changed")
	{
		int nb_callback_called = 0;

		g1.signalChanged().connect([&]([[maybe_unused]] Item& dbi) { nb_callback_called++; });

		SECTION("Test int")
		{
			p10 = 10;
			CHECK(static_cast<Property&>(g1["int"]) == 10);
			CHECK(nb_callback_called == 1);
		}
		SECTION("Test string")
		{
			p20.setValue<Glib::ustring>("bépo");
			CHECK(static_cast<Property&>(g1["ustring"]).getValue<Glib::ustring>() == "bépo");
			CHECK(nb_callback_called == 1);
		}
		SECTION("Operator=")
		{
			CHECK(static_cast<Property&>(g1["int"]) == 1);
			CHECK(static_cast<Property&>(g1["ustring"]).getValue<Glib::ustring>() == "blop");
			g1 = g2;
			CHECK(static_cast<Property&>(g1["int"]) == 5);
			CHECK(static_cast<Property&>(g1["ustring"]).getValue<Glib::ustring>() == "toto");
			CHECK(nb_callback_called == 2);
		}
	}
}


TEST_CASE("Item", "[DataBase]")
{
	ItemTest dbi1(1, 1, "toto", ItemTest::e1);
	ItemTest dbi2(2, 2, "blop", ItemTest::e2);
	ItemTest dbi3(3);

	SECTION("Attributes")
	{
		ItemPtr dbi1_clone = dbi1.clone();

		CHECK(dbi1.linkedToDatabase());
		CHECK_FALSE(dbi1.sameDatabaseId(dbi2));
		CHECK_FALSE(dbi1_clone->linkedToDatabase());
		CHECK(dbi1_clone->sameDatabaseId(dbi1));
		CHECK(dbi1.testInt() == static_cast<ItemTest&>(*dbi1_clone).testInt());
		CHECK(dbi1.testString() == static_cast<ItemTest&>(*dbi1_clone).testString());
		CHECK(dbi1.testEnum() == static_cast<ItemTest&>(*dbi1_clone).testEnum());

		auto& dbi1_properties = std::as_const(dbi1).properties();
		CHECK(dbi1_properties.size() == 3);
		CHECK(static_cast<const Property&>(dbi1_properties["int"]).getValue<guint32>() == dbi1.testInt());
		CHECK(static_cast<const Property&>(dbi1_properties["string"]).getValue<Glib::ustring>() == dbi1.testString());
		CHECK(static_cast<const Property&>(dbi1_properties["enum"]).getValue<ItemTest::e>() == dbi1.testEnum());
		CHECK_THROWS(dbi1_properties["uint"]);

		auto& dbi1_clone_properties = std::as_const(*dbi1_clone).properties();
		CHECK(dbi1_clone_properties.size() == 3);
		CHECK(static_cast<const Property&>(dbi1_clone_properties["int"]).getValue<guint32>() == dbi1.testInt());
		CHECK(static_cast<const Property&>(dbi1_clone_properties["string"]).getValue<Glib::ustring>() == dbi1.testString());
		CHECK(static_cast<const Property&>(dbi1_clone_properties["enum"]).getValue<ItemTest::e>() == dbi1.testEnum());
		CHECK_THROWS(dbi1_clone_properties["uint"]);

		auto& dbi3_properties = std::as_const(dbi3).properties();
		CHECK(dbi3_properties.size() == 3);
		CHECK(static_cast<const Property&>(dbi3_properties["int"]).getValue<guint32>() == dbi3.testInt());
		CHECK(static_cast<const Property&>(dbi3_properties["string"]).getValue<Glib::ustring>() == dbi3.testString());
		CHECK(static_cast<const Property&>(dbi3_properties["enum"]).getValue<ItemTest::e>() == dbi3.testEnum());
		CHECK_THROWS(dbi3_properties["uint"]);
	}

	SECTION("Properties")
	{
		int nb_callback_called = 0;

		dbi1.signalChanged().connect([&]([[maybe_unused]] Item& dbi) { nb_callback_called++; });

		SECTION("Test int")
		{
			CHECK(dbi1.testInt() == 1);
			dbi1.setTestInt(2);
			dbi1.setTestInt(2);
			CHECK(dbi1.testInt() == 2);

			CHECK(dbi3.testInt() == 5);
		}
		SECTION("Test string")
		{
			CHECK(dbi1.testString() == "toto");
			dbi1.setTestString("tutu");
			dbi1.setTestString("tutu");
			CHECK(dbi1.testString() == "tutu");

			CHECK(dbi3.testString() == "bépo");
		}
		SECTION("Test string")
		{
			CHECK(dbi1.testEnum() == ItemTest::e1);
			dbi1.setTestEnum(ItemTest::e2);
			dbi1.setTestEnum(ItemTest::e2);
			CHECK(dbi1.testEnum() == ItemTest::e2);

			CHECK(dbi3.testEnum() == ItemTest::e1);
		}
		SECTION("Operator=")
		{
			dbi1 = dbi2;
			CHECK(dbi1.testInt() == 2);
			CHECK(dbi1.testString() == "blop");
			CHECK(dbi1.testEnum() == ItemTest::e2);
			CHECK(dbi1 == dbi2);
		}

		CHECK(nb_callback_called == 1);
	}
}
