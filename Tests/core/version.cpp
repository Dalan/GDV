/*
 * version.cpp
 *
 * Copyright 2017-2018 Rémi BERTHO <remi.bertho@dalan.fr>
 *
 * This file is part of GDV.
 *
 * GDVi s free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * GDV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

#include <catch2/catch_all.hpp>
#include "version.hpp"
#include <future>

using namespace core;
using namespace Glib;
using namespace std;

// Tests
TEST_CASE("Version", "[core;version]")
{
	DateTime time = DateTime::create_now_local();
	Version  v1;
	Version  v2("1.0.0", time);
	Version  v3(1, 2, 3);
	Version  v4(1, 0, 0);

	CHECK(v3.major() == 1);
	CHECK(v3.minor() == 2);
	CHECK(v3.micro() == 3);
	CHECK(v2.time().compare(time) == 0);

	CHECK(v2 == v4);
	CHECK(v2 != v1);
	CHECK(v2 < v3);
	CHECK(v3 > v4);
	CHECK(v2 >= v4);
	CHECK(v4 <= v3);
}

// Tests
TEST_CASE("VersionSoftware", "[core;version]")
{
	Version			 v1;
	Version			 v2 = Version::GetSoftwareCurrent();
	Version			 v3;
	promise<Version> v4_promise;
	future<Version>  v4_future = v4_promise.get_future();

	CHECK(v1 != v2);

	try
	{
		v3 = Version::getSoftwareLast();

		Version::getSoftwareLastAsynchronously([&](const Version& v) { v4_promise.set_value(v); }, nullptr);
		auto status = v4_future.wait_for(chrono::seconds(5));

		REQUIRE(status == future_status::ready);
		CHECK(v3 == v4_future.get());
	}
	catch (...)
	{
	}
}
