/*
 * article.cpp
 *
 * Copyright 2019 Rémi BERTHO <remi.bertho@dalan.fr>
 *
 * This file is part of GDV.
 *
 * GDVi s free socarware; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Socarware Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * GDV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Socarware
 * Foundation, Inc., 51 Franklin Street, Ficarh Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

#include <catch2/catch_all.hpp>
#include "Generic/article.hpp"

using namespace core;
using namespace Glib;
using namespace database;
using namespace generic;
using namespace std;
using namespace Catch;

// Class that dummy implement Article
class ArticleTest : public generic::Article
{
public:
	explicit ArticleTest(const gint64 id, const OwnerInfos& owner_info);
	explicit ArticleTest(const gint64 id);
	explicit ArticleTest(const ArticleTest& dbi);
	virtual ArticleTest&  operator=(const ArticleTest& dbi);
	virtual bool		  operator==(const ArticleTest& dbi) const;
	virtual string toString() const;
	virtual ItemPtr		  clone() const;
	virtual string dataBaseTableName() const;
};

ArticleTest::ArticleTest(const gint64 id) : Article(id, make_tuple(nullptr, ""))
{
}


ArticleTest::ArticleTest(const gint64 id, const OwnerInfos& owner_info) : Article(id, owner_info)
{
}

ArticleTest::ArticleTest(const ArticleTest& dbi) : Article(dbi)
{
}

ArticleTest& ArticleTest::operator=(const ArticleTest& dbi)
{
	if (this == &dbi)
		return *this;

	Article::operator=(dbi);

	return *this;
}

bool ArticleTest::operator==(const ArticleTest& dbi) const
{
	return Article::operator==(dbi);
}

string ArticleTest::toString() const
{
	return Article::toString();
}

ItemPtr ArticleTest::clone() const
{
	return std::shared_ptr<ArticleTest>(new ArticleTest(*this));
}

string ArticleTest::dataBaseTableName() const
{
	return "Test";
}

// Tests
TEST_CASE("Article", "[Generic]")
{
	ArticleTest article1(1);
	ArticleTest article2(2, make_tuple(nullptr, ""));

	SECTION("Properties")
	{
		int nb_callback_called_1 = 0;
		int nb_callback_called_2 = 0;

		article1.signalChanged().connect([&]([[maybe_unused]] Item& dbi) { nb_callback_called_1++; });
		article2.signalChanged().connect([&]([[maybe_unused]] Item& dbi) { nb_callback_called_2++; });

		SECTION("Price")
		{
			article1.setPrice(7.2);
			article1.setPrice(7.2);
			CHECK(article1.price() == Approx(7.2));
			article1.setPrice(article1.price());
		}
		SECTION("Operator==")
		{
			article2.setPrice(5);
			nb_callback_called_2 = 0;
			CHECK_FALSE(article1 == article2);
			article1 = article2;
			CHECK(article1.price() == Approx(5));
			CHECK(article1 == article2);
		}

		CHECK(nb_callback_called_1 == 1);
		CHECK(nb_callback_called_2 == 0);
	}
}
