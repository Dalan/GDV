/*
 * ownedobject.cpp
 *
 * Copyright 2019 Rémi BERTHO <remi.bertho@dalan.fr>
 *
 * This file is part of GDV.
 *
 * GDVi s free socarware; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Socarware Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * GDV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Socarware
 * Foundation, Inc., 51 Franklin Street, Ficarh Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

#include <catch2/catch_all.hpp>
#include "Generic/owned_object.hpp"
#include "Generic/object_array.hpp"

using namespace core;
using namespace Glib;
using namespace database;
using namespace generic;
using namespace std;



// Class that dummy implement OwnedObject
class OwnedObjectTest : public generic::OwnedObject
{
public:
	explicit OwnedObjectTest(const gint64 id, const OwnerInfos& owner_info);
	explicit OwnedObjectTest(const OwnedObjectTest& dbi);
	virtual OwnedObjectTest& operator=(const OwnedObjectTest& dbi);
	virtual bool			 operator==(const OwnedObjectTest& dbi) const;
	virtual string	 toString() const;
	virtual ItemPtr			 clone() const;
	virtual string	 dataBaseTableName() const;
};

OwnedObjectTest::OwnedObjectTest(const gint64 id, const OwnerInfos& owner_info) : OwnedObject(id, owner_info)
{
}

OwnedObjectTest::OwnedObjectTest(const OwnedObjectTest& dbi) : OwnedObject(dbi)
{
}

OwnedObjectTest& OwnedObjectTest::operator=(const OwnedObjectTest& dbi)
{
	if (this == &dbi)
		return *this;

	OwnedObject::operator=(dbi);

	return *this;
}

bool OwnedObjectTest::operator==(const OwnedObjectTest& dbi) const
{
	return OwnedObject::operator==(dbi);
}

string OwnedObjectTest::toString() const
{
	return OwnedObject::toString();
}

ItemPtr OwnedObjectTest::clone() const
{
	return std::shared_ptr<OwnedObjectTest>(new OwnedObjectTest(*this));
}

string OwnedObjectTest::dataBaseTableName() const
{
	return "Test";
}

// Tests
TEST_CASE("OwnedObject", "[Generic]")
{
	OwnedObjectTest owner_1(5, std::make_tuple(nullptr, ""));
	OwnedObjectTest owner_2(2, std::make_tuple(nullptr, ""));

	auto owner_info_1 = std::make_tuple(&owner_1, "");
	auto owner_info_2 = std::make_tuple(&owner_2, "");

	OwnedObjectTest owned_object1(1, owner_info_1);
	OwnedObjectTest owned_object2(2, owner_info_2);

	SECTION("Properties")
	{
		SECTION("Owner ID")
		{
			CHECK(owned_object1.ownerId() == (gint64)5);
			CHECK(owned_object2.ownerId() == (gint64)2);
		}
	}
}




TEST_CASE("OjectArray", "[Generic]")
{
	ObjectArray cia1;
	auto		owner1 = std::make_tuple(nullptr, "");


	CHECK(cia1.size() == 0);
	CHECK(cia1.empty() == true);


	int nb_callback_added_called   = 0;
	int nb_callback_removed_called = 0;

	cia1.signalAdded().connect([&]([[maybe_unused]] generic::Object& ci) { nb_callback_added_called++; });
	cia1.signalRemoved().connect([&]([[maybe_unused]] ItemPtr dbi, [[maybe_unused]] guint index) { nb_callback_removed_called++; });


	// Add
	OwnedObjectTest* ci1 = new OwnedObjectTest(1, owner1);
	cia1.add(ci1);
	CHECK(nb_callback_added_called == 1);
	CHECK(cia1.size() == 1);
	CHECK(cia1.empty() == false);
	CHECK(cia1.at(0) == *ci1);
	CHECK(cia1[0] == *ci1);

	OwnedObjectTest* ci3 = new OwnedObjectTest(2, owner1);
	cia1.add(ci3);
	CHECK(nb_callback_added_called == 2);
	CHECK(cia1.size() == 2);
	CHECK(cia1.at(0) == *ci3);
	CHECK(cia1[0] == *ci3);

	OwnedObjectTest* ci2 = new OwnedObjectTest(3, owner1);
	cia1.add(ci2);
	CHECK(nb_callback_added_called == 3);
	CHECK(cia1.size() == 3);
	CHECK(cia1.at(0) == *ci3);
	CHECK(cia1[0] == *ci3);
	CHECK(cia1.at(1) == *ci2);
	CHECK(cia1[1] == *ci2);
	CHECK(cia1.at(2) == *ci1);
	CHECK(cia1[2] == *ci1);

	CHECK_THROWS(cia1.at(4));
	CHECK_THROWS(cia1[4]);


	// Iterators
	guint nb = 0;
	for (generic::Object* ci : cia1)
	{
		switch (nb)
		{
		case 0:
			CHECK(*ci == *ci3);
			break;
		case 1:
			CHECK(*ci == *ci2);
			break;
		case 2:
			CHECK(*ci == *ci1);
			break;
		default:
			break;
		}
		nb++;
	}
	CHECK(nb == 3);


	// Remove
	SECTION("Remove")
	{
		SECTION("Remove one by one")
		{
			CHECK_FALSE(nb_callback_removed_called);
			cia1.remove(*ci2);
			CHECK(nb_callback_removed_called == 1);
			CHECK(cia1.size() == 2);
			CHECK(cia1[0] == *ci3);
			CHECK(cia1[1] == *ci1);

			cia1.remove(*ci3);
			CHECK(nb_callback_removed_called == 2);
			CHECK(cia1.size() == 1);
			CHECK(cia1[0] == *ci1);

			cia1.remove(*ci1);
			CHECK(nb_callback_removed_called == 3);
		}
		SECTION("Clear")
		{
			cia1.clear();
			delete ci1;
			delete ci2;
			delete ci3;
			CHECK(nb_callback_removed_called == 0);
		}
		SECTION("Delete all")
		{
			cia1.deleteAll();
			CHECK(nb_callback_removed_called == 0);
		}

		CHECK(cia1.size() == 0);
		CHECK(cia1.empty() == true);

		OwnedObjectTest* ci4 = new OwnedObjectTest(4, owner1);
		CHECK_THROWS(cia1.remove(*ci4));
		delete ci4;
	}
	cia1.deleteAll();
}
