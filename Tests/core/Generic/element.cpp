/*
 * element.cpp
 *
 * Copyright 2017-2018 Rémi BERTHO <remi.bertho@dalan.fr>
 *
 * This file is part of GDV.
 *
 * GDVi s free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * GDV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

#include <catch2/catch_all.hpp>
#include "Generic/element_array.hpp"

using namespace core;
using namespace Glib;
using namespace std;
using namespace database;
using namespace generic;
using namespace Catch;

// Class that dummy implement DatBaseItem
class ElementTest : public Element
{
public:
	explicit ElementTest(const gint64 id, const OwnerInfos& owner);
	explicit ElementTest(const ElementTest& dbi);
	virtual ElementTest&  operator=(const ElementTest& dbi);
	virtual bool		  operator==(const ElementTest& dbi) const;
	virtual string toString() const;
	Element*			  createNext(const bool copy_objects = false) const;
	virtual ItemPtr		  clone() const;
	virtual string nameId() const;
	virtual string dataBaseTableName() const;
	virtual double		  price() const;
	virtual double		  distance() const;
};


ElementTest::ElementTest(const gint64 id, const OwnerInfos& owner) : Element(id, owner)
{
}

ElementTest::ElementTest(const ElementTest& dbi) : Element(dbi)
{
}

ElementTest& ElementTest::operator=(const ElementTest& dbi)
{
	Element::operator=(dbi);

	return *this;
}

bool ElementTest::operator==(const ElementTest& dbi) const
{
	return Element::operator==(dbi);
}

string ElementTest::toString() const
{
	return "ElementTest";
}

Element* ElementTest::createNext([[maybe_unused]] const bool copy_objects) const
{
	return nullptr;
}

ItemPtr ElementTest::clone() const
{
	return std::shared_ptr<Element>(new ElementTest(*this));
}

string ElementTest::nameId() const
{
	return beginDate().format_string("%x");
}

string ElementTest::dataBaseTableName() const
{
	return "Test";
}

double ElementTest::price() const
{
	return 1;
}

double ElementTest::distance() const
{
	return 1;
}


// Tests
TEST_CASE("Element", "[Generic]")
{
	auto car = std::make_tuple(nullptr, "");
	Date date_now;
	date_now.set_time_current();
	Date date_before = date_now;
	Date date_after	 = date_now;
	date_before.subtract_days(10);
	date_after.add_days(10);

	// ElementTest ci1(0, "", car, 0, date_before, date_now);
	ElementTest ci1(0, car);
	ci1.setBeginDate(date_before);
	ci1.setEndDate(date_now);
	ElementTest ci2(0, car);
	ci2.setBeginDate(date_now);
	ci2.setEndDate(date_after);

	SECTION("Properties")
	{
		int nb_callback_called_1 = 0;
		int nb_callback_called_2 = 0;

		ci1.signalChanged().connect([&]([[maybe_unused]] Item& dbi) { nb_callback_called_1++; });
		ci2.signalChanged().connect([&]([[maybe_unused]] Item& dbi) { nb_callback_called_2++; });

		SECTION("Begin date")
		{
			CHECK(ci1.beginDate() == date_before);
			ci1.setBeginDate(date_after, Element::NONE);
			CHECK(ci1.beginDate() == date_after);
			ci2.setBeginDate(as_const(ci2).beginDate());
			CHECK(nb_callback_called_1 == 1);
			CHECK_FALSE(nb_callback_called_2 == 1);
		}
		SECTION("End date")
		{
			CHECK(ci1.endDate() == date_now);
			ci1.setEndDate(date_before);
			CHECK(ci1.endDate() == date_before);
			ci2.setEndDate(as_const(ci2).endDate());
			CHECK(nb_callback_called_1 == 1);
			CHECK_FALSE(nb_callback_called_2 == 1);
		}
		SECTION("Operator=")
		{
			ci1 = ci2;
			CHECK(ci1.beginDate() == date_now);
			CHECK(ci1.endDate() == date_after);
			CHECK(ci1 == ci2);
			CHECK(nb_callback_called_1 == 1);
			CHECK_FALSE(nb_callback_called_2 == 1);
		}
	}

	SECTION("Function")
	{
		CHECK(ci1.currentlyUsed());
		CHECK(ci1.dayBefore() == 0);
		CHECK(ci2.dayBefore() == 10);
		CHECK(ci1.age() == 10);
		CHECK(ci2.age() == 0);
		CHECK(ci1.duration() == 10);
		CHECK(ci2.duration() == 10);

		Date date_mean = date_now;
		date_mean.subtract_days(5);
		CHECK(ci1.meanDate() == date_mean);
		CHECK(ci1.compareDate(ci2) < 0);

		CHECK_FALSE(ci1 == ci2);
		CHECK(ci1 == ci1);
	}
}

TEST_CASE("ElementArray", "[Generic]")
{
	ElementArray cia1;
	auto		 car = std::make_tuple(nullptr, "");


	CHECK(cia1.size() == 0);
	CHECK(cia1.empty() == true);
	CHECK(cia1.totalPrice() == Approx(0.));
	CHECK(cia1.meanPrice() == Approx(0.));
	CHECK(cia1.meanDistance() == Approx(0.));
	CHECK(cia1.meanDuration() == Approx(0.));


	int nb_callback_added_called   = 0;
	int nb_callback_removed_called = 0;

	cia1.signalAdded().connect([&]([[maybe_unused]] Element& ci) { nb_callback_added_called++; });
	cia1.signalRemoved().connect([&]([[maybe_unused]] ItemPtr dbi, [[maybe_unused]] guint index) { nb_callback_removed_called++; });


	// Add
	ElementTest* ci1 = new ElementTest(1, car);
	ci1->setBeginDate(Date(10));
	ci1->setEndDate(Date(20));
	cia1.add(ci1);
	CHECK(nb_callback_added_called == 1);
	CHECK(cia1.size() == 1);
	CHECK(cia1.empty() == false);
	CHECK(cia1.totalPrice() == Approx(1.));
	CHECK(cia1.meanPrice() == Approx(1.));
	CHECK(cia1.meanDistance() == Approx(1.));
	CHECK(cia1.meanDuration() == Approx(10.));
	CHECK(cia1.at(0) == *ci1);
	CHECK(cia1[0] == *ci1);

	ElementTest* ci3 = new ElementTest(2, car);
	ci3->setBeginDate(Date(50));
	ci3->setEndDate(Date(60));
	cia1.add(ci3);
	CHECK(nb_callback_added_called == 2);
	CHECK(cia1.size() == 2);
	CHECK(cia1.totalPrice() == Approx(2.));
	CHECK(cia1.meanPrice() == Approx(1.));
	CHECK(cia1.meanDistance() == Approx(1.));
	CHECK(cia1.meanDuration() == Approx(10.));
	CHECK(cia1.at(0) == *ci3);
	CHECK(cia1[0] == *ci3);

	ElementTest* ci2 = new ElementTest(3, car);
	ci2->setBeginDate(Date(20));
	ci2->setEndDate(Date(30));
	cia1.add(ci2);
	CHECK(nb_callback_added_called == 3);
	CHECK(cia1.size() == 3);
	CHECK(cia1.totalPrice() == Approx(3.));
	CHECK(cia1.meanDuration() == Approx(10.));
	CHECK(cia1.at(0) == *ci3);
	CHECK(cia1[0] == *ci3);
	CHECK(cia1.at(1) == *ci2);
	CHECK(cia1[1] == *ci2);
	CHECK(cia1.at(2) == *ci1);
	CHECK(cia1[2] == *ci1);

	CHECK_THROWS(cia1.at(4));
	CHECK_THROWS(cia1[4]);


	// Iterators
	guint nb = 0;
	for (Element& ci : cia1)
	{
		switch (nb)
		{
		case 0:
			CHECK(ci == *ci3);
			break;
		case 1:
			CHECK(ci == *ci2);
			break;
		case 2:
			CHECK(ci == *ci1);
			break;
		default:
			break;
		}
		nb++;
	}
	CHECK(nb == 3);


	// Check update sort
	ci2->setBeginDate(Date(100));
	CHECK(cia1[0] == *ci2);


	// Element setBeginDate
	SECTION("Set begin date NONE")
	{
		ci2->setBeginDate(Date(50), Element::NONE);
		CHECK(as_const(*ci1).endDate() == Date(20));
	}
	SECTION("Set begin date END")
	{
		ci2->setBeginDate(Date(50), Element::END);
		CHECK(as_const(*ci1).endDate() == Date(20));
	}
	SECTION("Set begin date ALL")
	{
		ci2->setBeginDate(Date(50), Element::ALL);
		CHECK(as_const(*ci1).endDate() == Date(50));
	}


	// Remove
	SECTION("Remove")
	{
		SECTION("Remove one by one")
		{
			CHECK_FALSE(nb_callback_removed_called);
			cia1.remove(*ci2);
			CHECK(nb_callback_removed_called == 1);
			CHECK(cia1.size() == 2);
			CHECK(cia1.totalPrice() == Approx(2.));
			CHECK(cia1[0] == *ci3);
			CHECK(cia1[1] == *ci1);

			cia1.remove(*ci3);
			CHECK(nb_callback_removed_called == 2);
			CHECK(cia1.size() == 1);
			CHECK(cia1[0] == *ci1);

			cia1.remove(*ci1);
			CHECK(nb_callback_removed_called == 3);
		}
		SECTION("Clear")
		{
			cia1.clear();
			delete ci1;
			delete ci2;
			delete ci3;
			CHECK(nb_callback_removed_called == 0);
		}
		SECTION("Delete all")
		{
			cia1.deleteAll();
			CHECK(nb_callback_removed_called == 0);
		}

		CHECK(cia1.size() == 0);
		CHECK(cia1.empty() == true);

		ElementTest* ci4 = new ElementTest(4, car);
		ci4->setBeginDate(Date(20));
		ci4->setEndDate(Date(30));
		CHECK_THROWS(cia1.remove(*ci4));
		delete ci4;
	}
	cia1.deleteAll();
}
