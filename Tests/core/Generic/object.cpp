/*
 * object.cpp
 *
 * Copyright 2019 Rémi BERTHO <remi.bertho@dalan.fr>
 *
 * This file is part of GDV.
 *
 * GDVi s free socarware; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Socarware Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * GDV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Socarware
 * Foundation, Inc., 51 Franklin Street, Ficarh Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

#include <catch2/catch_all.hpp>
#include "Generic/object.hpp"

using namespace core;
using namespace Glib;
using namespace database;
using namespace generic;
using namespace std;


// Class that dummy implement Object
class ObjectTest : public generic::Object
{
public:
	explicit ObjectTest(const gint64 id, const ustring& name);
	explicit ObjectTest(const ObjectTest& dbi);
	virtual ObjectTest&	  operator=(const ObjectTest& dbi);
	virtual bool		  operator==(const ObjectTest& dbi) const;
	virtual string toString() const;
	virtual ItemPtr		  clone() const;
	virtual string dataBaseTableName() const;
};


ObjectTest::ObjectTest(const gint64 id, const ustring& name) : Object(id, name)
{
}

ObjectTest::ObjectTest(const ObjectTest& dbi) : Object(dbi)
{
}

ObjectTest& ObjectTest::operator=(const ObjectTest& dbi)
{
	if (this == &dbi)
		return *this;

	Object::operator=(dbi);

	return *this;
}

bool ObjectTest::operator==(const ObjectTest& dbi) const
{
	return Object::operator==(dbi);
}

string ObjectTest::toString() const
{
	return Object::toString();
}

ItemPtr ObjectTest::clone() const
{
	return std::shared_ptr<ObjectTest>(new ObjectTest(*this));
}

string ObjectTest::dataBaseTableName() const
{
	return "Test";
}

// Tests
TEST_CASE("Object", "[Generic]")
{
	ObjectTest object1(1, "Blop");
	ObjectTest object2(2, "Toto");

	SECTION("Properties")
	{
		int nb_callback_called_1 = 0;
		int nb_callback_called_2 = 0;

		object1.signalChanged().connect([&]([[maybe_unused]] Item& dbi) { nb_callback_called_1++; });
		object2.signalChanged().connect([&]([[maybe_unused]] Item& dbi) { nb_callback_called_2++; });

		SECTION("Name")
		{
			object1.setName("Titi");
			object1.setName("Titi");
			CHECK(object1.name() == "Titi");
			CHECK(object2.name() == "Toto");
			object2.setName(object2.name());
		}
		SECTION("Operator==")
		{
			CHECK_FALSE(object1 == object2);
			object1 = object2;
			CHECK(object1.name() == "Toto");
			CHECK(object1 == object2);
		}

		CHECK(nb_callback_called_1 == 1);
		CHECK(nb_callback_called_2 == 0);
	}
}
