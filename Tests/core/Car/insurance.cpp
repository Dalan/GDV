/*
 * insurance.cpp
 *
 * Copyright 2017-2018 Rémi BERTHO <remi.bertho@dalan.fr>
 *
 * This file is part of GDV.
 *
 * GDVi s free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * GDV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

#include <catch2/catch_all.hpp>
#include "Car/insurance.hpp"

using namespace core;
using namespace database;
using namespace Glib;
using namespace car;
using namespace std;
using namespace generic;
using namespace Catch;

// Tests

TEST_CASE("InsuranceGuarantee", "[Car]")
{
	Insurance::Guarantee i1(1);
	Insurance::Guarantee i2(2, nullptr);


	SECTION("Functions")
	{
		ItemPtr s1_clone = i1.clone();
		CHECK(s1_clone->sameDatabaseId(i1));
		CHECK(*static_cast<Insurance::Guarantee*>(s1_clone.get()) == i1);
	}
}


TEST_CASE("Insurance", "[Car]")
{
	Insurance::Guarantee* g1 = new Insurance::Guarantee(5, nullptr);
	Insurance::Guarantee* g2 = new Insurance::Guarantee(7, nullptr);
	Insurance			  i1(1);
	Insurance			  i2(2, nullptr);

	SECTION("Attributes")
	{
		int nb_callback_called_1 = 0;
		int nb_callback_called_2 = 0;
		int callback_index		 = 0;

		i1.signalChanged().connect([&]([[maybe_unused]] Item& dbi) { nb_callback_called_1++; });
		i2.signalChanged().connect([&]([[maybe_unused]] Item& dbi) { nb_callback_called_2++; });

		i1.signalObjectAdded<Insurance::Guarantee>().connect([&]([[maybe_unused]] OwnedObject& o) { callback_index = 1; });
		i1.signalObjectRemoved<Insurance::Guarantee>().connect(
				[&]([[maybe_unused]] ItemPtr dbi, [[maybe_unused]] guint index) { callback_index = 2; });
		i2.signalObjectAdded<Insurance::Guarantee>().connect([&]([[maybe_unused]] OwnedObject& o) { callback_index = 3; });
		i2.signalObjectRemoved<Insurance::Guarantee>().connect(
				[&]([[maybe_unused]] ItemPtr dbi, [[maybe_unused]] guint index) { callback_index = 4; });

		SECTION("Bonus")
		{
			i1.setBonus(60);
			CHECK(i1.bonus() == 60);
			i1.setBonus(60);
			i1.setBonus(i1.bonus());
			CHECK(nb_callback_called_1 == 1);
			i1.setPrice(9750);
			CHECK(i1.priceWithoutBonus() == Approx(16250));
			CHECK(i2.bonus() == 100);
		}
		SECTION("Emergency phone")
		{
			i1.setEmergencyPhone("02");
			CHECK(i1.emergencyPhone() == "02");
			i1.setEmergencyPhone("02");
			i1.setEmergencyPhone(i1.emergencyPhone());
			CHECK(nb_callback_called_1 == 1);
			CHECK(i2.emergencyPhone() == "");
		}
		SECTION("Agency")
		{
			i1.setAgencyId(25);
			CHECK(i1.agencyId() == 25);
			i1.setAgencyId(25);
			i1.setAgencyId(i1.agencyId());
			CHECK(nb_callback_called_1 == 1);
			CHECK(i2.agencyId() == -1);
		}
		SECTION("Guarantee")
		{
			i2.objects<Insurance::Guarantee>().add(g1);
			i2.objects<Insurance::Guarantee>().add(g2);
			Insurance::Guarantee& g3 = i1.addObject<Insurance::Guarantee>();
			CHECK(callback_index == 1);
			i1.removeObject<Insurance::Guarantee>(g3);
			CHECK(callback_index == 2);
			i2.removeObject<Insurance::Guarantee>(*g2);
			g2 = nullptr;
			CHECK(callback_index == 4);
			i2.objects<Insurance::Guarantee>().clear();
		}
		SECTION("Operator=")
		{
			i2.setPrice(9750);
			i2.setBonus(85);
			i2.setEmergencyPhone("05");
			i2.setAgencyId(42);
			i1 = i2;
			CHECK(i1 == i2);
			CHECK(i1.bonus() == i2.bonus());
			CHECK(i1.agencyId() == i2.agencyId());
			CHECK(i1.emergencyPhone() == i2.emergencyPhone());
			CHECK(nb_callback_called_1 == 1);
			CHECK(nb_callback_called_2 == 4);
		}
	}

	SECTION("Functions")
	{
		SECTION("Clone")
		{
			ItemPtr s1_clone = i1.clone();
			CHECK(s1_clone->sameDatabaseId(i1));
			CHECK(*static_cast<Insurance*>(s1_clone.get()) == i1);
		}

		SECTION("Name ID")
		{
			i1.setEndDate(Date(10));
			CHECK_FALSE(i1.nameId() == i2.nameId());
		}

		SECTION("Create next")
		{
			i1.objects<Insurance::Guarantee>().add(g1);
			i1.objects<Insurance::Guarantee>().add(g2);
			i1.setPrice(9750);
			i1.setBonus(85);
			i1.setEmergencyPhone("05");
			i1.setAgencyId(42);
			i1.setBeginKm(100);
			i1.setDelayKm(200, Element::LinkType::ALL);
			i1.setBeginDate(Date(1, Date::JANUARY, 2010));
			i1.setDelayYear(2, Element::LinkType::ALL);
			Insurance* i3 = dynamic_cast<Insurance*>(i1.createNext(true));
			CHECK(i3->price() == Approx(i1.price()));
			CHECK(i3->bonus() == static_cast<guint>(i1.bonus() * 0.95));
			CHECK(i3->emergencyPhone() == i1.emergencyPhone());
			CHECK(i3->agencyId() == i1.agencyId());
			CHECK(i3->objects<Insurance::Guarantee>().at(0).name() == g1->name());
			CHECK(i3->beginKm() == i1.endKm());
			CHECK(i3->endKm() == i3->beginKm() + i3->delayKm());
			CHECK(i3->beginDate() == i1.endDate());
			CHECK(i3->endDate() == i3->beginDate().add_years(static_cast<gint>(i3->delayYear())));
			CHECK(static_cast<Insurance::Guarantee&>(i3->objects<Insurance::Guarantee>().at(0)).price() == Approx(g1->price()));
			CHECK(i3->objects<Insurance::Guarantee>().size() == i1.objects<Insurance::Guarantee>().size());
			delete i3;
			i1.objects<Insurance::Guarantee>().clear();
		}
	}

	if (g1 != nullptr)
		delete g1;
	if (g2 != nullptr)
		delete g2;
}
