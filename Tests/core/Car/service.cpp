/*
 * service.cpp
 *
 * Copyright 2017-2018 Rémi BERTHO <remi.bertho@dalan.fr>
 *
 * This file is part of GDV.
 *
 * GDVi s free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * GDV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

#include <catch2/catch_all.hpp>
#include "Car/service.hpp"

using namespace std;
using namespace core;
using namespace database;
using namespace Glib;
using namespace car;
using namespace generic;
using namespace Catch;

// Tests
TEST_CASE("Operation", "[Car]")
{
	Service::Operation o1(1);
	Service::Operation o2(2, nullptr);


	SECTION("Functions")
	{
		ItemPtr o1_clone = o1.clone();
		CHECK(o1_clone->sameDatabaseId(o1));
		CHECK(*static_cast<Service::Operation*>(o1_clone.get()) == o1);
	}
}



TEST_CASE("Service", "[Car]")
{
	Service				s1(1);
	Service				s2(2, nullptr);
	Service::Operation* o1 = new Service::Operation(5, nullptr);
	Service::Operation* o2 = new Service::Operation(7, nullptr);

	SECTION("Properties")
	{
		int nb_callback_called_1 = 0;
		int callback_index		 = 0;

		s1.signalChanged().connect([&]([[maybe_unused]] Item& dbi) { nb_callback_called_1++; });

		s1.signalObjectAdded<Service::Operation>().connect([&]([[maybe_unused]] OwnedObject& o) { callback_index = 1; });
		s1.signalObjectRemoved<Service::Operation>().connect(
				[&]([[maybe_unused]] ItemPtr dbi, [[maybe_unused]] guint index) { callback_index = 2; });
		s2.signalObjectAdded<Service::Operation>().connect([&]([[maybe_unused]] OwnedObject& o) { callback_index = 3; });
		s2.signalObjectRemoved<Service::Operation>().connect(
				[&]([[maybe_unused]] ItemPtr dbi, [[maybe_unused]] guint index) { callback_index = 4; });

		SECTION("Garage")
		{
			s1.setGarageId(25);
			CHECK(s1.garageId() == 25);
			s1.setGarageId(25);
			s1.setGarageId(s1.garageId());
			CHECK(nb_callback_called_1 == 1);
			CHECK(s2.garageId() == -1);
		}
		SECTION("Operations")
		{
			s2.objects<Service::Operation>().add(o1);
			s2.objects<Service::Operation>().add(o2);
			Service::Operation& g3 = s1.addObject<Service::Operation>();
			CHECK(callback_index == 1);
			s1.removeObject<Service::Operation>(g3);
			CHECK(callback_index == 2);
			s2.removeObject<Service::Operation>(*o2);
			o2 = nullptr;
			CHECK(callback_index == 4);
			s2.objects<Service::Operation>().clear();
		}
		SECTION("Operator=")
		{
			s2.setGarageId(42);
			s1 = s2;
			CHECK(nb_callback_called_1 == 1);
			CHECK(s1 == s2);
			CHECK(s1.garageId() == s2.garageId());
		}
	}

	SECTION("Functions")
	{
		SECTION("Clone")
		{
			ItemPtr s1_clone = s1.clone();
			CHECK(s1_clone->sameDatabaseId(s1));
			CHECK(*static_cast<Service*>(s1_clone.get()) == s1);
		}

		SECTION("Name ID")
		{
			s2.setBeginDate(Date(50));
			CHECK_FALSE(s1.nameId() == s2.nameId());
		}

		SECTION("Create next")
		{
			s1.setPrice(9750);
			s1.setBeginKm(100);
			s1.setDelayKm(200, Element::LinkType::ALL);
			s1.setBeginDate(Date(1, Date::JANUARY, 2010));
			s1.setDelayYear(2, Element::LinkType::ALL);
			s1.setGarageId(42);
			Service* s3 = dynamic_cast<Service*>(s1.createNext(true));
			CHECK(s3->price() == Approx(s1.price()));
			CHECK(s3->beginKm() == s1.endKm());
			CHECK(s3->endKm() == s3->beginKm() + s3->delayKm());
			CHECK(s3->beginDate() == s1.endDate());
			CHECK(s3->garageId() == s1.garageId());
		}
	}
}
