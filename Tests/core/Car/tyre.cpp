/*
 * tyre.cpp
 *
 * Copyright 2017-2018 Rémi BERTHO <remi.bertho@dalan.fr>
 *
 * This file is part of GDV.
 *
 * GDVi s free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * GDV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

#include <catch2/catch_all.hpp>
#include "Car/tyre.hpp"

using namespace std;
using namespace core;
using namespace database;
using namespace Glib;
using namespace car;
using namespace generic;
using namespace Catch;

// Tests
TEST_CASE("Tyre", "[Car]")
{
	Tyre t1(1);
	Tyre t2(2, nullptr, Tyre::FRONT_RIGHT);

	SECTION("Properties")
	{
		int nb_callback_called_1 = 0;

		t1.signalChanged().connect([&]([[maybe_unused]] Item& dbi) { nb_callback_called_1++; });

		SECTION("Position")
		{
			CHECK(t1.position() == Tyre::BOTTOM_RIGHT);
			CHECK(t2.position() == Tyre::FRONT_RIGHT);
		}
		SECTION("Operator=")
		{
			t1 = t2;
			CHECK(t1.position() == Tyre::FRONT_RIGHT);
			CHECK(t1 == t2);
			CHECK(nb_callback_called_1 == 1);
		}
	}

	SECTION("Functions")
	{
		SECTION("Clone")
		{
			ItemPtr t1_clone = t1.clone();
			CHECK(t1_clone->sameDatabaseId(t1));
			CHECK(*static_cast<Tyre*>(t1_clone.get()) == t1);
		}

		SECTION("Name ID")
		{
			t2.setBeginDate(Date(50));
			CHECK_FALSE(t1.nameId() == t2.nameId());
		}

		SECTION("Create next")
		{
			t1.setPrice(9750);
			t1.setBeginKm(100);
			t1.setDelayKm(200, Element::LinkType::ALL);
			t1.setBeginDate(Date(1, Date::JANUARY, 2010));
			t1.setDelayYear(2, Element::LinkType::ALL);
			Tyre* t3 = dynamic_cast<Tyre*>(t1.createNext(true));
			CHECK(t3->price() == Approx(t1.price()));
			CHECK(t3->beginKm() == t1.endKm());
			CHECK(t3->endKm() == t3->beginKm() + t3->delayKm());
			CHECK(t3->beginDate() == t1.endDate());
		}
	}
}
