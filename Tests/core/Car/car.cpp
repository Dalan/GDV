/*
 * car.cpp
 *
 * Copyright 2017-2018 Rémi BERTHO <remi.bertho@dalan.fr>
 *
 * This file is part of GDV.
 *
 * GDVi s free socarware; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Socarware Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * GDV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Socarware
 * Foundation, Inc., 51 Franklin Street, Ficarh Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

#include <catch2/catch_all.hpp>
#include "Car/car.hpp"
#include "Car/fulltank.hpp"
#include "Car/inspection.hpp"
#include "Car/insurance.hpp"
#include "Car/service.hpp"
#include "Car/tyre.hpp"
#include "Car/wiper.hpp"

using namespace core;
using namespace Glib;
using namespace database;
using namespace car;
using namespace vehicle;
using namespace generic;
using namespace std;
using namespace Catch;

// Tests
TEST_CASE("Car", "[Car]")
{
	Car car1(1, "Blop");
	Car car2(2, "Toto");

	SECTION("Properties")
	{
		int nb_callback_called_1 = 0;
		int nb_callback_called_2 = 0;

		car1.signalChanged().connect([&]([[maybe_unused]] Item& dbi) { nb_callback_called_1++; });
		car2.signalChanged().connect([&]([[maybe_unused]] Item& dbi) { nb_callback_called_2++; });


		SECTION("Plate")
		{
			car1.setPlate("Titi");
			CHECK(car1.plate() == "Titi");
			car1.setPlate("Titi");
			CHECK(car2.plate() == "");
			car1.setPlate(car1.plate());
		}
		SECTION("Operator==")
		{
			car2.setPlate("aui");
			nb_callback_called_2 = 0;
			CHECK_FALSE(car1 == car2);
			car1 = car2;
			CHECK(car1.plate() == "aui");
			CHECK(car1 == car2);
		}

		CHECK(nb_callback_called_1 == 1);
		CHECK(nb_callback_called_2 == 0);
	}

	SECTION("Functions")
	{
		Date date_previous_year;
		date_previous_year.set_time_current();
		date_previous_year.subtract_years(1);

		car2.setPrice(3265);
		car2.setDistance(32150);
		car2.setDateOfOwnership(date_previous_year);

		// Signals
		int callback_index = 0;

		car2.signalAdded<FullTank>().connect([&]([[maybe_unused]] Element& ci) { callback_index = 12; });
		car2.signalRemoved<FullTank>().connect([&]([[maybe_unused]] ItemPtr dbi, [[maybe_unused]] guint index) { callback_index = 13; });

		car2.signalAdded<Wiper>(Wiper::BOTTOM).connect([&]([[maybe_unused]] Element& ci) { callback_index = 0; });
		car2.signalRemoved<Wiper>(Wiper::BOTTOM).connect([&]([[maybe_unused]] ItemPtr dbi, [[maybe_unused]] guint index) {
			callback_index = 1;
		});
		car2.signalAdded<Wiper>(Wiper::FRONT).connect([&]([[maybe_unused]] Element& ci) { callback_index = 2; });
		car2.signalRemoved<Wiper>(Wiper::FRONT).connect([&]([[maybe_unused]] ItemPtr dbi, [[maybe_unused]] guint index) {
			callback_index = 3;
		});
		car2.signalAdded<Tyre>(Tyre::BOTTOM_LEFT).connect([&]([[maybe_unused]] Element& ci) { callback_index = 4; });
		car2.signalRemoved<Tyre>(Tyre::BOTTOM_LEFT).connect([&]([[maybe_unused]] ItemPtr dbi, [[maybe_unused]] guint index) {
			callback_index = 5;
		});
		car2.signalAdded<Tyre>(Tyre::BOTTOM_RIGHT).connect([&]([[maybe_unused]] Element& ci) { callback_index = 6; });
		car2.signalRemoved<Tyre>(Tyre::BOTTOM_RIGHT).connect([&]([[maybe_unused]] ItemPtr dbi, [[maybe_unused]] guint index) {
			callback_index = 7;
		});
		car2.signalAdded<Tyre>(Tyre::FRONT_LEFT).connect([&]([[maybe_unused]] Element& ci) { callback_index = 8; });
		car2.signalRemoved<Tyre>(Tyre::FRONT_LEFT).connect([&]([[maybe_unused]] ItemPtr dbi, [[maybe_unused]] guint index) {
			callback_index = 9;
		});
		car2.signalAdded<Tyre>(Tyre::FRONT_RIGHT).connect([&]([[maybe_unused]] Element& ci) { callback_index = 10; });
		car2.signalRemoved<Tyre>(Tyre::FRONT_RIGHT).connect([&]([[maybe_unused]] ItemPtr dbi, [[maybe_unused]] guint index) {
			callback_index = 11;
		});
		car2.signalAdded<Inspection>().connect([&]([[maybe_unused]] Element& ci) { callback_index = 14; });
		car2.signalRemoved<Inspection>().connect([&]([[maybe_unused]] ItemPtr dbi, [[maybe_unused]] guint index) { callback_index = 15; });
		car2.signalAdded<Insurance>().connect([&]([[maybe_unused]] Element& ci) { callback_index = 16; });
		car2.signalRemoved<Insurance>().connect([&]([[maybe_unused]] ItemPtr dbi, [[maybe_unused]] guint index) { callback_index = 17; });
		car2.signalAdded<Service>().connect([&]([[maybe_unused]] Element& ci) { callback_index = 18; });
		car2.signalRemoved<Service>().connect([&]([[maybe_unused]] ItemPtr dbi, [[maybe_unused]] guint index) { callback_index = 19; });


		// Add
		FullTank& fulltank1 = car2.addElement<FullTank>();
		fulltank1.setPricePerLiter(1);
		fulltank1.setVolume(100);
		fulltank1.setDistance(1000);
		fulltank1.setBeginDate(date_previous_year, Element::ALL);
		CHECK(callback_index == 12);
		FullTank& fulltank2 = car2.addElement<FullTank>();
		fulltank2.setPricePerLiter(1.2);
		fulltank2.setVolume(50);
		fulltank2.setDistance(600);
		fulltank2.setBeginDate(date_previous_year.add_months(1), Element::ALL);
		FullTank& fulltank3 = car2.addElement<FullTank>();
		fulltank3.setPricePerLiter(1.5);
		fulltank3.setVolume(10);
		fulltank3.setDistance(120);
		fulltank3.setBeginDate(date_previous_year.add_months(1), Element::ALL);

		Wiper& wiper1 = car2.addElement<Wiper>(Wiper::BOTTOM);
		CHECK(callback_index == 0);
		wiper1.setPrice(100);
		Wiper& wiper2 = car2.addElement<Wiper>(Wiper::FRONT);
		wiper2.setPrice(100);
		CHECK(callback_index == 2);

		Tyre& tyre1 = car2.addElement<Tyre>(Tyre::BOTTOM_LEFT);
		tyre1.setPrice(100);
		CHECK(callback_index == 4);
		Tyre& tyre2 = car2.addElement<Tyre>(Tyre::BOTTOM_RIGHT);
		tyre2.setPrice(100);
		CHECK(callback_index == 6);
		Tyre& tyre3 = car2.addElement<Tyre>(Tyre::FRONT_LEFT);
		tyre3.setPrice(100);
		CHECK(callback_index == 8);
		Tyre& tyre4 = car2.addElement<Tyre>(Tyre::FRONT_RIGHT);
		tyre4.setPrice(100);
		CHECK(callback_index == 10);

		Inspection& inspection1 = car2.addElement<Inspection>();
		inspection1.setPrice(100);
		CHECK(callback_index == 14);

		Insurance& insurance1 = car2.addElement<Insurance>();
		insurance1.setPrice(100);
		CHECK(callback_index == 16);

		Service& service1 = car2.addElement<Service>();
		service1.setPrice(100);
		CHECK(callback_index == 18);


		// Functions tests
		ItemPtr car1_clone = car1.clone();
		CHECK(car1_clone->sameDatabaseId(car1));
		CHECK(*static_cast<Car*>(car1_clone.get()) == car1);

		CHECK(car2.nbElements<FullTank>() == 3);
		CHECK(car2.nbElements<Wiper>(Wiper::BOTTOM) == 1);
		CHECK(car2.nbElements<Wiper>(Wiper::FRONT) == 1);
		CHECK(car2.nbElements<Tyre>(Tyre::BOTTOM_LEFT) == 1);
		CHECK(car2.nbElements<Tyre>(Tyre::BOTTOM_RIGHT) == 1);
		CHECK(car2.nbElements<Tyre>(Tyre::FRONT_LEFT) == 1);
		CHECK(car2.nbElements<Tyre>(Tyre::FRONT_RIGHT) == 1);
		CHECK(car2.nbElements<Inspection>() == 1);
		CHECK(car2.nbElements<Insurance>() == 1);
		CHECK(car2.nbElements<Service>() == 1);

		CHECK(car2.totalPrice() == Approx(4340));
		CHECK(car2.pricePerMonth() == Approx(361.666666667).epsilon(0.05));
		CHECK(car2.pricePerYear() == Approx(4340).epsilon(0.01));
		CHECK(car2.pricePer100Km() == Approx(13.499222395));
		CHECK(car2.fullTankPricePerMonth() == Approx(80).epsilon(0.05));
		CHECK(car2.consumption() == Approx(9.302325581));
		CHECK(car2.fullTankPricePer100Km() == Approx(10.174418605));
		CHECK(car2.fullTanksTotalVolume() == Approx(160));
		CHECK(car2.fullTanksMeanVolume() == Approx(53.333333333));
		CHECK(car2.fullTanksMeanPricePerLiter() == Approx(1.233333333));
		CHECK(car2.fullTanksMeanConsumptionPerMonth() == Approx(75.).epsilon(0.05));
		CHECK(car2.fullTanksMeanConsumption() == Approx(9.3023));


		// Remove
		car2.removeElement<Wiper>(wiper1);
		CHECK(callback_index == 1);
		car2.removeElement<Wiper>(wiper2);
		CHECK(callback_index == 3);

		car2.removeElement<Tyre>(tyre1);
		CHECK(callback_index == 5);
		car2.removeElement<Tyre>(tyre2);
		CHECK(callback_index == 7);
		car2.removeElement<Tyre>(tyre3);
		CHECK(callback_index == 9);
		car2.removeElement<Tyre>(tyre4);
		CHECK(callback_index == 11);

		car2.removeElement<FullTank>(fulltank1);
		CHECK(callback_index == 13);

		car2.removeElement<Inspection>(inspection1);
		CHECK(callback_index == 15);

		car2.removeElement<Insurance>(insurance1);
		CHECK(callback_index == 17);

		car2.removeElement<Service>(service1);
		CHECK(callback_index == 19);
	}
}
