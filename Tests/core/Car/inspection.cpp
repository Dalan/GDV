/*
 * inspection.cpp
 *
 * Copyright 2017-2018 Rémi BERTHO <remi.bertho@dalan.fr>
 *
 * This file is part of GDV.
 *
 * GDVi s free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * GDV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

#include <catch2/catch_all.hpp>
#include "Car/inspection.hpp"

using namespace std;
using namespace core;
using namespace Glib;
using namespace database;
using namespace car;
using namespace generic;
using namespace Catch;

// Tests
TEST_CASE("Inspection", "[Car]")
{
	Inspection i1(1);
	Inspection i2(2, nullptr);

	SECTION("Properties")
	{
		int nb_callback_called_1 = 0;

		i1.signalChanged().connect([&]([[maybe_unused]] Item& dbi) { nb_callback_called_1++; });

		SECTION("Center")
		{
			i1.setCenterId(25);
			CHECK(i1.centerId() == 25);
			i1.setCenterId(25);
			i1.setCenterId(i1.centerId());
			CHECK(nb_callback_called_1 == 1);
			CHECK(i2.centerId() == -1);
		}
		SECTION("Repeat date")
		{
			i1.setRepeatDate(Date(25));
			CHECK(i1.repeatDate() == Date(25));
			i1.setRepeatDate(Date(25));
			i1.setRepeatDate(i1.repeatDate());
			CHECK(nb_callback_called_1 == 1);
			CHECK(i2.repeatDate() == Date(1));
		}
		SECTION("Operator=")
		{
			i2.setCenterId(42);
			i2.setRepeatDate(Date(25));
			i1 = i2;
			CHECK(i1 == i2);
			CHECK(nb_callback_called_1 == 1);
			CHECK(i1.centerId() == i2.centerId());
			CHECK(i1.repeatDate() == i2.repeatDate());
		}
	}

	SECTION("Functions")
	{
		SECTION("Clone")
		{
			ItemPtr i1_clone = i1.clone();
			CHECK(i1_clone->sameDatabaseId(i1));
			CHECK(*static_cast<Inspection*>(i1_clone.get()) == i1);
		}

		SECTION("Name ID")
		{
			i2.setBeginDate(Date(50));
			CHECK_FALSE(i1.nameId() == i2.nameId());
		}

		SECTION("Create next")
		{
			i1.setPrice(9750);
			i1.setBeginKm(100);
			i1.setDelayKm(200, Element::LinkType::ALL);
			i1.setBeginDate(Date(1, Date::JANUARY, 2010));
			i1.setDelayYear(2, Element::LinkType::ALL);
			Inspection* i3 = dynamic_cast<Inspection*>(i1.createNext(true));
			CHECK(i3->price() == Approx(i1.price()));
			CHECK(i3->beginKm() == i1.endKm());
			CHECK(i3->endKm() == i3->beginKm() + i3->delayKm());
			CHECK(i3->beginDate() == i1.endDate());
			CHECK(i3->endDate() == i3->beginDate().add_years(static_cast<gint>(i3->delayYear())));
			CHECK(i3->repeatDate() == Date(1));
		}
	}
}
