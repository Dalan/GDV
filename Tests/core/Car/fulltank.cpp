/*
 * fulltank.cpp
 *
 * Copyright 2017-2018 Rémi BERTHO <remi.bertho@dalan.fr>
 *
 * This file is part of GDV.
 *
 * GDVi s free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * GDV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

#include <catch2/catch_all.hpp>
#include "Car/fulltank.hpp"
#include "Generic/element_array.hpp"

#include <iostream>

using namespace std;
using namespace core;
using namespace Glib;
using namespace database;
using namespace car;
using namespace generic;
using namespace Catch;

// Tests
TEST_CASE("FullTank", "[Vehicle]")
{
	FullTank ft1(1);
	FullTank ft2(2, nullptr);

	Date date_tomorrow;
	date_tomorrow.set_time_current();
	date_tomorrow.add_days(1);

	Date date_tomorrow_month = date_tomorrow;
	date_tomorrow_month.add_months(1);

	SECTION("Attributes")
	{
		int nb_callback_called_1 = 0;
		int nb_callback_called_2 = 0;

		ft1.signalChanged().connect([&]([[maybe_unused]] Item& dbi) { nb_callback_called_1++; });
		ft2.signalChanged().connect([&]([[maybe_unused]] Item& dbi) { nb_callback_called_2++; });

		SECTION("Price per liter")
		{
			ft1.setPricePerLiter(1.236);
			CHECK(ft1.pricePerLiter() == Approx(1.236));
			ft1.setPricePerLiter(1.236);
			ft1.setPricePerLiter(ft1.pricePerLiter());
			CHECK(nb_callback_called_1 == 1);
			CHECK(ft2.pricePerLiter() == Approx(0));
		}
		SECTION("Distance")
		{
			ft1.setDistance(321.2);
			CHECK(ft1.distance() == Approx(321.2));
			ft1.setDistance(321.2);
			ft1.setDistance(ft1.distance());
			CHECK(nb_callback_called_1 == 1);
			CHECK(ft2.distance() == Approx(0));
		}
		SECTION("Volume")
		{
			ft1.setVolume(21.23);
			CHECK(ft1.volume() == Approx(21.23));
			ft1.setVolume(21.23);
			ft1.setVolume(ft1.volume());
			CHECK(nb_callback_called_1 == 1);
			CHECK(ft2.volume() == Approx(0));
		}
		SECTION("Additive")
		{
			ft1.setAdditive(true);
			CHECK(ft1.additive() == true);
			ft1.setAdditive(true);
			ft1.setAdditive(ft1.additive());
			CHECK(nb_callback_called_1 == 1);
			CHECK(ft2.additive() == false);
		}
		SECTION("Bio")
		{
			ft1.setBio(35);
			CHECK(ft1.bio() == 35);
			ft1.setBio(35);
			ft1.setBio(ft1.bio());
			CHECK(nb_callback_called_1 == 1);
			CHECK(ft2.bio() == 0);
		}
		SECTION("Station")
		{
			ft1.setStationId(35);
			CHECK(ft1.stationId() == 35);
			ft1.setStationId(35);
			ft1.setStationId(ft1.stationId());
			CHECK(nb_callback_called_1 == 1);
			CHECK(ft2.stationId() == -1);
		}
		SECTION("Operator=")
		{
			ft2.setPricePerLiter(1.236);
			ft2.setDistance(600);
			ft2.setVolume(30.32);
			ft2.setAdditive(true);
			ft2.setBio(15);
			nb_callback_called_2 = 0;
			ft1					 = ft2;
			CHECK(ft1.pricePerLiter() == Approx(1.236));
			CHECK(ft1.distance() == Approx(600));
			CHECK(ft1.volume() == Approx(30.32));
			CHECK(ft1.additive() == true);
			CHECK(ft1.bio() == 15);
			CHECK(ft1 == ft2);
			CHECK(nb_callback_called_1 == 1);
		}
		SECTION("Begin date None")
		{
			CHECK(ft1.beginDate() == Date(1, Date::JANUARY, 2020));
			ft1.setBeginDate(date_tomorrow, Element::NONE);
			ft1.setBeginDate(date_tomorrow, Element::NONE);
			CHECK(ft1.beginDate() == date_tomorrow);
			ft1.setBeginDate(ft1.beginDate(), Element::NONE);
			CHECK(nb_callback_called_1 == 1);
		}
		SECTION("Begin date End")
		{
			CHECK(ft1.beginDate() == Date(1, Date::JANUARY, 2020));
			ft1.setBeginDate(date_tomorrow, Element::END);
			ft1.setBeginDate(date_tomorrow, Element::END);
			CHECK(ft1.beginDate() == date_tomorrow);
			CHECK(ft1.endDate() == date_tomorrow_month);
			ft1.setBeginDate(ft1.beginDate(), Element::END);
			CHECK(nb_callback_called_1 == 2);
		}
		SECTION("Begin date All")
		{
			CHECK(ft1.beginDate() == Date(1, Date::JANUARY, 2020));
			ft1.setBeginDate(date_tomorrow, Element::ALL);
			ft1.setBeginDate(date_tomorrow, Element::ALL);
			CHECK(ft1.beginDate() == date_tomorrow);
			CHECK(ft1.endDate() == date_tomorrow_month);
			ft1.setBeginDate(ft1.beginDate(), Element::ALL);
			CHECK(nb_callback_called_1 == 2);
		}

		CHECK(nb_callback_called_2 == 0);
	}

	SECTION("Functions")
	{
		SECTION("Clone")
		{
			ItemPtr ft1_clone = ft1.clone();
			CHECK(ft1_clone->sameDatabaseId(ft1));
			CHECK(*static_cast<FullTank*>(ft1_clone.get()) == ft1);
		}

		SECTION("Name ID")
		{
			ft2.setBeginDate(Date(10));
			CHECK_FALSE(ft1.nameId() == ft2.nameId());
		}

		SECTION("Create next")
		{
			ft1.setPricePerLiter(1.236);
			ft1.setDistance(600);
			ft1.setVolume(30.32);
			ft1.setAdditive(true);
			ft1.setBio(15);
			FullTank* ft3 = static_cast<FullTank*>(ft1.createNext());
			CHECK(ft3->bio() == ft1.bio());
			CHECK(ft3->price() == Approx(0));
			CHECK(ft3->volume() == Approx(0));
			CHECK(ft3->additive() == ft1.additive());
			CHECK(ft3->distance() == Approx(0));
			CHECK(ft3->pricePerLiter() == Approx(ft1.pricePerLiter()));
			CHECK(ft3->beginDate() == ft1.endDate());
			CHECK(ft3->endDate() == ft3->beginDate().add_months(1));
			delete ft3;
		}

		SECTION("Other")
		{
			CHECK(ft2.pricePer100Km() == Approx(0));
			CHECK(ft2.consumption() == Approx(0));
		}
	}

	SECTION("Array")
	{
		FullTank* ft3 = new FullTank(2, nullptr);
		ft3->setDistance(789.3);
		ft3->setVolume(30.32);
		ft3->setPricePerLiter(1.236);
		ft3->setBeginDate(Date(10));
		ft3->setEndDate(Date(20));
		FullTank* ft4 = new FullTank(2, nullptr);
		ft4->setDistance(221.);
		ft4->setVolume(12.36);
		ft4->setPricePerLiter(1.321);
		ft4->setBeginDate(Date(20));
		ft4->setEndDate(Date(33));
		ElementArray array;

		array.add(ft3);
		array.add(ft4);

		CHECK(ft3->consumption() == Approx(5.592760181));
		CHECK(ft3->pricePer100Km() == Approx(6.912651584));

		CHECK(ft4->consumption() == Approx(0));
		CHECK(ft4->pricePer100Km() == Approx(0));

		array.deleteAll();
	}
}
