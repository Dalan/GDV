/*
 * wiper.cpp
 *
 * Copyright 2017-2018 Rémi BERTHO <remi.bertho@dalan.fr>
 *
 * This file is part of GDV.
 *
 * GDVi s free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * GDV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

#include <catch2/catch_all.hpp>
#include "Car/wiper.hpp"

using namespace std;
using namespace core;
using namespace database;
using namespace Glib;
using namespace car;
using namespace generic;
using namespace Catch;

// Tests
TEST_CASE("Wiper", "[Car]")
{
	Wiper w1(1);
	Wiper w2(2, nullptr, Wiper::FRONT);

	SECTION("Properties")
	{
		int nb_callback_called_1 = 0;

		w1.signalChanged().connect([&]([[maybe_unused]] Item& dbi) { nb_callback_called_1++; });

		SECTION("Position")
		{
			CHECK(w1.position() == Wiper::BOTTOM);
			CHECK(w2.position() == Wiper::FRONT);
		}
		SECTION("Operator=")
		{
			w1 = w2;
			CHECK(w1.position() == Wiper::FRONT);
			CHECK(w1 == w2);
			CHECK(nb_callback_called_1 == 1);
		}
	}

	SECTION("Functions")
	{
		SECTION("Clone")
		{
			ItemPtr w1_clone = w1.clone();
			CHECK(w1_clone->sameDatabaseId(w1));
			CHECK(*static_cast<Wiper*>(w1_clone.get()) == w1);
		}

		SECTION("Name ID")
		{
			w2.setBeginDate(Date(50));
			CHECK_FALSE(w1.nameId() == w2.nameId());
		}

		SECTION("Create next")
		{
			w1.setPrice(9750);
			w1.setBeginKm(100);
			w1.setDelayKm(200, Element::LinkType::ALL);
			w1.setBeginDate(Date(1, Date::JANUARY, 2010));
			w1.setDelayYear(2, Element::LinkType::ALL);
			Wiper* w3 = dynamic_cast<Wiper*>(w1.createNext(true));
			CHECK(w3->price() == Approx(w1.price()));
			CHECK(w3->beginKm() == w1.endKm());
			CHECK(w3->endKm() == w3->beginKm() + w3->delayKm());
			CHECK(w3->beginDate() == w1.endDate());
		}
	}
}
