/*
 * main.cpp
 *
 * Copyright 2017-2018 Rémi BERTHO <remi.bertho@dalan.fr>
 *
 * This file is part of GDV.
 *
 * GDVi s free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * GDV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

#define CATCH_CONFIG_RUNNER
#include <catch2/catch_all.hpp>
#include <giomm.h>

int main(int argc, char* argv[])
{
	// global setup...
	auto app = Gio::Application::create("fr.dalan.gdv.test", Gio::APPLICATION_FLAGS_NONE);

	int result = Catch::Session().run(argc, argv);

	// global clean-up...

	return result;
}
